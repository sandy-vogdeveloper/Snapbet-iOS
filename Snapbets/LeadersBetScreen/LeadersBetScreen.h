//
//  LeadersBetScreen.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 15/12/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WebConnection1.h"
#import "AsyncImageView.h"
#import "SpecifiBetPage.h"
#import "SearchScreen.h"
@interface LeadersBetScreen : UIViewController<WebRequestResult1,UITableViewDelegate,UITableViewDataSource>
{
    WebConnection1 *connection;
    NSMutableDictionary *dict;
    CGRect vscreenSize;
    CGFloat vscreenHeight,vscreenWidth;
     NSUInteger slectedShareIndex;
}
@property(nonatomic)IBOutlet UIView *viewForShare;

@property (nonatomic)AsyncImageView *asyImgBet;
@property (nonatomic)IBOutlet UIButton *btnBack,*btnBackBG,*btnShareFB,*btnShareTwit,*btnShareYoutube,*btnCloseShareUI;
@property (nonatomic)IBOutlet UITableView *tblLeadersBet;
@property (nonatomic)NSMutableArray *arrTblData;
@property(nonatomic)IBOutlet NSString *strUID;

@end
