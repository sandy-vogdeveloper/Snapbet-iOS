//
//  LeadersBetScreen.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 15/12/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "LeadersBetScreen.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <Social/Social.h>
#import "HeaderAndConstants.h"

@interface LeadersBetScreen ()
#pragma mark - FB sharing property
@property(strong,nonatomic)IBOutlet FBSDKShareButton *shareButton,*shareButton1;
@property(strong,nonatomic)  FBSDKShareLinkContent *content;
@property(strong,nonatomic) FBSDKShareVideo *video;
@property (nonatomic)FBSDKShareVideoContent *vContent;
@end

@implementation LeadersBetScreen

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadMainUI];
    [self loadData];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}
#pragma mark: load InitialUI
-(void)loadMainUI
{
    _btnBack=[[UIButton alloc]initWithFrame:CGRectMake(2,5,30,30)];
    _btnBackBG=[[UIButton alloc]initWithFrame:CGRectMake(0,0,55,55)];
    
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"iconBackArrow"] forState:UIControlStateNormal];
    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_btnBackBG addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnBack];
    [self.view addSubview:_btnBackBG];
    
    
}
#pragma mark: Load TableData
-(void)loadTableData
{
    _tblLeadersBet=[[UITableView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_btnBack.frame)+3,screenWidth,screenHeight-30)];
    _tblLeadersBet.tableFooterView=[[UIView alloc]init];
    _tblLeadersBet.backgroundColor=[UIColor clearColor];
    _tblLeadersBet.delegate=self;
    _tblLeadersBet.dataSource=self;
    [self.view addSubview:_tblLeadersBet];
}
#pragma mark: LoadData
-(void)loadData
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *urlString;
        NSString *strBaseURL=base_URL;
        
        urlString=[NSString stringWithFormat:@"wsgetActiveBeats.php?id=%@",_strUID];
        strBaseURL=[strBaseURL stringByAppendingString:urlString];
        NSURL *urlBet=[NSURL URLWithString:strBaseURL];
        request=[NSMutableURLRequest requestWithURL:[urlBet standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
        request = nil;
    }
    else
    {
        
    }
}
-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnBetsPressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    
    SpecifiBetPage *specificBet=[[SpecifiBetPage alloc]initWithNibName:@"SpecifiBetPage" bundle:nil];
    specificBet.strThemeName=[[_arrTblData valueForKey:@"name"]objectAtIndex:row];
    specificBet.selectedBetID1=[[_arrTblData valueForKey:@"id"]objectAtIndex:row];
    specificBet.strBetImg=[[_arrTblData valueForKey:@"bgimage"]objectAtIndex:row];
    [self.navigationController pushViewController:specificBet animated:YES];
}

-(IBAction)btnSharePressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    slectedShareIndex=btn.tag;
    [self viewForShareUI];
}     

-(IBAction)btnFBSharePressed:(id)sender
{
    [self closeViewButonPressed];
}

-(IBAction)btnTwitterSharePressed:(id)sender
{
    SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    NSString *strShareText = [NSString stringWithFormat:@"View Snapbets to accept new Challenges"];
    
    [mySLComposerSheet setInitialText:strShareText];
    
    // [mySLComposerSheet addImage:[UIImage imageNamed:strShareImg]];
    
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                
                NSLog(@"Post Canceled");
                break;
            case SLComposeViewControllerResultDone:
                
                NSLog(@"Post Sucessful");
                break;
            default:
                break;
        }
    }];
    [self closeViewButonPressed];
    [self presentViewController:mySLComposerSheet animated:YES completion:nil];
}

-(IBAction)btnSendPressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    NSLog(@"%ld",row);
    
    theAppDelegate.isSearch=NO;
    SearchScreen *serch=[[SearchScreen alloc]initWithNibName:@"SearchScreen" bundle:nil];
    serch.strSrchReciverID=[NSString stringWithFormat:@"%ld",(long)row];
    
    [self.navigationController pushViewController:serch animated:YES];
}
#pragma mark-Close Comment View
-(void)closeViewButonPressed
{
    @try
    {
        [self enableSubViews];
        for(UIView *subview in [_viewForShare subviews])
        {
            [subview removeFromSuperview];
        }
        CGRect frameForViewShare=_viewForShare.frame;
        
        frameForViewShare=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForShare.frame=frameForViewShare;
        } completion:^(BOOL finished){
            [_viewForShare removeFromSuperview];
            
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark: Enable Subview
-(void)enableSubViews
{
    for(UIView *subview in [self.view subviews])
    {
        subview.userInteractionEnabled=YES;
                // subview.alpha=1;
    }
}
#pragma mark: view For Share
-(void)viewForShareUI
{
    [_viewForShare removeFromSuperview];
    _viewForShare =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForShare.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForShare.tag=20;
    [self.view addSubview:_viewForShare];
    CGRect frameForViewForHelp=_viewForShare.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForShare.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForShare.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForShare.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForShare addSubview:img];
        UILabel *lblShare=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForShare.frame.size.width,30)];
        lblShare.text=@"Share";
        lblShare.textAlignment=NSTextAlignmentCenter;
        lblShare.textColor=[UIColor whiteColor];
        lblShare.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:25];
        [_viewForShare addSubview:lblShare];
        
        _btnShareFB=[[UIButton alloc]init];
        _btnShareTwit=[[UIButton alloc]init];
        _btnShareYoutube=[[UIButton alloc]init];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnShareFB.frame=CGRectMake(180,CGRectGetMaxY(lblShare.frame)+50,50,50);
            
            _shareButton1 = [[FBSDKShareButton alloc] initWithFrame:CGRectMake(180,CGRectGetMaxY(lblShare.frame)+50,50,50)];
            _shareButton = [[FBSDKShareButton alloc] initWithFrame:CGRectMake(0,0,50,50)];
            _btnShareTwit.frame=CGRectMake(_viewForShare.frame.size.width/2-25,CGRectGetMaxY(lblShare.frame)+50,50,50);
            _btnShareYoutube.frame=CGRectMake(_viewForShare.frame.size.width-220,CGRectGetMaxY(lblShare.frame)+50,50,50);
        }
        else
        {
            _btnShareFB.frame=CGRectMake(30,CGRectGetMaxY(lblShare.frame)+50,50,50);
            _shareButton1 = [[FBSDKShareButton alloc] initWithFrame:CGRectMake(30,CGRectGetMaxY(lblShare.frame)+50,50,50)];
            _shareButton = [[FBSDKShareButton alloc] initWithFrame:CGRectMake(0,0,50,50)];
            _btnShareTwit.frame=CGRectMake(_viewForShare.frame.size.width/2-25,CGRectGetMaxY(lblShare.frame)+50,50,50);
            _btnShareYoutube.frame=CGRectMake(_viewForShare.frame.size.width-80,CGRectGetMaxY(lblShare.frame)+50,50,50);
            
        }
        //[_btnShareFB setBackgroundImage:[UIImage imageNamed:@"icon_FB"] forState:UIControlStateNormal];
        [_btnShareTwit setBackgroundImage:[UIImage imageNamed:@"icon_Twitter"] forState:UIControlStateNormal];
        [_btnShareYoutube setBackgroundImage:[UIImage imageNamed:@"icon_Youtube"] forState:UIControlStateNormal];
        
        [_btnShareFB addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        [_btnShareTwit addTarget:self action:@selector(btnTwitterSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        [_btnShareYoutube addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        _shareButton1.transform = CGAffineTransformMakeScale(0.1,0.1);
        _btnShareTwit.transform = CGAffineTransformMakeScale(0.1,0.1);
        _btnShareYoutube.transform = CGAffineTransformMakeScale(0.1,0.1);
        
        [UIView beginAnimations:@"fadeInNewView" context:NULL];
        
        [UIView setAnimationDuration:1.0];
        _shareButton1.transform = CGAffineTransformMakeScale(1,1);
        _btnShareTwit.transform = CGAffineTransformMakeScale(1,1);
        _btnShareYoutube.transform = CGAffineTransformMakeScale(1,1);
        
        _shareButton1.alpha = 1.0f;
        _btnShareTwit.alpha = 1.0f;
        _btnShareYoutube.alpha = 1.0f;
        
        [UIView commitAnimations];
        
        // [_viewForShare addSubview:_btnShareFB];
        [_viewForShare addSubview:_btnShareTwit];
        [_viewForShare addSubview:_btnShareYoutube];
        
        _content = [[FBSDKShareLinkContent alloc] init];
        // NSURL *urlImage = [NSURL URLWithString:strShareURL];
        
        NSString *strBaseURL=ImgBase_URL;
        NSString *strVURL=vBase_URL;
        
        NSString *str=[[_arrTblData valueForKey:@"image"]objectAtIndex:slectedShareIndex];
        NSString *strVideo=[[_arrTblData valueForKey:@"video"]objectAtIndex:slectedShareIndex];
        
        strBaseURL=[strBaseURL stringByAppendingString:str];
        strVURL=[strVURL stringByAppendingString:strVideo];
        
        if ([strVideo isEqualToString:@"No"])
        {
            NSLog(@"No Video");
        }
        else
        {
            if (strVideo.length>0)
            {
                NSURL *url=[NSURL URLWithString:strVURL];
                FBSDKShareVideo *FBvideo = [[FBSDKShareVideo alloc] init];
                FBvideo.videoURL =url;
                FBSDKShareVideoContent *content1 = [[FBSDKShareVideoContent alloc] init];
                content1.video =FBvideo;
                [FBSDKShareDialog showFromViewController:self withContent:content1 delegate:nil];
            }
            else
            {
                
            }
        }
        NSString *strShareText = [NSString stringWithFormat:@"View my bets on Snapbets app"];
        _content.contentURL = [NSURL URLWithString:strBaseURL];
        [_content setContentTitle:strShareText];
        [_content setContentDescription:@"Description"];
        [_content setImageURL:[NSURL URLWithString: strBaseURL]];
        
        _shareButton1.shareContent = _content;
        [_shareButton1  addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        [_shareButton1 setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [_shareButton1 setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateSelected];
        [_shareButton1 setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
        _shareButton1.backgroundColor=[UIColor clearColor];
        [_viewForShare addSubview:_shareButton1];
        
        _shareButton.shareContent = _content;
        
        [_shareButton setTitle:@"" forState:UIControlStateNormal];
        [_shareButton1 setTitle:@"" forState:UIControlStateNormal];
        
        [_shareButton  addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        _shareButton.layer.cornerRadius=_shareButton.frame.size.height/2;
        _shareButton1.layer.cornerRadius=_shareButton1.frame.size.height/2;
        
        [_shareButton setClipsToBounds:YES];
        [_shareButton1 setClipsToBounds:YES];
        [_shareButton1 addSubview:_shareButton];
        
        _btnCloseShareUI=[[UIButton alloc]initWithFrame:CGRectMake(_viewForShare.frame.size.width-40,10,25,25)];
        [_btnCloseShareUI setBackgroundImage:[UIImage imageNamed:@"icon_Cross"] forState:UIControlStateNormal];
        [_btnCloseShareUI addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
        [_viewForShare addSubview:_btnCloseShareUI];
    }];
   // [self disableSubViews:_viewForShare];
}

#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    dict=[[NSMutableDictionary alloc]init];
    dict =[[NSMutableDictionary alloc]initWithDictionary:response];
    NSString *strMessage=[dict valueForKey:@"message"];
    NSLog(@"%@",dict);
    

            theAppDelegate.isMyBeetSelected=NO;
    
        if ([strMessage isEqualToString:@"sus"])
        {
            _arrTblData=[[NSMutableArray alloc]init];
            _arrTblData=[dict valueForKey:@"info"];
            [self loadTableData];
        }
        else
        {
            _arrTblData=[[NSMutableArray alloc]init];
            [self loadTableData];
            
        }
    
    [self.view setUserInteractionEnabled:YES];
    [theAppDelegate stopSpinner];
}

/*--------------     Bottom TableView delegate and datasource declaration here     -----------*/
#pragma mark- TableView DataSource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_arrTblData count];
}

//--------------------- cellforrow At Index TableView -----------------------//

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@""];
    if (cell == nil)
    {
        //  cell.transform= CGAffineTransformMakeRotation(M_PI/2);
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    
    tableView.separatorColor =UIColor.clearColor;
    cell.backgroundColor= [UIColor clearColor];
    [self.tblLeadersBet setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    UIImageView *imgCell=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,screenWidth,[UIScreen mainScreen].bounds.size.height/100*20)];
        imgCell.image=[UIImage imageNamed:@"icon_BG"];
        //        [cell.contentView addSubview:imgCell];
        
        NSString *strBUrl=ImgBase_URL;
        NSString *strURL=[[_arrTblData valueForKey:@"bgimage"]objectAtIndex:indexPath.row];
        
        if (strURL.length>0)
        {
            strBUrl=[strBUrl stringByAppendingString:strURL];
            NSURL *imgURL=[NSURL URLWithString:strBUrl];
            _asyImgBet=[[AsyncImageView alloc]initWithFrame:CGRectMake(0,0,screenWidth,[UIScreen mainScreen].bounds.size.height/100*20)];
            _asyImgBet.backgroundColor = [UIColor clearColor];
            [_asyImgBet loadImageFromURL:imgURL];
            [_asyImgBet setContentMode:UIViewContentModeScaleAspectFill];
            [_asyImgBet sizeToFit];
            _asyImgBet.clipsToBounds = YES;
            [cell.contentView addSubview:_asyImgBet];
        }
        
        UIView *viewForCell=[[UIView alloc]initWithFrame:CGRectMake(0,[UIScreen mainScreen].bounds.size.height/100*20-45,screenWidth,45)];
        UILabel *lblBetName=[[UILabel alloc]initWithFrame:CGRectMake(5,2,viewForCell.frame.size.width/3+10,30)];
        UILabel *lblBetDate=[[UILabel alloc]initWithFrame:CGRectMake(5,CGRectGetMaxY(lblBetName.frame)-10,60,30)];
        UILabel *lblBetVote=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(lblBetDate.frame)+5,CGRectGetMaxY(lblBetName.frame)-10,82,30)];
        
        UIFont *fnt=[UIFont fontWithName:@"Avenir-Roman" size:15];
        
        viewForCell.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.6];
        lblBetName.text=[[_arrTblData valueForKey:@"name"]objectAtIndex:indexPath.row];
        NSString *strExpDate=[[_arrTblData valueForKey:@"expdate"]objectAtIndex:indexPath.row];
        
        NSDate *dateFromString = [[NSDate alloc] init];
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"yyyy/MM/dd"];
        
        dateFromString = [dateformate dateFromString:strExpDate];
        NSDateFormatter *dateformate1=[[NSDateFormatter alloc]init];
        [dateformate1 setDateFormat:@"dd/MM/yy"]; // Date formater
        strExpDate = [dateformate1 stringFromDate:dateFromString];
        
        NSLog(@"date :%@",strExpDate);
        lblBetDate.text=strExpDate;
        NSString *strVotes=@"Votes ";
        strVotes=[strVotes stringByAppendingString:[[_arrTblData valueForKey:@"vote"]objectAtIndex:indexPath.row]];
        lblBetVote.text=strVotes;
        
        lblBetDate.textColor=[UIColor whiteColor];
        lblBetName.textColor=[UIColor whiteColor];
        lblBetVote.textColor=[UIColor whiteColor];
        
        lblBetName.textAlignment=NSTextAlignmentLeft;
        lblBetDate.textAlignment=NSTextAlignmentLeft;
        lblBetVote.textAlignment=NSTextAlignmentLeft;
        
        lblBetName.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:15];
        lblBetDate.font=[UIFont fontWithName:@"Avenir-Roman" size:11];
        lblBetVote.font=[UIFont fontWithName:@"Avenir-Roman" size:11];
        
        [viewForCell addSubview:lblBetName];
        [viewForCell addSubview:lblBetDate];
        [viewForCell addSubview:lblBetVote];
        
        [cell.contentView addSubview:viewForCell];
        UIButton *btnShare=[[UIButton alloc]init];
        UIButton *btnSend=[[UIButton alloc]init];
        UIButton *btnBets=[[UIButton alloc]init];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            btnShare.frame=CGRectMake(viewForCell.frame.size.width-290,0,95,50);
            btnSend.frame=CGRectMake(CGRectGetMaxX(btnShare.frame)+5,0,95,50);
            btnBets.frame=CGRectMake(CGRectGetMaxX(btnSend.frame)+5,0,95,50);
        }
        else
        {
            if(screenWidth<325)
            {
                btnShare.frame=CGRectMake(viewForCell.frame.size.width-185,0,60,45);
                btnSend.frame=CGRectMake(CGRectGetMaxX(btnShare.frame)+3,0,60,45);
                btnBets.frame=CGRectMake(CGRectGetMaxX(btnSend.frame)+3,0,60,45);
            }
            else
            {
                btnShare.frame=CGRectMake(viewForCell.frame.size.width-230,0,75,45);
                btnSend.frame=CGRectMake(CGRectGetMaxX(btnShare.frame)+3,0,75,45);
                btnBets.frame=CGRectMake(CGRectGetMaxX(btnSend.frame)+3,0,75,45);
            }
        }
        
        [btnShare setBackgroundImage:[UIImage imageNamed:@"icon_ShareBG"] forState:UIControlStateNormal];
        [viewForCell addSubview:btnShare];
        
        [btnSend setBackgroundImage:[UIImage imageNamed:@"icon_ShareBG"] forState:UIControlStateNormal];
        [viewForCell addSubview:btnSend];
        [btnBets setBackgroundImage:[UIImage imageNamed:@"icon_ShareBG"] forState:UIControlStateNormal];
        [viewForCell addSubview:btnBets];
        
        [btnSend setTitle:@"Send" forState:UIControlStateNormal];
        [btnShare setTitle:@"Share" forState:UIControlStateNormal];
        [btnBets setTitle:@"Responses" forState:UIControlStateNormal];
        
        [btnShare setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnSend setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnBets setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [btnShare addTarget:self action:@selector(btnSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        [btnSend addTarget:self action:@selector(btnSendPressed:) forControlEvents:UIControlEventTouchUpInside];
        [btnBets addTarget:self action:@selector(btnBetsPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        btnSend.titleLabel.font=fnt;
        btnShare.titleLabel.font=fnt;
        btnBets.titleLabel.font=fnt;
        
        btnBets.tag=indexPath.row;
        btnSend.tag=indexPath.row;
        btnShare.tag=indexPath.row;
        
        UIButton *btnMore=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-50,[UIScreen mainScreen].bounds.size.height/100*5,50,25)];
        [btnMore setBackgroundImage:[UIImage imageNamed:@"icon_More"] forState:UIControlStateNormal];
        [btnMore addTarget:self action:@selector(btnBetsPressed:) forControlEvents:UIControlEventTouchUpInside];
        btnMore.tag=indexPath.row;
        [cell.contentView addSubview:btnMore];
        return cell;
}

#pragma Mark-delegate methods
//--------------------- Tableview didselected index ---------------------//
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [UIScreen mainScreen].bounds.size.height/100*20;
}

@end
