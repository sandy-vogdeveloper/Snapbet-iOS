//
//  APPChildViewController.m
//  PageView Demo
//
//  Created by Syneotek on 16/08/14.
//  Copyright (c) 2014 Syneotek Software Solution. All rights reserved.
//

#import "APPChildViewController.h"

@interface APPChildViewController ()

@end

@implementation APPChildViewController

@synthesize lblTitle,txtDesc,imgDesc,strlblTitle,strImgName,strTxtDesc;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenNumber.text = [NSString stringWithFormat:@"Screen #%ld", (long)self.index];
    // Do any additional setup after loading the view from its nib.
    
    lblTitle.text=strlblTitle;
    txtDesc.text=strTxtDesc;
    imgDesc.image=[UIImage imageNamed:strImgName];
    // _imgSept.image=[UIImage imageNamed:_strImgSept];
   // imgDesc.contentMode=UIViewContentModeScaleToFill;
    //[imgDesc setClipsToBounds:YES];
   // [imgDesc sizeToFit];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
