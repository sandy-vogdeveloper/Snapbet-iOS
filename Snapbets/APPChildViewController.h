//
//  APPChildViewController.h
//  PageView Demo
//
//  Created by Syneotek on 16/08/14.
//  Copyright (c) 2014 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APPChildViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *screenNumber;
@property (assign, nonatomic) NSInteger index;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIImageView *imgDesc,*imgSept;

@property (weak, nonatomic) IBOutlet UITextView *txtDesc;


@property(strong,nonatomic)NSString *strlblTitle;
@property(strong,nonatomic)NSString *strImgName,*strImgSept;
@property(strong,nonatomic)NSString *strTxtDesc;





@end
