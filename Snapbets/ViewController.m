//
//  ViewController.m
//  Snapbets
//  Created by Bhimashankar Vibhute on 06/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
CGRect vscreenSize;
CGFloat vscreenHeight,vscreenWidth;
- (void)viewDidLoad
{
    [super viewDidLoad];
         /*----  Load MainUI    ---*/
    _isfbLogin=NO;
    _isStatus=NO;
    
            [self loadInitialUI];
    curntIndexPge=0;
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}

//-(void)viewDidAppear:(BOOL)animated
//{
//    
//    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
//    
//    NSInteger loadCount=[userDefaults integerForKey:@"LoadCount"];
//    if (loadCount<=0)
//    {
//        [self showHelpView];
//    }
//    loadCount++;
//    [userDefaults setInteger:loadCount forKey:@"LoadCount"];
//    [userDefaults synchronize];
//}
#pragma mark: Load InitialUI
-(void)loadInitialUI
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
         _imgTrangleBG=[[UIImageView alloc]initWithFrame:CGRectMake(40,screenHeight/2+30,screenWidth-80,200)];
        _btnFB=[[UIButton alloc]initWithFrame:CGRectMake(60,screenHeight/2+90,_imgMainBG.frame.size.width-120,40)];
        _btnSignIn=[[UIButton alloc]initWithFrame:CGRectMake(60,CGRectGetMaxY(_btnFB.frame)+20,_imgMainBG.frame.size.width-120,40)];
    }
    else
    {
         _imgTrangleBG=[[UIImageView alloc]initWithFrame:CGRectMake(10,screenHeight/2+30,screenWidth-20,200)];
        _btnFB=[[UIButton alloc]initWithFrame:CGRectMake(20,screenHeight/2+90,_imgMainBG.frame.size.width-40,40)];
        _btnSignIn=[[UIButton alloc]initWithFrame:CGRectMake(20,CGRectGetMaxY(_btnFB.frame)+20,_imgMainBG.frame.size.width-40,40)];
    }
   
    _imgTrangleBG.image=[UIImage imageNamed:@"img_Trangle"];
    [self.view addSubview:_imgTrangleBG];    
    
    [_btnFB setBackgroundColor:[UIColor blackColor]];
    _btnFB.layer.cornerRadius=4.0;
    _btnFB.alpha=0.9;
    [_btnFB addTarget:self action:@selector(btnFBPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnFB];
    
    UIImageView *iconFB=[[UIImageView alloc]initWithFrame:CGRectMake(10,5,28,28)];
    iconFB.image=[UIImage imageNamed:@"img_FBIcon"];
    [_btnFB addSubview:iconFB];

    UILabel *lblFB=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(iconFB.frame)+10,5,200,30)];
    lblFB.text=@"Sign in with Facebook";
    lblFB.textColor=[UIColor whiteColor];
    lblFB.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
    [_btnFB addSubview:lblFB];
    
    UIImageView *iconFBFrwd=[[UIImageView alloc]initWithFrame:CGRectMake(_btnFB.frame.size.width-30,10,20,20)];
    iconFBFrwd.image=[UIImage imageNamed:@"icon_forward"];
    [_btnFB addSubview:iconFBFrwd];
    
    [_btnSignIn setBackgroundColor:[UIColor blackColor]];
    _btnSignIn.layer.cornerRadius=4.0;
    _btnSignIn.alpha=0.9;
    [self.view addSubview:_btnSignIn];
    

    UIImageView *iconSignIn=[[UIImageView alloc]initWithFrame:CGRectMake(10,5,28,28)];
    iconSignIn.image=[UIImage imageNamed:@"icon_Profile"];
    [_btnSignIn addSubview:iconSignIn];

    UILabel *lblSignIN=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(iconSignIn.frame)+10,5,200,30)];
    lblSignIN.text=@"Sign in";
    lblSignIN.textColor=[UIColor whiteColor];
    lblSignIN.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
    [_btnSignIn addSubview:lblSignIN];
    
    
   UIImageView *iconSignInFrwd=[[UIImageView alloc]initWithFrame:CGRectMake(_btnSignIn.frame.size.width-30,10,20,20)];
    iconSignInFrwd.image=[UIImage imageNamed:@"icon_forward"];
    [_btnSignIn addSubview:iconSignInFrwd];
    
    [_btnSignIn addTarget:self action:@selector(btnSignInPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_btnSignUp addTarget:self action:@selector(btnSignUpPressed:) forControlEvents:UIControlEventTouchUpInside];
    
   /*
    [UIView animateWithDuration:0.3/1.5 animations:^{
        _btnFB.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            _btnFB.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                _btnFB.transform = CGAffineTransformIdentity;
            }];
        }];
    }];
    
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        _btnSignIn.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            _btnSignIn.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                _btnSignIn.transform = CGAffineTransformIdentity;
            }];
        }];
    }];*/
    
    _btnFB.alpha = 0.0f;
    _btnFB.transform = CGAffineTransformMakeScale(0.1,0.1);
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:1.0];
    _btnFB.transform = CGAffineTransformMakeScale(1,1);
    _btnFB.alpha = 1.0f;
    [UIView commitAnimations];
    
    _btnSignIn.alpha = 0.0f;
    _btnSignIn.transform = CGAffineTransformMakeScale(0.1,0.1);
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:1.0];
    _btnSignIn.transform = CGAffineTransformMakeScale(1,1);
    _btnSignIn.alpha = 1.0f;
    [UIView commitAnimations];
    
   /*
    [UIView animateWithDuration:0.5f animations:^{
        
        self.btnFB.transform = CGAffineTransformMakeScale(1.5, 1.5);
    } completion:^(BOOL finished){}];
    // for zoom out
    [UIView animateWithDuration:0.3f animations:^{
        
        self.btnFB.transform = CGAffineTransformMakeScale(1, 1);
    }completion:^(BOOL finished){}];
    
    
    
    [UIView animateWithDuration:0.5f animations:^{
        
        self.btnSignIn.transform = CGAffineTransformMakeScale(1.5, 1.5);
    } completion:^(BOOL finished){}];
    // for zoom out
    [UIView animateWithDuration:0.3f animations:^{
        
        self.btnSignIn.transform = CGAffineTransformMakeScale(1, 1);
    }completion:^(BOOL finished){}];
    
    */
}


-(void)disableSubViews:(UIView *)view
{
    for(UIView *subview in [self.view subviews])
    {
        if ((subview.tag==view.tag&&[subview isMemberOfClass:[UIView class]]))
        {}
        else
        {
            subview.userInteractionEnabled=NO;
            subview.alpha=0.3;
        }
    }
}
-(void)enableSubViews
{
    for(UIView *subview in [self.view subviews])
    {
        subview.userInteractionEnabled=YES;
        subview.alpha=1;
    }
}

#pragma mark: UIButton Pressed delegate declarations here
-(IBAction)btnSignUpPressed:(id)sender
{
    RegistrationScreen *reg=[[RegistrationScreen alloc]initWithNibName:@"RegistrationScreen" bundle:nil];
    [self.navigationController pushViewController:reg animated:YES];
}


-(IBAction)btnSignInPressed:(id)sender
{
    SignInScreen *sign=[[SignInScreen alloc]initWithNibName:@"SignInScreen" bundle:nil];
    [self.navigationController pushViewController:sign animated:YES];
}
-(IBAction)btnFBPressed:(id)sender
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    if ([FBSDKAccessToken currentAccessToken])
    {
       
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        [self fetchUserInfo];
    }
    else
    {
        [login logInWithReadPermissions:@[@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult* result, NSError *error)
         {
             if (error)
             {
                 NSLog(@"Login process error");
             }
             else if (result.isCancelled)
             {
                 NSLog(@"User cancelled login");
             }
             else
             {
                 NSLog(@"Login Success");
                 
                 if ([result.grantedPermissions containsObject:@"email"])
                 {
                     NSLog(@"result is:%@",result);
                     [self fetchUserInfo];
                 }
                 else
                 {
                     //[SVProgressHUD showErrorWithStatus:@"Facebook email permission error"];
                 }
             }
         }];
    }
}
#pragma mark: Facebook Info fetching function
-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, email"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 NSLog(@"results:%@",result);
                 
                 // [appDelegate showSpinnerInView:self.view];
                 _strFBMail = [result objectForKey:@"email"];
                 _strFBID =[result objectForKey:@"id"];
                 _strFBName=[result objectForKey:@"name"];
                 //_urlFB= [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=normal",_strFBID]];
                 
                 _urlFB=[NSURL URLWithString:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?width=500&height=500",_strFBID]];
                         
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     NSError* error = nil;
                     theAppDelegate.imgData=[NSData dataWithContentsOfURL:_urlFB options:NSDataReadingUncached error:&error];
                     NSLog(@"Url String=%@",theAppDelegate.imgData);
                 });
                 
                 if (_strFBMail.length >0 )
                 {
                     //Start you app Todo
                 }
                 else
                 {
                     // NSLog(@“Facebook email is not verified");
                 }
                 
                 [self sendFBSErverCall];
                 
             }
             else
             {
                 NSLog(@"Error %@",error);
             }
             if (theAppDelegate.imgData==nil) {
                 
             }
             else
             {
                 [self sendFBSErverCall];
             }
         }];
    }
}

#pragma mark: FBLogin Server call
-(void)sendFBSErverCall
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
        
        if (theAppDelegate.strDeviceToken.length==0)
        {
            theAppDelegate.strDeviceToken=theAppDelegate.strDeviceID;
        }

        _isfbLogin=YES;
        _isStatus=NO;
        NSArray *arrNameString=[_strFBName componentsSeparatedByString:@" "];
        NSString *strBaseURL=base_URL;
        NSString *urlString=[NSString stringWithFormat:@"usersignup.php?fname=%@&lname=%@&email=%@&facebook_id=%@&contact=121&image=%@&dob=1989/12/01&did=%@&dtype=I",[arrNameString objectAtIndex:0],[arrNameString objectAtIndex:1],_strFBMail,_strFBID,_urlFB,theAppDelegate.strDeviceToken];
        
        strBaseURL=[strBaseURL stringByAppendingString:urlString];
        strBaseURL=[strBaseURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:strBaseURL];
        request=[NSMutableURLRequest requestWithURL:[url standardizedURL]];
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"The Internet connection appears to be offline" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    request = nil;
}

#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    
    dict =[[NSMutableDictionary alloc]initWithDictionary:response];
    NSString *strMsg=[dict valueForKey:@"message"];
    if ([strMsg isEqualToString:@"sus"])
    {
        if(_isfbLogin)
        {
            _arrLoginInfo=[dict valueForKey:@"info"];
            [self checkUserStatus];
        }
        else
        {
            [theAppDelegate stopSpinner];
            [self.view setUserInteractionEnabled:YES];
            NSArray *arrStatus=[dict valueForKey:@"info"];
            NSString *strStatus=[arrStatus valueForKey:@"block"];
            
            if ([strStatus isEqualToString:@"N"]) {
                [self saveDatatoDatabase];
                MainDashboardScreen *mainDash=[[MainDashboardScreen alloc]initWithNibName:@"MainDashboardScreen" bundle:nil];
                [self.navigationController pushViewController:mainDash animated:YES];
            }
            else
            {
                [self performSelector:@selector(viewForBlockedAlertUI) withObject:nil afterDelay:0.5];
            }
        }
        
    }
    else
    {
        _strAlertTitle=@"Facebook registration failed";
         [self viewForAlertUI];
    }
}

-(void)checkUserStatus
{
    _isStatus=YES;
    _isfbLogin=NO;
    NSString *strUID=[_arrLoginInfo valueForKey:@"id"];
    
    NSMutableURLRequest *request;
    
        NSString *strBaseURL=base_URL;
        NSString *urlString=[NSString stringWithFormat:@"wschecklogin.php?id=%@",strUID];
        
        strBaseURL=[strBaseURL stringByAppendingString:urlString];
        strBaseURL=[strBaseURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:strBaseURL];
        request=[NSMutableURLRequest requestWithURL:[url standardizedURL]];
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
}

#pragma mark: Save data to DB
-(void)saveDatatoDatabase
{
    NSArray *arrDBTemp=[[NSMutableArray alloc]init];
    theAppDelegate.arrUserLoginData=[[NSMutableArray alloc]init];
    _objDBManager=[DBManager getSharedInstance];
    arrDBTemp=[_objDBManager selectTableDataWithQuery:@"select *from tblUser"];
    
    if ([arrDBTemp count])
    {
        [_objDBManager deleteFromTableWithQuery:@"delete from tblUser"];
    }
    
    NSString *strUID,*strEmail,*strFName,*strLName,*strContact,*strFBLogin;    
    
    strUID=[_arrLoginInfo valueForKey:@"id"];
    strEmail=[_arrLoginInfo valueForKey:@"email"];
    strFName=[_arrLoginInfo valueForKey:@"fname"];
    strLName=[_arrLoginInfo valueForKey:@"lname"];
    strContact=[_arrLoginInfo valueForKey:@"contact"];
    strFBLogin=@"Yes";
    
    [theAppDelegate.arrUserLoginData addObject:strUID];
    [theAppDelegate.arrUserLoginData addObject:strEmail];
    [theAppDelegate.arrUserLoginData addObject:strFName];
    [theAppDelegate.arrUserLoginData addObject:strLName];
    [theAppDelegate.arrUserLoginData addObject:strContact];
    [theAppDelegate.arrUserLoginData addObject:_urlFB];
    [theAppDelegate.arrUserLoginData addObject:theAppDelegate.strDeviceToken];
    [theAppDelegate.arrUserLoginData addObject:strFBLogin];
    NSLog(@"%@",theAppDelegate.arrUserLoginData);
    
    NSString *queryString=[NSString stringWithFormat:@"insert into tblUser values('%@','%@','%@','%@','%@','%@','%@','%@')",strUID,strEmail,strFName,strLName,strContact,_urlFB,theAppDelegate.strDeviceToken,strFBLogin];
    
    if ([_objDBManager insertDataWithQuery:queryString])
    {
        
    }
    else
    {
    }
}

#pragma mark: view For Alert
-(void)viewForAlertUI
{
    [_viewForAlert removeFromSuperview];
    _viewForAlert =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForAlert.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForAlert.tag=20;
    [self.view addSubview:_viewForAlert];
    
    CGRect frameForViewForHelp=_viewForAlert.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForAlert.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForAlert.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForAlert addSubview:img];
        UILabel *lblAlert=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForAlert.frame.size.width,30)];
        lblAlert.text=_strAlertTitle;
        lblAlert.textAlignment=NSTextAlignmentCenter;
        lblAlert.textColor=[UIColor whiteColor];
        lblAlert.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForAlert addSubview:lblAlert];
        
        _btnCloseAlert=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseAlert.frame=CGRectMake(_viewForAlert.frame.size.width/2-60,CGRectGetMaxY(lblAlert.frame)+50,120,40);
        }
        else
        {
            _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,CGRectGetMaxY(lblAlert.frame)+50,100,40)];
        }
    
        [_btnCloseAlert setTitle:@"Close" forState:UIControlStateNormal];
            
       
        [_btnCloseAlert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseAlert.layer.borderWidth=2.0;
        _btnCloseAlert.layer.cornerRadius=4.0;
        _btnCloseAlert.layer.borderColor=[[UIColor orangeColor]CGColor];
        //[_btnVoteSeeMore addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [_btnCloseAlert addTarget:self action:@selector(viewForCloseAlert) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewForAlert addSubview:_btnCloseAlert];
    }];
}

#pragma mark-Close Comment View
-(void)viewForCloseAlert
{
    @try
    {
        for(UIView *subview in [_viewForAlert subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForVote=_viewForAlert.frame;
        frameForViewForVote=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            _viewForAlert.frame=frameForViewForVote;
        } completion:^(BOOL finished){
            
            [_viewForAlert removeFromSuperview];
            
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark: view For Block User Alert
-(void)viewForBlockedAlertUI
{
    [_viewForBlockedAlert removeFromSuperview];
    _viewForBlockedAlert =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForBlockedAlert.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForBlockedAlert.tag=20;
    [self.view addSubview:_viewForBlockedAlert];
    
    CGRect frameForViewForHelp=_viewForBlockedAlert.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForBlockedAlert.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForBlockedAlert.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForBlockedAlert.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForBlockedAlert addSubview:img];
        UILabel *lblAlert=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForBlockedAlert.frame.size.width,30)];
        lblAlert.text=@"Your account has blocked";
        lblAlert.textAlignment=NSTextAlignmentCenter;
        lblAlert.textColor=[UIColor whiteColor];
        lblAlert.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:18];
        [_viewForBlockedAlert addSubview:lblAlert];
        
        UILabel *lblAlert1=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblAlert.frame)+5,_viewForBlockedAlert.frame.size.width,30)];
        lblAlert1.text=@"Please contact to admin";
        lblAlert1.textAlignment=NSTextAlignmentCenter;
        lblAlert1.textColor=[UIColor whiteColor];
        lblAlert1.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForBlockedAlert addSubview:lblAlert1];
        
        _btnCloseAlert=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseAlert.frame=CGRectMake(_viewForBlockedAlert.frame.size.width/2-60,CGRectGetMaxY(lblAlert1.frame)+50,120,40);
        }
        else
        {
            _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForBlockedAlert.frame.size.width/2-50,CGRectGetMaxY(lblAlert1.frame)+50,100,40)];
        }
        
        [_btnCloseAlert setTitle:@"Close" forState:UIControlStateNormal];
        
        
        [_btnCloseAlert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseAlert.layer.borderWidth=2.0;
        _btnCloseAlert.layer.cornerRadius=4.0;
        _btnCloseAlert.layer.borderColor=[[UIColor orangeColor]CGColor];
        //[_btnVoteSeeMore addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [_btnCloseAlert addTarget:self action:@selector(viewForCloseAlert1) forControlEvents:UIControlEventTouchUpInside];
        [_viewForBlockedAlert addSubview:_btnCloseAlert];
    }];
}

#pragma mark-Close Comment View
-(void)viewForCloseAlert1
{
    @try
    {
        for(UIView *subview in [_viewForBlockedAlert subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForVote=_viewForBlockedAlert.frame;
        frameForViewForVote=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            _viewForBlockedAlert.frame=frameForViewForVote;
        } completion:^(BOOL finished){
            
            [_viewForBlockedAlert removeFromSuperview];
            
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}
@end
