//
//  ProfileScreen.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 08/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "ProfileScreen.h"
#import "WebConnection1.h"

@interface ProfileScreen ()<WebRequestResult1>
{
    WebConnection1 *connection;
    NSMutableDictionary *dict;
    NSString *totalResponses;
}
@end

@implementation ProfileScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    _isActiveBet=NO;
    _isResponses=NO;
    _isBitHistory=NO;
    _isVoteDone=NO;
    [self profileDetailsServerAPI];
    _arrProfileDetails=[[NSMutableArray alloc]init];
    [self loadInitialUI];
    
    _arrTblImges=[[NSMutableArray alloc]initWithObjects:@"icon_ActiveBts",@"icon_ProfResponse",@"icon_ProfBetHist",nil];
    _arrTblTitles=[[NSMutableArray alloc]initWithObjects:@"Active Bets",@"My Responses",@"Bet History",nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark: Load InitialUI
-(void)loadInitialUI
{
    _asyProfBlur=[[AsyncImageView alloc]initWithFrame:CGRectMake(0,0,screenWidth,10)];
    _asyProfBlur.backgroundColor = [UIColor clearColor];
    [_asyProfBlur setContentMode:UIViewContentModeScaleAspectFill];
    //[_asyProfBlur sizeToFit];
    _asyProfBlur.layer.masksToBounds=YES;
   // _asyProfBlur.layer.cornerRadius=(_asyProfBlur.frame.size.height/2);
    _asyProfBlur.alpha=0.7;
    [self.view addSubview:_asyProfBlur];
    
    /*-----------  Button For Menu Selection for SidebarViewController-----------*/
    _btnBack=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBack.frame = CGRectMake(5,8,25,25);
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"icon_BackMove"] forState:UIControlStateNormal];
    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    /*----------- Button Pressed Event Fired and goto SidebarViewController--------*/
    _btnBackPress=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBackPress.frame = CGRectMake(0,0,105,55);
    [_btnBackPress addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnBack];
    [self.view addSubview:_btnBackPress];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        _tblProfileDetails=[[UITableView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+250,screenWidth,screenHeight/2-30)];
    }
    else
    {
        if (screenWidth<325)
        {
            _tblProfileDetails=[[UITableView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+50,screenWidth,screenHeight/2-30)];
        }
        else
        {
            _tblProfileDetails=[[UITableView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+80,screenWidth,screenHeight/2-30)];
        }
    }
    [_btnAddProfile addTarget:self action:@selector(btnAddFriendPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    _tblProfileDetails.delegate=self;
    _tblProfileDetails.dataSource=self;
    _tblProfileDetails.tableFooterView=[[UIView alloc]init];
    _tblProfileDetails.backgroundColor=[UIColor clearColor];
    [self.view addSubview:_tblProfileDetails];
    if (theAppDelegate.isProfFromMenu)
    {
        _btnAddProfile.hidden=YES;
    }
    else
    {
        _btnAddProfile.hidden=NO;
    }
    _btnAddProfile.hidden=YES;
}

-(void)viewForUpdateData
{
    _lblWins.text=[_arrProfileDetails valueForKey:@"win"];
    _lblCreates.text=[NSString stringWithFormat:@"%@",[_arrProfileDetails valueForKey:@"created"]];
    _lblVotes.text=[NSString stringWithFormat:@"%@",[_arrProfileDetails valueForKey:@"votes"]];
//    _lblResponse.text=[_arrProfileDetails valueForKey:@"response"];
   _lblUserName.text=[_arrProfileDetails valueForKey:@"username"];
    NSString *strImg=[_arrProfileDetails valueForKey:@"profileimage"];
    
    CGFloat xPosition=_imgUserProf.frame.origin.x;
    CGFloat yPosition=_imgUserProf.frame.origin.y;
    
    if ([strImg containsString:@"http://graph.facebook.com/"])
    {
        NSArray *arr=[strImg componentsSeparatedByString:@"http://graph.facebook.com/"];
        NSLog(@"%@",arr);
        NSString *strString=[arr objectAtIndex:1];
        NSArray *arr1=[strString componentsSeparatedByString:@"/"];
         NSLog(@"%@",arr1);
        strImg=[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?width=500&height=500",[arr1 objectAtIndex:0]];
        
        _asyProfileImg=[[AsyncImageView alloc]initWithFrame:CGRectMake(xPosition,yPosition,_imgUserProf.frame.size.width,_imgUserProf.frame.size.height)];
        _asyProfileImg.backgroundColor = [UIColor clearColor];
        [_asyProfileImg loadImageFromURL:[NSURL URLWithString:strImg]];
        _asyProfileImg.layer.cornerRadius =_asyProfileImg.frame.size.width / 2;
        _asyProfileImg.clipsToBounds = YES;
        _asyProfileImg.contentMode = UIViewContentModeScaleAspectFit;
        _asyProfileImg.layer.masksToBounds = YES;
        [self.view bringSubviewToFront:_asyProfileImg];
        [self.view addSubview:_asyProfileImg];
        
        [_asyProfBlur loadImageFromURL:[NSURL URLWithString:strImg]];
        
    }
    else if ([strImg containsString:@"."])
    {
        NSString *strBase=ImgBase_URL;
        strBase=[strBase stringByAppendingString:strImg];
        
        _asyProfileImg=[[AsyncImageView alloc]initWithFrame:CGRectMake(xPosition,yPosition,_imgUserProf.frame.size.width,_imgUserProf.frame.size.height)];
        
        _asyProfileImg.backgroundColor = [UIColor clearColor];
        
        [_asyProfileImg loadImageFromURL:[NSURL URLWithString:strBase]];
        _asyProfileImg.layer.cornerRadius=_asyProfileImg.frame.size.height/2;
        [_asyProfileImg setContentMode:UIViewContentModeScaleAspectFit];
        [_asyProfileImg setClipsToBounds:YES];
        _asyProfileImg.layer.masksToBounds = YES;
        
        [self.view bringSubviewToFront:_asyProfileImg];
        [self.view addSubview:_asyProfileImg];
        
        [_asyProfBlur loadImageFromURL:[NSURL URLWithString:strBase]];
    }
    else
    {
            [self.view bringSubviewToFront:_imgUserProf];
        _imgUserProf.image=[UIImage imageNamed:@"profileIcon"];
        _imgProfBlue.image=[UIImage imageNamed:@"profileIcon"];
        _imgProfBlue.alpha=0.5;
    }
    CGFloat height=_imgProfBlue.frame.size.height;
    _asyProfBlur.frame=CGRectMake(0,0,screenWidth,height);
   // [_asyProfBlur bringSubviewToFront:_btnBack];
    
}
-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnAddFriendPressed:(id)sender
{
    [self addFriendServerAPI];
    
}
-(IBAction)btnVotePressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    NSLog(@"%ld",(long)row);
    if (theAppDelegate.isProfFromMenu||[_strUserID isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:0]])
    {
        if ([_strCollapsedTitle isEqualToString:@"Active Bets"]||[_strCollapsedTitle isEqualToString:@"Bet History"])
        {
            _isActBPresed=YES;
            _isRespPresed=NO;
            _isAddFrndAPI=NO;
            _isProfileAPI=NO;
            
            NSMutableArray *arrData=[[NSMutableArray alloc]init];
            arrData=[_arrDataForTableDisplay objectAtIndex:row];
            strSID=[arrData valueForKey:@"id"];
            strUID=[arrData valueForKey:@"uid"];
            NSLog(@"%@",strSID);
            [self viewfForYesNoAlertUI1];
        }
        else if ([_strCollapsedTitle isEqualToString:@"My Responses"])
        {
            _isActBPresed=NO;
            _isAddFrndAPI=NO;
            _isProfileAPI=NO;
            _isRespPresed=YES;
            
            NSMutableArray *arrData=[[NSMutableArray alloc]init];
            arrData=[_arrDataForTableDisplay objectAtIndex:row];
            
            strSID=[arrData valueForKey:@"id"];
            strUID=[arrData valueForKey:@"uid"];
                    NSLog(@"%@",arrData);
            [self viewfForYesNoAlertUI1];
        }
    }
    else
    {
        if(theAppDelegate.isConnected)
        {
            NSMutableURLRequest *request;
            [theAppDelegate showSpinnerInView:self.view];
            [self.view setUserInteractionEnabled:NO];
            _isVoteDone=YES;
            _isProfileAPI=NO;
            _isAddFrndAPI=NO;
            _isAlreadyFrndOrNot=NO;
            NSMutableArray *arrData=[[NSMutableArray alloc]init];
            arrData=[_arrDataForTableDisplay objectAtIndex:row];
            strSID=[arrData valueForKey:@"id"];
            strUID=[arrData valueForKey:@"uid"];
            NSLog(@"%@",strSID);

            NSString *strBaseURL=base_URL;                        
            NSString *urlString;
            
            if([_strCollapsedTitle isEqualToString:@"My Responses"])
            {
                urlString=[NSString stringWithFormat:@"wsaddchallenger_vote.php?uid=%@&sid=%@&tid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],strSID,strUID];
            }
            else
            {
                urlString=[NSString stringWithFormat:@"wsaddsnapvote.php?uid=%@&sid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],strSID];
            }
            
            strBaseURL=[strBaseURL stringByAppendingString:urlString];
            strBaseURL=[strBaseURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSURL *urlChallenger=[NSURL URLWithString:strBaseURL];
            request=[NSMutableURLRequest requestWithURL:[urlChallenger standardizedURL]];
            
            if(!connection)
                connection= [[WebConnection1 alloc] init];
            connection.delegate = self;
            [connection makeConnection:request];
        }
    }
}

-(IBAction)btnPlay1Pressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    NSLog(@"%ld",row);
    if (_isActiveBet)
    {
        row=row-1;
        _strBetImage=[[_arrActivebet valueForKey:@"image"]objectAtIndex:row];
        _strVideoName=[[_arrActivebet valueForKey:@"video"]objectAtIndex:row];
        _strVideotitle=[[_arrActivebet valueForKey:@"name"]objectAtIndex:row];
        
        [self viewForVideoScreenPopUpUI];
    }
    else if (_isResponses)
    {
        row=row-2;
        _strBetImage=[[_arrResponse valueForKey:@"image"]objectAtIndex:row];
        _strVideoName=[[_arrResponse valueForKey:@"video"]objectAtIndex:row];
        _strVideotitle=[[_arrResponse valueForKey:@"name"]objectAtIndex:row];
        [self viewForVideoScreenPopUpUI];
    }
    else if (_isBitHistory)
    {
        row=row-3;
        _strBetImage=[[_arrBethistory valueForKey:@"image"]objectAtIndex:row];
        _strVideoName=[[_arrBethistory valueForKey:@"video"]objectAtIndex:row];
        _strVideotitle=[[_arrBethistory valueForKey:@"name"]objectAtIndex:row];
        [self viewForVideoScreenPopUpUI];
    }
}

-(IBAction)btnCloseAletUIPressed:(id)sender
{
    [self closeAlertView];
    [self profileDetailsServerAPI];
}
#pragma mark-Close Comment View
-(void)closeViewButonPressed
{
    @try
    {
        for(UIView *subview in [_viewForAddFrnd subviews])
        {
            [subview removeFromSuperview];
        }
      
        CGRect frameForViewForAddFrnd=_viewForAddFrnd.frame;
      
        
        frameForViewForAddFrnd=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForAddFrnd.frame=frameForViewForAddFrnd;
         
        } completion:^(BOOL finished){
            [_viewForAddFrnd removeFromSuperview];
             //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark-Close Comment View
-(void)ViewForCloseVideoUI
{
    @try
    {
        for(UIView *subview in [_viewForVideoScreen subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForAddFrnd=_viewForVideoScreen.frame;
        
        frameForViewForAddFrnd=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            [_playerViewController.player pause];
            [_playerViewController.view removeFromSuperview];
            _viewForVideoScreen.frame=frameForViewForAddFrnd;
            
        } completion:^(BOOL finished){
            [_viewForVideoScreen removeFromSuperview];
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark: view For VoteUI
-(void)viewfForYesNoAlertUI1
{
    [_viewForYNAlertUI removeFromSuperview];
    _viewForYNAlertUI =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForYNAlertUI.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForYNAlertUI.tag=20;
    [self.view addSubview:_viewForYNAlertUI];
    CGRect frameForViewForHelp=_viewForYNAlertUI.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForYNAlertUI.frame=frameForViewForHelp;
    } completion:^(BOOL finished)
     {
         vscreenSize=_viewForYNAlertUI.frame;
         vscreenHeight=vscreenSize.size.height;
         vscreenWidth=vscreenSize.size.width;
         
         UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForYNAlertUI.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
         img.image=[UIImage imageNamed:@"icon_Share"];
         [_viewForYNAlertUI addSubview:img];
         UILabel *lblShare=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+25,_viewForYNAlertUI.frame.size.width,30)];
         lblShare.text=@"Are you sure you want to remove this bet!";
         lblShare.textAlignment=NSTextAlignmentCenter;
         lblShare.textColor=[UIColor whiteColor];
         [_viewForYNAlertUI addSubview:lblShare];
         
         if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
         {
             _btnYes=[[UIButton alloc]initWithFrame:CGRectMake(_viewForYNAlertUI.frame.size.width/2-60,CGRectGetMaxY(lblShare.frame)+50,120,40)];
             _btnNo=[[UIButton alloc]initWithFrame:CGRectMake(_viewForYNAlertUI.frame.size.width/2-60,CGRectGetMaxY(lblShare.frame)+50,120,40)];
         }
         else
         {
             _btnYes=[[UIButton alloc]initWithFrame:CGRectMake(40,CGRectGetMaxY(lblShare.frame)+50,screenWidth/2-80,45)];
             _btnNo=[[UIButton alloc]initWithFrame:CGRectMake(_viewForYNAlertUI.frame.size.width/2+40,CGRectGetMaxY(lblShare.frame)+50,screenWidth/2-80,45)];
         }
         [_btnYes setTitle:@"Yes" forState:UIControlStateNormal];
         [_btnNo setTitle:@"No" forState:UIControlStateNormal];
         
         [_btnYes setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         [_btnNo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         
         _btnYes.layer.borderWidth=2.0;
         _btnNo.layer.borderWidth=2.0;
         
         _btnYes.layer.borderColor=[[UIColor orangeColor]CGColor];
         _btnNo.layer.borderColor=[[UIColor orangeColor]CGColor];
         
         _btnYes.layer.cornerRadius=4.0;
         _btnNo.layer.cornerRadius=4.0;
         
         [_btnYes addTarget:self action:@selector(viewForButtonYesRemoveSnapPressed) forControlEvents:UIControlEventTouchUpInside];
         [_btnNo addTarget:self action:@selector(closeYesNoButonPressed) forControlEvents:UIControlEventTouchUpInside];
         
         [_viewForYNAlertUI addSubview:_btnYes];
         [_viewForYNAlertUI addSubview:_btnNo];
         
     }];
}
-(void)viewForButtonYesRemoveSnapPressed
{
    [self closeYesNoButonPressed];
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isVoteDone=NO;
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *strBaseURL=base_URL;
        
        NSString *urlString=[[NSString alloc]init];
        
        if (_isActBPresed)
        {
            urlString=[NSString stringWithFormat:@"wssnapremove.php?sid=%@",strSID];
        }
        else if (_isRespPresed)
        {
            urlString=[NSString stringWithFormat:@"wschallengeremove.php?uid=%@&sid=%@",strUID,strSID];
        }
        strBaseURL=[strBaseURL stringByAppendingString:urlString];
        NSURL *urlChallenger=[NSURL URLWithString:strBaseURL];
        
        request=[NSMutableURLRequest requestWithURL:[urlChallenger standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        theAppDelegate.alert.message=@"Check Internet connection";
        [theAppDelegate.alert show];
    }
    request = nil;
    
}
#pragma mark-Close Yes/No Alert View
-(void)closeYesNoButonPressed
{
    @try
    {
        for(UIView *subview in [_viewForYNAlertUI subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForVideo=_viewForYNAlertUI.frame;
        CGRect frameForViewForAlert=_viewForAlert.frame;
        frameForViewForVideo=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        frameForViewForAlert=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForYNAlertUI.frame=frameForViewForVideo;
            _viewForAlert.frame=frameForViewForAlert;
        } completion:^(BOOL finished){
            [_viewForYNAlertUI removeFromSuperview];
            [_viewForAlert removeFromSuperview];
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark:Close AlertView
-(void)closeAlertView
{
    @try
    {
        for(UIView *subview in [_viewForAlert subviews])
        {
            [subview removeFromSuperview];
        }
        CGRect frameForViewForAlert=_viewForAlert.frame;
        frameForViewForAlert=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        [UIView animateWithDuration:0.5f animations:^{
            
             _viewForAlert.frame=frameForViewForAlert;
        } completion:^(BOOL finished){
             [_viewForAlert removeFromSuperview];
            _isMenuCollapsed=NO;
            
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+250,screenWidth,screenHeight/2-30);
            }
            else
            {
                if (screenWidth<325)
                {
                    _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+50,screenWidth,screenHeight/2-30);
                }
                else
                {
                    _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+80,screenWidth,screenHeight/2-30);
                }
            }
            _arrTblImges=[[NSMutableArray alloc]initWithObjects:@"icon_ActiveBts",@"icon_ProfResponse",@"icon_ProfBetHist",nil];
            _arrTblTitles=[[NSMutableArray alloc]initWithObjects:@"Active Bets",@"My Responses",@"Bet History",nil];
             [self profileDetailsServerAPI];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}


#pragma mark: Viewfor Buy single Theme Package
-(void)viewForAddFriendUI
{
    [_viewForAddFrnd removeFromSuperview];
    _viewForAddFrnd =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForAddFrnd.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForAddFrnd.tag=20;
    [self.view addSubview:_viewForAddFrnd];
    CGRect frameForViewForHelp=_viewForAddFrnd.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForAddFrnd.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForAddFrnd.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UILabel *lblText=[[UILabel alloc]initWithFrame:CGRectMake(0,screenHeight/3,_viewForAddFrnd.frame.size.width,40)];
        lblText.text=_strFriendTitle;
        lblText.textAlignment=NSTextAlignmentCenter;
        lblText.textColor=[UIColor whiteColor];
        lblText.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:18];
        [_viewForAddFrnd addSubview:lblText];
        
         _btnCloseFrndPopUp=[[UIButton alloc]initWithFrame:CGRectMake(_viewForAddFrnd.frame.size.width/2-70,CGRectGetMaxY(lblText.frame)+50,140,40)];
        
        [_btnCloseFrndPopUp setTitle:@"Close" forState:UIControlStateNormal];
        
         [_btnCloseFrndPopUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
          _btnCloseFrndPopUp.layer.borderColor=[[UIColor orangeColor]CGColor];
        _btnCloseFrndPopUp.layer.borderWidth=2.0;
          _btnCloseFrndPopUp.layer.cornerRadius=5.0;
        
        [_btnCloseFrndPopUp addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
            [_viewForAddFrnd addSubview:_btnCloseFrndPopUp];
    }];
}

#pragma mark: Viewfor Play Video popUP UI
-(void)viewForVideoScreenPopUpUI
{
    [_viewForVideoScreen removeFromSuperview];
    _viewForVideoScreen =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForVideoScreen.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForVideoScreen.tag=20;
    [self.view addSubview:_viewForVideoScreen];
    CGRect frameForViewForHelp=_viewForVideoScreen.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForVideoScreen.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForVideoScreen.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        _viewForVideoArea=[[UIView alloc]init];
        UILabel *lbltitle=[[UILabel alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            lbltitle.frame=CGRectMake(0,[UIScreen mainScreen].bounds.size.height/100*20,_viewForVideoScreen.frame.size.width,30);
            _viewForVideoArea.frame=CGRectMake(60,[UIScreen mainScreen].bounds.size.height/100*25,_viewForVideoScreen.frame.size.width-120,[UIScreen mainScreen].bounds.size.height/100*50);
        }
        else
        {
            lbltitle.frame=CGRectMake(0,[UIScreen mainScreen].bounds.size.height/100*20,_viewForVideoScreen.frame.size.width,30);
            _viewForVideoArea.frame=CGRectMake(10,[UIScreen mainScreen].bounds.size.height/100*25,_viewForVideoScreen.frame.size.width-20,[UIScreen mainScreen].bounds.size.height/100*50);
        }
        
        lbltitle.text=_strVideotitle;
        lbltitle.textAlignment=NSTextAlignmentCenter;
        lbltitle.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:18];
        lbltitle.textColor=[UIColor whiteColor];
        [_viewForVideoScreen addSubview:lbltitle];
        
        _viewForVideoArea.backgroundColor=[UIColor colorWithRed:38/255.0 green:38/255.0 blue:38/255.0 alpha:1];
        _viewForVideoArea.layer.borderWidth=2.0;
        _viewForVideoArea.layer.borderColor=[[UIColor whiteColor]CGColor];
        [_viewForVideoScreen addSubview:_viewForVideoArea];
        _asySelectedBG=[[AsyncImageView alloc]initWithFrame:CGRectMake(0,0,_viewForVideoArea.frame.size.width,_viewForVideoArea.frame.size.height)];
        _asySelectedBG.backgroundColor = [UIColor clearColor];
        NSString *imgBaseURL=ImgBase_URL;
       
        imgBaseURL=[imgBaseURL stringByAppendingString:_strBetImage];
        
        NSString *webStr = [NSString stringWithFormat:@"%@",imgBaseURL];
        NSURL *imageUrl = [[NSURL alloc] initWithString:[webStr stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
        [_asySelectedBG setContentMode:UIViewContentModeScaleAspectFill];
        [_asySelectedBG setClipsToBounds:YES];
        [_asySelectedBG loadImageFromURL:imageUrl];
        [_asySelectedBG sizeToFit];
        [_viewForVideoArea addSubview:_asySelectedBG];
        
        if([_strVideoName isEqualToString:@"No"]||[_strVideoName isEqualToString:@"(null)"])
        {
            
        }
        else
        {
            UIButton *btnPlay=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoArea.frame.size.width/2-25,_viewForVideoArea.frame.size.height/2-22,50,50)];
            [btnPlay setBackgroundImage:[UIImage imageNamed:@"img_AppIcon"] forState:UIControlStateNormal];
            [btnPlay addTarget:self action:@selector(playVideo) forControlEvents:UIControlEventTouchUpInside];
            [_viewForVideoArea addSubview:btnPlay];
        }
        
        _btnCloseVideoScreen=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoScreen.frame.size.width-40,20,30,30)];
        [_btnCloseVideoScreen setBackgroundImage:[UIImage imageNamed:@"icon_Cross"] forState:UIControlStateNormal];
        [_btnCloseVideoScreen addTarget:self action:@selector(ViewForCloseVideoUI) forControlEvents:UIControlEventTouchUpInside];
        [_viewForVideoScreen addSubview:_btnCloseVideoScreen];
    }];
}

#pragma mark: Play selected challage video
-(void)playVideo
{
    [_playerViewController.view removeFromSuperview];
    NSString *strVBase=vBase_URL;
    NSString *strVideoName=_strVideoName;
    strVBase=[strVBase stringByAppendingString:strVideoName];
    
    NSURL *vURL=[NSURL URLWithString:strVBase];
    _playerViewController = [[AVPlayerViewController alloc] init];
    _playerViewController.player = [AVPlayer playerWithURL:vURL];
    _playerViewController.view.frame =CGRectMake(0,0,_viewForVideoArea.frame.size.width,_viewForVideoArea.frame.size.height);
    //[_playerViewController setVideoGravity:AVLayerVideoGravityResizeAspect];
     [_playerViewController.player play];
    _playerViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:_playerViewController animated:YES completion:nil];    
   
    //[_viewForVideoArea addSubview:_playerViewController.view];
}

/*--------------     Bottom TableView delegate and datasource declaration here     -----------*/
#pragma mark- TableView DataSource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_arrTblTitles count];
}

//--------------------- cellforrow At Index TableView -----------------------//

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@""];
    if (cell == nil)
    {
        //  cell.transform= CGAffineTransformMakeRotation(M_PI/2);
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    
    tableView.separatorColor =UIColor.clearColor;
    cell.backgroundColor= [UIColor clearColor];
    [self.tblProfileDetails setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;

    NSString *strTitleString=[_arrTblTitles objectAtIndex:indexPath.row];
    UIView *viewForCell=[[UIView alloc]init];
    [cell.contentView addSubview:viewForCell];
    
    UIImageView *imgCell=[[UIImageView alloc]initWithFrame:CGRectMake(5,5,30,30)];
    imgCell.image=[UIImage imageNamed:[_arrTblImges objectAtIndex:indexPath.row]];
    [viewForCell addSubview:imgCell];
    
    UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imgCell.frame)+30,5,110,30)];
    
    lblTitle.textColor=[UIColor blackColor];
    lblTitle.font=[UIFont fontWithName:@"Avenir-Light" size:16];
    [viewForCell addSubview:lblTitle];
    
    if ([strTitleString isEqualToString:@"Active Bets"]||[strTitleString isEqualToString:@"My Responses"]||[strTitleString isEqualToString:@"Bet History"])
    {
        viewForCell.frame=CGRectMake(0,0,screenWidth,40);
        viewForCell.backgroundColor=[[UIColor whiteColor]colorWithAlphaComponent:.8];
        UIImageView *imgForward=[[UIImageView alloc]initWithFrame:CGRectMake(viewForCell.frame.size.width-30,10,20,20)];
        //imgForward.image=[UIImage imageNamed:@"icon_BlckFrwd"];
        [viewForCell addSubview:imgForward];
        lblTitle.text=[NSString stringWithFormat:@"%@",[_arrTblTitles objectAtIndex:indexPath.row]];
        
        UIImageView *imgBdge=[[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(lblTitle.frame)+30,5,45,30)];
        imgBdge.backgroundColor=[UIColor orangeColor];
        imgBdge.layer.cornerRadius=10.0;
        [viewForCell addSubview:imgBdge];
        
        UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(0,0,45,30)];
        
        NSInteger count;
        if (indexPath.row==0)
        {
            count=[_arrActivebet count];
        }
        else if (indexPath.row ==1)
        {
            count=[_arrResponse count];
        }
        else
        {
            if(_isMenuCollapsed)
            {
                NSLog(@"%ld",(long)indexPath.row);
                
                if(indexPath.row==[_arrActivebet count]+1)
                {
                    count=[_arrResponse count];
                }
                else
                {
                    count=[_arrBethistory count];
                }
            }
            else
            {
                count=[_arrBethistory count];
            }
        }
        
        lbl.text=[NSString stringWithFormat:@"%ld",(long)count];
        
        lbl.textColor=[UIColor whiteColor];
        lbl.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
        lbl.textAlignment=NSTextAlignmentCenter;
        [imgBdge addSubview:lbl];
    }
    else
    {
        UIView *viewFirstBG=[[UIView alloc]init];
        UIView *viewForCell1=[[UIView alloc]init];
        UIImageView *imgTemp=[[UIImageView alloc]init];
        UILabel *lblVideoName=[[UILabel alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            viewForCell.frame=CGRectMake(-2,0,screenWidth+4,150);
            viewFirstBG.frame=CGRectMake(0,0,screenWidth/3,150);
            viewForCell1.frame=CGRectMake(CGRectGetMaxX(viewFirstBG.frame),0,screenWidth-100,150);
            imgTemp.frame=CGRectMake(0,viewForCell1.frame.size.height-35,screenWidth-viewFirstBG.frame.size.width*1.5,35);
            _btnVote=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imgTemp.frame),viewForCell1.frame.size.height-35,150,35)];
            lblVideoName.frame=CGRectMake(0,viewFirstBG.frame.size.height-35,viewFirstBG.frame.size.width,35);
        }
        else
        {
            viewForCell.frame=CGRectMake(-2,0,screenWidth+4,100);
            viewFirstBG.frame=CGRectMake(0,0,screenWidth/3,100);
            viewForCell1.frame=CGRectMake(CGRectGetMaxX(viewFirstBG.frame),0,screenWidth-100,100);
            imgTemp.frame=CGRectMake(0,viewForCell1.frame.size.height-30,viewForCell1.frame.size.width/2+23,30);
            _btnVote=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imgTemp.frame),viewForCell1.frame.size.height-30,100,30)];
            lblVideoName.frame=CGRectMake(0,viewFirstBG.frame.size.height-25,viewFirstBG.frame.size.width,25);
        }
        
        viewForCell.backgroundColor=[UIColor colorWithRed:243.0/255.0 green:144.0/255.0 blue:0/255.0 alpha:1];
        viewForCell.layer.borderWidth=1.0;
        viewForCell.layer.borderColor=[[UIColor whiteColor]CGColor];
    
        
        viewFirstBG.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.7];
        viewFirstBG.layer.borderWidth=1.0;
        viewFirstBG.layer.borderColor=[[UIColor whiteColor]CGColor];
        
        [viewForCell addSubview:viewFirstBG];
        
        NSString *strBURL=ImgBase_URL;
        
          strBURL=[strBURL stringByAppendingString:[[_arrDataForTableDisplay objectAtIndex:indexPath.row]valueForKey:@"image"]];
        
        NSString *webStr1 = [NSString stringWithFormat:@"%@",strBURL];
        webStr1=[webStr1 stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *imageUrl1 = [[NSURL alloc] initWithString:webStr1];
        
        _asyImgTumbnil=[[AsyncImageView alloc]init];
        _asyImgTumbnil.frame=CGRectMake(0,0,viewFirstBG.frame.size.width,viewFirstBG.frame.size.height);
        [_asyImgTumbnil loadImageFromURL:imageUrl1];
        [_asyImgTumbnil setContentMode:UIViewContentModeScaleAspectFill];
        [_asyImgTumbnil sizeToFit];
        _asyImgTumbnil.clipsToBounds = YES;
        [viewFirstBG bringSubviewToFront:_asyImgTumbnil];
        [viewFirstBG addSubview:_asyImgTumbnil];
        
        _btnPlay1=[[UIButton alloc]initWithFrame:CGRectMake(viewFirstBG.frame.size.width/2-25,viewFirstBG.frame.size.height/2-35,50,50)];
        [_btnPlay1 setBackgroundImage:[UIImage imageNamed:@"img_AppIcon"] forState:UIControlStateNormal];
        [_btnPlay1 addTarget:self action:@selector(btnPlay1Pressed:) forControlEvents:UIControlEventTouchUpInside];
        _btnPlay1.tag=indexPath.row;
        [viewFirstBG addSubview:_btnPlay1];
        
        NSLog(@"%ld",(long)indexPath.row);
    
        NSString *strVideoBy=[[_arrDataForTableDisplay objectAtIndex:indexPath.row]valueForKey:@"name"];
        
        lblVideoName.text=strVideoBy;
        lblVideoName.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:14];
        lblVideoName.textColor=[UIColor whiteColor];
        lblVideoName.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7];
        lblVideoName.textAlignment=NSTextAlignmentCenter;
        [viewFirstBG addSubview:lblVideoName];
        
        viewForCell1.backgroundColor=[UIColor colorWithRed:243.0/255.0 green:144.0/255.0 blue:0/255.0 alpha:1];
        viewForCell1.layer.borderWidth=1.0;
        viewForCell1.layer.borderColor=[[UIColor whiteColor]CGColor];
        [viewForCell addSubview:viewForCell1];
        
        UILabel *lblDisChallenge=[[UILabel alloc]initWithFrame:CGRectMake(5,5,viewForCell1.frame.size.width-27,0)];
        NSString *strDetails=[[_arrDataForTableDisplay objectAtIndex:indexPath.row]valueForKey:@"details"];
        lblDisChallenge.text=strDetails;
        lblDisChallenge.textColor=[UIColor blackColor];
        lblDisChallenge.font=[UIFont fontWithName:@"Avenir-Medium" size:12];
        [lblDisChallenge setNumberOfLines:3];
        [lblDisChallenge sizeToFit];
        [viewForCell1 addSubview:lblDisChallenge];
        
        imgTemp.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7];
        
        
        UILabel *lblVotes=[[UILabel alloc]initWithFrame:CGRectMake(0,0,imgTemp.frame.size.width,30)];
        NSString *strVotes=@"Votes ";
        @try {
            strVotes=[strVotes stringByAppendingString:[[_arrDataForTableDisplay objectAtIndex:indexPath.row]valueForKey:@"votes"]];
        } @catch (NSException *exception){
            
        } @finally {
            
        }
        lblVotes.text=strVotes;
        lblVotes.textColor=[UIColor whiteColor];
        lblVotes.textAlignment=NSTextAlignmentCenter;
        lblVotes.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
        [imgTemp addSubview:lblVotes];
        [viewForCell1 addSubview:imgTemp];
        
        if (theAppDelegate.isProfFromMenu||[_strUserID isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:0]])
        {
             [_btnVote setTitle:@"Remove" forState:UIControlStateNormal];
        }
        else
        {
           [_btnVote setTitle:@"Vote" forState:UIControlStateNormal];
        }
       
        [_btnVote setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnVote.titleLabel.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
        _btnVote.backgroundColor=[[UIColor colorWithRed:166/255.0 green:87/255.0 blue:19/255.0 alpha:1]colorWithAlphaComponent:0.7];
        [_btnVote addTarget:self action:@selector(btnVotePressed:) forControlEvents:UIControlEventTouchUpInside];
        _btnVote.tag=indexPath.row;
        [viewForCell1 addSubview:_btnVote];

    }

    return cell;
}

#pragma Mark-delegate methods
//--------------------- Tableview didselected index ---------------------//
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _strCollapsedTitle=[[NSString alloc]init];
    _strTblSectionTitle=[_arrTblTitles objectAtIndex:indexPath.row];
    _strCollapsedTitle=[_arrTblTitles objectAtIndex:indexPath.row];
    if ([_strTblSectionTitle isEqualToString:@"Active Bets"]) {
        
        if (_isMenuCollapsed)
        {
            _isMenuCollapsed=NO;
            _arrTblImges=[[NSMutableArray alloc]initWithObjects:@"icon_ActiveBts",@"icon_ProfResponse",@"icon_ProfBetHist",nil];
            _arrTblTitles=[[NSMutableArray alloc]initWithObjects:@"Active Bets",@"My Responses",@"Bet History",nil];
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+250,screenWidth,screenHeight/2-30);
            }
            else
            {
                if (screenWidth<325)
                {
                    _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+50,screenWidth,screenHeight/2-30);
                }
                else
                {
                    _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+80,screenWidth,screenHeight/2-30);
                }
            }
            [_tblProfileDetails reloadData];
        }
        else
        {
            _isMenuCollapsed=YES;
            _isActiveBet=YES;
            _isBitHistory=NO;
            _isResponses=NO;
            
            _arrTblImges=[[NSMutableArray alloc]init];
            _arrTblTitles=[[NSMutableArray alloc]init];
            _arrDataForTableDisplay=[[NSMutableArray alloc]init];
            
            [_arrTblImges addObject:@"icon_ActiveBts"];
            [_arrTblTitles addObject:@"Active Bets"];
            
            for (int i=0;i<[_arrActivebet count];i++)
            {
                [_arrTblImges addObject:@""];
            }
            
            for (int j=0;j<[_arrActivebet count];j++)
            {
                [_arrTblTitles addObject:@""];
            }
            
            [_arrTblImges addObject:@"icon_ProfResponse"];
            [_arrTblImges addObject:@"icon_ProfBetHist"];
            
            [_arrTblTitles addObject:@"My Responses"];
            [_arrTblTitles addObject:@"Bet History"];
            
            [_arrDataForTableDisplay addObject:@""];
           
            for (int i=0;i<[_arrActivebet count];i++)
            {
                NSMutableArray *arr=[_arrActivebet objectAtIndex:i];
                [_arrDataForTableDisplay addObject:arr];
            }
            [_arrDataForTableDisplay addObject:@""];
             [_arrDataForTableDisplay addObject:@""];
            _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame),screenWidth,screenHeight/2);
            [_tblProfileDetails reloadData];
        }
    }
    else if ([_strTblSectionTitle isEqualToString:@"My Responses"])
    {
        if (_isMenuCollapsed)
        {
            _isMenuCollapsed=NO;
            _arrTblImges=[[NSMutableArray alloc]initWithObjects:@"icon_ActiveBts",@"icon_ProfResponse",@"icon_ProfBetHist",nil];
            _arrTblTitles=[[NSMutableArray alloc]initWithObjects:@"Active Bets",@"My Responses",@"Bet History",nil];
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+250,screenWidth,screenHeight/2-30);
            }
            else
            {
                if (screenWidth<325)
                {
                    _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+50,screenWidth,screenHeight/2-30);
                }
                else
                {
                    _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+80,screenWidth,screenHeight/2-30);
                }
            }

            [_tblProfileDetails reloadData];
        }
        else
        {
            _isMenuCollapsed=YES;
            _isResponses=YES;
            _isBitHistory=NO;
            _isActiveBet=NO;
            
            _arrTblImges=[[NSMutableArray alloc]init];
            _arrTblTitles=[[NSMutableArray alloc]init];
            _arrDataForTableDisplay=[[NSMutableArray alloc]init];
            
            [_arrTblImges addObject:@"icon_ActiveBts"];
            [_arrTblImges addObject:@"icon_ProfResponse"];
            
            [_arrTblTitles addObject:@"Active Bets"];
            [_arrTblTitles addObject:@"My Responses"];
            
            for (int i=0;i<[_arrResponse count];i++)
            {
                [_arrTblImges addObject:@""];
            }
            
            for (int j=0;j<[_arrResponse count];j++)
            {
                [_arrTblTitles addObject:@""];
            }
            
            [_arrTblImges addObject:@"icon_ProfBetHist"];
            [_arrTblTitles addObject:@"Bet History"];
            
            [_arrDataForTableDisplay addObject:@""];
            [_arrDataForTableDisplay addObject:@""];
            for (int i=0;i<[_arrResponse count];i++)
            {
                NSMutableArray *arr=[_arrResponse objectAtIndex:i];
                [_arrDataForTableDisplay addObject:arr];
            }
            [_arrDataForTableDisplay addObject:@""];
            _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame),screenWidth,screenHeight/2);
            [_tblProfileDetails reloadData];
            [_tblProfileDetails reloadData];
            
        }
    }
    else if ([_strTblSectionTitle isEqualToString:@"Bet History"])
    {
        if (_isMenuCollapsed)
        {
            _isMenuCollapsed=NO;
            _arrTblImges=[[NSMutableArray alloc]initWithObjects:@"icon_ActiveBts",@"icon_ProfResponse",@"icon_ProfBetHist",nil];
            _arrTblTitles=[[NSMutableArray alloc]initWithObjects:@"Active Bets",@"My Responses",@"Bet History",nil];
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+250,screenWidth,screenHeight/2-30);
            }
            else
            {
                if (screenWidth<325)
                {
                    _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+50,screenWidth,screenHeight/2-30);
                }
                else
                {
                    _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+80,screenWidth,screenHeight/2-30);
                }
            }

            [_tblProfileDetails reloadData];
        }
        else
        {
            _isMenuCollapsed=YES;
            _isBitHistory=YES;
            _isActiveBet=NO;
            _isResponses=NO;
            
            _arrTblImges=[[NSMutableArray alloc]init];
            _arrTblTitles=[[NSMutableArray alloc]init];
            _arrDataForTableDisplay=[[NSMutableArray alloc]init];
            
            [_arrTblImges addObject:@"icon_ActiveBts"];
            [_arrTblImges addObject:@"icon_ProfResponse"];
            [_arrTblImges addObject:@"icon_ProfBetHist"];
            
            [_arrTblTitles addObject:@"Active Bets"];
            [_arrTblTitles addObject:@"My Responses"];
            [_arrTblTitles addObject:@"Bet History"];
            
            for (int i=0;i<[_arrBethistory count];i++)
            {
                [_arrTblImges addObject:@""];
            }
            
            for (int j=0;j<[_arrBethistory count];j++)
            {
                [_arrTblTitles addObject:@""];
            }
            [_arrDataForTableDisplay addObject:@""];
            [_arrDataForTableDisplay addObject:@""];
            [_arrDataForTableDisplay addObject:@""];
            for (int i=0;i<[_arrBethistory count];i++)
            {
                NSMutableArray *arr=[_arrBethistory objectAtIndex:i];
                [_arrDataForTableDisplay addObject:arr];
            }
            
            if([_arrBethistory count]>0)
            {
                _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame),screenWidth,screenHeight/2);
                [_tblProfileDetails reloadData];
            }
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strTitleString=[_arrTblTitles objectAtIndex:indexPath.row];

    if (_isMenuCollapsed)
    {
        if ([strTitleString isEqualToString:@"Active Bets"])
        {
            return 40;
        }
        else if ([strTitleString isEqualToString:@"My Responses"])
        {
            return 40;
        }
        else if ([strTitleString isEqualToString:@"Bet History"])
        {
            return 40;
        }
        else
        {
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                return 150;
            }
            else
            {
                return 100;
            }
        }
    }
    else
    {
        return 50;
    }
    
}

#pragma mark: Add Friend API 
-(void)addFriendServerAPI
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isAddFrndAPI=YES;
        _isProfileAPI=NO;
        
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        
        NSString *strBaseURL=base_URL;
        NSString *urlString;
            urlString=[NSString stringWithFormat:@"wsaddfriend.php?senderid=%@&receiverid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],[_arrProfileDetails valueForKey:@"id"]];
        
        strBaseURL=[strBaseURL stringByAppendingString:urlString];
        NSURL *urlProfile=[NSURL URLWithString:strBaseURL];
        request=[NSMutableURLRequest requestWithURL:[urlProfile standardizedURL]];
    }
    if(!connection)
        connection= [[WebConnection1 alloc] init];
    connection.delegate = self;
    [connection makeConnection:request];
    request = nil;

}

#pragma mark: Check already friend or Not
-(void)checkAlreadyFriendOrNot
{
    if([[theAppDelegate.arrUserLoginData objectAtIndex:0]isEqualToString:_strUserID])
    {
        _btnAddProfile.hidden=YES;
    }
    else
    {
        NSMutableURLRequest *request;
        if (theAppDelegate.isConnected)
        {
            _isAddFrndAPI=NO;
            _isProfileAPI=NO;
            _isAlreadyFrndOrNot=YES;
            _isRespPresed=NO;
            _isActBPresed=NO;
        
            [theAppDelegate showSpinnerInView:self.view];
            [self.view setUserInteractionEnabled:NO];
        
            NSString *strBaseURL=base_URL;
            NSString *urlString;
        
            urlString=[NSString stringWithFormat:@"wscheckfriend.php?uid=%@&checkuid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],_strUserID];
            
            strBaseURL=[strBaseURL stringByAppendingString:urlString];
            NSURL *urlProfile=[NSURL URLWithString:strBaseURL];
            request=[NSMutableURLRequest requestWithURL:[urlProfile standardizedURL]];
        }
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
        request = nil;
    }
}

#pragma mark: User SignIn Server Call
-(void)profileDetailsServerAPI
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isAddFrndAPI=NO;
        _isRespPresed=NO;
        _isActBPresed=NO;
        _isProfileAPI=YES;
        _isVoteDone=NO;
         [theAppDelegate showSpinnerInView:self.view];
         [self.view setUserInteractionEnabled:NO];
    
         NSString *strBaseURL=base_URL;
        NSString *urlString;
        if (theAppDelegate.isProfFromMenu)
        {
            urlString=[NSString stringWithFormat:@"wsviewprofile.php?id=%@",[theAppDelegate.arrUserLoginData  objectAtIndex:0]];
        }
        else
        {
            urlString=[NSString stringWithFormat:@"wsviewprofile.php?id=%@",_strUserID];
        }
         strBaseURL=[strBaseURL stringByAppendingString:urlString];
         NSURL *urlProfile=[NSURL URLWithString:strBaseURL];
         request=[NSMutableURLRequest requestWithURL:[urlProfile standardizedURL]];
     }
    if(!connection)
        connection= [[WebConnection1 alloc] init];
    connection.delegate = self;
    [connection makeConnection:request];
    request = nil;
}

#pragma mark: view For AlertUI
-(void)viewForAlertUI
{
    [_viewForAlert removeFromSuperview];
    _viewForAlert =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForAlert.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForAlert.tag=20;
    [self.view addSubview:_viewForAlert];
    CGRect frameForViewForHelp=_viewForAlert.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForAlert.frame=frameForViewForHelp;
    } completion:^(BOOL finished)
     {
         vscreenSize=_viewForAlert.frame;
         vscreenHeight=vscreenSize.size.height;
         vscreenWidth=vscreenSize.size.width;
         
         UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
         img.image=[UIImage imageNamed:@"icon_Share"];
         [_viewForAlert addSubview:img];
         UILabel *lblShare=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForAlert.frame.size.width,30)];
         lblShare.text=_strAlertTitle;
         lblShare.textAlignment=NSTextAlignmentCenter;
         lblShare.textColor=[UIColor whiteColor];
         lblShare.font=[UIFont fontWithName:@"Avenir-Roman" size:16];
         [_viewForAlert addSubview:lblShare];
         
         _btnAlertClose=[[UIButton alloc]init];
         if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
         {
             _btnAlertClose.frame=CGRectMake(_viewForAlert.frame.size.width/2-60,CGRectGetMaxY(lblShare.frame)+50,120,40);
         }
         else
         {
             _btnAlertClose=[[UIButton alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,CGRectGetMaxY(lblShare.frame)+50,100,40)];
         }
         [_btnAlertClose setTitle:@"Close" forState:UIControlStateNormal];
         [_btnAlertClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         _btnAlertClose.layer.borderWidth=2.0;
         _btnAlertClose.layer.borderColor=[[UIColor orangeColor]CGColor];
         _btnAlertClose.layer.cornerRadius=4.0;
         [_btnAlertClose addTarget:self action:@selector(btnCloseAletUIPressed:) forControlEvents:UIControlEventTouchUpInside];
         [_viewForAlert addSubview:_btnAlertClose];
     }];
}

#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    @try
    {
        [self.view setUserInteractionEnabled:YES];
        [theAppDelegate stopSpinner];
        dict =[[NSMutableDictionary alloc]initWithDictionary:response];
        NSLog(@"%@",dict);
        NSString *strMsg=[dict valueForKey:@"message"];
        if ([strMsg isEqualToString:@"sus"]||[strMsg isEqualToString:@"Yes"])
        {
            if (_isProfileAPI)
            {
                _arrBethistory=[[NSMutableArray alloc]init];
                _arrProfileDetails=[[NSMutableArray alloc]init];
                _arrBethistory=[[NSMutableArray alloc]init];
                _arrActivebet=[[NSMutableArray alloc]init];
                
                _arrProfileDetails=[dict valueForKey:@"info"];
                _arrActivebet=[dict valueForKey:@"activebet"];
                _arrBethistory=[dict valueForKey:@"bethistory"];
                _arrResponse=[dict valueForKey:@"responses"];
                totalResponses=[dict valueForKey:@"totalresponses"];
                _lblResponse.text=[NSString stringWithFormat:@"%@",totalResponses];
        
                [self viewForUpdateData];
                [_tblProfileDetails reloadData];
                [self checkAlreadyFriendOrNot];
            }
            else if (_isAddFrndAPI)
            {
                _btnAddProfile.hidden=YES;
                _strFriendTitle=[_arrProfileDetails valueForKey:@"username"];
                NSArray *arr=[_strFriendTitle componentsSeparatedByString:@" "];
                _strFriendTitle=[arr objectAtIndex:0];
                _strFriendTitle=[_strFriendTitle stringByAppendingString:@" has been added as a friend!"];
                [self performSelector:@selector(viewForAddFriendUI) withObject:nil afterDelay:0.2];
            }
            else if (_isRespPresed)
            {
                _strAlertTitle=@"Bet has been removed successfully!";
                [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:0.5];
            }
            else if(_isActBPresed)
            {
                _strAlertTitle=@"Bet has been removed successfully!";
                [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:0.5];
            }
            else if(_isAlreadyFrndOrNot)
            {
                NSString *strStatus=[dict valueForKey:@"status"];
                NSLog(@"%@",strStatus);
                
                NSString *strMsg=[dict valueForKey:@"message"];
                if ([strMsg isEqualToString:@"Yes"]) {
                    _btnAddProfile.hidden=YES;
                }
                else
                {
                    _btnAddProfile.hidden=NO;
                    if (theAppDelegate.isProfFromMenu) {
                        _btnAddProfile.hidden=YES;
                    }

                }
            }
        else if (_isVoteDone)
        {
            NSArray *arrVotes=[dict valueForKey:@"info"];
            NSString *strVote=[arrVotes valueForKey:@"challenger_vote_id"];
            if (strVote.length==0) {
                strVote=[arrVotes valueForKey:@"snap_vote_id"];
            }
            if ([strVote isEqualToString:@"allready"])
            {
                _strVoteAlertTitle=@"Vote has already been placed!";
                [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
            }
            else
            {
                _strVoteAlertTitle=@"Vote has been placed!";
                [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
                
            }
        }
        }
        else
        {
            if(_isAddFrndAPI)
            {
                _strFriendTitle=[_arrProfileDetails valueForKey:@"username"];
                NSArray *arr=[_strFriendTitle componentsSeparatedByString:@" "];
                _strFriendTitle=[arr objectAtIndex:0];
            
                _strFriendTitle=[_strFriendTitle stringByAppendingString:@" already added as a friend!"];
                [self performSelector:@selector(viewForAddFriendUI) withObject:nil afterDelay:0.2];
            }
            else if(_isAlreadyFrndOrNot)
            {
                if ([strMsg isEqualToString:@"No"]) {
                    _btnAddProfile.hidden=NO;
                    if (theAppDelegate.isProfFromMenu) {
                        _btnAddProfile.hidden=YES;
                    }

                }
            }
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}


#pragma mark: view For VoteUI
-(void)viewForVoteUI
{
    [_viewForVote removeFromSuperview];
    _viewForVote =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForVote.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForVote.tag=20;
    [self.view addSubview:_viewForVote];
    CGRect frameForViewForHelp=_viewForVote.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForVote.frame=frameForViewForHelp;
    } completion:^(BOOL finished)
     {
         vscreenSize=_viewForVote.frame;
         vscreenHeight=vscreenSize.size.height;
         vscreenWidth=vscreenSize.size.width;
         
         UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForVote.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
         img.image=[UIImage imageNamed:@"icon_Share"];
         [_viewForVote addSubview:img];
         UILabel *lblShare=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForVote.frame.size.width,30)];
         lblShare.text=_strVoteAlertTitle;
         lblShare.textAlignment=NSTextAlignmentCenter;
         lblShare.textColor=[UIColor whiteColor];
         
         if([_strVoteAlertTitle isEqualToString:@"Video should be 15 seconds or less"]||[_strVoteAlertTitle isEqualToString:@"Snapbet uploaded successfully!"])
         {
             lblShare.font=[UIFont fontWithName:@"Avenir-Roman" size:18];
         }
         else if ([_strVoteAlertTitle isEqualToString:@"Bet has been removed successfully!"]||[_strVoteAlertTitle isEqualToString:@"Response submitted successfully"])
         {
             lblShare.font=[UIFont fontWithName:@"Avenir-Roman" size:16];
         }
         else
         {
             lblShare.font=[UIFont fontWithName:@"Avenir-Roman" size:23];
         }
         
         [_viewForVote addSubview:lblShare];
         
         _btnVoteClose=[[UIButton alloc]init];
         if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
         {
             _btnVoteClose.frame=CGRectMake(_viewForVote.frame.size.width/2-60,CGRectGetMaxY(lblShare.frame)+50,120,40);
         }
         else
         {
             _btnVoteClose=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVote.frame.size.width/2-50,CGRectGetMaxY(lblShare.frame)+50,100,40)];
         }
         
         [_btnVoteClose setTitle:@"Close" forState:UIControlStateNormal];
         [_btnVoteClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         _btnVoteClose.layer.borderWidth=2.0;
         _btnVoteClose.layer.borderColor=[[UIColor orangeColor]CGColor];
         _btnVoteClose.layer.cornerRadius=4.0;
         [_btnVoteClose addTarget:self action:@selector(btnCloseVoteBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
         
         [_viewForVote addSubview:_btnVoteClose];
         
     }];
}
-(IBAction)btnCloseVoteBtnPressed:(id)sender
{
    [self closeVoteUIButonPressed];
   // [self profileDetailsServerAPI];
}
#pragma mark-Close Comment View
-(void)closeVoteUIButonPressed
{
    @try
    {
        for(UIView *subview in [_viewForVote subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForVideo=_viewForVote.frame;
        
        frameForViewForVideo=CGRectMake(30, screenHeight/2,screenWidth-60,0);
         [UIView animateWithDuration:0.5f animations:^{
            
            _viewForVote.frame=frameForViewForVideo;
            
        } completion:^(BOOL finished){
            [_viewForVote removeFromSuperview];
            _isMenuCollapsed=NO;
            
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+250,screenWidth,screenHeight/2-30);
            }
            else
            {
                if (screenWidth<325)
                {
                    _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+50,screenWidth,screenHeight/2-30);
                }
                else
                {
                    _tblProfileDetails.frame=CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+80,screenWidth,screenHeight/2-30);
                }
            }
            _arrTblImges=[[NSMutableArray alloc]initWithObjects:@"icon_ActiveBts",@"icon_ProfResponse",@"icon_ProfBetHist",nil];
            _arrTblTitles=[[NSMutableArray alloc]initWithObjects:@"Active Bets",@"My Responses",@"Bet History",nil];
            [self profileDetailsServerAPI];
            
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

@end
