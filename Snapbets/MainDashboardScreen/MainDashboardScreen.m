//
//  MainDashboardScreen.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 07/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "MainDashboardScreen.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <Social/Social.h>
#define kMaxIdleTimeSeconds 1.0


@interface MainDashboardScreen ()<FBSDKAppInviteDialogDelegate>
{
    NSTimer  *myidleTimer;
    NSString *neverShowAgain;
    NSUserDefaults *defaults;
    NSMutableArray *arrProductIDs;
    NSTimer *Ntimer;
}

#pragma mark - FB sharing property
@property(strong,nonatomic)IBOutlet FBSDKShareButton *shareButton,*shareButton1;
@property(strong,nonatomic)  FBSDKShareLinkContent *content;
@property(strong,nonatomic) FBSDKShareVideo *video;
@property (nonatomic)FBSDKShareVideoContent *vContent;
@end

@implementation MainDashboardScreen

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer
{
    [self incrimentUserDefaults];
}
#pragma mark: Incriment UserDefaults
-(void)incrimentUserDefaults
{
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    NSInteger loadCount=[userDefaults integerForKey:@"LoadCount"];
    loadCount++;
    [userDefaults setInteger:loadCount forKey:@"LoadCount"];
    [userDefaults synchronize];
}

#pragma mark: ViewDidLoad Method
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getDataFromDB];
    defaults=[NSUserDefaults standardUserDefaults];
    neverShowAgain=[defaults objectForKey:@"IsShowPopUp"];
    
    Ntimer = [NSTimer scheduledTimerWithTimeInterval: 0.5
                                              target: self
                                            selector: @selector(CheckUStatusBackgroundAPI)
                                            userInfo: nil
                                             repeats: YES];
    NSLog(@"%@",Ntimer);
    
    _arrMenuImages=[[NSMutableArray alloc]initWithObjects:@"icon_Search",@"icon_MSetting",@"icon_MRestore",@"icon_Upgrade",@"icon_MProfile",@"icon_MNotification",@"icon_MSnapbet",@"icon_Snap",@"icon_MClose",nil];
    _arrMenuTitles=[[NSMutableArray alloc]initWithObjects:@"Search",@"Settings",@"Restore",@"Upgrade",@"Profile",@"Notifications",@"Snapbets Themes",@"New Snapbet",@"",nil];
    
    _imgFeedBG=[[UIImageView alloc]init];
    _imgTrendingBG=[[UIImageView alloc]init];
    _imgMyBetBG=[[UIImageView alloc]init];
    _imgLeadersBG=[[UIImageView alloc]init];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        _imgFeedBG.frame=CGRectMake(screenWidth-100,40,100,25);
        _imgTrendingBG.frame=CGRectMake(screenWidth-100,CGRectGetMaxY(_imgFeedBG.frame)+20,100,25);
        _imgMyBetBG.frame=CGRectMake(screenWidth-100,CGRectGetMaxY(_imgTrendingBG.frame)+20,100,25);
        _imgLeadersBG.frame=CGRectMake(screenWidth-100,CGRectGetMaxY(_imgMyBetBG.frame)+20,100,25);
    }
    else
    {
        if(screenWidth<325)
        {
            _imgFeedBG.frame=CGRectMake(screenWidth-100,8,100,25);
            _imgTrendingBG.frame=CGRectMake(screenWidth-100,CGRectGetMaxY(_imgFeedBG.frame)+2,100,25);
            _imgMyBetBG.frame=CGRectMake(screenWidth-100,CGRectGetMaxY(_imgTrendingBG.frame)+2,100,25);
            _imgLeadersBG.frame=CGRectMake(screenWidth-100,CGRectGetMaxY(_imgMyBetBG.frame)-2,100,25);
        }
        else
        {
            CGFloat FYposition = _btnFeed.frame.origin.y;
            CGFloat TYposition = _btnTrending.frame.origin.y;
            CGFloat MYposition = _btnMyBets.frame.origin.y;
            CGFloat LYposition = _btnLeaders.frame.origin.y;
            
            //            _imgFeedBG.frame=CGRectMake(screenWidth-100,FYposition-1,100,25);
            //            _imgTrendingBG.frame=CGRectMake(screenWidth-100,CGRectGetMaxY(_imgFeedBG.frame)+8,100,25);
            //            _imgMyBetBG.frame=CGRectMake(screenWidth-100,CGRectGetMaxY(_imgTrendingBG.frame)+8,100,25);
            //            _imgLeadersBG.frame=CGRectMake(screenWidth-100,CGRectGetMaxY(_imgMyBetBG.frame)+6,100,25);
            
            _imgFeedBG.frame=CGRectMake(screenWidth-100,FYposition,100,27);
            _imgTrendingBG.frame=CGRectMake(screenWidth-100,TYposition,100,27);
            _imgMyBetBG.frame=CGRectMake(screenWidth-100,MYposition,100,27);
            _imgLeadersBG.frame=CGRectMake(screenWidth-100,LYposition,100,27);
            if (screenHeight==736 && screenWidth==414)
            {
                _imgFeedBG.frame=CGRectMake(screenWidth-100,FYposition+2,100,27);
                _imgTrendingBG.frame=CGRectMake(screenWidth-100,TYposition+7,100,27);
                _imgMyBetBG.frame=CGRectMake(screenWidth-100,MYposition+7,100,27);
                _imgLeadersBG.frame=CGRectMake(screenWidth-100,LYposition+7,100,27);
            }
        }
    }
    
    _imgFeedBG.image=[UIImage imageNamed:@"img_1BG"];
    _imgTrendingBG.image=[UIImage imageNamed:@"img_1BG"];
    _imgMyBetBG.image=[UIImage imageNamed:@"img_1BG"];
    _imgLeadersBG.image=[UIImage imageNamed:@"img_1BG"];
    
    _imgTrendingBG.hidden=YES;
    _imgMyBetBG.hidden=YES;
    _imgLeadersBG.hidden=YES;
    
    [self.view addSubview:_imgFeedBG];
    [self.view addSubview:_imgTrendingBG];
    [self.view addSubview:_imgMyBetBG];
    [self.view addSubview:_imgLeadersBG];
    
    [self.view bringSubviewToFront:_btnFeed];
    [self.view bringSubviewToFront:_btnTrending];
    [self.view bringSubviewToFront:_btnMyBets];
    [self.view bringSubviewToFront:_btnLeaders];
    
    [self loadInitialUI];
    
    _isFeedData=YES;_isTrendingData=NO;_isMyBetsData=NO;_isLeadersData=NO;
    [self MainDashboardDataAPI];
        // Do any additional setup after loading the view from its nib.
    [self updatePurchaseSubscriptionToServer];
}

- (void) enableIdleTimer
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

- (void)getLatestBets
{
    [self.view setUserInteractionEnabled:NO];
    theAppDelegate.isPoolRefresh=YES;
    _btnFeed.userInteractionEnabled=NO;
    _btnMyBets.userInteractionEnabled=NO;
    _btnTrending.userInteractionEnabled=NO;
    _btnLeaders.userInteractionEnabled=NO;
   
    [self MainDashboardDataAPI];
    theAppDelegate.isPoolRefresh=NO;
    
}

- (void)idleTimerExceeded
{
    NSLog(@"%@",myidleTimer);
    //[self performSelector:@selector (enableIdleTimer) withObject:nil afterDelay:3];
    [self viewForTutorialUI];
    // [self startScreenSaverOrSomethingInteresting];
    // [self resetIdleTimer];
}

//---------------------


-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewForTutorialUI
{
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    
    NSInteger loadCount=[userDefaults integerForKey:@"LoadCount"];
    if (loadCount<=0)
    {
        [_viewForUpgradeAccount removeFromSuperview];
        theAppDelegate.isStartFromAppDelegate=NO;
        [self showHelpView];
    }
    else
    {
        if (theAppDelegate.isStartFromAppDelegate)
        {
            if (neverShowAgain == nil)
            {
                [self viewForUpgradeAccountPopUpUI];
            }
        }
    }
    theAppDelegate.isStartFromAppDelegate=NO;
    loadCount++;
    [userDefaults setInteger:loadCount forKey:@"LoadCount"];
    [userDefaults synchronize];
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}

#pragma mark: LoadInitial UI
-(void)loadInitialUI
{
    [_btnProfile addTarget:self action:@selector(btnProfilePressed:) forControlEvents:UIControlEventTouchUpInside];
    [_btnSnapbetThemes addTarget:self action:@selector(btnSnapBetThemePressed:) forControlEvents:UIControlEventTouchUpInside];
    [_btnSnapbetThemes1 addTarget:self action:@selector(btnSnapBetThemePressed:) forControlEvents:UIControlEventTouchUpInside];
    [self viewForTableDeclarationsHere];
    
    //    // for zoom out
    //    [UIView animateWithDuration:0.5f animations:^{
    //
    //        self.btnMenu.transform = CGAffineTransformMakeScale(1, 1);
    //    }completion:^(BOOL finished){}];
    
    [_btnFeed addTarget:self action:@selector(btnFeedPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_btnMyBets addTarget:self action:@selector(btnMyBetsPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_btnTrending addTarget:self action:@selector(btnTrendingPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_btnLeaders addTarget:self action:@selector(btnLeadersPressed:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark:Load UITableView Declaratiosn here
-(void)viewForTableDeclarationsHere
{
    [_tblBets removeFromSuperview];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        _tblBets=[[UITableView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_imgHederBG.frame)-5,screenWidth,screenHeight-120)];
    }
    else
    {
        if (screenWidth<325)
        {
            _tblBets=[[UITableView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_imgHederBG.frame),screenWidth,screenHeight-_imgHederBG.frame.size.height)];
        }
        else
        {
            _tblBets=[[UITableView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_imgHederBG.frame),screenWidth,screenHeight-_imgHederBG.frame.size.height)];
        }
    }
    _tblBets.tableFooterView=[[UIView alloc]init];
    _tblBets.dataSource=self;
    _tblBets.delegate=self;
    _tblBets.backgroundColor=[UIColor clearColor];
    [self.view addSubview:_tblBets];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self
                       action:@selector(getLatestBets)
             forControlEvents:UIControlEventValueChanged];
     [_tblBets addSubview:refreshControl];
    
    [_btnMenu removeFromSuperview];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        _btnMenu=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-90,screenHeight-100,80,80)];
    }
    else
    {
        if (screenWidth<325)
        {
            _btnMenu=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-75,screenHeight-70,60,60)];
        }
        else
        {
            _btnMenu=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-80,screenHeight-80,70,70)];
        }
    }
    
    [_btnMenu setBackgroundImage:[UIImage imageNamed:@"icon_Menu"] forState:UIControlStateNormal];
    //    [_btnMenu addTarget:self action:@selector(viewForMenuUI) forControlEvents:UIControlEventTouchUpInside];
    [_btnMenu addTarget:self action:@selector(viewForMenuUI) forControlEvents:UIControlEventTouchUpInside];
    _btnMenu.layer.cornerRadius=_btnMenu.frame.size.height/2;
    _btnMenu.layer.shadowColor = [UIColor blackColor].CGColor;
    _btnMenu.layer.shadowOffset = CGSizeMake(5, 5);
    _btnMenu.layer.shadowRadius = 5;
    _btnMenu.layer.shadowOpacity = 0.5;
    [self.view addSubview:_btnMenu];
    
    _btnMenu.alpha = 0.0f;
    _btnMenu.transform = CGAffineTransformMakeScale(0.1,0.1);
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:1.0];
    _btnMenu.transform = CGAffineTransformMakeScale(1,1);
    _btnMenu.alpha = 1.0f;
    [UIView commitAnimations];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    [self.tblBets addGestureRecognizer:tapGestureRecognizer];
    
}

#pragma mark:Load UITableView Declaratiosn here
-(void)viewForLeadersTable
{
    [_tblBets removeFromSuperview];
    [_tblLeaders removeFromSuperview];
    _tblLeaders=[[UITableView alloc]init];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        _tblLeaders.frame=CGRectMake(0,CGRectGetMaxY(_imgHederBG.frame)-5,screenWidth,screenHeight-120);
    }
    else
    {
        if (screenWidth<325)
        {
            _tblLeaders.frame=CGRectMake(0,CGRectGetMaxY(_imgHederBG.frame),screenWidth,screenHeight-_imgHederBG.frame.size.height);
        }
        else
        {
            _tblLeaders.frame=CGRectMake(0,CGRectGetMaxY(_imgHederBG.frame),screenWidth,screenHeight-_imgHederBG.frame.size.height);
        }
    }
    _tblLeaders.tableFooterView=[[UIView alloc]init];
    _tblLeaders.dataSource=self;
    _tblLeaders.delegate=self;
    _tblLeaders.backgroundColor=[UIColor clearColor];
    [self.view addSubview:_tblLeaders];
    
    UIRefreshControl *refreshControl1 = [[UIRefreshControl alloc] init];
    refreshControl1.backgroundColor = [UIColor clearColor];
    refreshControl1.tintColor = [UIColor blackColor];
    [refreshControl1 addTarget:self
                       action:@selector(getLatestBets)
             forControlEvents:UIControlEventValueChanged];
    [_tblLeaders addSubview:refreshControl1];
    
    [_btnMenu removeFromSuperview];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        _btnMenu=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-90,screenHeight-100,80,80)];
    }
    else
    {
        if (screenWidth<325)
        {
            _btnMenu=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-75,screenHeight-70,60,60)];
        }
        else
        {
            _btnMenu=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-80,screenHeight-80,70,70)];
        }
    }
    
    [_btnMenu setBackgroundImage:[UIImage imageNamed:@"icon_Menu"] forState:UIControlStateNormal];
    //    [_btnMenu addTarget:self action:@selector(viewForMenuUI) forControlEvents:UIControlEventTouchUpInside];
    [_btnMenu addTarget:self action:@selector(viewForMenuUI) forControlEvents:UIControlEventTouchUpInside];
    _btnMenu.layer.cornerRadius=_btnMenu.frame.size.height/2;
    _btnMenu.layer.shadowColor = [UIColor blackColor].CGColor;
    _btnMenu.layer.shadowOffset = CGSizeMake(5, 5);
    _btnMenu.layer.shadowRadius = 5;
    _btnMenu.layer.shadowOpacity = 0.5;
    [self.view addSubview:_btnMenu];
    
    _btnMenu.alpha = 0.0f;
    _btnMenu.transform = CGAffineTransformMakeScale(0.1,0.1);
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:1.0];
    _btnMenu.transform = CGAffineTransformMakeScale(1,1);
    _btnMenu.alpha = 1.0f;
    [UIView commitAnimations];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    [self.tblLeaders addGestureRecognizer:tapGestureRecognizer];
    
}
#pragma mark: view For Share
-(void)viewForMenuUI
{
    [_viewForMenu removeFromSuperview];
    _viewForMenu =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForMenu.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForMenu.tag=20;
    [self.view addSubview:_viewForMenu];
    CGRect frameForViewForHelp=_viewForMenu.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForMenu.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForMenu.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _tblMenu=[[UITableView alloc]initWithFrame:CGRectMake(0,([UIScreen mainScreen].bounds.size.height/100*23),screenWidth,screenHeight)];
        }
        else
        {
            
            if (screenWidth<325) {
                _tblMenu=[[UITableView alloc]initWithFrame:CGRectMake(0,([UIScreen mainScreen].bounds.size.height/100*2),screenWidth,screenHeight)];
            }
            else
            {
                _tblMenu=[[UITableView alloc]initWithFrame:CGRectMake(0,([UIScreen mainScreen].bounds.size.height/100*13),screenWidth,screenHeight)];
            }
        }
        _tblMenu.dataSource=self;
        _tblMenu.delegate=self;
        [_tblMenu setScrollEnabled:NO];
        _tblMenu.tableFooterView=[[UIView alloc]init];
        [_tblMenu setBackgroundColor:[UIColor clearColor]];
        [_viewForMenu addSubview:_tblMenu];
    }];
    [self disableSubViews:_viewForMenu];
}

#pragma mark: view For Share
-(void)viewForShareUI
{
    [_viewForShare removeFromSuperview];
    _viewForShare =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForShare.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForShare.tag=20;
    [self.view addSubview:_viewForShare];
    CGRect frameForViewForHelp=_viewForShare.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForShare.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForShare.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForShare.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForShare addSubview:img];
        UILabel *lblShare=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForShare.frame.size.width,30)];
        lblShare.text=@"Share";
        lblShare.textAlignment=NSTextAlignmentCenter;
        lblShare.textColor=[UIColor whiteColor];
        lblShare.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:25];
        [_viewForShare addSubview:lblShare];
        
        _btnShareFB=[[UIButton alloc]init];
        _btnShareTwit=[[UIButton alloc]init];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnShareFB.frame=CGRectMake(_viewForShare.frame.size.width/2-100,CGRectGetMaxY(lblShare.frame)+50,50,50);
            //_shareButton1 = [[FBSDKShareButton alloc] initWithFrame:CGRectMake(_viewForShare.frame.size.width/2-100,CGRectGetMaxY(lblShare.frame)+50,50,50)];
           // _shareButton = [[FBSDKShareButton alloc] initWithFrame:CGRectMake(0,0,50,50)];
            _btnShareTwit.frame=CGRectMake(_viewForShare.frame.size.width/2+50,CGRectGetMaxY(lblShare.frame)+50,50,50);
        }
        else
        {
            _btnShareFB.frame=CGRectMake(_viewForShare.frame.size.width/2-100,CGRectGetMaxY(lblShare.frame)+50,50,50);
            //_shareButton1 = [[FBSDKShareButton alloc] initWithFrame:CGRectMake(_viewForShare.frame.size.width/2-100,CGRectGetMaxY(lblShare.frame)+50,50,50)];
            //_shareButton = [[FBSDKShareButton alloc] initWithFrame:CGRectMake(0,0,50,50)];
            _btnShareTwit.frame=CGRectMake(_viewForShare.frame.size.width/2+50,CGRectGetMaxY(lblShare.frame)+50,50,50);
        }
        [_btnShareFB setBackgroundImage:[UIImage imageNamed:@"icon_FB"] forState:UIControlStateNormal];
        [_btnShareTwit setBackgroundImage:[UIImage imageNamed:@"icon_Twitter"] forState:UIControlStateNormal];
       
        [_btnShareFB addTarget:self action:@selector(btnFbInvitePressed:) forControlEvents:UIControlEventTouchUpInside];
        //[_btnShareFB addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        [_btnShareTwit addTarget:self action:@selector(btnTwitterSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        
       // _shareButton1.transform = CGAffineTransformMakeScale(0.1,0.1);
       // _btnShareTwit.transform = CGAffineTransformMakeScale(0.1,0.1);
        
        [UIView beginAnimations:@"fadeInNewView" context:NULL];
        
        [UIView setAnimationDuration:1.0];
       //_shareButton1.transform = CGAffineTransformMakeScale(1,1);
        //_btnShareTwit.transform = CGAffineTransformMakeScale(1,1);
        
        //_shareButton1.alpha = 1.0f;
        _btnShareTwit.alpha = 1.0f;
        
        [UIView commitAnimations];
        
        [_viewForShare addSubview:_btnShareFB];
        [_viewForShare addSubview:_btnShareTwit];
        
       /* _content = [[FBSDKShareLinkContent alloc] init];
        // NSURL *urlImage = [NSURL URLWithString:strShareURL];
        
        NSString *strBaseURL=ImgBase_URL;
        NSString *strVURL=vBase_URL;
        
        NSString *strBetName=[[_arrDashboardInfo valueForKey:@"name"]objectAtIndex:slectedShareIndex];
        NSString *str=[[_arrDashboardInfo valueForKey:@"image"]objectAtIndex:slectedShareIndex];
        NSString *strVideo=[[_arrDashboardInfo valueForKey:@"video"]objectAtIndex:slectedShareIndex];
        
        strBaseURL=[strBaseURL stringByAppendingString:str];
        strVURL=[strVURL stringByAppendingString:strVideo];
        
        if ([strVideo isEqualToString:@"No"])
        {
            NSLog(@"No Video");
            
            //_content.contentTitle=@"MY TEST APP";
            _content.contentURL = [NSURL URLWithString:strBaseURL];
            [_content setContentTitle:strBetName];
            [_content setImageURL:[NSURL URLWithString: strBaseURL]];
            _content.contentDescription=@"Check this out on the Snapbets app!";
            
        }
        else
        {
            if (strVideo.length>0)
            {
                NSURL *videoURL = [NSURL URLWithString:strVURL];
                FBSDKShareVideo *video = [[FBSDKShareVideo alloc] init];
                video.videoURL = videoURL;
                FBSDKShareVideoContent *content = [[FBSDKShareVideoContent alloc] init];
                content.video = video;
                
                _content.contentURL = [NSURL URLWithString:strVURL];
                [_content setContentTitle:[NSString stringWithFormat: @"%@", strBetName]];
                [_content setContentDescription:@"Check this out on the Snapbets app!"];
                [_content setImageURL:[NSURL URLWithString: strBaseURL]];
                
                [FBSDKShareDialog showFromViewController:self withContent:content delegate:nil];
            }
            else
            {
                _content.contentURL = [NSURL URLWithString:strBaseURL];
                [_content setContentTitle:strBetName];
                [_content setContentDescription:[NSString stringWithFormat: @"Check this out on the Snapbets app!"]];
                [_content setImageURL:[NSURL URLWithString: strBaseURL]];
            }
        }
        _shareButton1.shareContent = _content;
        [_shareButton1  addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        _shareButton1.backgroundColor=[UIColor clearColor];
        [_viewForShare addSubview:_shareButton1];
        
        _shareButton.shareContent = _content;
        [_shareButton setTitle:@"" forState:UIControlStateNormal];
        [_shareButton1 setTitle:@"" forState:UIControlStateNormal];
        
        [_shareButton  addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        _shareButton.layer.cornerRadius=_shareButton.frame.size.height/2;
        _shareButton1.layer.cornerRadius=_shareButton1.frame.size.height/2;
        
        [_shareButton setClipsToBounds:YES];
        [_shareButton1 setClipsToBounds:YES];
        [_shareButton1 addSubview:_shareButton];
        [_shareButton1 setBackgroundImage:[UIImage imageNamed:@"fbwhiteback.png"] forState:UIControlStateNormal];
        [_shareButton1 setBackgroundImage:[UIImage imageNamed:@"fbwhiteback.png"] forState:UIControlStateSelected];
        [_shareButton1 setBackgroundImage:[UIImage imageNamed:@"fbwhiteback.png"] forState:UIControlStateHighlighted];
        */
        _btnCloseShareUI=[[UIButton alloc]initWithFrame:CGRectMake(_viewForShare.frame.size.width-40,10,25,25)];
        [_btnCloseShareUI setBackgroundImage:[UIImage imageNamed:@"icon_Cross"] forState:UIControlStateNormal];
        [_btnCloseShareUI addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
        [_viewForShare addSubview:_btnCloseShareUI];
    }];
    [self disableSubViews:_viewForShare];
}


#pragma mark: view For Share
-(void)viewForInAppUI
{
    [_viewForVote removeFromSuperview];
    _viewForVote =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForVote.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForVote.tag=20;
    [self.view addSubview:_viewForVote];
    CGRect frameForViewForHelp=_viewForVote.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^
     {
         _viewForVote.frame=frameForViewForHelp;
     } completion:^(BOOL finished) {
         vscreenSize=_viewForVote.frame;
         vscreenHeight=vscreenSize.size.height;
         vscreenWidth=vscreenSize.size.width;
         
         UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForVote.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
         img.image=[UIImage imageNamed:@"icon_Share"];
         [_viewForVote addSubview:img];
         UILabel *lblShare=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForVote.frame.size.width,30)];
         lblShare.text=@"Already Purchased";
         lblShare.textAlignment=NSTextAlignmentCenter;
         lblShare.textColor=[UIColor whiteColor];
         lblShare.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
         [_viewForVote addSubview:lblShare];
         
         
         _btnVoteSeeMore=[[UIButton alloc]init];
         _btnVoteClose=[[UIButton alloc]init];
         
         
         if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
         {
            _btnVoteClose.frame=CGRectMake(_viewForVote.frame.size.width/2-70,CGRectGetMaxY(lblShare.frame)+50,140,40);
         }
         else
         {
             _btnVoteClose=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVote.frame.size.width/2-70,CGRectGetMaxY(lblShare.frame)+50,140,40)];
         }
         [_btnVoteClose setTitle:@"Close" forState:UIControlStateNormal];
         [_btnVoteClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         _btnVoteClose.layer.borderWidth=2.0;
         _btnVoteClose.layer.borderColor=[[UIColor orangeColor]CGColor];
         
         _btnVoteClose.layer.cornerRadius=4.0;
         
         [_btnVoteClose addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
         [_viewForVote addSubview:_btnVoteClose];
         
     }];
    [self disableSubViews:_viewForVote];
}

#pragma mark: ViewforUpgrate Account
-(void)viewForUpgradeAccountPopUpUI
{
    [_viewForUpgradeAccount removeFromSuperview];
    _viewForUpgradeAccount =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForUpgradeAccount.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForUpgradeAccount.tag=20;
    [self.view addSubview:_viewForUpgradeAccount];
    
    CGRect frameForViewForHelp=_viewForUpgradeAccount.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForUpgradeAccount.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForUpgradeAccount.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        [self.view bringSubviewToFront:_viewForUpgradeAccount];
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForUpgradeAccount.frame.size.width/2-50,80,100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForUpgradeAccount addSubview:img];
        UILabel *lblPurchase=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForUpgradeAccount.frame.size.width,30)];
        lblPurchase.text=@"Upgrade Account";
        lblPurchase.textAlignment=NSTextAlignmentCenter;
        lblPurchase.textColor=[UIColor whiteColor];
        lblPurchase.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:25];
        [_viewForUpgradeAccount addSubview:lblPurchase];
        
        UILabel *lblPurchase1=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblPurchase.frame)+10,_viewForUpgradeAccount.frame.size.width,30)];
        lblPurchase1.text=@"Upgrade your account";
        lblPurchase1.textAlignment=NSTextAlignmentCenter;
        lblPurchase1.textColor=[UIColor whiteColor];
        lblPurchase1.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForUpgradeAccount addSubview:lblPurchase1];
        
        _btnUpgrade=[[UIButton alloc]initWithFrame:CGRectMake(30,CGRectGetMaxY(lblPurchase1.frame)+50,_viewForUpgradeAccount.frame.size.width/2-40,50)];
        _btnCloseUpgradeUI=[[UIButton alloc]initWithFrame:CGRectMake(_viewForUpgradeAccount.frame.size.width/2+15,CGRectGetMaxY(lblPurchase1.frame)+50,_viewForUpgradeAccount.frame.size.width/2-30,50)];
        
        [_btnCloseUpgradeUI setTitle:@"No Thanks" forState:UIControlStateNormal];
        
        [_btnCloseUpgradeUI setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        _btnCloseUpgradeUI.layer.borderWidth=2.0;
        _btnCloseUpgradeUI.layer.borderColor=[[UIColor orangeColor]CGColor];
        
        _btnUpgrade.backgroundColor=[UIColor orangeColor];
        
        _btnUpgrade.layer.cornerRadius=5.0;
        _btnCloseUpgradeUI.layer.cornerRadius=5.0;
        
        [_btnUpgrade addTarget:self action:@selector(viewForUpdateNowPopUp) forControlEvents:UIControlEventTouchUpInside];
        
        [_btnCloseUpgradeUI addTarget:self action:@selector(btnNoThnksUpgradePressed) forControlEvents:UIControlEventTouchUpInside];
        [_viewForUpgradeAccount addSubview:_btnUpgrade];
        [_viewForUpgradeAccount addSubview:_btnCloseUpgradeUI];
        
        UILabel *lblBuy=[[UILabel alloc]initWithFrame:CGRectMake(0,10,_btnUpgrade.frame.size.width,30)];
        lblBuy.text=@"Upgrade Now";
        lblBuy.textAlignment=NSTextAlignmentCenter;
        lblBuy.textColor=[UIColor whiteColor];
        lblBuy.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_btnUpgrade addSubview:lblBuy];
        
        
        UILabel *lblNeverShow=[[UILabel alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/100*37, screenHeight-40, 200, 30)];
        lblNeverShow.text=[NSString stringWithFormat:@"Never Show Again"];
        lblNeverShow.textColor=[UIColor whiteColor];
        lblNeverShow.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:15];
        [_viewForUpgradeAccount addSubview:lblNeverShow];
        
        
        _btnCheck=[[UIButton alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/100*28, screenHeight-35, 22, 22)];
        [_btnCheck setBackgroundImage:[UIImage imageNamed:@"checkBoxSnap"] forState:UIControlStateNormal];
        [_btnCheck addTarget:self action:@selector(btnCheckPressed) forControlEvents:UIControlEventTouchUpInside];
        [_viewForUpgradeAccount addSubview:_btnCheck];
        _isCheck=NO;
    }];
}

#pragma mark: View for UpgradeNowUI
-(void)viewForUpdateNowPopUp
{
    if (neverShowAgain == nil)
    {
        
    }
    else
    {
        [self closeViewButonPressed];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productsLoaded:) name:kProductsLoadedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:kProductPurchasedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(productPurchaseFailed:) name:kProductPurchaseFailedNotification object: nil];
    
    if ([InAppRageIAPHelper sharedHelper].products == nil)
    {
        
        [[InAppRageIAPHelper sharedHelper] requestProducts];
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.labelText =@"Loading";
        [self performSelector:@selector(timeout:) withObject:nil afterDelay:30.0];
    }
    
    [_viewForUpdateNow removeFromSuperview];
    _viewForUpdateNow =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForUpdateNow.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForUpdateNow.tag=20;
    [self.view addSubview:_viewForUpdateNow];
    CGRect frameForViewForHelp=_viewForUpdateNow.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForUpdateNow.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForUpdateNow.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UILabel *lblPurchase=[[UILabel alloc]initWithFrame:CGRectMake(0,10,_viewForUpdateNow.frame.size.width,30)];
        lblPurchase.text=@"Upgrade Account";
        lblPurchase.textAlignment=NSTextAlignmentCenter;
        lblPurchase.textColor=[UIColor whiteColor];
        lblPurchase.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:25];
        [_viewForUpdateNow addSubview:lblPurchase];
        
        _btnCloseUpgradeNow=[[UIButton alloc]initWithFrame:CGRectMake(_viewForUpdateNow.frame.size.width-40,10,30,30)];
        [_btnCloseUpgradeNow setBackgroundImage:[UIImage imageNamed:@"icon_Cross"] forState:UIControlStateNormal];
        //[_btnCloseUpgradeNow setTitle:@"X" forState:UIControlStateNormal];
        // [_btnCloseUpgradeNow setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        // _btnCloseUpgradeNow.titleLabel.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:25];
        //_btnCloseUpgradeNow.layer.cornerRadius=_btnCloseUpgradeNow.frame.size.height/2;
        //_btnCloseUpgradeNow.layer.borderWidth=1.0;
        [_btnCloseUpgradeNow addTarget:self action:@selector(closeUpgradeView) forControlEvents:UIControlEventTouchUpInside];
        _btnCloseUpgradeNow.layer.borderColor=[[UIColor orangeColor]CGColor];
        [_viewForUpdateNow addSubview:_btnCloseUpgradeNow];
        
        [_tblUpgradeAcc removeFromSuperview];
        _tblUpgradeAcc=[[UITableView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblPurchase.frame)+10,_viewForUpdateNow.frame.size.width,_viewForUpdateNow.frame.size.height-120)];
        _tblUpgradeAcc.tableFooterView=[[UIView alloc]init];
        [_tblUpgradeAcc setBackgroundColor:[UIColor clearColor]];
        _tblUpgradeAcc.delegate=self;
        _tblUpgradeAcc.dataSource=self;
        
        [_viewForUpdateNow addSubview:_tblUpgradeAcc];
        
        UILabel *lblDescText=[[UILabel alloc]initWithFrame:CGRectMake(5,CGRectGetMaxY(_tblUpgradeAcc.frame)-50,_viewForUpdateNow.frame.size.width-10,0)];
        lblDescText.text=@"Disclaimer: Payment will be charged to your iTunes Account at confirmation of purchases. Subscription automatically renews unless auto-renew is turned-off 24 hours before the end of the current subscription period. Subscriptions can be managed through your iTunes Account Setting after purchase.";
        lblDescText.font=[UIFont fontWithName:@"Avenir-Roman" size:12];
        [lblDescText setNumberOfLines:0];
        lblDescText.textColor=[UIColor whiteColor];
        lblDescText.textAlignment=NSTextAlignmentLeft;
        [lblDescText sizeToFit];
        
        [_viewForUpdateNow addSubview:lblDescText];
    }];
    
}

#pragma mark: Viewfor Buy single Theme Package
-(void)viewForRestorePopUpUI
{
    [_viewForRestore removeFromSuperview];
    _viewForRestore =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForRestore.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForRestore.tag=20;
    [self.view addSubview:_viewForRestore];
    [_viewForRestore bringSubviewToFront:self.view];
    CGRect frameForViewForHelp=_viewForRestore.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForRestore.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForRestore.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForRestore.frame.size.width/2-50,80,100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForRestore addSubview:img];
        UILabel *lblPurchase=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForRestore.frame.size.width,30)];
        lblPurchase.text=@"Upgrade Account";
        lblPurchase.textAlignment=NSTextAlignmentCenter;
        lblPurchase.textColor=[UIColor whiteColor];
        lblPurchase.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:25];
        [_viewForRestore addSubview:lblPurchase];
        
        UILabel *lblPurchase1=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblPurchase.frame)+10,_viewForRestore.frame.size.width,30)];
        lblPurchase1.text=@"Upgrade your account";
        lblPurchase1.textAlignment=NSTextAlignmentCenter;
        lblPurchase1.textColor=[UIColor whiteColor];
        lblPurchase1.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForRestore addSubview:lblPurchase1];
        
        _btnUpgrade=[[UIButton alloc]initWithFrame:CGRectMake(20,CGRectGetMaxY(lblPurchase1.frame)+50,_viewForUpgradeAccount.frame.size.width/2-30,45)];
        _btnCloseUpgradeUI=[[UIButton alloc]initWithFrame:CGRectMake(_viewForUpgradeAccount.frame.size.width/2+15,CGRectGetMaxY(lblPurchase1.frame)+50,_viewForUpgradeAccount.frame.size.width/2-30,45)];
        
        [_btnCloseUpgradeUI setTitle:@"Cancel" forState:UIControlStateNormal];
        
        [_btnCloseUpgradeUI setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        _btnCloseUpgradeUI.layer.borderWidth=2.0;
        _btnCloseUpgradeUI.layer.borderColor=[[UIColor orangeColor]CGColor];
        
        _btnUpgrade.backgroundColor=[UIColor orangeColor];
        
        _btnUpgrade.layer.cornerRadius=5.0;
        _btnCloseUpgradeUI.layer.cornerRadius=5.0;
        
        [_btnCloseUpgradeUI addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
        [_viewForUpgradeAccount addSubview:_btnUpgrade];
        [_viewForUpgradeAccount addSubview:_btnCloseUpgradeUI];
        
        UILabel *lblBuy=[[UILabel alloc]initWithFrame:CGRectMake(0,0,_btnUpgrade.frame.size.width,30)];
        lblBuy.text=@"Upgrade";
        lblBuy.textAlignment=NSTextAlignmentCenter;
        lblBuy.textColor=[UIColor whiteColor];
        lblBuy.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_btnUpgrade addSubview:lblBuy];
        
        UILabel *lblBuyPrice=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblBuy.frame)-10,_btnUpgrade.frame.size.width,30)];
        lblBuyPrice.text=@"$10.99";
        lblBuyPrice.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        lblBuyPrice.textAlignment=NSTextAlignmentCenter;
        lblBuyPrice.textColor=[UIColor whiteColor];
        [_btnUpgrade addSubview:lblBuyPrice];
    }];
}

#pragma mark: Disable SubView
-(void)disableSubViews:(UIView *)view
{
    for(UIView *subview in [self.view subviews])
    {
        if ((subview.tag==view.tag&&[subview isMemberOfClass:[UIView class]]))
        {
            
        }
        else
        {
            subview.userInteractionEnabled=NO;
            // subview.alpha=1.0;
        }
    }
}

#pragma mark: Enable Subview
-(void)enableSubViews
{
    for(UIView *subview in [self.view subviews])
    {
        subview.userInteractionEnabled=YES;
        // subview.alpha=1;
    }
}

#pragma mark-Close Comment View
-(void)closeViewButonPressed
{
    @try
    {
        [self enableSubViews];
        for(UIView *subview in [_viewForMenu subviews])
        {
            [subview removeFromSuperview];
        }
        for(UIView *subview in [_viewForShare subviews])
        {
            [subview removeFromSuperview];
        }
        
        for(UIView *subview in [_viewForVote subviews])
        {
            [subview removeFromSuperview];
        }
        for(UIView *subview in [_viewForUpgradeAccount subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForHelp=_viewForMenu.frame;
        CGRect frameForViewForShare=_viewForShare.frame;
        CGRect frameForViewForVote=_viewForVote.frame;
        CGRect frameForViewForUpgrade=_viewForUpgradeAccount.frame;
        
        frameForViewForHelp=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        frameForViewForShare=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        frameForViewForVote=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        frameForViewForUpgrade=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForMenu.frame=frameForViewForHelp;
            _viewForShare.frame=frameForViewForShare;
            _viewForShare.frame=frameForViewForVote;
            _viewForUpgradeAccount.frame=frameForViewForVote;
            
        } completion:^(BOOL finished){
            [_viewForMenu removeFromSuperview];
            [_viewForShare removeFromSuperview];
            [_viewForVote removeFromSuperview];
            [_viewForUpgradeAccount removeFromSuperview];
            //   [self enableSubViews];
            if (_isVoteBTNPressed)
            {
                if ([_strCurrentFeed isEqualToString:@"My Feed"])
                {
                    _isFeedData=YES;
                    _isTrendingData=NO;
                    _isMyBetsData=NO;
                    _isLeadersData=NO;
                    _isVoteBTNPressed=NO;
                    [self MainDashboardDataAPI];
                }
                else if ([_strCurrentFeed isEqualToString:@"Trending"])
                {
                    _isFeedData=NO;
                    _isTrendingData=YES;
                    _isMyBetsData=NO;
                    _isLeadersData=NO;
                    _isVoteBTNPressed=NO;
                    [self MainDashboardDataAPI];
                }
                else if ([_strCurrentFeed isEqualToString:@"My Beet"])
                {
                    _isFeedData=NO;
                    _isTrendingData=NO;
                    _isMyBetsData=YES;
                    _isLeadersData=NO;
                    _isVoteBTNPressed=NO;
                    [self MainDashboardDataAPI];
                }
                else if ([_strCurrentFeed isEqualToString:@"Leaders"])
                {
                    _isFeedData=NO;
                    _isTrendingData=NO;
                    _isMyBetsData=NO;
                    _isLeadersData=YES;
                    _isVoteBTNPressed=NO;
                    [self MainDashboardDataAPI];
                }
            }
            
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma marl: Menu Buttons Pressed delegate declarations here
-(IBAction)buttonPressedAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSInteger row = button.tag;
    NSLog(@"%ld",(long)row);
    
    if (row==0)
    {
        theAppDelegate.isSearch=YES;
        SearchScreen *serch=[[SearchScreen alloc]initWithNibName:@"SearchScreen" bundle:nil];
        [self.navigationController pushViewController:serch animated:YES];
    }
    else if (row==1)
    {
        SettingScreen *seting=[[SettingScreen alloc]initWithNibName:@"SettingScreen" bundle:nil];
        [self.navigationController pushViewController:seting animated:YES];
    }
    else if (row==2)
    {
        [self viewForRestoreAlertUI];
    }
    else if (row==3)
    {
        /*if (neverShowAgain == nil)
        {
            [self viewForUpgradeAccountPopUpUI];
        }
        else
        {
         */
            [self viewForUpdateNowPopUp];
        //}
        
        
    }
    else if (row==4)
    {
        theAppDelegate.isProfFromMenu=YES;
        ProfileScreen *prof=[[ProfileScreen alloc]initWithNibName:@"ProfileScreen" bundle:nil];
        [self.navigationController pushViewController:prof animated:YES];
    }
    else if (row==5)
    {
        NotificationScreen *notify=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
        [self.navigationController pushViewController:notify animated:YES];
    }
    else if (row==6)
    {
        SnapbetsThemeScreen  *snap=[[SnapbetsThemeScreen alloc]initWithNibName:@"SnapbetsThemeScreen" bundle:nil];
        [self.navigationController pushViewController:snap animated:YES];
    }
    else if (row==7)
    {
        NewSnapScreen *nSnap=[[NewSnapScreen alloc]initWithNibName:@"NewSnapScreen" bundle:nil];
        [self.navigationController pushViewController:nSnap animated:YES];
    }
    else
    {
        [self serverCallToCheckProfileImage];
        [self closeViewButonPressed];
    }
}

-(IBAction)btnFBSharePressed:(id)sender
{
    [self closeViewButonPressed];
}


-(IBAction)btnFbInvitePressed:(id)sender
{
    [self closeViewButonPressed];
    NSString *strBaseURL=ImgBase_URL;
    NSString *strVURL=vBase_URL;
    
    // NSString *strBetName=[[_arrDashboardInfo valueForKey:@"name"]objectAtIndex:slectedShareIndex];
    NSString *str=[[_arrDashboardInfo valueForKey:@"image"]objectAtIndex:slectedShareIndex];
    NSString *strVideo=[[_arrDashboardInfo valueForKey:@"video"]objectAtIndex:slectedShareIndex];
    
    strBaseURL=[strBaseURL stringByAppendingString:str];
    strVURL=[strVURL stringByAppendingString:strVideo];
    
    NSString *strBase=ImgBase_URL;
    strBase=[strBase stringByAppendingString:str];
    
    FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init];
    //content.appLinkURL = [NSURL URLWithString:@"https://fb.me/1444996258928276"];
    content.appLinkURL = [NSURL URLWithString:@"https://fb.me/1445063282254907"];
    //optionally set previewImageURL
    content.appInvitePreviewImageURL = [NSURL URLWithString:strBaseURL];
    
    // Present the dialog. Assumes self is a view controller
    // which implements the protocol `FBSDKAppInviteDialogDelegate`.
    [FBSDKAppInviteDialog showFromViewController:self
                                     withContent:content
                                        delegate:self];
}

- (void)appInviteDialog:	(FBSDKAppInviteDialog *)appInviteDialog
didCompleteWithResults:	(NSDictionary *)results
{
    NSLog(@"%@",results);
}

-(void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error
{
    
    NSLog(@"%@",error);
}

-(IBAction)btnTwitterSharePressed:(id)sender
{
    _loader = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_loader startAnimating];
    _loader.backgroundColor=[UIColor blackColor];
    _loader.center = self.view.center;
    [self.view addSubview:_loader];
    
      //[self closeViewButonPressed];
     [_loader startAnimating];
    [theAppDelegate showSpinnerInView:self.view];
    NSString *strBaseURL=ImgBase_URL;
    NSString *strVURL=vBase_URL;
    
   // NSString *strBetName=[[_arrDashboardInfo valueForKey:@"name"]objectAtIndex:slectedShareIndex];
    NSString *str=[[_arrDashboardInfo valueForKey:@"image"]objectAtIndex:slectedShareIndex];
    NSString *strVideo=[[_arrDashboardInfo valueForKey:@"video"]objectAtIndex:slectedShareIndex];
    
    strBaseURL=[strBaseURL stringByAppendingString:str];
    strVURL=[strVURL stringByAppendingString:strVideo];
    
    NSString *strBase=ImgBase_URL;
    strBase=[strBase stringByAppendingString:str];
    
    [self saveImagesInLocalDirectory:strBaseURL imgNAME:str];
    
   /* SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
   // NSString *strShareText = [NSString stringWithFormat:@"View Snapbets to accept new Challenges"];
    
    NSString *strBaseURL=ImgBase_URL;
    NSString *strVURL=vBase_URL;
    
    NSString *strBetName=[[_arrDashboardInfo valueForKey:@"name"]objectAtIndex:slectedShareIndex];
    NSString *str=[[_arrDashboardInfo valueForKey:@"image"]objectAtIndex:slectedShareIndex];
    NSString *strVideo=[[_arrDashboardInfo valueForKey:@"video"]objectAtIndex:slectedShareIndex];
    
    strBaseURL=[strBaseURL stringByAppendingString:str];
    strVURL=[strVURL stringByAppendingString:strVideo];
    
    NSString *strBase=ImgBase_URL;
    strBase=[strBase stringByAppendingString:str];
    [mySLComposerSheet setInitialText:strBase];
        
    [mySLComposerSheet setTitle:strBetName];
    [mySLComposerSheet addImage:[UIImage imageNamed:str]];
    
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                
                NSLog(@"Post Canceled");
                break;
            case SLComposeViewControllerResultDone:
                
                NSLog(@"Post Sucessful");
                break;
            default:
                break;
        }
    }];
    [self closeViewButonPressed];
    [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    */
    
    /*SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    NSString *strShareText = [NSString stringWithFormat:@"%@ \n Check this out on the Snapbets app!",strBetName];
    
    [mySLComposerSheet setInitialText:strShareText];
    
    [mySLComposerSheet addImage:[UIImage imageNamed:strTwiterImagePath]];
    
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                
                NSLog(@"Post Canceled");
                break;
            case SLComposeViewControllerResultDone:
                
                NSLog(@"Post Sucessful");
                break;
            default:
                break;
        }
    }];
    
    [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    */
}

-(void)twiterSharing
{
    [theAppDelegate stopSpinner];
    NSString *strBetName=[[_arrDashboardInfo valueForKey:@"name"]objectAtIndex:slectedShareIndex];
    SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    NSString *strShareText = [NSString stringWithFormat:@"%@ \n Check this out on the Snapbets app!",strBetName];
    
    [mySLComposerSheet setInitialText:strShareText];
    
    [mySLComposerSheet addImage:[UIImage imageNamed:strTwiterImagePath]];
    
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                
                NSLog(@"Post Canceled");
                break;
            case SLComposeViewControllerResultDone:
                
                NSLog(@"Post Sucessful");
                break;
            default:
                break;
        }
    }];
    [_loader stopAnimating];
    [self closeViewButonPressed];
     [self presentViewController:mySLComposerSheet animated:YES completion:nil];

}
-(void)saveImagesInLocalDirectory:(NSString *)imgUrl imgNAME:(NSString*)imgName
{
    NSString * documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *writablePath = [documentsDirectoryPath stringByAppendingPathComponent:imgName];
    
    if(![fileManager fileExistsAtPath:writablePath]){
        // file doesn't exist
        NSLog(@"file doesn't exist");
        //save Image From URL
        NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgUrl]];
        
        NSError *error = nil;
        [data writeToFile:[documentsDirectoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName]] options:NSAtomicWrite error:&error];
        
        if (error)
        {
            [theAppDelegate stopSpinner];
            [_loader stopAnimating];
            NSLog(@"Error Writing File : %@",error);
        }else
        {
            [_loader stopAnimating];
            NSLog(@"Image %@ Saved SuccessFully",imgName);
            NSString *strPath=[documentsDirectoryPath stringByAppendingString:@"/"];
            strTwiterImagePath=[strPath stringByAppendingString:imgName];
            NSLog(@"%@",strPath);
            [self twiterSharing];
            
        }
    }
    else{
        // file exist
        NSLog(@"file exist");
        NSString *strPath=[documentsDirectoryPath stringByAppendingString:@"/"];
        strTwiterImagePath=[strPath stringByAppendingString:imgName];
        NSLog(@"%@",strPath);
        [self twiterSharing];
    }
}
-(IBAction)btnYoutubePressed:(id)sender
{
    
}

-(IBAction)btnSharePressed:(id)sender
{
    [self incrimentUserDefaults];
    UIButton *btn=(UIButton*)sender;
    slectedShareIndex=btn.tag;
    [self viewForShareUI];
}

-(IBAction)btnSendPressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    NSLog(@"%ld",row);
    
    theAppDelegate.isSearch=NO;
    SearchScreen *serch=[[SearchScreen alloc]initWithNibName:@"SearchScreen" bundle:nil];
    serch.strSrchReciverID=[NSString stringWithFormat:@"%ld",(long)row];
    if (_isFeedData)
    {
        serch.strUID=[[_arrDashboardInfo valueForKey:@"uid"]objectAtIndex:row];
        serch.snapID=[[_arrDashboardInfo valueForKey:@"id"]objectAtIndex:row];
    }
    else if (_isMyBetsData)
    {
        NSString *str=[theAppDelegate.arrUserLoginData objectAtIndex:0];
        NSInteger i=[str integerValue];
        NSLog(@"%ld",(long)i);
        serch.strUID=[theAppDelegate.arrUserLoginData objectAtIndex:0];
        serch.snapID=[[_arrDashboardInfo valueForKey:@"id"]objectAtIndex:row];
    }
    [self.navigationController pushViewController:serch animated:YES];
}

-(IBAction)btnVotePressed:(id)sender
{
    _isVoteBTNPressed=YES,_isFeedData=NO,_isMyBetsData=NO,_isLeadersData=NO,_isTrendingData=NO;
    
    UIButton *button = (UIButton *)sender;
    NSInteger row = button.tag;
    NSLog(@"%ld",(long)row);
    
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        [theAppDelegate showSpinnerInView:self.view];
        //[self.view setUserInteractionEnabled:NO];
        NSString *urlString;
        NSString *strBaseURL=base_URL;
        
        urlString =[NSString stringWithFormat:@"wsaddsnapvote.php?uid=%@&sid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],[[_arrDashboardInfo valueForKey:@"id"]objectAtIndex:row]];
        
        strBaseURL=[strBaseURL stringByAppendingString:urlString];
        NSURL *urlAllTicket=[NSURL URLWithString:strBaseURL];
        request=[NSMutableURLRequest requestWithURL:[urlAllTicket standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
        request = nil;
    }
    else
    {
        
    }
}

-(IBAction)btnBetsPressed:(id)sender
{
    [self incrimentUserDefaults];
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    
    SpecifiBetPage *specificBet=[[SpecifiBetPage alloc]initWithNibName:@"SpecifiBetPage" bundle:nil];
    specificBet.strThemeName=[[_arrDashboardInfo valueForKey:@"name"]objectAtIndex:row];
    specificBet.selectedBetID1=[[_arrDashboardInfo valueForKey:@"id"]objectAtIndex:row];
    specificBet.strBetImg=[[_arrDashboardInfo valueForKey:@"bgimage"]objectAtIndex:row];
    specificBet.strSelectBUID=[[_arrDashboardInfo valueForKey:@"uid"]objectAtIndex:row];    
    [self.navigationController pushViewController:specificBet animated:YES];
}

-(IBAction)btnProfilePressed:(id)sender
{
    [self incrimentUserDefaults];
    theAppDelegate.isProfFromMenu=YES;
    ProfileScreen *prof=[[ProfileScreen alloc]initWithNibName:@"ProfileScreen" bundle:nil];
    [self.navigationController pushViewController:prof animated:YES];
}

-(IBAction)btnLedBetProfilePressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    
    theAppDelegate.isProfFromMenu=NO;
    ProfileScreen *prof=[[ProfileScreen alloc]initWithNibName:@"ProfileScreen" bundle:nil];
    prof.strUserID=[[_arrDashboardInfo valueForKey:@"uid"]objectAtIndex:row];
    [self.navigationController pushViewController:prof animated:YES];
}
-(IBAction)btnSnapBetThemePressed:(id)sender
{
    [self incrimentUserDefaults];
    SnapbetsThemeScreen  *snap=[[SnapbetsThemeScreen alloc]initWithNibName:@"SnapbetsThemeScreen" bundle:nil];
    [self.navigationController pushViewController:snap animated:YES];
}
-(IBAction)btnFeedPressed:(id)sender
{
    [self incrimentUserDefaults];
    _imgFeedBG.hidden=NO;
    _imgTrendingBG.hidden=YES;
    _imgMyBetBG.hidden=YES;
    _imgLeadersBG.hidden=YES;
    
    NSLog(@"Feed Button Pressed");
    _isFeedData=YES;_isTrendingData=NO;_isMyBetsData=NO;_isLeadersData=NO;
    
    _btnFeed.titleLabel.font=[UIFont fontWithName:@"Avenir-Heavy" size:25];
    _btnTrending.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:20];
    _btnLeaders.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:20];
    _btnMyBets.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:20];
    
    [self MainDashboardDataAPI];
}
-(IBAction)btnTrendingPressed:(id)sender
{
    [self incrimentUserDefaults];
    _imgFeedBG.hidden=YES;
    _imgTrendingBG.hidden=NO;
    _imgMyBetBG.hidden=YES;
    _imgLeadersBG.hidden=YES;
    
    NSLog(@"Trending Button Pressed");
    _isFeedData=NO;_isTrendingData=YES;_isMyBetsData=NO;_isLeadersData=NO;
    
    _btnFeed.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:20];
    _btnTrending.titleLabel.font=[UIFont fontWithName:@"Avenir-Heavy" size:25];
    _btnLeaders.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:20];
    _btnMyBets.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:20];
    [self MainDashboardDataAPI];
}

-(IBAction)btnMyBetsPressed:(id)sender
{
    [self incrimentUserDefaults];
    _imgFeedBG.hidden=YES;
    _imgTrendingBG.hidden=YES;
    _imgMyBetBG.hidden=NO;
    _imgLeadersBG.hidden=YES;
    NSLog(@"My Bets Button Pressed");
    _isFeedData=NO;_isTrendingData=NO;_isMyBetsData=YES;_isLeadersData=NO;
    
    _btnFeed.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:20];
    _btnTrending.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:20];
    _btnLeaders.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:20];
    _btnMyBets.titleLabel.font=[UIFont fontWithName:@"Avenir-Heavy" size:25];
    [self MainDashboardDataAPI];
}

-(IBAction)btnLeadersPressed:(id)sender
{
    [self incrimentUserDefaults];
    _imgFeedBG.hidden=YES;
    _imgTrendingBG.hidden=YES;
    _imgMyBetBG.hidden=YES;
    _imgLeadersBG.hidden=NO;
    
    _btnFeed.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:20];
    _btnTrending.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:20];
    _btnLeaders.titleLabel.font=[UIFont fontWithName:@"Avenir-Heavy" size:25];
    _btnMyBets.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:20];
    
    NSLog(@"Leaders Button Pressed");
    _isFeedData=NO;_isTrendingData=NO;_isMyBetsData=NO;_isLeadersData=YES;
    [self MainDashboardDataAPI];
}

-(void)btnCheckPressed
{
    if (_isCheck == NO)
    {
        _isCheck=YES;
        [_btnCheck setBackgroundImage:[UIImage imageNamed:@"checkedBoxSnap"] forState:UIControlStateNormal];
    }
    else
    {
        _isCheck=NO;
        [_btnCheck setBackgroundImage:[UIImage imageNamed:@"checkBoxSnap"] forState:UIControlStateNormal];
    }
}

#pragma mark-Close Comment View
-(void)closeUpgradeView
{
    @try
    {
        [self enableSubViews];
        for(UIView *subview in [_viewForUpdateNow subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForHelp=_viewForUpdateNow.frame;
        
        frameForViewForHelp=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForUpdateNow.frame=frameForViewForHelp;
            
        } completion:^(BOOL finished){
            [_viewForUpdateNow removeFromSuperview];
            
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark-Close Comment View
-(void)btnNoThnksUpgradePressed
{
    @try
    {
        if (_isCheck == YES)
        {
            [defaults setObject:@"YES" forKey:@"IsShowPopUp"];
        }
        
        
        [self enableSubViews];
        for(UIView *subview in [_viewForUpgradeAccount subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForHelp=_viewForUpgradeAccount.frame;
        
        frameForViewForHelp=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^
         {
             
             _viewForUpgradeAccount.frame=frameForViewForHelp;
             
         } completion:^(BOOL finished)
         {
             [_viewForUpgradeAccount removeFromSuperview];
         }];
    } @catch (NSException *exception)
    {
        
    } @finally
    {
        
    }
}

-(IBAction)btnbuyInAppPressed:(id)sender
{
    [self getDataFromDB];
    
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    NSLog(@"%ld",row);
    SKProduct *product;
   
        if(row==0)
        {
            product=[[InAppRageIAPHelper sharedHelper].products objectAtIndex:2];
        NSLog(@"Buying %@...", product.productIdentifier);
        }
        else
        {
            product=[[InAppRageIAPHelper sharedHelper].products objectAtIndex:1];
            NSLog(@"Buying %@...", product.productIdentifier);
        }
        if ([arrProductIDs containsObject:product.productIdentifier])
        {
            [self viewForInAppUI];
        }
        else
        {
            [[InAppRageIAPHelper sharedHelper] buyProduct:product];
            hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.labelText =@"Buying...";
            [self performSelector:@selector(timeout:) withObject:nil afterDelay:60*5];
        }
}

-(IBAction)btnLeadrsBetPressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    NSString *strId=[[_arrDashboardInfo valueForKey:@"uid"]objectAtIndex:row];
    strLedSetcedBetID=[[_arrDashboardInfo valueForKey:@"uid"]objectAtIndex:row];
    
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isFeedData=NO;_isTrendingData=NO;_isMyBetsData=NO;_isLeadersData=NO;_isCheckProfileImage=NO;
        [theAppDelegate showSpinnerInView:self.view];
        //[self.view setUserInteractionEnabled:NO];
        NSString *urlString;
        NSString *strBaseURL=base_URL;
        
        urlString=[NSString stringWithFormat:@"wsgetActiveBeats.php?id=%@",strId];
        strBaseURL=[strBaseURL stringByAppendingString:urlString];
        NSURL *urlBet=[NSURL URLWithString:strBaseURL];
        request=[NSMutableURLRequest requestWithURL:[urlBet standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
        request = nil;
    }
    else
    {
        
    }

}

#pragma mark: view For Restore AlertUI
-(void)viewForRestoreAlertUI
{
    [_viewForRestoreAlert removeFromSuperview];
    _viewForRestoreAlert =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForRestoreAlert.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForRestoreAlert.tag=20;
    [self.view addSubview:_viewForRestoreAlert];
    CGRect frameForViewForHelp=_viewForRestoreAlert.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForRestoreAlert.frame=frameForViewForHelp;
    } completion:^(BOOL finished)
     {
         vscreenSize=_viewForRestoreAlert.frame;
         vscreenHeight=vscreenSize.size.height;
         vscreenWidth=vscreenSize.size.width;
         
         UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForRestoreAlert.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
         img.image=[UIImage imageNamed:@"icon_Share"];
         [_viewForRestoreAlert addSubview:img];
         UILabel *lblShare=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+25,_viewForRestoreAlert.frame.size.width,30)];
         lblShare.text=@"Are you sure you want to restore app!";
         lblShare.textAlignment=NSTextAlignmentCenter;
         lblShare.textColor=[UIColor whiteColor];
         [_viewForRestoreAlert addSubview:lblShare];
         
         _btnVoteSeeMore=[[UIButton alloc]init];
         _btnVoteClose=[[UIButton alloc]init];
         if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
         {
             _btnRestoreYes=[[UIButton alloc]initWithFrame:CGRectMake(_viewForRestoreAlert.frame.size.width/2-60,CGRectGetMaxY(lblShare.frame)+50,120,40)];
             _btnRestoreNo=[[UIButton alloc]initWithFrame:CGRectMake(_viewForRestoreAlert.frame.size.width/2-60,CGRectGetMaxY(lblShare.frame)+50,120,40)];
         }
         else
         {
             _btnRestoreYes=[[UIButton alloc]initWithFrame:CGRectMake(40,CGRectGetMaxY(lblShare.frame)+50,screenWidth/2-80,45)];
             _btnRestoreNo=[[UIButton alloc]initWithFrame:CGRectMake(_viewForRestoreAlert.frame.size.width/2+40,CGRectGetMaxY(lblShare.frame)+50,screenWidth/2-80,45)];
         }
         [_btnRestoreYes setTitle:@"Yes" forState:UIControlStateNormal];
         [_btnRestoreNo setTitle:@"No" forState:UIControlStateNormal];
         
         [_btnRestoreYes setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         [_btnRestoreNo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         
         _btnRestoreYes.layer.borderWidth=2.0;
         _btnRestoreNo.layer.borderWidth=2.0;
         
         _btnRestoreYes.layer.borderColor=[[UIColor orangeColor]CGColor];
         _btnRestoreNo.layer.borderColor=[[UIColor orangeColor]CGColor];
         
         _btnRestoreYes.layer.cornerRadius=4.0;
         _btnRestoreNo.layer.cornerRadius=4.0;
         
         [_btnRestoreYes addTarget:self action:@selector(restoreAppFunctionality) forControlEvents:UIControlEventTouchUpInside];
         [_btnRestoreNo addTarget:self action:@selector(viewForCloseRestoreAlert) forControlEvents:UIControlEventTouchUpInside];
         
         [_viewForRestoreAlert addSubview:_btnRestoreYes];
         [_viewForRestoreAlert addSubview:_btnRestoreNo];
         
     }];
}

#pragma mark: Restore InApp Purchase
-(void)restoreAppFunctionality
{

        NSString *selectQuery,*selectQury1;
        DBManager *objDBManager=[DBManager getSharedInstance];
    
        selectQuery=[NSString stringWithFormat:@"select * from tblInApp where UID='%@'",[theAppDelegate.arrUserLoginData objectAtIndex:0]];
       selectQury1=[NSString stringWithFormat:@"select * from tblThemeInApp where UID='%@'",[theAppDelegate.arrUserLoginData objectAtIndex:0]];
        NSArray *arr1=[objDBManager selectTableDataWithQuery:selectQuery];
        NSArray *arr2=[objDBManager selectTableDataWithQuery:selectQury1];
        
        if ([arr1 count]||[arr2 count])
        {
            [objDBManager deleteFromTableWithQuery:@"delete from tblInApp"];
            [objDBManager deleteFromTableWithQuery:@"delete from tblThemeInApp"];
        }
    [self viewForCloseRestoreAlert];
}
#pragma mark-Close Comment View
-(void)viewForCloseRestoreAlert
{
    @try
    {
        for(UIView *subview in [_viewForRestoreAlert subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForVote=_viewForRestoreAlert.frame;
        frameForViewForVote=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            _viewForRestoreAlert.frame=frameForViewForVote;
        } completion:^(BOOL finished){
            
            [_viewForRestoreAlert removeFromSuperview];
            
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}


/*--------------     Bottom TableView delegate and datasource declaration here     -----------*/
#pragma mark- TableView DataSource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_tblMenu)
    {
        return [_arrMenuImages count];
    }
    else if (tableView==_tblBets)
    {
        return [_arrDashboardInfo count];
    }
    else if(tableView==_tblUpgradeAcc)
    {
        return 2;
    }
    else if (tableView==_tblLeaders)
    {
        return [_arrDashboardInfo count];
    }
    return 0;
}

//--------------------- cellforrow At Index TableView -----------------------//

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@""];
    if (cell == nil)
    {
        //  cell.transform= CGAffineTransformMakeRotation(M_PI/2);
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    
    tableView.separatorColor =UIColor.clearColor;
    cell.backgroundColor= [UIColor clearColor];
    [self.tblMenu setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if (tableView==_tblMenu)
    {
        UILabel *lblTitle=[[UILabel alloc]init];
        
        UIButton *btnImg;
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            lblTitle.frame=CGRectMake(screenWidth/2,5,140,30);
            
            if ([_arrMenuImages count]-1==indexPath.row)
            {
                btnImg=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-185,0,70,70)];
            }
            else
            {
                btnImg=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-180,0,45,45)];
            }
        }
        else
        {
            if(screenWidth<325)
            {
                lblTitle.frame=CGRectMake(screenWidth/3-20,5,150,30);
            }
            else
            {
                lblTitle.frame=CGRectMake(screenWidth/2.8,5,150,30);
            }
            
            if ([_arrMenuImages count]-1==indexPath.row)
            {
                btnImg=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-93,0,70,70)];
            }
            else
            {
                btnImg=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-80,0,45,45)];
            }
        }
        
        lblTitle.text=[_arrMenuTitles objectAtIndex:indexPath.row];
        lblTitle.textColor=[UIColor whiteColor];
        lblTitle.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:18];
        lblTitle.textAlignment=NSTextAlignmentRight;
        [cell.contentView addSubview:lblTitle];
        
        NSString *strMImage=[_arrMenuImages objectAtIndex:indexPath.row];
        [btnImg setBackgroundImage:[UIImage imageNamed:strMImage] forState:UIControlStateNormal];
        btnImg.tag=indexPath.row;
        [btnImg addTarget:self action:@selector(buttonPressedAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:btnImg];
        return cell;
    }
    else if (tableView==_tblBets)
    {
        UIImageView *imgCell=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,screenWidth,[UIScreen mainScreen].bounds.size.height/100*20)];
        imgCell.image=[UIImage imageNamed:@"profileIcon"];
        //        [cell.contentView addSubview:imgCell];
        
        NSString *strBUrl=ImgBase_URL;
        
        NSString *strURL=[[_arrDashboardInfo valueForKey:@"bgimage"]objectAtIndex:indexPath.row];
        
        if (strURL.length>0)
        {
            if([strURL containsString:@"http://graph.facebook.com/"])
            {
                NSArray *arr=[strURL componentsSeparatedByString:@"http://graph.facebook.com/"];
                NSLog(@"%@",arr);
                NSString *strString=[arr objectAtIndex:1];
                NSArray *arr1=[strString componentsSeparatedByString:@"/"];
                NSLog(@"%@",arr1);
                strURL=[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?width=500&height=500",[arr1 objectAtIndex:0]];
            }
            else
            {
                strBUrl=[strBUrl stringByAppendingString:strURL];
            }
            NSURL *imgURL=[NSURL URLWithString:strBUrl];
            _asyImgBet=[[AsyncImageView alloc]initWithFrame:CGRectMake(0,0,screenWidth,[UIScreen mainScreen].bounds.size.height/100*20)];
            _asyImgBet.backgroundColor = [UIColor clearColor];
            [_asyImgBet loadImageFromURL:imgURL];
            [_asyImgBet setContentMode:UIViewContentModeScaleAspectFill];
            [_asyImgBet sizeToFit];
            _asyImgBet.clipsToBounds = YES;
            [cell.contentView addSubview:_asyImgBet];
        }
        
        UIView *viewForCell=[[UIView alloc]initWithFrame:CGRectMake(0,[UIScreen mainScreen].bounds.size.height/100*20-45,screenWidth,45)];
        UILabel *lblBetName=[[UILabel alloc]initWithFrame:CGRectMake(5,2,viewForCell.frame.size.width/3+10,30)];
        UILabel *lblBetDate=[[UILabel alloc]initWithFrame:CGRectMake(5,CGRectGetMaxY(lblBetName.frame)-10,60,30)];
        UILabel *lblBetVote=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(lblBetDate.frame)+5,CGRectGetMaxY(lblBetName.frame)-10,82,30)];
        
        UIFont *fnt;
        if(screenWidth<325)
        {
            fnt=[UIFont fontWithName:@"Avenir-Roman" size:13];
        }
        else
        {
            fnt=[UIFont fontWithName:@"Avenir-Roman" size:14];
        }
        viewForCell.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.6];
        lblBetName.text=[[_arrDashboardInfo valueForKey:@"name"]objectAtIndex:indexPath.row];
        NSString *strExpDate=[[_arrDashboardInfo valueForKey:@"expdate"]objectAtIndex:indexPath.row];
        
        NSDate *dateFromString = [[NSDate alloc] init];
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"yyyy/MM/dd"];
        
        dateFromString = [dateformate dateFromString:strExpDate];
        NSDateFormatter *dateformate1=[[NSDateFormatter alloc]init];
        [dateformate1 setDateFormat:@"dd/MM/yy"]; // Date formater
        strExpDate = [dateformate1 stringFromDate:dateFromString];
        
        NSLog(@"date :%@",strExpDate);
        lblBetDate.text=strExpDate;
        NSString *strVotes=@"Votes ";
        strVotes=[strVotes stringByAppendingString:[[_arrDashboardInfo valueForKey:@"vote"]objectAtIndex:indexPath.row]];
        lblBetVote.text=strVotes;
        
        lblBetDate.textColor=[UIColor whiteColor];
        lblBetName.textColor=[UIColor whiteColor];
        lblBetVote.textColor=[UIColor whiteColor];
        
        lblBetName.textAlignment=NSTextAlignmentLeft;
        lblBetDate.textAlignment=NSTextAlignmentLeft;
        lblBetVote.textAlignment=NSTextAlignmentLeft;
        
        lblBetName.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:15];
        lblBetDate.font=[UIFont fontWithName:@"Avenir-Roman" size:11];
        lblBetVote.font=[UIFont fontWithName:@"Avenir-Roman" size:11];
        
        [viewForCell addSubview:lblBetName];
        [viewForCell addSubview:lblBetDate];
        [viewForCell addSubview:lblBetVote];
        
        [cell.contentView addSubview:viewForCell];
        UIButton *btnShare=[[UIButton alloc]init];
        UIButton *btnSend=[[UIButton alloc]init];
        UIButton *btnBets=[[UIButton alloc]init];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            btnShare.frame=CGRectMake(viewForCell.frame.size.width-290,0,95,50);
            btnSend.frame=CGRectMake(CGRectGetMaxX(btnShare.frame)+5,0,95,50);
            btnBets.frame=CGRectMake(CGRectGetMaxX(btnSend.frame)+5,0,95,50);
        }
        else
        {
            if(screenWidth<325)
            {
                btnShare.frame=CGRectMake(viewForCell.frame.size.width-195,0,63,45);
                btnSend.frame=CGRectMake(CGRectGetMaxX(btnShare.frame)+3,0,63,45);
                btnBets.frame=CGRectMake(CGRectGetMaxX(btnSend.frame)+3,0,63,45);
            }
            else
            {
                btnShare.frame=CGRectMake(viewForCell.frame.size.width-230,0,75,45);
                btnSend.frame=CGRectMake(CGRectGetMaxX(btnShare.frame)+3,0,75,45);
                btnBets.frame=CGRectMake(CGRectGetMaxX(btnSend.frame)+3,0,75,45);
            }
        }
        
        [btnShare setBackgroundImage:[UIImage imageNamed:@"icon_ShareBG"] forState:UIControlStateNormal];
        [viewForCell addSubview:btnShare];
        
        [btnSend setBackgroundImage:[UIImage imageNamed:@"icon_ShareBG"] forState:UIControlStateNormal];
        [viewForCell addSubview:btnSend];
        [btnBets setBackgroundImage:[UIImage imageNamed:@"icon_ShareBG"] forState:UIControlStateNormal];
        [viewForCell addSubview:btnBets];
        
        [btnSend setTitle:@"Send" forState:UIControlStateNormal];
        [btnShare setTitle:@"Invite" forState:UIControlStateNormal];
        [btnBets setTitle:@"Responses" forState:UIControlStateNormal];
        
        [btnShare setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnSend setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnBets setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [btnShare addTarget:self action:@selector(btnSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        [btnSend addTarget:self action:@selector(btnSendPressed:) forControlEvents:UIControlEventTouchUpInside];
        [btnBets addTarget:self action:@selector(btnBetsPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        btnSend.titleLabel.font=fnt;
        btnShare.titleLabel.font=fnt;
        btnBets.titleLabel.font=fnt;
        
        [btnBets.titleLabel sizeToFit];
        btnBets.tag=indexPath.row;
        btnSend.tag=indexPath.row;
        btnShare.tag=indexPath.row;
        
        UIButton *btnMore=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-50,[UIScreen mainScreen].bounds.size.height/100*5,50,25)];
        [btnMore setBackgroundImage:[UIImage imageNamed:@"icon_More"] forState:UIControlStateNormal];
        [btnMore addTarget:self action:@selector(btnBetsPressed:) forControlEvents:UIControlEventTouchUpInside];
        btnMore.tag=indexPath.row;
        [cell.contentView addSubview:btnMore];
        return cell;
    }
    else if (tableView==_tblLeaders)
    {
        [self.view setUserInteractionEnabled:YES];
        UIImageView *imgCellBG=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,screenWidth,[UIScreen mainScreen].bounds.size.height/100*20)];
        imgCellBG.image=[UIImage imageNamed:@"icon_BG"];
        [cell.contentView addSubview:imgCellBG];
        
        UIImageView *imgCell=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,85,85)];
        
        NSString *strURL=[[_arrDashboardInfo valueForKey:@"profileimage"]objectAtIndex:indexPath.row];
        NSURL *imgURL;
        if ([strURL isEqualToString:@""])
        {
            imgCell.image=[UIImage imageNamed:@"profileIcon"];
            [cell.contentView addSubview:imgCell];
        }
        else if (strURL.length>0)
        {
            if ([strURL containsString:@"http://graph.facebook.com/"])
            {
               imgURL=[NSURL URLWithString:strURL];
            }
            else if ([strURL containsString:@"."])
            {
                NSString *strBase=ImgBase_URL;
                strBase=[strBase stringByAppendingString:strURL];
                imgURL=[NSURL URLWithString:strBase];
            }
            _asyImgBet=[[AsyncImageView alloc]initWithFrame:CGRectMake(0,0,100,85)];
            _asyImgBet.backgroundColor = [UIColor clearColor];
            [_asyImgBet loadImageFromURL:imgURL];
            [_asyImgBet setContentMode:UIViewContentModeScaleAspectFill];
            [_asyImgBet sizeToFit];
            _asyImgBet.clipsToBounds = YES;
            [cell.contentView addSubview:_asyImgBet];
        }
        
        UIView *viewForCell=[[UIView alloc]initWithFrame:CGRectMake(0,130-45,screenWidth,45)];
        UILabel *lblBetName=[[UILabel alloc]initWithFrame:CGRectMake(5,2,viewForCell.frame.size.width/2.5,30)];
        UILabel *lblBetDate=[[UILabel alloc]initWithFrame:CGRectMake(5,CGRectGetMaxY(lblBetName.frame)-10,200,30)];
        
        UIFont *fnt=[UIFont fontWithName:@"Avenir-Roman" size:15];
        
        viewForCell.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.6];
        lblBetName.text=[[_arrDashboardInfo valueForKey:@"name"]objectAtIndex:indexPath.row];
       NSInteger totVotes=[[[_arrDashboardInfo valueForKey:@"totalvote"]objectAtIndex:indexPath.row]integerValue];
        NSLog(@"%ld",(long)totVotes);
        
        NSString *strCompltedSnap=[NSString stringWithFormat:@"%ld",(long)totVotes];
        if([strCompltedSnap isKindOfClass:[NSNull class]])
        {
            strCompltedSnap=@"";
        }
        NSString *str=@"Total snapbets votes- ";
        str=[str stringByAppendingString:strCompltedSnap];
        lblBetDate.text=str;
        
        lblBetDate.textColor=[UIColor whiteColor];
        lblBetName.textColor=[UIColor whiteColor];
        
        lblBetName.textAlignment=NSTextAlignmentLeft;
        lblBetDate.textAlignment=NSTextAlignmentLeft;
        
        lblBetName.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:15];
        lblBetDate.font=[UIFont fontWithName:@"Avenir-Roman" size:11];
        
        [viewForCell addSubview:lblBetName];
        [viewForCell addSubview:lblBetDate];
        
        [cell.contentView addSubview:viewForCell];
        UIButton *btnView=[[UIButton alloc]init];
        UIButton *btnBet=[[UIButton alloc]init];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            btnView.frame=CGRectMake(viewForCell.frame.size.width-195,0,95,50);
            btnBet.frame=CGRectMake(CGRectGetMaxX(btnView.frame)+5,0,95,50);
        }
        else
        {
            if(screenWidth<325)
            {
                btnView.frame=CGRectMake(viewForCell.frame.size.width-123,0,60,45);
                btnBet.frame=CGRectMake(CGRectGetMaxX(btnView.frame)+3,0,60,45);
            }
            else
            {
                btnView.frame=CGRectMake(viewForCell.frame.size.width-153,0,75,45);
                btnBet.frame=CGRectMake(CGRectGetMaxX(btnView.frame)+3,0,75,45);
            }
        }
        
        [btnView setBackgroundImage:[UIImage imageNamed:@"icon_ShareBG"] forState:UIControlStateNormal];
        [viewForCell addSubview:btnView];
        
        [btnBet setBackgroundImage:[UIImage imageNamed:@"icon_ShareBG"] forState:UIControlStateNormal];
        [viewForCell addSubview:btnBet];
        
        [btnView setTitle:@"View" forState:UIControlStateNormal];
        [btnBet setTitle:@"Bets" forState:UIControlStateNormal];
        
        [btnView setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnBet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [btnView addTarget:self action:@selector(btnLedBetProfilePressed:) forControlEvents:UIControlEventTouchUpInside];
        [btnBet addTarget:self action:@selector(btnLeadrsBetPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        btnView.titleLabel.font=fnt;
        btnBet.titleLabel.font=fnt;
        
        btnView.tag=indexPath.row;
        btnBet.tag=indexPath.row;
        
        return cell;
    }
    else if (tableView==_tblUpgradeAcc)
    {
        UILabel *lblOptions=[[UILabel alloc]initWithFrame:CGRectMake(10,5,_viewForUpdateNow.frame.size.width-20,30)];
        UILabel *lblDescription=[[UILabel alloc]initWithFrame:CGRectMake(10,CGRectGetMaxY(lblOptions.frame)-5,screenWidth-20,0)];
        UIButton *btnUPgrade;
        
        if(screenWidth<325)
        {
            btnUPgrade=[[UIButton alloc]initWithFrame:CGRectMake(_viewForUpdateNow.frame.size.width/2-80,120,160,60)];
        }
        else
        {
            btnUPgrade=[[UIButton alloc]initWithFrame:CGRectMake(_viewForUpdateNow.frame.size.width/2-80,110,160,60)];
        }
        UILabel *lblUpgrade=[[UILabel alloc]initWithFrame:CGRectMake(0,2,btnUPgrade.frame.size.width,30)];
        UILabel *lblPrice=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblUpgrade.frame)-4,btnUPgrade.frame.size.width,30)];
        
        lblUpgrade.text=@"Upgrade Now";
        lblUpgrade.textColor=[UIColor whiteColor];
        lblPrice.textColor=[UIColor whiteColor];
        
        lblUpgrade.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        lblPrice.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        
        lblPrice.textAlignment=NSTextAlignmentCenter;
        lblUpgrade.textAlignment=NSTextAlignmentCenter;
        
        if (indexPath.row==0)
        {
            lblOptions.text=@"Option 1:$2.99 Subscription";
            lblDescription.text=@"Monthly subscription to Snapbets which includes one free Snapbets package a month and allow users to upload videos that are upto 90 seconds long.";
            lblPrice.text=@"$2.99";
        }
        else if (indexPath.row==1)
        {
            lblOptions.text=@"Option 2: $4.99 (Not a Subscription)";
            lblDescription.text=@"Do you have a lot to say? Want more video time? One time payment upgrade features 90 seconds of recording time";
            lblPrice.text=@"$4.99";
        }
        
        lblOptions.textColor=[UIColor whiteColor];
        lblOptions.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [cell.contentView addSubview:lblOptions];
        
        [lblDescription setNumberOfLines:0];
        [lblDescription sizeToFit];
        lblDescription.textColor=[UIColor whiteColor];
        lblDescription.font=[UIFont fontWithName:@"Avenir-Roman" size:14];
        [cell.contentView addSubview:lblDescription];
        
        [btnUPgrade setBackgroundImage:[UIImage imageNamed:@"img_BuySnap"] forState:UIControlStateNormal];
        [cell.contentView addSubview:btnUPgrade];
        [btnUPgrade addSubview:lblUpgrade];
        btnUPgrade.tag=indexPath.row;
        [btnUPgrade addTarget:self action:@selector(btnbuyInAppPressed:) forControlEvents:UIControlEventTouchUpInside];
        [btnUPgrade addSubview:lblPrice];
        return cell;
    }
    return nil;
}

#pragma Mark-delegate methods
//--------------------- Tableview didselected index ---------------------//
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblMenu)
    {
        return 60;
    }
    else if (tableView==_tblLeaders)
    {
        return 130;
    }
    else if (tableView==_tblUpgradeAcc)
    {
        if(screenWidth<325)
        {
            if(indexPath.row==1)
            {
                return 220;
            }
            else
            {
                return 190;
            }
        }
        else
        {
            if(indexPath.row==1)
            {
                return 210;
            }
            else
            {
                return 180;
            }
        }
    }
    else
    {
        return [UIScreen mainScreen].bounds.size.height/100*20;
    }
    return 50;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblMenu)
    {
        CGRect frame = cell.frame;
        [cell setFrame:CGRectMake(0,0,screenWidth,60)];
        [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve  animations:^{
            [cell setFrame:frame];
        } completion:^(BOOL finished) {
        }];
    }
}

#pragma mark: User SignIn Server Call
-(void)MainDashboardDataAPI
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isCheckProfileImage=NO;
        _btnMenu.hidden=YES;
         [self.view setUserInteractionEnabled:NO];
        if (theAppDelegate.isPoolRefresh) {
            theAppDelegate.isPoolRefresh=NO;
        }
        else
        {
            [theAppDelegate showSpinnerInView:self.view];
        }
        
       
        NSString *urlString;
        NSString *strBaseURL=base_URL;
        if (_isFeedData)
        {
            _strCurrentFeed=@"My Feed";
            urlString =[NSString stringWithFormat:@"wsfeeds.php?id=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0]];
        }
        else if (_isTrendingData)
        {
            _strCurrentFeed=@"Trending";
            urlString=[NSString stringWithFormat:@"wstending.php"];
        }
        else if (_isMyBetsData)
        {
            _strCurrentFeed=@"My Beet";
            urlString=[NSString stringWithFormat:@"wsmybets.php?id=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0]];
        }
        else if (_isLeadersData)
        {
            _strCurrentFeed=@"Leaders";
            urlString=[NSString stringWithFormat:@"wsleaders.php"];
        }
        else
        {
           urlString=[NSString stringWithFormat:@"wsgetActiveBeats.php?id=%@",strLedSetcedBetID];
        }
        
        strBaseURL=[strBaseURL stringByAppendingString:urlString];
        NSURL *urlAllBets=[NSURL URLWithString:strBaseURL];
        request=[NSMutableURLRequest requestWithURL:[urlAllBets standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
        request = nil;
    }
    
}


#pragma mark: User SignIn Server Call
-(void)serverCallToCheckProfileImage
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isCheckProfileImage=YES;
        [theAppDelegate showSpinnerInView:self.view];
        //[self.view setUserInteractionEnabled:NO];
        
        NSString *strBaseURL=base_URL;
        NSString *urlString;
        urlString=[NSString stringWithFormat:@"wsviewprofile.php?id=%@",[theAppDelegate.arrUserLoginData  objectAtIndex:0]];
        strBaseURL=[strBaseURL stringByAppendingString:urlString];
        NSURL *urlProfile=[NSURL URLWithString:strBaseURL];
        request=[NSMutableURLRequest requestWithURL:[urlProfile standardizedURL]];
    }
    if(!connection)
        connection= [[WebConnection1 alloc] init];
    connection.delegate = self;
    [connection makeConnection:request];
    request = nil;
}

#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    CGFloat XPosition=_btnProfile.frame.origin.x;
    CGFloat YPosition=_btnProfile.frame.origin.y;
    [_asyImgProfile removeFromSuperview];
    _asyImgProfile=[[AsyncImageView alloc]initWithFrame:CGRectMake(XPosition,YPosition,_btnProfile.frame.size.width,_btnProfile.frame.size.height)];
    
    NSString *strCheckLogin=[theAppDelegate.arrUserLoginData objectAtIndex:7];
    _asyImgProfile.backgroundColor = [UIColor clearColor];
    _objDBManager=[DBManager getSharedInstance];
    NSArray *arrDBData=[_objDBManager selectTableDataWithQuery:@"select *from tblUser"];
    
    if ([arrDBData count])
    {
        theAppDelegate.arrUserLoginData=[[NSMutableArray alloc]init];
        theAppDelegate.arrUserLoginData=[[arrDBData objectAtIndex:0] mutableCopy];
    }
    if ([strCheckLogin isEqualToString:@"No"])
    {
        NSString *strBase;
        
        theAppDelegate.arrUserLoginData=[[NSMutableArray alloc]init];
        theAppDelegate.arrUserLoginData=[[arrDBData objectAtIndex:0] mutableCopy];
        
        strBase=[theAppDelegate.arrUserLoginData objectAtIndex:5];
        [_asyImgProfile loadImageFromURL:[NSURL URLWithString:strBase]];
        [_btnProfile setBackgroundColor:[UIColor clearColor]];
        
        if ([[theAppDelegate.arrUserLoginData objectAtIndex:5] isEqualToString:@"NO"])
        {
            [_btnProfile setBackgroundImage:[UIImage imageNamed:@"icon_prof1"] forState:UIControlStateNormal];
        }
        else
        {
            [_btnProfile setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        }
    }
    else if ([strCheckLogin isEqualToString:@"Yes"])
    {
        NSString *strImg=[theAppDelegate.arrUserLoginData objectAtIndex:5];
        @try {
            [_btnProfile setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            if([strImg containsString:@"http://graph.facebook.com/"])
            {
                NSArray *arr=[strImg componentsSeparatedByString:@"http://graph.facebook.com/"];
                NSLog(@"%@",arr);
                NSString *strString=[arr objectAtIndex:1];
                NSArray *arr1=[strString componentsSeparatedByString:@"/"];
                NSLog(@"%@",arr1);
                strImg=[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?width=500&height=500",[arr1 objectAtIndex:0]];
            }
            [_asyImgProfile loadImageFromURL:[NSURL URLWithString:strImg]];
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        [_btnProfile setBackgroundColor:[UIColor clearColor]];
    }
    [_asyImgProfile setContentMode:UIViewContentModeScaleAspectFill];
    [_asyImgProfile sizeToFit];
    _asyImgProfile.layer.cornerRadius=4.0;
    _asyImgProfile.clipsToBounds = YES;
    
    [self.view addSubview:_asyImgProfile];
    [self.view bringSubviewToFront:_btnProfile];
    
    dict=[[NSMutableDictionary alloc]init];
    dict =[[NSMutableDictionary alloc]initWithDictionary:response];
    NSString *strMessage=[dict valueForKey:@"message"];
    NSLog(@"%@",dict);
    
    if(_isCheckProfileImage)
    {
        if ([strMessage isEqualToString:@"sus"])
        {
            NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
            arrTemp=[dict valueForKey:@"info"];
            NSString *strImg=[arrTemp valueForKey:@"profileimage"];
            
            if ([strImg containsString:@"http://graph.facebook.com/"])
            {
                NSLog(@"Found!!");
                
                if ([strImg isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:5]]) {
                    
                }
                else
                {
                    [_asyImgProfile loadImageFromURL:[NSURL URLWithString:strImg]];
                     [self.view bringSubviewToFront:_viewForUpgradeAccount];
                    _objDBManager=[DBManager getSharedInstance];
                    
                    NSString*update=[NSString stringWithFormat:@"update tblUser Set IMAGE='%@' Where UID='%@'",strImg,[theAppDelegate.arrUserLoginData objectAtIndex:0]];
                    
                    BOOL check=[_objDBManager updateDataWithQuery:update];
                    if (check)
                    {
                        NSLog(@"Update to Database");
                    }
                    else
                    {
                        NSLog(@"Failed to update Profile Image");
                    }
                }
            }
            else if ([strImg containsString:@"."])
            {
                NSString *strBase=ImgBase_URL;
                strBase=[strBase stringByAppendingString:strImg];
                
                if ([strBase isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:5]])
                {
                    
                }
                else
                {
                    [_asyImgProfile loadImageFromURL:[NSURL URLWithString:strBase]];
                     [self.view bringSubviewToFront:_viewForUpgradeAccount];
                    _objDBManager=[DBManager getSharedInstance];
                    
                    NSString*update=[NSString stringWithFormat:@"update tblUser Set IMAGE='%@' Where UID='%@'",strBase,[theAppDelegate.arrUserLoginData objectAtIndex:0]];
                    
                    BOOL check=[_objDBManager updateDataWithQuery:update];
                    if (check) {
                        NSLog(@"Update to Database");
                        
                    }
                    else
                    {
                        NSLog(@"Failed to update Profile Image");
                    }
                }
                
            }
            
            NSLog(@"%@",arrTemp);
        }
    }
    else
    {
        if (_isMyBetsData)
        {
            [_tblLeaders removeFromSuperview];
            if ([strMessage isEqualToString:@"sus"])
            {
                theAppDelegate.isMyBeetSelected=YES;
                _arrDashboardInfo=[[NSMutableArray alloc]init];
                _arrDashboardInfo=[dict valueForKey:@"info"];
                
                if ([_arrDashboardInfo isKindOfClass:[NSNull class]])
                {
                    _arrDashboardInfo=[[NSMutableArray alloc]init];
                }
                NSLog(@"%@",_arrDashboardInfo);
                [self viewForTableDeclarationsHere];
                [self.view bringSubviewToFront:_viewForUpgradeAccount];
                [self.view bringSubviewToFront:_viewForTutorial];
            }
            else
            {
                _arrDashboardInfo=[[NSMutableArray alloc]init];
                _arrDashboardInfo=[dict valueForKey:@"info"];
                [self viewForTableDeclarationsHere];
                 [self.view bringSubviewToFront:_viewForUpgradeAccount];
                [self.view bringSubviewToFront:_viewForTutorial];
            }
            [self serverCallToCheckProfileImage];
           //[self performSelector:@selector(showMenuButton) withObject:nil afterDelay:0.5];
        }
        else if(_isLeadersData)
        {
            if ([strMessage isEqualToString:@"sus"])
            {
                theAppDelegate.isMyBeetSelected=NO;
                _arrDashboardInfo=[[NSMutableArray alloc]init];
                _arrDashboardInfo=[dict valueForKey:@"info"];
                if ([_arrDashboardInfo isKindOfClass:[NSNull class]])
                {
                    _arrDashboardInfo=[[NSMutableArray alloc]init];
                }
                NSLog(@"%@",_arrDashboardInfo);
                
                [self viewForLeadersTable];
                [self.view bringSubviewToFront:_viewForUpgradeAccount];
                [self.view bringSubviewToFront:_viewForTutorial];
            }
            else
            {
                _arrDashboardInfo=[[NSMutableArray alloc]init];
                _arrDashboardInfo=[dict valueForKey:@"info"];
                [self viewForLeadersTable];
            }
            [self serverCallToCheckProfileImage];
            
        }
        else
        {
            theAppDelegate.isMyBeetSelected=NO;
            [_tblLeaders removeFromSuperview];
            if ([strMessage isEqualToString:@"sus"])
            {
                _arrDashboardInfo=[[NSMutableArray alloc]init];
                _arrDashboardInfo=[dict valueForKey:@"info"];
                if ([_arrDashboardInfo isKindOfClass:[NSNull class]])
                {
                    _arrDashboardInfo=[[NSMutableArray alloc]init];
                }
                [self viewForTableDeclarationsHere];
             [self.view bringSubviewToFront:_viewForUpgradeAccount];
                if (theAppDelegate.isFirstTime == YES)
                {
                    theAppDelegate.isFirstTime=NO;
                    if (neverShowAgain == nil)
                    {
                        //[self viewForUpgradeAccountPopUpUI];
                    }
                    else
                    {
                    
                    }
                }
            }
            else
            {
                _arrDashboardInfo=[[NSMutableArray alloc]init];
                [self viewForTableDeclarationsHere];
            
                if (theAppDelegate.isFirstTime == YES)
                {
                    theAppDelegate.isFirstTime=NO;
                    if (neverShowAgain == nil)
                    {
                        [self viewForUpgradeAccountPopUpUI];
                    }
                    else
                    {
                    
                    }
                }
             }
            [self serverCallToCheckProfileImage];
            //[self performSelector:@selector(showMenuButton) withObject:nil afterDelay:0.5];
        }
    }
     [self.view bringSubviewToFront:_viewForUpgradeAccount];
        myidleTimer =[NSTimer scheduledTimerWithTimeInterval:kMaxIdleTimeSeconds
                                                  target:self
                                                selector:@selector(idleTimerExceeded)
                                                userInfo:nil
                                                 repeats:NO];
    [self performSelector:@selector(showMenuButton) withObject:nil afterDelay:0.8];
}

-(void)showMenuButton
{
     [theAppDelegate stopSpinner];
    [self.view setUserInteractionEnabled:YES];
    _btnMenu.hidden=NO;
    _btnFeed.userInteractionEnabled=YES;
    _btnMyBets.userInteractionEnabled=YES;
    _btnTrending.userInteractionEnabled=YES;
    _btnLeaders.userInteractionEnabled=YES;
}
#pragma mark Help View
-(void)showHelpView
{
    [_viewForTutorial removeFromSuperview];
    [_btnClose removeFromSuperview];
    [_btnNext1 removeFromSuperview];
    _viewForTutorial =[[UIView alloc]initWithFrame:CGRectMake(0,0,screenWidth,screenHeight)];
    _viewForTutorial.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.0];
    _viewForTutorial.alpha=1;
    _viewForTutorial.tag=20;
    [self.view addSubview:_viewForTutorial];
    
    CGRect frameForViewForHelp=_viewForTutorial.frame;
    frameForViewForHelp=CGRectMake(0,0, screenWidth, screenHeight);
    
    [UIView animateWithDuration:0.5f animations:^{
        _viewForTutorial.frame=frameForViewForHelp;
    } completion:^(BOOL finished)
     {
         vscreenSize=_viewForTutorial.frame;
         vscreenHeight=vscreenSize.size.height;
         vscreenWidth=vscreenSize.size.width;
         
         self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
         
         self.pageController.dataSource = self;
         [[self.pageController view] setFrame:CGRectMake(0,10,_viewForTutorial.frame.size.width,_viewForTutorial.frame.size.height)];
         
         APPChildViewController *initialViewController = [self viewControllerAtIndex:0];
         NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
         [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
         [self addChildViewController:self.pageController];
         [_viewForTutorial addSubview:[self.pageController view]];
         [self.pageController didMoveToParentViewController:self];
         
         _btnNext1=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-80,screenHeight-40,70,30)];
         [_btnNext1 setTitle:@"Next" forState:UIControlStateNormal];
         
         _btnClose=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth/2-70,screenHeight-60,140,50)];
         [_btnClose setTitle:@"Next" forState:UIControlStateNormal];
         _btnClose.backgroundColor=[UIColor orangeColor];
         
         [_btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         _btnClose.layer.borderColor=[UIColor orangeColor].CGColor;
         _btnClose.layer.borderWidth=2;
         
         self.btnClose.layer.cornerRadius =4.0;
         self.btnClose.clipsToBounds = YES;
         [_btnClose addTarget:self action:@selector(btnNextPressed) forControlEvents:UIControlEventTouchUpInside];
         [self.view addSubview:_btnClose];
         
         [_btnNext1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         _btnNext1.layer.borderColor=[UIColor orangeColor].CGColor;
         _btnNext1.layer.borderWidth=2;
         _btnNext1.backgroundColor=[UIColor orangeColor];
         self.btnNext1.layer.cornerRadius = 2;
         self.btnNext1.clipsToBounds = YES;
         [_btnNext1 addTarget:self action:@selector(btnNextPressed) forControlEvents:UIControlEventTouchUpInside];
         [_btnNext1 setUserInteractionEnabled:YES];
         // [_viewForTutorial bringSubviewToFront:_btnNext1];
         // [_viewForTutorial addSubview:_btnNext1];
         
         //  UIView *currentView=[self.view viewWithTag:447];
         // currentView.alpha=1;
         
     }];
    
    // [self disableSubViews:_viewForTutorial];
}
-(void)closeHelpView
{
    for(UIView *subview in [_viewForTutorial subviews])
    {
        [subview removeFromSuperview];
        [self.btnClose removeFromSuperview];
        [_btnNext1 removeFromSuperview];
    }
    
    CGRect frameForViewForHelp=_viewForTutorial.frame;
    frameForViewForHelp=CGRectMake(0, screenHeight/2,screenWidth,0);
    
    [UIView animateWithDuration:0.5f animations:^{
        _viewForTutorial.frame=frameForViewForHelp;
    } completion:^(BOOL finished)
     {
         [_viewForTutorial removeFromSuperview];
         [self enableSubViews];
     }];
}

-(UIViewController*)randomVC
{
    UIViewController *vc = [[UIViewController alloc] init];
    UIColor *color = [UIColor colorWithRed:arc4random_uniform(255)/255.0 green:arc4random_uniform(255)/255.0 blue:arc4random_uniform(255)/255.0 alpha:1];
    vc.view.backgroundColor = color;
    return vc;
}

-(void)btnNextPressed
{
    for(UIView *subview in [_viewForTutorial subviews])
    {
        NSLog(@"%@",subview);
        
        NSLog(@"Button Index------------------------------%ld",(long)pageViewIndex);
    }
    
    CGRect frameForViewForHelp=_viewForTutorial.frame;
    
    [UIView animateWithDuration:0.5f animations:^{
        _viewForTutorial.frame=frameForViewForHelp;
    } completion:^(BOOL finished)
     {
         if (pageViewIndex < 4)
         {
             pageViewIndex =pageViewIndex+1;
         }
         else
         {
             if (pageViewIndex == 4)
             {
                 // [_btnNext removeFromSuperview];
                 
                 /* _btnFinish=[[UIButton alloc]initWithFrame:CGRectMake(80, screenHeight-90,screenWidth-160, 40)];
                  [_btnFinish setTitle:@"Finish" forState:UIControlStateNormal];
                  [_btnFinish addTarget:self action:@selector(btnFinishPressed) forControlEvents:UIControlEventTouchUpInside];
                  _btnFinish.layer.cornerRadius=18;
                  _btnFinish.layer.masksToBounds=YES;
                  _btnFinish.titleLabel.font=[UIFont fontWithName:@"OpenSans" size:13];
                  _btnFinish.backgroundColor=[UIColor colorWithRed:145/255.0 green:169/255.0 blue:222/255.0 alpha:1.0];
                  [self.view addSubview:_btnFinish];*/
             }
         }
         APPChildViewController *startingViewController = [self viewControllerAtIndex:pageViewIndex];
         NSArray *viewControllers = @[startingViewController];
         [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
     }];
    
}
#pragma mark- Page View Methods

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    
    NSUInteger index = [(APPChildViewController *)viewController index];
    if (index == 0) {
        return nil;
    }
    index--;
    pageViewIndex=index;
    if (index == 5)
    {
        [[self.pageController view] setFrame:CGRectMake(0,0,_viewForTutorial.frame.size.width,_viewForTutorial.frame.size.height-100)];[_btnClose setTitle:@"Proceed to app" forState:UIControlStateNormal];
        [_btnClose addTarget:self action:@selector(closeHelpView) forControlEvents:UIControlEventTouchUpInside];
        UIImageView *img;
        [img removeFromSuperview];
        img.frame=CGRectMake(0,0,screenWidth,screenHeight);
        img.image=[UIImage imageNamed:@"imgBG_Temp"];
        [_viewForTutorial addSubview:img];
        return nil;
    }
    else
    {
        [[self.pageController view] setFrame:CGRectMake(0,0,_viewForTutorial.frame.size.width,_viewForTutorial.frame.size.height)];
        [_btnClose setTitle:@"Next" forState:UIControlStateNormal];
        [_btnClose addTarget:self action:@selector(btnNextPressed) forControlEvents:UIControlEventTouchUpInside];
    }
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [(APPChildViewController *)viewController index];
    index++;
    pageViewIndex=index;
    if (index == 5)
    {
        [[self.pageController view] setFrame:CGRectMake(0,0,_viewForTutorial.frame.size.width,[UIScreen mainScreen].bounds.size.height/100*85)];
        [_btnClose setTitle:@"Proceed to app" forState:UIControlStateNormal];
        [_btnClose addTarget:self action:@selector(closeHelpView) forControlEvents:UIControlEventTouchUpInside];
        UIImageView *img;
        [img removeFromSuperview];
        img.frame=CGRectMake(0,0,screenWidth,screenHeight);
        img.image=[UIImage imageNamed:@"imgBG_Temp"];
        [_viewForTutorial addSubview:img];
        [_viewForTutTemp removeFromSuperview];
        _viewForTutTemp=[[UIView alloc]initWithFrame:CGRectMake(0,[UIScreen mainScreen].bounds.size.height/100*85,[UIScreen mainScreen].bounds.size.width/100*75,[UIScreen mainScreen].bounds.size.height/100*15)];
        _viewForTutTemp.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"icon_Transprnt"]];
        [_viewForTutorial addSubview:_viewForTutTemp];
        return nil;
    }
    else
    {
        [_viewForTutTemp removeFromSuperview];
        [[self.pageController view] setFrame:CGRectMake(0,0,_viewForTutorial.frame.size.width,_viewForTutorial.frame.size.height)];
        [_btnClose setTitle:@"Next" forState:UIControlStateNormal];
        [_btnClose addTarget:self action:@selector(btnNextPressed) forControlEvents:UIControlEventTouchUpInside];
    }
    return [self viewControllerAtIndex:index];
}
- (APPChildViewController *)viewControllerAtIndex:(NSUInteger)index
{
    curntIndexPge=index;
    APPChildViewController *cvc = [[APPChildViewController alloc] initWithNibName:@"APPChildViewController" bundle:nil];
    UIPageControl *pageControlAppearance = [UIPageControl appearanceWhenContainedIn:[UIPageViewController class], nil];
    pageControlAppearance.pageIndicatorTintColor = [UIColor darkGrayColor];
    pageControlAppearance.currentPageIndicatorTintColor = [UIColor whiteColor];
    
    [[self.pageController view] setFrame:CGRectMake(0,0,_viewForTutorial.frame.size.width,_viewForTutorial.frame.size.height)];
    [_viewForTutTemp removeFromSuperview];
    if (index==0)
    {
        [_viewForTutTemp removeFromSuperview];
        [_btnClose setTitle:@"Next" forState:UIControlStateNormal];
        [_btnClose addTarget:self action:@selector(btnNextPressed) forControlEvents:UIControlEventTouchUpInside];
        cvc.strImgName=@"tt_1";
    }
    else if (index==1)
    {
        [_viewForTutTemp removeFromSuperview];
        [_btnClose setTitle:@"Next" forState:UIControlStateNormal];
        [_btnClose addTarget:self action:@selector(btnNextPressed) forControlEvents:UIControlEventTouchUpInside];
        cvc.strImgName=@"tutorial1";
    }
    else if (index==2)
    {
        [_viewForTutTemp removeFromSuperview];
        [_btnClose setTitle:@"Next" forState:UIControlStateNormal];
        [_btnClose addTarget:self action:@selector(btnNextPressed) forControlEvents:UIControlEventTouchUpInside];
        cvc.strImgName=@"tutorial2";
    }
    else if (index==3)
    {
        [_viewForTutTemp removeFromSuperview];
        [_btnClose setTitle:@"Next" forState:UIControlStateNormal];
        [_btnClose addTarget:self action:@selector(btnNextPressed) forControlEvents:UIControlEventTouchUpInside];
        cvc.strImgName=@"tutorial3";
    }
    else if(index==4)
    {
        [[self.pageController view] setFrame:CGRectMake(0,0,_viewForTutorial.frame.size.width,[UIScreen mainScreen].bounds.size.height/100*85)];[_btnClose setTitle:@"Proceed to app" forState:UIControlStateNormal];
        [_btnClose addTarget:self action:@selector(closeHelpView) forControlEvents:UIControlEventTouchUpInside];
        [_viewForTutTemp removeFromSuperview];
        _viewForTutTemp=[[UIView alloc]initWithFrame:CGRectMake(0,[UIScreen mainScreen].bounds.size.height/100*84.9,[UIScreen mainScreen].bounds.size.width/100*75,[UIScreen mainScreen].bounds.size.height/100*15.8)];
        _viewForTutTemp.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"icon_Transprnt"]];
        [_viewForTutorial addSubview:_viewForTutTemp];
        cvc.strImgName=@"tutorial4";
    }
        cvc.index = index;
    return cvc;
}


- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    // The number of items reflected in the page indicator.
    return 0;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}

#pragma mark: InApp Purchases product loading methods
- (void)productsLoaded:(NSNotification *)notification
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
}

- (void)productPurchased:(NSNotification *)notification
{
    _objDBManager=[DBManager getSharedInstance];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    NSString *productIdentifier = (NSString *) notification.object;
    NSLog(@"Purchased: %@", productIdentifier);
    NSDate *currDate = [NSDate date];
    NSDateFormatter *tempDateFormatter = [[NSDateFormatter alloc]init];
    [tempDateFormatter setDateFormat:@"dd-MMM-yy"];
    NSString *strMStartDate= [tempDateFormatter stringFromDate:currDate];
    NSLog(@"%@",strMStartDate);
    NSString *strExpireDate;
    
    if ([notification.name isEqualToString:@"ProductPurchased"])
    {
        NSDate *currDate = [NSDate date];
        NSDateFormatter *tempDateFormatter = [[NSDateFormatter alloc]init];
        [tempDateFormatter setDateFormat:@"dd-MMM-yy"];
        strMStartDate= [tempDateFormatter stringFromDate:currDate];
        
        NSDate *expiryDate=[currDate dateByAddingTimeInterval:30*(60*60*24)];
        strExpireDate=[tempDateFormatter stringFromDate:expiryDate];
        NSLog(@"%@",strExpireDate);
        
        NSString *insertQuery;
        
        if ([productIdentifier isEqualToString:@"com.Snapbets_2"])
        {
            NSMutableURLRequest *request;            
            NSHTTPURLResponse* response =[[NSHTTPURLResponse alloc] init];
            NSError* error = [[NSError alloc] init] ;
            NSString *strUrlString=[NSString stringWithFormat:@"http://54.67.95.152/snapbets/aws/wssubscription.php?uid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0]];
            
            strUrlString=[strUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSURL *url = [NSURL URLWithString:strUrlString];
            request=[NSMutableURLRequest requestWithURL:[url standardizedURL]];
            //synchronous filling of data from HTTP POST response
            NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            NSString *responseString;
            //convert data into string
            @try {
                responseString = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];
                
                NSLog(@"Response String %@",responseString);
                if ([responseString isKindOfClass:[NSNull class]]||[responseString isEqualToString:@"nil"]) {
                    
                }
                else
                {
                    NSError *error1;
                    NSDictionary *mainDict=[NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error1];
                    
                    NSString *strMsg=[mainDict valueForKey:@"message"];
                    NSLog(@"%@",strMsg);
                 
                }
            } @catch (NSException *exception) {
                
            } @finally {
                
            }

            
           // http://54.67.95.152/snapbets/aws/wssubscription.php?uid=6
            insertQuery=[NSString stringWithFormat:@"insert into tblInApp values('%@','%@','One Month','%@','%@')",[theAppDelegate.arrUserLoginData objectAtIndex:0],productIdentifier,strMStartDate,strExpireDate];
           
            if ([_objDBManager insertDataWithQuery:insertQuery])
            {
                
            }
            else
            {
                
            }
        }
        else
        {
            insertQuery=[NSString stringWithFormat:@"insert into tblInApp values('%@','%@','one time','%@','No Expiry')",[theAppDelegate.arrUserLoginData objectAtIndex:0],productIdentifier,strMStartDate];
            
            if ([_objDBManager insertDataWithQuery:insertQuery])
            {
                
            }
            else
            {
                
            }
        }
    }
    [self performSelector:@selector(dismissHUD:) withObject:nil afterDelay:0.0];
}

- (void)productPurchaseFailed:(NSNotification *)notification
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    
    SKPaymentTransaction * transaction = (SKPaymentTransaction *) notification.object;
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        theAppDelegate.alert.title=@"Error";
        theAppDelegate.alert.message=transaction.error.localizedDescription;
        theAppDelegate.alert.show;
        [self performSelector:@selector(dismissHUD:) withObject:nil afterDelay:0.0];
    }
}

- (void)timeout:(id)arg
{
    hud.labelText =@"Timeout!";
    hud.detailsLabelText =@"Please try again later.";
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
    hud.mode = MBProgressHUDModeCustomView;
    [self performSelector:@selector(dismissHUD:) withObject:nil afterDelay:3.0];
    [self performSelector:@selector(showErrorMessage) withObject:nil afterDelay:3.0];
}

- (void)dismissHUD:(id)arg
{
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    hud = nil;
}

- (void) showErrorMessage
{
    theAppDelegate.alert.title=@"Error";
    theAppDelegate.alert.message=@"Please try again later";
    theAppDelegate.alert.show;
}

#pragma mark-Get Data From Database
-(void)getDataFromDB
{
    _objDBManager=[DBManager getSharedInstance];
    NSString *selectQuery;
    NSMutableArray *arrTemp1=[[NSMutableArray alloc]init];
    NSArray *arrTemp=[[NSArray alloc] init];
    arrProductIDs=[[NSMutableArray alloc]init];
    
    selectQuery=[NSString stringWithFormat:@"select * from tblInApp where UID='%@'",[theAppDelegate.arrUserLoginData objectAtIndex:0]];
    arrTemp=[_objDBManager selectTableDataWithQuery:selectQuery];
    if ([arrTemp count])
    {
        for (int i=0; i<[arrTemp count]; i++)
        {
            arrTemp1=[arrTemp objectAtIndex:i];
            NSString *productName=[arrTemp1 objectAtIndex:1];
            [arrProductIDs addObject:productName];
        }
        
    }
}

-(void)CheckUStatusBackgroundAPI
{
    NSString *strUID=[theAppDelegate.arrUserLoginData objectAtIndex:0];
    
    NSMutableURLRequest *request;
    NSString *strBaseURL=base_URL;
    NSString *urlString=[NSString stringWithFormat:@"wschecklogin.php?id=%@",strUID];
    
    strBaseURL=[strBaseURL stringByAppendingString:urlString];
    strBaseURL=[strBaseURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *url = [NSURL URLWithString:strBaseURL];
    request=[NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    _ConnUStatus=[[NSURLConnection alloc]initWithRequest:request delegate:self];

}
#pragma mark- Connection  Methods
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _webdata =[[NSMutableData alloc] init];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_webdata appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    @try {
        NSError *error;
        _dictUStatus=[NSJSONSerialization JSONObjectWithData:_webdata options:NSJSONReadingMutableContainers error:&error];
        NSString *strMSG=[_dictUStatus valueForKey:@"message"];
        if([strMSG isEqualToString:@"sus"])
        {
            NSArray *arr=[_dictUStatus valueForKey:@"info"];
            NSString *strUStatus=[arr valueForKey:@"block"];
            
            if ([strUStatus isEqualToString:@"N"]) {
                if (theAppDelegate.isAppBackground) {
                    NSLog(@"Background");
                }
                else
                {
                    
                }
            }
            else
            {
                [Ntimer invalidate];
                if (theAppDelegate.isAppBackground) {
                    NSLog(@"User Blocked");
                    NSArray *arrDBTemp=[_objDBManager selectTableDataWithQuery:@"select *from tblUser"];
                    
                    if ([arrDBTemp count])
                    {
                        [_objDBManager deleteFromTableWithQuery:@"delete from tblUser"];
                    }
                    
                }
                else
                {
                    NSArray *arrDBTemp=[_objDBManager selectTableDataWithQuery:@"select *from tblUser"];
                    
                    if ([arrDBTemp count])
                    {
                        [_objDBManager deleteFromTableWithQuery:@"delete from tblUser"];
                    }
                    theAppDelegate.isUserBlocked=YES;
                    _objDBManager=[DBManager getSharedInstance];
                    theAppDelegate.isStartFromAppDelegate=YES;
                    [_objDBManager deleteFromTableWithQuery:@"delete from tblUser"];
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    ViewController *vc=[storyboard instantiateViewControllerWithIdentifier:@"Loginview"];
                    UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:vc];
                    [self presentViewController:nav animated:YES completion:nil];
                    [self viewForBlockedAlertUI];
                }
                
            }
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }    
}

#pragma mark: view For Block User Alert
-(void)viewForBlockedAlertUI
{
    [_viewForBlockedAlert removeFromSuperview];
    _viewForBlockedAlert =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForBlockedAlert.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForBlockedAlert.tag=20;
    [self.view addSubview:_viewForBlockedAlert];
    
    CGRect frameForViewForHelp=_viewForBlockedAlert.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForBlockedAlert.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForBlockedAlert.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForBlockedAlert.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForBlockedAlert addSubview:img];
        
        UILabel *lblAlert=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForBlockedAlert.frame.size.width,30)];
        lblAlert.text=@"Your account has blocked";
        lblAlert.textAlignment=NSTextAlignmentCenter;
        lblAlert.textColor=[UIColor whiteColor];
        lblAlert.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:18];
        //[lblAlert sizeToFit];
        [_viewForBlockedAlert addSubview:lblAlert];
        
        UILabel *lblAlert1=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblAlert.frame)+5,_viewForBlockedAlert.frame.size.width,30)];
        lblAlert1.text=@"Please contact to admin";
        lblAlert1.textAlignment=NSTextAlignmentCenter;
        lblAlert1.textColor=[UIColor whiteColor];
        lblAlert1.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        //[lblAlert1 sizeToFit];
        [_viewForBlockedAlert addSubview:lblAlert1];
        
        _btnCloseAlert=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseAlert.frame=CGRectMake(_viewForBlockedAlert.frame.size.width/2-60,CGRectGetMaxY(lblAlert1.frame)+50,120,40);
        }
        else
        {
            _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForBlockedAlert.frame.size.width/2-50,CGRectGetMaxY(lblAlert1.frame)+50,100,40)];
        }
        
        [_btnCloseAlert setTitle:@"Close" forState:UIControlStateNormal];
        
        
        [_btnCloseAlert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseAlert.layer.borderWidth=2.0;
        _btnCloseAlert.layer.cornerRadius=4.0;
        _btnCloseAlert.layer.borderColor=[[UIColor orangeColor]CGColor];
        //[_btnVoteSeeMore addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [_btnCloseAlert addTarget:self action:@selector(viewForCloseAlert1) forControlEvents:UIControlEventTouchUpInside];
        [_viewForBlockedAlert addSubview:_btnCloseAlert];
    }];
}

#pragma mark-Close Comment View
-(void)viewForCloseAlert1
{
    @try
    {
        for(UIView *subview in [_viewForBlockedAlert subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForVote=_viewForBlockedAlert.frame;
        frameForViewForVote=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            _viewForBlockedAlert.frame=frameForViewForVote;
        } completion:^(BOOL finished){
            
            NSArray *arrDBTemp=[_objDBManager selectTableDataWithQuery:@"select *from tblUser"];
            
            if ([arrDBTemp count])
            {
                [_objDBManager deleteFromTableWithQuery:@"delete from tblUser"];
            }
            [_viewForBlockedAlert removeFromSuperview];
            
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark: view For Share
-(void)viewForNotiUI
{
    [_viewForNotificationUI removeFromSuperview];
    _viewForNotificationUI =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForNotificationUI.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForNotificationUI.tag=20;
    [self.view addSubview:_viewForNotificationUI];
    
    CGRect frameForViewForHelp=_viewForNotificationUI.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForNotificationUI.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForNotificationUI.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForNotificationUI.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForNotificationUI addSubview:img];
        UILabel *lblShare=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForNotificationUI.frame.size.width,30)];
        lblShare.text=_strNotiAlertTitle;
        lblShare.textAlignment=NSTextAlignmentCenter;
        lblShare.textColor=[UIColor whiteColor];
        lblShare.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForNotificationUI addSubview:lblShare];
        
        _btnVoteSeeMore=[[UIButton alloc]init];
        _btnVoteClose=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnVoteSeeMore.frame=CGRectMake(70,CGRectGetMaxY(lblShare.frame)+50,_viewForNotificationUI.frame.size.width/2-140,40);
            _btnVoteClose.frame=CGRectMake(_viewForNotificationUI.frame.size.width/2-60,CGRectGetMaxY(lblShare.frame)+50,120,40);
        }
        else
        {
            _btnVoteSeeMore=[[UIButton alloc]initWithFrame:CGRectMake(20,CGRectGetMaxY(lblShare.frame)+50,_viewForNotificationUI.frame.size.width/2-30,40)];
            _btnVoteClose=[[UIButton alloc]initWithFrame:CGRectMake(_viewForNotificationUI.frame.size.width/2-50,CGRectGetMaxY(lblShare.frame)+50,100,40)];
        }
        
        [_btnVoteSeeMore setTitle:@"View" forState:UIControlStateNormal];
        [_btnVoteClose setTitle:@"Close" forState:UIControlStateNormal];
        
        [_btnVoteSeeMore setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnVoteClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        _btnVoteSeeMore.backgroundColor=[UIColor orangeColor];
        _btnVoteClose.layer.borderWidth=2.0;
        _btnVoteClose.layer.borderColor=[[UIColor orangeColor]CGColor];
        _btnVoteClose.layer.cornerRadius=4.0;
        _btnVoteSeeMore.layer.cornerRadius=4.0;
        
        [_btnVoteSeeMore addTarget:self action:@selector(MoveToSpecificBetPage) forControlEvents:UIControlEventTouchUpInside];
        [_btnVoteClose addTarget:self action:@selector(closeNotificationeView) forControlEvents:UIControlEventTouchUpInside];
        
         [_viewForVote addSubview:_btnVoteSeeMore];
        [_viewForNotificationUI addSubview:_btnVoteClose];
        
    }];
}

-(void)MoveToSpecificBetPage
{
    [self incrimentUserDefaults];
    
    SpecifiBetPage *specificBet=[[SpecifiBetPage alloc]initWithNibName:@"SpecifiBetPage" bundle:nil];
    specificBet.strThemeName=[_arrDashboardInfo valueForKey:@"name"];
    specificBet.selectedBetID1=[_arrDashboardInfo valueForKey:@"id"];
    specificBet.strBetImg=[_arrDashboardInfo valueForKey:@"bgimage"];
    specificBet.strSelectBUID=[theAppDelegate.arrUserLoginData objectAtIndex:0];
    [self.navigationController pushViewController:specificBet animated:YES];
}

-(void)closeNotificationeView
{
    @try
    {
        for(UIView *subview in [_viewForNotificationUI subviews])
        {
            [subview removeFromSuperview];
        }
    
        CGRect frameForViewForVote=_viewForNotificationUI.frame;
        frameForViewForVote=CGRectMake(30, screenHeight/2,screenWidth-60,0);
    
        [UIView animateWithDuration:0.5f animations:^{
        _viewForNotificationUI.frame=frameForViewForVote;
        } completion:^(BOOL finished)
         {
             [_viewForNotificationUI removeFromSuperview];
         //   [self enableSubViews];
         }];
    } @catch (NSException *exception){
    } @finally{
    }
}

-(void)updatePurchaseSubscriptionToServer
{
    if(theAppDelegate.isAlreadyPurchased)
    {
        NSMutableURLRequest *request;
        if (theAppDelegate.isConnected)
        {
            connSession=[[VNSessionLib alloc]init];
            NSDate *todayDate = [NSDate date]; //Get todays date
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date.
            [dateFormatter setDateFormat:@"yyyy-MM-dd"]; //Here we can set the format which we need
            NSString *convertedDateString = [dateFormatter stringFromDate:todayDate];// Here convert date in NSString
            NSLog(@"Today formatted date is %@",convertedDateString);
            
            NSString *strBaseURL=base_URL;
            NSString *urlString;
            urlString=[NSString stringWithFormat:@"wssubscription_update.php?uid=%@&date=%@",[theAppDelegate.arrUserLoginData  objectAtIndex:0],convertedDateString];
            strBaseURL=[strBaseURL stringByAppendingString:urlString];
        
        if(!connSession)
            connSession= [[VNSessionLib alloc] init];
        connSession.delegate = self;
        [connSession makeConnection:strBaseURL];
        request = nil;
    }
   }
}

#pragma mark: Web Response From JSON
-(void) webResponse1:(NSMutableDictionary*)response
{
    dictSession=[[NSMutableDictionary alloc]init];
    dictSession =[[NSMutableDictionary alloc]initWithDictionary:response];
    NSLog(@"%@",dictSession);
}
@end
