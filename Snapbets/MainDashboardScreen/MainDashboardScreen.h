//
//  MainDashboardScreen.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 07/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderAndConstants.h"
#import "NotificationScreen.h"
#import "ProfileScreen.h"
#import "SnapbetsThemeScreen.h"
#import "SpecifiBetPage.h"
#import "SettingScreen.h"
#import "NewSnapScreen.h"
#import "WebConnection1.h"
#import "SearchScreen.h"
#import <Social/Social.h>
#import "APPChildViewController.h"
#import "AsyncImageView.h"
#import "DBManager.h"
#import "MBProgressHUD.h"
#import "IAPHelper.h"
#import "InAppRageIAPHelper.h"
#import "LeadersBetScreen.h"
#import "VNSessionLib.h"
#import <Social/Social.h>

@interface MainDashboardScreen : UIViewController<UITableViewDataSource,UITableViewDelegate,WebRequestResult1,UIPageViewControllerDataSource,UIGestureRecognizerDelegate,VNRequestResult>
{
    CGRect vscreenSize;
    CGFloat vscreenHeight,vscreenWidth;
    WebConnection1 *connection;
    VNSessionLib *connSession;
    NSMutableDictionary *dict,*dictSession;
    NSInteger curntIndexPge,sletedLdrBet;
    NSUInteger pageViewIndex,slectedShareIndex;
    MBProgressHUD *hud;
    int productId;
    NSString *strLedSetcedBetID,*strTwiterImagePath;
    
}

@property (nonatomic)DBManager *objDBManager;

@property (nonatomic)AsyncImageView *asyImgBet,*asyImgProfile;

@property (nonatomic)IBOutlet UIView *viewForShare,*viewForMenu,*viewForVote,*viewForUpgradeAccount,*viewForRestore,*viewForTutorial,*viewForUpdateNow,*viewForTutTemp,*viewForRestoreAlert,*viewForBlockedAlert,*viewForNotificationUI;

@property (nonatomic)IBOutlet UIButton *btnMenu,*btnMClose,*btnMNSnap,*btnMNotification,*btnMProfile,*btnMUpgrade,*btnMRestore,*btnMSetting,*btnShareFB,*btnShareTwit,*btnShareYoutube,*btnVoteSeeMore,*btnVoteClose,*btnProfile,*btnSnapbetThemes,*btnSnapbetThemes1,*btnFeed,*btnTrending,*btnMyBets,*btnLeaders,*btnUpgrade,*btnCloseUpgradeUI,*btnClose,*btnNext1,*btnCloseShareUI,*btnCloseUpgradeNow,*btnCheck,*btnRestoreYes,*btnRestoreNo,*btnCloseAlert;

#pragma mark: UITableView properties declarations here
@property (nonatomic)IBOutlet UITableView *tblBets,*tblMenu,*tblUpgradeAcc,*tblLeaders;

#pragma mark: NSMutable Array Properties declarations here
@property(nonatomic)NSMutableArray *arrMenuImages,*arrMenuTitles,*arrDashboardInfo;

#pragma mark: NSString Properties declarations here
@property (nonatomic)NSString *strCurrentFeed,*strNotiAlertTitle;

#pragma mark:UIImageView Properties declarations here
@property (nonatomic)IBOutlet UIImageView *imgHederBG,*imgFeedBG,*imgTrendingBG,*imgMyBetBG,*imgLeadersBG;

#pragma mark: Bool Properties declaratiosn here
@property(nonatomic)BOOL isFeedData,isTrendingData,isMyBetsData,isLeadersData,isVoteBTNPressed,isFirstTimeUpgrade,isCheck,isCheckProfileImage;

#pragma mark- UIPageViewController Properties
@property (strong, nonatomic) UIPageViewController *pageController;


#pragma mark- Connetion Objects
#pragma mark - UIActivityIndicatorView properties declare here
@property(strong,nonatomic)IBOutlet UIActivityIndicatorView *loader;
@property(nonatomic) NSMutableData *webdata;
@property (nonatomic)NSURLConnection *ConnUStatus;
@property (nonatomic)NSMutableDictionary *dictUStatus;
@end
