//
//  SnapbetsThemeScreen.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 09/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "SnapbetsThemeScreen.h"

@interface SnapbetsThemeScreen ()

@end

@implementation SnapbetsThemeScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadInitialUI];
    [self GetAllThemeListAPI];
   
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)loadInitialUI
{
    
    _btnBack=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBack.frame = CGRectMake(5,8,25,25);
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"icon_BackMove"] forState:UIControlStateNormal];
    
    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    _btnBackPress=[[UIButton alloc]initWithFrame:CGRectMake(0,0,55,30)];
    [_btnBackPress addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnBack];
    [self.view addSubview:_btnBackPress];
    UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,5,screenWidth,30)];
    lblTitle.text=@"Choose a Theme";
    lblTitle.textColor=[UIColor orangeColor];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.font=[UIFont fontWithName:@"Avenir Medium" size:18];
    [self.view addSubview:lblTitle];

    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        _tblTheme=[[UITableView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_btnBack.frame)+7,screenWidth,screenHeight-31)];
    }
    else
    {
        _tblTheme=[[UITableView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_btnBack.frame)+7,screenWidth,screenHeight-31)];
    }
    
    _tblTheme.delegate=self;
    _tblTheme.dataSource=self;
    _tblTheme.tableFooterView=[[UIView alloc]init];
    _tblTheme.backgroundColor=[UIColor colorWithRed:243.0/255.0 green:144.0/255.0 blue:0/255.0 alpha:1];
    [self.view addSubview:_tblTheme];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self
                        action:@selector(getLatestBets)
              forControlEvents:UIControlEventValueChanged];
    [_tblTheme addSubview:refreshControl];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productsLoaded:) name:kProductsLoadedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:kProductPurchasedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(productPurchaseFailed:) name:kProductPurchaseFailedNotification object: nil];
    
    if ([InAppRageIAPHelper sharedHelper].products == nil) {
        
        [[InAppRageIAPHelper sharedHelper] requestProducts];
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.labelText =@"Loading";
        [self performSelector:@selector(timeout:) withObject:nil afterDelay:30.0];
        
    }
}

- (void)getLatestBets
{
    theAppDelegate.isPoolRefresh=YES;
    [self GetAllThemeListAPI];
    theAppDelegate.isPoolRefresh=NO;
}

#pragma mark-Get Data From Database
-(void)getDataFromDB
{
    objDBManager=[DBManager getSharedInstance];
    NSString *selectQuery,*deleteQuery;
    NSArray *arrTemp=[[NSArray alloc] init];
    
    selectQuery=[NSString stringWithFormat:@"select * from tblInAppThemePurchase where UID='%@' and ThemeID='%ld'",[theAppDelegate.arrUserLoginData objectAtIndex:0],(long)strPThmID];
    arrTemp=[objDBManager selectTableDataWithQuery:selectQuery];
    if ([arrTemp count])
    {
        NSDate *currentDate = [NSDate date];
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"dd-MMM-yy"]; // Date formater
        NSString *strToday = [dateformate stringFromDate:currentDate]; // CONVERT date to string
        NSLog(@"date :%@",strToday);
        NSLog(@"1 Week");
        NSString *strExpDate=[[arrTemp objectAtIndex:0]objectAtIndex:4];
        
        if ([strExpDate isEqualToString:strToday])
        {
            theAppDelegate.isAlreadyPurchased=NO;
            
            deleteQuery=[NSString stringWithFormat:@"delete from tblInAppThemePurchase where UID='%@' and ThemeID='%ld'",[theAppDelegate.arrUserLoginData objectAtIndex:0],(long)sltedThemeID];
             [objDBManager deleteFromTableWithQuery:deleteQuery];
            [self loadPurchaseView];
        }
        else
        {
            theAppDelegate.isAlreadyPurchased=YES;
            [self loadPurchaseView];
        }
    }
    
    SKPaymentTransaction * transaction;
    NSLog(@"%ld",(long)transaction.transactionState);
    if([_strIAPStatus isEqualToString:@"free"])
    {
        
    }
    else
    {
        
        if (theAppDelegate.isAlreadyPurchased)
        {
            
        }
        else
        {
            [self loadPurchaseView];
        }
    }

}

#pragma mark: UIButton
-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnViewPressed:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSInteger row = button.tag;
    sltedThemeID = row;
    NSString *strBuyType=[[_arrTblData valueForKey:@"buy"]objectAtIndex:row];
    NSString *strThemeID=[[_arrTblData valueForKey:@"id"]objectAtIndex:row];
    strPThmID=[strThemeID integerValue];
    
   if([strBuyType isEqualToString:@"No"])
   {
       [self viewForPurchaseAllThemePkg];
   }
   else
   {
       [self closeViewButonPressed];
       _strAlertTitle=@"Already Purchased";
       [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:0];
   }
//    NSLog(@"%ld",(long)row);
//    SpecificThemePackgScreen *speciTheme=[[SpecificThemePackgScreen alloc]initWithNibName:@"SpecificThemePackgScreen" bundle:nil];
//    speciTheme.strSelectedThmeID=[[_arrTblData valueForKey:@"id"]objectAtIndex:row];
//    speciTheme.strTImage=[[_arrTblData valueForKey:@"image" ] objectAtIndex:row];
//    speciTheme.strThName=[[_arrTblData valueForKey:@"package_name"]objectAtIndex:row];
//    speciTheme.strTDesc=[[_arrTblData valueForKey:@"details"]objectAtIndex:row];
//    speciTheme.strIAPStatus=[[_arrTblData valueForKey:@"type"]objectAtIndex:row];
//    
//    [self.navigationController pushViewController:speciTheme animated:YES];
    
}
/*--------------     Bottom TableView delegate and datasource declaration here     -----------*/
#pragma mark- TableView DataSource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_arrTblData count];
}

//--------------------- cellforrow At Index TableView -----------------------//

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@""];
    if (cell == nil)
    {
        //  cell.transform= CGAffineTransformMakeRotation(M_PI/2);
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    
    tableView.separatorColor =UIColor.clearColor;
    cell.backgroundColor= [UIColor clearColor];
    [self.tblTheme setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    UIView *viewForInfo=[[UIView alloc]init];
    UIButton *btnView=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-70,69,70,30)];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        _asynchronusImage=[[AsyncImageView alloc] initWithFrame:CGRectMake(0,0,screenWidth/2.8,150)];
         viewForInfo.frame=CGRectMake(CGRectGetMaxX(_asynchronusImage.frame),0,screenWidth-_asynchronusImage.frame.size.width,150);
        btnView.frame=CGRectMake(screenWidth-70,114,70,35);
    }
    else
    {
        _asynchronusImage=[[AsyncImageView alloc] initWithFrame:CGRectMake(1,0,screenWidth/3,100)];
        viewForInfo.frame=CGRectMake(CGRectGetMaxX(_asynchronusImage.frame),0,screenWidth-_asynchronusImage.frame.size.width,100);
        btnView.frame=CGRectMake(screenWidth-70,69,70,30);
    }
    
    _asynchronusImage.backgroundColor = [UIColor clearColor];
    NSString *imgUrlString =[[_arrTblData valueForKey:@"image"] objectAtIndex:indexPath.row];
    NSLog(@"Image URL- %@", imgUrlString);
    
    NSString *webStr = [NSString stringWithFormat:@"%@",imgUrlString];
    webStr=[webStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *imageUrl = [[NSURL alloc] initWithString:webStr];
    
    _asynchronusImage.contentMode = UIViewContentModeScaleAspectFit;
    _asynchronusImage.clipsToBounds = YES;
     [_asynchronusImage loadImageFromURL:imageUrl];
   // [_asynchronusImage sizeToFit];
   
    [cell.contentView addSubview:_asynchronusImage];
   
    NSString *imgUrlString1 =[[_arrTblData valueForKey:@"image" ] objectAtIndex:indexPath.row];
    NSLog(@"Image URL- %@", imgUrlString1);
    
    viewForInfo.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7];
    viewForInfo.layer.borderColor=[[UIColor whiteColor]CGColor];
    _asynchronusImage.layer.borderColor=[[UIColor whiteColor]CGColor];
    
    _asynchronusImage.layer.borderWidth=0.5;
     viewForInfo.layer.borderWidth=0.5;
    
    [cell.contentView addSubview:viewForInfo];
    UILabel *lblThmeTitle=[[UILabel alloc]initWithFrame:CGRectMake(5,3,viewForInfo
                                                                   .frame.size.width,30)];
    lblThmeTitle.text=[[_arrTblData valueForKey:@"package_name"]objectAtIndex:indexPath.row];
    lblThmeTitle.textColor=[UIColor whiteColor];
    lblThmeTitle.font=[UIFont fontWithName:@"Avenir-Roman" size:16];
    [viewForInfo addSubview:lblThmeTitle];
    
    UILabel *lblThmeSubDtls=[[UILabel alloc]initWithFrame:CGRectMake(5,CGRectGetMaxY(lblThmeTitle.frame)-10,viewForInfo.frame.size.width-15,0)];
    NSString *strDes=[[_arrTblData valueForKey:@"details"]objectAtIndex:indexPath.row];
    lblThmeSubDtls.text=strDes;
    lblThmeSubDtls.textColor=[UIColor whiteColor];
    lblThmeSubDtls.font=[UIFont fontWithName:@"Avenir-Roman" size:11];
    lblThmeSubDtls.lineBreakMode = NSLineBreakByTruncatingTail;
    [lblThmeSubDtls setNumberOfLines:3];
    [lblThmeSubDtls sizeToFit];
    [viewForInfo addSubview:lblThmeSubDtls];
    
    NSString *strBuyCheck=[[_arrTblData valueForKey:@"buy"]objectAtIndex:indexPath.row];
    
    if([strBuyCheck isEqualToString:@"No"])
    {
        [btnView setTitle:@"Buy" forState:UIControlStateNormal];
        
    }
    else
    {
         [btnView setTitle:@"Purchased" forState:UIControlStateNormal];
    }
    [btnView addTarget:self action:@selector(btnViewPressed:) forControlEvents:UIControlEventTouchUpInside];
    [btnView setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnView setBackgroundColor:[UIColor colorWithRed:168/255.0 green:86/255.0  blue:24/255.0  alpha:1]];
    
    btnView.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:14];
    btnView.tag=indexPath.row;
    [cell.contentView addSubview:btnView];
    
    return cell;
}

#pragma Mark-delegate methods
//--------------------- Tableview didselected index ---------------------//
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        return 150;
    }
    else
    {
        return 100;
    }
}


#pragma mark: FBLogin Server call
-(void)GetAllThemeListAPI
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        if (theAppDelegate.isPoolRefresh)
        {
            theAppDelegate.isPoolRefresh=NO;
        }
        else
        {
            [theAppDelegate showSpinnerInView:self.view];
        }
        _isThemeList=YES;
        _isBuyAllList=NO;
        
        [self.view setUserInteractionEnabled:NO];
        [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
        NSString *strUrlString=base_URL;
        
         NSString *strURL=[NSString stringWithFormat:@"wsviewtheme.php?uid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0]];
        
        strUrlString=[strUrlString stringByAppendingString:strURL];
        
        strUrlString=[strUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:strUrlString];
        
        request=[NSMutableURLRequest requestWithURL:[url standardizedURL]];
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        
    }
    request = nil;
}

#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    [theAppDelegate stopSpinner];
    [self.view setUserInteractionEnabled:YES];
  
    dict =[[NSMutableDictionary alloc]initWithDictionary:response];
    NSString *strMsg=[dict valueForKey:@"message"];
    if ([strMsg isEqualToString:@"sus"])
    {
        if (_isThemeList)
        {
            if ([strMsg isEqualToString:@"sus"])
            {
                _arrTblData=[dict valueForKey:@"info"];
                [_tblTheme reloadData];
            }
            else
            {
        
            }
        }
        else if (_isBuyAllList)
        {
            [self closeViewButonPressed];
            if(theAppDelegate.isAlreadyPurchased)
            {
                _strAlertTitle=@"Theme selection done";
                [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:1];
            }
            NSLog(@"Purchase All Successfully");
        }
    }
    else
    {
        if (_isBuyAllList)
        {
            if([strMsg isEqualToString:@"all ready"])
            {
                //[self closeViewButonPressed];
                //_strAlertTitle=@"All ready purchased";
               // [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:1];
            }
            else
            {
                [self closeViewButonPressed];
                _strAlertTitle=@"Failed to purchase";
                [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:1];
            }
            
        }
    }
}

#pragma mark: Viewfor Buy single Theme Package
-(void)viewForPurchaseAllThemePkg
{
    [_viewForBuyAll removeFromSuperview];
    _viewForBuyAll =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForBuyAll.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForBuyAll.tag=20;
    [self.view addSubview:_viewForBuyAll];
    CGRect frameForViewForHelp=_viewForBuyAll.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForBuyAll.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForBuyAll.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForBuyAll.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForBuyAll addSubview:img];
        UILabel *lblPurchase=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForBuyAll.frame.size.width,30)];
        lblPurchase.text=@"Theme Purchase";
        lblPurchase.textAlignment=NSTextAlignmentCenter;
        lblPurchase.textColor=[UIColor whiteColor];
        lblPurchase.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:25];
        [_viewForBuyAll addSubview:lblPurchase];
        
        UILabel *lblPurchase1=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblPurchase.frame)+10,_viewForBuyAll.frame.size.width,30)];
        lblPurchase1.text=@"Would you like to purchase this theme?";
        lblPurchase1.textAlignment=NSTextAlignmentCenter;
        lblPurchase1.textColor=[UIColor whiteColor];
        lblPurchase1.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForBuyAll addSubview:lblPurchase1];
        
        _btnSBuySnpbet=[[UIButton alloc]initWithFrame:CGRectMake(20,CGRectGetMaxY(lblPurchase1.frame)+50,_viewForBuyAll.frame.size.width/2-30,45)];
        _btnClseBuySnpbet=[[UIButton alloc]initWithFrame:CGRectMake(_viewForBuyAll.frame.size.width/2+15,CGRectGetMaxY(lblPurchase1.frame)+50,_viewForBuyAll.frame.size.width/2-30,45)];
        
        [_btnClseBuySnpbet setTitle:@"Cancel" forState:UIControlStateNormal];
        
        [_btnSBuySnpbet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnClseBuySnpbet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        _btnSBuySnpbet.backgroundColor=[UIColor orangeColor];
        
        _btnClseBuySnpbet.layer.borderWidth=2.0;
        _btnClseBuySnpbet.layer.borderColor=[[UIColor orangeColor]CGColor];
        
        _btnSBuySnpbet.layer.cornerRadius=5.0;
        _btnClseBuySnpbet.layer.cornerRadius=5.0;
        
        [_btnClseBuySnpbet addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
        [_btnSBuySnpbet addTarget:self action:@selector(btnBuyAllPkgValuePressed:) forControlEvents:UIControlEventTouchUpInside];
        [_viewForBuyAll addSubview:_btnSBuySnpbet];
        [_viewForBuyAll addSubview:_btnClseBuySnpbet];
        
        UILabel *lblBuy=[[UILabel alloc]initWithFrame:CGRectMake(0,0,_btnSBuySnpbet.frame.size.width,30)];
        lblBuy.text=@"Buy Package";
        lblBuy.textAlignment=NSTextAlignmentCenter;
        lblBuy.textColor=[UIColor whiteColor];
        lblBuy.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_btnSBuySnpbet addSubview:lblBuy];
        
        UILabel *lblBuyPrice=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblBuy.frame)-10,_btnSBuySnpbet.frame.size.width,30)];
        lblBuyPrice.text=@"$5.99";
        lblBuyPrice.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        lblBuyPrice.textAlignment=NSTextAlignmentCenter;
        lblBuyPrice.textColor=[UIColor whiteColor];
        [_btnSBuySnpbet addSubview:lblBuyPrice];
    }];
}

#pragma mark: Button buy package pressed
-(IBAction)btnBuyAllPkgValuePressed:(id)sender
{
    [self getDataFromDB];
}

#pragma mark: ViewFor LoadPurchase
-(void)loadPurchaseView
{
    if (theAppDelegate.isAlreadyPurchased)
    {
        [self closeViewButonPressed];
        _strAlertTitle=@"Already Purchased";
        [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:0];
        
        
    }
    else
    {
        [self closeViewButonPressed];
        SKProduct *product;
        
        product=[[InAppRageIAPHelper sharedHelper].products objectAtIndex:0];
        if([product isKindOfClass:[NSNull class]])
        {
            
        }
        else
        {
            NSLog(@"Buying %@...", product.productIdentifier);
            [[InAppRageIAPHelper sharedHelper] buyProduct:product];
            hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.labelText =@"Buying...";
            [self performSelector:@selector(timeout:) withObject:nil afterDelay:60*5];
            //[self buyAllSnapbetTheme];
        }
    }
}
#pragma mark: BuyAll Snapbet Server call
-(void)buyAllSnapbetTheme
{
    NSString *strThemeID=[[_arrTblData valueForKey:@"id"]objectAtIndex:sltedThemeID];
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isBuyAllList=YES;
        _isThemeList=NO;
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
        NSString *strUrlString=base_URL;
        NSString *strURL=[NSString stringWithFormat:@"wsthemepurchase.php?uid=%@&tid=%@&vid=All",[theAppDelegate.arrUserLoginData objectAtIndex:0],strThemeID];
        
        strUrlString=[strUrlString stringByAppendingString:strURL];
        strUrlString=[strUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:strUrlString];
        request=[NSMutableURLRequest requestWithURL:[url standardizedURL]];
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        
    }
    request = nil;
}

#pragma mark: InApp Purchases product loading methods
- (void)productsLoaded:(NSNotification *)notification
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
}

- (void)productPurchased:(NSNotification *)notification
{
    objDBManager=[DBManager getSharedInstance];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    NSString *productIdentifier = (NSString *) notification.object;
    NSLog(@"Purchased: %@", productIdentifier);
    NSDate *currDate = [NSDate date];
    NSDateFormatter *tempDateFormatter = [[NSDateFormatter alloc]init];
    [tempDateFormatter setDateFormat:@"dd-MMM-yy"];
    NSString *strMStartDate= [tempDateFormatter stringFromDate:currDate];
    NSLog(@"%@",strMStartDate);
    
    NSDate *expiryDate=[currDate dateByAddingTimeInterval:30*(60*60*24)];
    NSString *strExpireDate=[tempDateFormatter stringFromDate:expiryDate];
    NSLog(@"%@",strExpireDate);
    
    NSString *strQuery;
    if ([notification.name isEqualToString:@"ProductPurchased"])
    {
        if ([productIdentifier isEqualToString:@"com.Snapbet_10"])
        {
            strQuery=[NSString stringWithFormat:@"insert into tblInAppThemePurchase values('%@','%@','single','%@','%@','%ld')",[theAppDelegate.arrUserLoginData objectAtIndex:0],productIdentifier,strMStartDate,strExpireDate,(long)strPThmID];
            
            if ([objDBManager insertDataWithQuery:strQuery])
            {
                [self buyAllSnapbetTheme];
            }
            else
            {
                
            }
            
        }
        else if ([productIdentifier isEqualToString:@"com.Snapbet_10"])
        {
            strQuery=[NSString stringWithFormat:@"insert into tblInAppThemePurchase values('%@','%@','All','%@','%@','%ld')",[theAppDelegate.arrUserLoginData objectAtIndex:0],productIdentifier,strMStartDate,strExpireDate,(long)sltedThemeID];
            
            if ([objDBManager insertDataWithQuery:strQuery])
            {
                [self buyAllSnapbetTheme];
            }
            else
            {
                
            }
        }
        _strAlertTitle=@"Thank you for purchasing a Snapbets Theme!";
        [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:1];
        NSDate *currDate = [NSDate date];
        NSDateFormatter *tempDateFormatter = [[NSDateFormatter alloc]init];
        [tempDateFormatter setDateFormat:@"dd-MMM-yy"];
        strMStartDate= [tempDateFormatter stringFromDate:currDate];
    }
    [self performSelector:@selector(dismissHUD:) withObject:nil afterDelay:0.0];
}

- (void)productPurchaseFailed:(NSNotification *)notification
{
    [theAppDelegate stopSpinner];
    [self.view setUserInteractionEnabled:YES];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    
    SKPaymentTransaction * transaction = (SKPaymentTransaction *) notification.object;
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        theAppDelegate.alert.title=@"Error";
        theAppDelegate.alert.message=transaction.error.localizedDescription;
        theAppDelegate.alert.show;
        [self performSelector:@selector(dismissHUD:) withObject:nil afterDelay:0.0];
    }
}


- (void)timeout:(id)arg
{
    hud.labelText =@"Timeout!";
    hud.detailsLabelText =@"Please try again later.";
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
    hud.mode = MBProgressHUDModeCustomView;
    [self performSelector:@selector(dismissHUD:) withObject:nil afterDelay:3.0];
    [self performSelector:@selector(showErrorMessage) withObject:nil afterDelay:3.0];
}

- (void)dismissHUD:(id)arg
{
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    hud = nil;
}

- (void) showErrorMessage
{
    theAppDelegate.alert.title=@"Error";
    theAppDelegate.alert.message=@"Please try again later";
    theAppDelegate.alert.show;
}

#pragma mark-Close Comment View
-(void)closeViewButonPressed
{
    @try
    {
        for(UIView *subview in [_viewForBuyAll subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForBuyAll=_viewForBuyAll.frame;
        
        frameForViewForBuyAll=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForBuyAll.frame=frameForViewForBuyAll;
            
        } completion:^(BOOL finished){
            [_viewForBuyAll removeFromSuperview];
                //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}


#pragma mark: view For Alert
-(void)viewForAlertUI
{
    [_viewForAlert removeFromSuperview];
    _viewForAlert =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForAlert.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForAlert.tag=20;
    [self.view addSubview:_viewForAlert];
    
    CGRect frameForViewForHelp=_viewForAlert.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForAlert.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForAlert.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForAlert addSubview:img];
        UILabel *lblAlert=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForAlert.frame.size.width,30)];
        lblAlert.text=_strAlertTitle;
        lblAlert.textAlignment=NSTextAlignmentCenter;
        lblAlert.textColor=[UIColor whiteColor];
        lblAlert.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:14];
        [_viewForAlert addSubview:lblAlert];
        
        _btnCloseAlert=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseAlert.frame=CGRectMake(_viewForAlert.frame.size.width/2-60,CGRectGetMaxY(lblAlert.frame)+50,120,40);
        }
        else
        {
            _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,CGRectGetMaxY(lblAlert.frame)+50,100,40)];
        }
        [_btnCloseAlert setTitle:@"Close" forState:UIControlStateNormal];
        [_btnCloseAlert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseAlert.layer.borderWidth=2.0;
        _btnCloseAlert.layer.cornerRadius=4.0;
        _btnCloseAlert.layer.borderColor=[[UIColor orangeColor]CGColor];
        //[_btnVoteSeeMore addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        [_btnCloseAlert addTarget:self action:@selector(viewForCloseAlert) forControlEvents:UIControlEventTouchUpInside];
        [_viewForAlert addSubview:_btnCloseAlert];
    }];
}

#pragma mark-Close Comment View
-(void)viewForCloseAlert
{
    @try
    {
        for(UIView *subview in [_viewForAlert subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewAlert=_viewForAlert.frame;
        
        
        frameForViewAlert=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForAlert.frame=frameForViewAlert;
            
        } completion:^(BOOL finished){
            [_viewForAlert removeFromSuperview];
            
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

@end
