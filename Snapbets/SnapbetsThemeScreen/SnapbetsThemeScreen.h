//
//  SnapbetsThemeScreen.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 09/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderAndConstants.h"
#import "SpecificThemePackgScreen.h"
#import "WebConnection1.h"
#import "AppDelegate.h"
#import "AsyncImageView.h"
#import "DBManager.h"
#import "MBProgressHUD.h"
#import "IAPHelper.h"
#import "InAppRageIAPHelper.h"

@interface SnapbetsThemeScreen : UIViewController<UITableViewDelegate,UITableViewDataSource,WebRequestResult1>
{
    WebConnection1 *connection;
    NSMutableDictionary *dict;
    DBManager *objDBManager;
    CGRect vscreenSize;
    CGFloat vscreenHeight,vscreenWidth;
    MBProgressHUD *hud;
    int productId;
    NSInteger sltedThemeID,strPThmID;
}


#pragma mark: UIView properties declarations here
@property (nonatomic)IBOutlet UIView *viewForBuyAll,*viewForAlert;

@property(nonatomic)IBOutlet UITableView *tblTheme;

@property (nonatomic)IBOutlet UIButton *btnBack,*btnBackPress,*btnSBuySnpbet,*btnClseBuySnpbet,*btnCloseAlert;

@property (nonatomic)NSMutableArray *arrTblData;

@property(nonatomic) AsyncImageView *asynchronusImage,*asynchronusThemeImg;

@property (nonatomic)NSString *strIAPStatus,*strAlertTitle;

@property (nonatomic)BOOL isBuyAllList,isThemeList;
@end
