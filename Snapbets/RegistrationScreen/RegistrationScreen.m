//
//  RegistrationScreen.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 06/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "RegistrationScreen.h"
#import "WebConnection1.h"
#define kOFFSET_FOR_KEYBOARD 90.0
@interface RegistrationScreen ()<WebRequestResult1>
{
    UIBarButtonItem *doneButtons;
    UIToolbar *keyboardToolBar1;
}
@end

@implementation RegistrationScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    _isLogin=NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
        /*----  Load Initial UI ------*/
    [self loadInitialUI];
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark: Load InitialUI
-(void)loadInitialUI
{
 
    /*-----------  Button For Menu Selection for SidebarViewController-----------*/
    _btnBack=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBack.frame = CGRectMake(8,8,25,25);
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"iconBackArrow"] forState:UIControlStateNormal];
    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    /*----------- Button Pressed Event Fired and goto SidebarViewController--------*/
    _btnBackPress=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBackPress.frame = CGRectMake(0,0,105,55);
    [_btnBackPress addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnBack];
    [self.view addSubview:_btnBackPress];
    
    UIView *viewForMail=[[UIView alloc]init];
    UIView *viewForFName=[[UIView alloc]init];
    UIView *viewForLName=[[UIView alloc]init];
    UIView *viewForContact=[[UIView alloc]init];
    UIView *viewForPassword=[[UIView alloc]init];
    //UIView *viewForDOB=[[UIView alloc]init];
    
    UILabel *lblTermsCond=[[UILabel alloc]init];
    _btnTermsCondi=[[UIButton alloc]init];
    UILabel *lblPrivyPoly=[[UILabel alloc]init];
    _btnPrivyPolycy=[[UIButton alloc]init];
    
    UIImageView *imgTrangle=[[UIImageView alloc]initWithFrame:CGRectMake(screenWidth/2-15,40,30,30)];
    imgTrangle.image=[UIImage imageNamed:@"img_Diagonal"];
    [self.view addSubview:imgTrangle];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
         _imgTrangleBG=[[UIImageView alloc]initWithFrame:CGRectMake(40,60,screenWidth-80,460)];
        viewForMail.frame=CGRectMake(60,97,screenWidth-120,40);
        viewForFName.frame=CGRectMake(60,CGRectGetMaxY(viewForMail.frame)+20,screenWidth-120,40);
        viewForLName.frame=CGRectMake(60,CGRectGetMaxY(viewForFName.frame)+20,screenWidth-120,40);
        viewForContact.frame=CGRectMake(60,CGRectGetMaxY(viewForLName.frame)+20,screenWidth-120,40);
        viewForPassword.frame=CGRectMake(60,CGRectGetMaxY(viewForContact.frame)+20,screenWidth-120,40);
        //viewForDOB.frame=CGRectMake(60,CGRectGetMaxY(viewForPassword.frame)+20,screenWidth-120,40);
        
        lblTermsCond.frame=CGRectMake(180,CGRectGetMaxY(viewForPassword.frame),_imgTrangleBG.frame.size.width/2-30,30);
        
        _btnTermsCondi.frame=CGRectMake(CGRectGetMaxX(lblTermsCond.frame)-13,CGRectGetMaxY(viewForPassword.frame),100,30);
        lblPrivyPoly.frame=CGRectMake(180,CGRectGetMaxY(lblTermsCond.frame)-10,_imgTrangleBG.frame.size.width/2-30,30);
        
        _btnPrivyPolycy.frame=CGRectMake(CGRectGetMaxX(lblPrivyPoly.frame)-13,CGRectGetMaxY(lblTermsCond.frame)-10,100,30);
         _btnSubmit=[[UIButton alloc]initWithFrame:CGRectMake(60,CGRectGetMaxY(lblPrivyPoly.frame)-2,screenWidth-120,40)];
    }
    else
    {
         _imgTrangleBG=[[UIImageView alloc]initWithFrame:CGRectMake(10,CGRectGetMaxY(imgTrangle.frame),screenWidth-20,405)];
          viewForMail.frame=CGRectMake(20,87,screenWidth-40,40);
          viewForFName.frame=CGRectMake(20,CGRectGetMaxY(viewForMail.frame)+17,screenWidth-40,40);
          viewForLName.frame=CGRectMake(20,CGRectGetMaxY(viewForFName.frame)+17,screenWidth-40,40);
          viewForContact.frame=CGRectMake(20,CGRectGetMaxY(viewForLName.frame)+17,screenWidth-40,40);
          viewForPassword.frame=CGRectMake(20,CGRectGetMaxY(viewForContact.frame)+17,screenWidth-40,40);
          //viewForDOB.frame=CGRectMake(20,CGRectGetMaxY(viewForPassword.frame)+17,screenWidth-40,40);
        
          lblTermsCond.frame=CGRectMake(15,CGRectGetMaxY(viewForPassword.frame),screenWidth/2+50,30);
        
        _btnTermsCondi.frame=CGRectMake(CGRectGetMaxX(lblTermsCond.frame)-13,CGRectGetMaxY(viewForPassword.frame),100,30);
        
        lblPrivyPoly.frame=CGRectMake(15,CGRectGetMaxY(lblTermsCond.frame)-10,screenWidth/2+55,30);
        
        _btnPrivyPolycy.frame=CGRectMake(CGRectGetMaxX(lblPrivyPoly.frame)-13,CGRectGetMaxY(lblTermsCond.frame)-10,100,30);
      
        UIFont *fnt1=[UIFont fontWithName:@"AvenirNext-DemiBold" size:11.3];
        UIFont *fnt2=[UIFont fontWithName:@"AvenirNext-DemiBold" size:12.5];
        
        if (screenWidth<325) {
            lblTermsCond.font=fnt1;
            lblPrivyPoly.font=fnt1;
        }
        else
        {
            lblTermsCond.frame=CGRectMake(20,CGRectGetMaxY(viewForPassword.frame),screenWidth/2+50,30);
            _btnTermsCondi.frame=CGRectMake(CGRectGetMaxX(lblTermsCond.frame),CGRectGetMaxY(viewForPassword.frame),80,30);
            lblPrivyPoly.frame=CGRectMake(20,CGRectGetMaxY(lblTermsCond.frame)-10,screenWidth/2+50,30);
            _btnPrivyPolycy.frame=CGRectMake(CGRectGetMaxX(lblPrivyPoly.frame),CGRectGetMaxY(lblTermsCond.frame)-10,80,30);
            _btnTermsCondi.titleLabel.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:15.5];
            _btnPrivyPolycy.titleLabel.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:15.5];
            lblTermsCond.font=fnt2;
            lblPrivyPoly.font=fnt2;
            lblTermsCond.textAlignment=NSTextAlignmentCenter;
            lblPrivyPoly.textAlignment=NSTextAlignmentCenter;
        }
        
         _btnSubmit=[[UIButton alloc]initWithFrame:CGRectMake(20,CGRectGetMaxY(lblPrivyPoly.frame)-2,screenWidth-40,40)];
    }
    _imgTrangleBG.image=[UIImage imageNamed:@"img_NSnap"];
    [self.view addSubview:_imgTrangleBG];
    
    [viewForMail setBackgroundColor:[UIColor blackColor]];
    [viewForFName setBackgroundColor:[UIColor blackColor]];
    [viewForLName setBackgroundColor:[UIColor blackColor]];
    [viewForContact setBackgroundColor:[UIColor blackColor]];
    [viewForPassword setBackgroundColor:[UIColor blackColor]];
   // [viewForDOB setBackgroundColor:[UIColor blackColor]];
    
    viewForMail.layer.cornerRadius=4.0;
    viewForFName.layer.cornerRadius=4.0;
    viewForLName.layer.cornerRadius=4.0;
    viewForPassword.layer.cornerRadius=4.0;
    viewForContact.layer.cornerRadius=4.0;
   // viewForDOB.layer.cornerRadius=4.0;
    
    [self.view addSubview:viewForMail];
    [self.view addSubview:viewForFName];
    [self.view addSubview:viewForLName];
    [self.view addSubview:viewForContact];
    [self.view addSubview:viewForPassword];
   // [self.view addSubview:viewForDOB];
    
    _txtEmail=[[UITextField alloc]initWithFrame:CGRectMake(10,5,viewForMail.frame.size.width-10,30)];
    _txtFName=[[UITextField alloc]initWithFrame:CGRectMake(10,5,viewForFName.frame.size.width-10,30)];
    _txtLName=[[UITextField alloc]initWithFrame:CGRectMake(10,5,viewForLName.frame.size.width-10,30)];
    _txtMob=[[UITextField alloc]initWithFrame:CGRectMake(10,5,viewForContact.frame.size.width-10,30)];
    _txtPass=[[UITextField alloc]initWithFrame:CGRectMake(10,5,viewForPassword.frame.size.width-10,30)];
    //_txtDob=[[UITextField alloc]initWithFrame:CGRectMake(10,5,viewForDOB.frame.size.width-10,30)];
    
    [viewForMail addSubview:_txtEmail];
    [viewForFName addSubview:_txtFName];
    [viewForLName addSubview:_txtLName];
    [viewForContact addSubview:_txtMob];
    [viewForPassword addSubview:_txtPass];
    //[viewForDOB addSubview:_txtDob];
    
    [self getTextFieldWithProperties:_txtEmail placeholder:@"Email" forLogin:YES];
    [self getTextFieldWithProperties:_txtFName placeholder:@"FName" forLogin:YES];
    [self getTextFieldWithProperties:_txtLName placeholder:@"FName" forLogin:YES];
    [self getTextFieldWithProperties:_txtMob placeholder:@"Mob" forLogin:YES];
    [self getTextFieldWithProperties:_txtPass placeholder:@"New Password" forLogin:YES];
    //[self getTextFieldWithProperties:_txtDob placeholder:@"DOB" forLogin:YES];
    
    /*tbDOB = [[UIToolbar alloc] initWithFrame:CGRectMake(0,screenHeight/2,screenWidth,25)];
    
    tbDOB.barStyle = UIBarStyleDefault;
    [tbDOB setItems: [NSArray arrayWithObjects:
                             [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                             [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(btnDOBDonePressed:)],
                             nil]];
    [tbDOB setBarStyle:UIBarStyleBlackOpaque];
    arrtbDOB= [NSArray arrayWithObjects:
                   doneButton, nil];
     _txtDob.inputAccessoryView =tbDOB;
    [self.txtDob setInputView:_dteDOB1];
    _dteDOB1 = [[UIDatePicker alloc]init];
    [_dteDOB1 setDate:[NSDate date]];
   // _dteDOB1.datePickerMode = UIDatePickerModeDate;
    [_dteDOB1 setDatePickerMode:UIDatePickerModeDate];
    _dteDOB1.enabled=true;
    _dteDOB1.date = [NSDate date];
    [_dteDOB1 addTarget:self action:@selector(txtDOBPressed:)forControlEvents:UIControlEventValueChanged];
    [_txtDob setInputView:_dteDOB1];
    */
  
    
   // NSMutableAttributedString *strTitle1  = [[NSMutableAttributedString alloc]initWithString:@"By creating an account, you agree to the Terms of Use and acknowledge that you have read the Privacy Policy"];
    
    lblTermsCond.text=@"By creating an account, you agree to the";
    lblPrivyPoly.text=@"and acknowledge that you have read the";
    [_btnTermsCondi setTitle:@"Terms of Use" forState:UIControlStateNormal];
    [_btnPrivyPolycy setTitle:@"Privacy Policy" forState:UIControlStateNormal];
    
    lblTermsCond.textColor=[UIColor darkGrayColor];
    lblPrivyPoly.textColor=[UIColor darkGrayColor];
    
    [_btnTermsCondi setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnPrivyPolycy setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    _btnTermsCondi.titleLabel.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:12];
    _btnPrivyPolycy.titleLabel.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:12];
    
    _btnPrivyPolycy.titleLabel.textAlignment=NSTextAlignmentLeft;
    
    [_btnTermsCondi addTarget:self action:@selector(btnTandCPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_btnPrivyPolycy addTarget:self action:@selector(btnPandPPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:lblTermsCond];
    [self.view addSubview:_btnTermsCondi];
    [self.view addSubview:lblPrivyPoly];
    [self.view addSubview:_btnPrivyPolycy];
    
   
    [_btnSubmit setBackgroundColor:[UIColor whiteColor]];
    _btnSubmit.layer.cornerRadius=4.0;
    [self.view addSubview:_btnSubmit];
    
    UILabel *lblSignUp=[[UILabel alloc]initWithFrame:CGRectMake(10,0,_btnSubmit.frame.size.width-10,40)];
    lblSignUp.text=@"Sign up";
    lblSignUp.textColor=[UIColor grayColor];
    lblSignUp.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
    [_btnSubmit addSubview:lblSignUp];
    [_btnSubmit addTarget:self action:@selector(btnSignUpPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIImageView *iconSignInFrwd=[[UIImageView alloc]initWithFrame:CGRectMake(_btnSubmit.frame.size.width-30,10,20,20)];
    iconSignInFrwd.image=[UIImage imageNamed:@"icon_BlckFrwd"];
    [_btnSubmit addSubview:iconSignInFrwd];
    
    UIImageView *imgAppIcon;
    if(screenWidth<325)
    {
        imgAppIcon.frame=CGRectMake(screenWidth/2-50,CGRectGetMaxY(_imgTrangleBG.frame)+5,100,100);
    }
    else
    {
        imgAppIcon.frame=CGRectMake(screenWidth/2-50,CGRectGetMaxY(_imgTrangleBG.frame)+20,100,100);
    }
     imgAppIcon.image=[UIImage imageNamed:@"img_AppIcon"];
    [self.view addSubview:imgAppIcon];
}

#pragma mark: Set Proerties for TextFields
-(UITextField*)getTextFieldWithProperties:(UITextField*)textField placeholder:(NSString*)placeholder forLogin:(BOOL)forLogin
{
    //------------- Add properties to textfield ------------- //
    //  textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.placeholder =placeholder;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.keyboardType=UIKeyboardAppearanceDefault;
    
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.keyboardAppearance=UIKeyboardAppearanceLight;
    [textField addTarget:self action:@selector(resignTxt:) forControlEvents:UIControlEventEditingDidEndOnExit];
    _txtMob.keyboardType=UIKeyboardTypePhonePad;
    _txtEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
    _txtFName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"First Name" attributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
    _txtLName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Last Name" attributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
    _txtMob.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mob. Number" attributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
    _txtPass.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"New Password" attributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
    _txtDob.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"DOB" attributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
    UIFont *fnt=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
    _txtEmail.font=fnt;
    _txtPass.font=fnt;
    _txtFName.font=fnt;
    _txtLName.font=fnt;
    _txtMob.font=fnt;
    _txtDob.font=fnt;
    
    _txtEmail.textAlignment=NSTextAlignmentLeft;
    _txtFName.textAlignment=NSTextAlignmentLeft;
    _txtLName.textAlignment=NSTextAlignmentLeft;
    _txtMob.textAlignment=NSTextAlignmentLeft;
    _txtPass.textAlignment=NSTextAlignmentLeft;
    _txtDob.textAlignment=NSTextAlignmentLeft;
    
    _txtEmail.textColor=[[UIColor whiteColor]colorWithAlphaComponent:0.8];
    _txtFName.textColor=[[UIColor whiteColor]colorWithAlphaComponent:0.8];
    _txtLName.textColor=[[UIColor whiteColor]colorWithAlphaComponent:0.8];
    _txtMob.textColor=[[UIColor whiteColor]colorWithAlphaComponent:0.8];
    _txtPass.textColor=[[UIColor whiteColor]colorWithAlphaComponent:0.8];
    _txtDob.textColor=[[UIColor whiteColor]colorWithAlphaComponent:0.8];
    
    _imgLogo.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView animateWithDuration:0.5
                     animations:^{
                         _imgLogo.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
                     } completion:^(BOOL finished) {
                     }];
    
    //----------- Validate return keys -------------------- //
    
    if (forLogin && textField==_txtPass)
    {        _txtPass.returnKeyType=UIReturnKeyDone;      }
    
    else if (forLogin && textField==_txtMob)
    {
        _txtMob.returnKeyType=UIReturnKeyNext;
    }
    else if (forLogin && textField==_txtLName)
    {
        _txtLName.returnKeyType=UIReturnKeyNext;
    }
    else if (forLogin && textField==_txtFName)
    {
        _txtFName.returnKeyType=UIReturnKeyNext;
    }
    else if (forLogin && textField ==_txtEmail)
    {        _txtEmail.returnKeyType=UIReturnKeyNext;    }
    
   
    //----------- Validate secure key entry for password field ----//
    
    if (textField==_txtPass)
    {        textField.secureTextEntry=YES;    }
    
    //---------- Validate keyboard type for email address --------//
    
    if (textField==_txtEmail)
    {        textField.keyboardType=UIKeyboardTypeEmailAddress;    }
    
    keyboardToolBar1 = [[UIToolbar alloc] initWithFrame:CGRectMake(0,screenHeight/2,screenWidth,30)];
    keyboardToolBar1.barStyle = UIBarStyleDefault;
    [keyboardToolBar1 setItems: [NSArray arrayWithObjects:
                                 [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                 [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneActionPkg)],
                                 nil]];
    _txtMob.inputAccessoryView = keyboardToolBar1;

    
    return textField;//------- Return fully updated textfield
}

-(void)doneActionPkg
{
    [_txtMob resignFirstResponder];
}
#pragma mark- TextField DelegateMethods
-(void)resignTxt:(UITextField*)txt
{
    //----------- Validate textfield return action ------ //
    if (txt==_txtEmail)
    {   [_txtFName becomeFirstResponder];       }
   
    else if (txt==_txtFName)
    {      [_txtLName becomeFirstResponder];    }
    else if (txt==_txtLName)
    {   [_txtMob becomeFirstResponder];         }
    else if (txt==_txtMob)
    {        [_txtPass becomeFirstResponder];   }
   
    else if(txt==_txtPass)
    {                                           }
    else
    {   [txt resignFirstResponder];             }
    //------------------------------------------------ //
}

#pragma mark- Keyboard Show Hide Methods
-(void)keyboardWillShow:(NSNotification*)notification {
    // Animate the current view out of the way
    if (self.view.frame.origin.y == 0)
    {
        [self setViewMovedUp:YES];
    }
    //    else if (self.view.frame.origin.y < 0)
    //    {
    //        [self setViewMovedUp:NO];
    //    }
}
-(void)keyboardWillHide:(NSNotification*)notification {
    //    if (self.view.frame.origin.y >= 0)
    //    {
    //        [self setViewMovedUp:YES];
    //    }
    //    else
    if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}


#pragma mark- Keyboard movedup Methods
//---------- KeyBoard Movedup methods -----------------------//
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        _isKeyboardOnScreen=YES;
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        _isKeyboardOnScreen=NO;
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

-(void)dismisskeyboradafterEndInput:(UITextField*)sender
{
    [sender resignFirstResponder];
    
    // NSString *firstStr=_txtName.text;
    //  NSString *secondStr=_txtPass.text;
    
    if([_txtEmail.text length] ==0)
    {
    }
    if ([_txtFName.text length]==0) {
    }
    if ([_txtLName.text length]==0) {
    }
    if ([_txtMob.text length]==0) {
    }
    if ([_txtPass.text length]==0) {
    }
    
}


#pragma mark: TextField Validation Here
-(BOOL)isValidTextfield
{
    NSString *strMsg;
    if(_txtEmail.text!=nil && _txtEmail.text.length>0)
    {
        if (_txtFName.text!=nil && _txtFName.text.length>0)
        {
            if (_txtLName.text!=nil && _txtLName.text.length>0)
            {
                if (_txtMob.text!=nil && _txtMob.text.length>0)
                {
                    if (_txtPass.text!=nil && _txtPass.text.length>0)
                    {
                            return true;
                    }
                    else
                    {
                        strMsg=NSLocalizedString(@"Please enter Password.",nil);
                    }
                }
                else
                {
                    strMsg=NSLocalizedString(@"Please enter Mobile Number.",nil);
                }
            }
            else
            {
                strMsg=NSLocalizedString(@"Please enter Last Name.",nil);
            }
        }
        else
        {
            strMsg=NSLocalizedString(@"Please enter First Name.",nil);
        }
    }
    else
    {
        strMsg=NSLocalizedString(@"Please enter Email ID.",nil);
    }
    
    UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil                                                            message:strMsg
        delegate:nil
        cancelButtonTitle:nil
        otherButtonTitles:nil, nil];
    toast.backgroundColor=[UIColor redColor];
    [toast show];
    int duration = 2; // duration in seconds
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{                [toast dismissWithClickedButtonIndex:0 animated:YES];            });
    return NO;
}

#pragma mark: UIButton Pressed delegate declarations here
-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnTandCPressed:(id)sender
{
    [self viewForTermsAndCondition];
}

-(IBAction)btnPandPPressed:(id)sender
{
    [self viewForPrivcyAndPolicy];
}

#pragma mark: Text DOB Pressed
-(IBAction)txtDOBPressed:(id)sender
{
    _dteDOB1.datePickerMode=YES;
    _dteDOB1.selected=YES;
    
    UIDatePicker *picker = (UIDatePicker*)_txtDob.inputView;
    [picker setMaximumDate:[NSDate date]];
    NSDate *date = picker.date;
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateformate stringFromDate:date];
    _txtDob.text = [NSString stringWithFormat:@"%@",dateString];
    _txtDob.textColor=[[UIColor whiteColor]colorWithAlphaComponent:0.8];
}

#pragma mark: UIButton Pressed delegate declarations here
-(IBAction)btnDOBDonePressed:(id)sender
{
    [_txtDob resignFirstResponder];
    [tbDOB removeFromSuperview];
    self.view.userInteractionEnabled=YES;
}

-(IBAction)btnSignUpPressed:(id)sender
{
    if ([self isValidTextfield])
    {
        [self SignUpServerCall];
    }
}
#pragma mark-Close Comment View
-(void)closeTTandPPViewPressed
{
    @try
    {
        _viewForTandC.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
        _viewForPandP.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
        
        [self enableSubViews];
        for(UIView *subview in [_viewForTandC subviews])
        {
            [subview removeFromSuperview];
        }
        for(UIView *subview in [_viewForPandP subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForTT=_viewForTandC.frame;
        CGRect frameForViewForPP=_viewForPandP.frame;
        
        frameForViewForTT=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        frameForViewForPP=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForTandC.frame=frameForViewForTT;
            _viewForPandP.frame=frameForViewForTT;
        } completion:^(BOOL finished){
            [_viewForTandC removeFromSuperview];
            [_viewForPandP removeFromSuperview];
            
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}
#pragma mark: view For Terms & Condition
-(void)viewForTermsAndCondition
{
    [_viewForTandC removeFromSuperview];
    _viewForTandC =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForTandC.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForTandC.tag=20;
    [self.view addSubview:_viewForTandC];
    CGRect frameForViewForHelp=_viewForTandC.frame;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        frameForViewForHelp=CGRectMake(10,70,screenWidth-20,screenHeight-80);
    }
    else
    {
        frameForViewForHelp=CGRectMake(10,40,screenWidth-20,screenHeight-80);
    }
    [UIView animateWithDuration:0.5f animations:^{
        _viewForTandC.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        _viewForTandC.backgroundColor=[UIColor clearColor];
        vscreenSize=_viewForTandC.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,_viewForTandC.frame.size.width,_viewForTandC.frame.size.height)];
        img.image=[UIImage imageNamed:@"img_T&C"];
        [_viewForTandC addSubview:img];
        
        UILabel *lblTerms=[[UILabel alloc]initWithFrame:CGRectMake(0,10,img.frame.size.width,50)];
        lblTerms.text=@"Terms of Service";
        lblTerms.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:35];
        lblTerms.textColor=[UIColor whiteColor];
        lblTerms.textAlignment=NSTextAlignmentCenter;
        [img addSubview:lblTerms];
        
        UIView *viewSept=[[UIView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblTerms.frame)-5,img.frame.size.width,2)];
        [viewSept setBackgroundColor:[UIColor colorWithRed:237/255.0 green:124/255.0 blue:8/255.0 alpha:1]];
        [img addSubview:viewSept];
        
        UILabel *lbltermsOfService=[[UILabel alloc]initWithFrame:CGRectMake(0,img.frame.size.height/2.5-15,img.frame.size.width,30)];
        lbltermsOfService.text=@"Terms of Service is here";
        lbltermsOfService.textColor=[UIColor whiteColor];
        lbltermsOfService.textAlignment=NSTextAlignmentCenter;
        lbltermsOfService.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [img addSubview:lbltermsOfService];
        
        _btnCloseTandC=[[UIButton alloc]initWithFrame:CGRectMake(_viewForTandC.frame.size.width-60,_viewForTandC.frame.size.height-60,40,40)];
        [_btnCloseTandC setBackgroundImage:[UIImage imageNamed:@"btn_Close"] forState:UIControlStateNormal];
        [_btnCloseTandC addTarget:self action:@selector(closeTTandPPViewPressed) forControlEvents:UIControlEventTouchUpInside];
        [_viewForTandC addSubview:_btnCloseTandC];
        UIWebView *webData=[[UIWebView alloc]initWithFrame:CGRectMake(10,CGRectGetMaxY(viewSept.frame)+5,_viewForTandC.frame.size.width-20,_viewForTandC.frame.size.height-130)];
        webData.backgroundColor=[UIColor clearColor];
        NSString *urlString=base_URL;
        NSString *strURL=[NSString stringWithFormat:@"wsterm_condation.php"];
        urlString=[urlString stringByAppendingString:strURL];
        [webData loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
        [_viewForTandC addSubview:webData];
    }];
    [self disableSubViews:_viewForTandC];
}

#pragma mark: view For privacy policy
-(void)viewForPrivcyAndPolicy
{
    [_viewForPandP removeFromSuperview];
    _viewForPandP =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForPandP.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForPandP.tag=20;
    [self.view addSubview:_viewForPandP];
    CGRect frameForViewForHelp=_viewForPandP.frame;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        frameForViewForHelp=CGRectMake(10,70,screenWidth-20,screenHeight-80);
    }
    else
    {
        frameForViewForHelp=CGRectMake(10,40,screenWidth-20,screenHeight-80);
    }
    [UIView animateWithDuration:0.5f animations:^{
        _viewForPandP.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        _viewForPandP.backgroundColor=[UIColor clearColor];
        vscreenSize=_viewForPandP.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,_viewForPandP.frame.size.width,_viewForPandP.frame.size.height)];
        img.image=[UIImage imageNamed:@"img_T&C"];
        [_viewForPandP addSubview:img];
        
        UILabel *lblPolicy=[[UILabel alloc]initWithFrame:CGRectMake(0,10,img.frame.size.width,50)];
        lblPolicy.text=@"Privacy Policy";
        lblPolicy.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:35];
        lblPolicy.textColor=[UIColor whiteColor];
        lblPolicy.textAlignment=NSTextAlignmentCenter;
        [img addSubview:lblPolicy];
        
        UIView *viewSept=[[UIView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblPolicy.frame)-5,img.frame.size.width,2)];
        [viewSept setBackgroundColor:[UIColor colorWithRed:237/255.0 green:124/255.0 blue:8/255.0 alpha:1]];
        [img addSubview:viewSept];
        
        
        UILabel *lblPP=[[UILabel alloc]initWithFrame:CGRectMake(0,img.frame.size.height/2.5-15,img.frame.size.width,30)];
        lblPP.text=@"Privacy Policy is here";
        lblPP.textColor=[UIColor whiteColor];
        lblPP.textAlignment=NSTextAlignmentCenter;
        lblPP.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [img addSubview:lblPP];
        
        _btnClosePandP=[[UIButton alloc]initWithFrame:CGRectMake(_viewForPandP.frame.size.width-60,_viewForPandP.frame.size.height-60,40,40)];
        [_btnClosePandP setBackgroundImage:[UIImage imageNamed:@"btn_Close"] forState:UIControlStateNormal];
        [_btnClosePandP addTarget:self action:@selector(closeTTandPPViewPressed) forControlEvents:UIControlEventTouchUpInside];
        [_viewForPandP addSubview:_btnClosePandP];
        
        UIWebView *webData=[[UIWebView alloc]initWithFrame:CGRectMake(10,CGRectGetMaxY(viewSept.frame)+5,_viewForPandP.frame.size.width-20,_viewForPandP.frame.size.height-130)];
        webData.backgroundColor=[UIColor clearColor];
        [webData loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://54.67.95.152/snapbets/aws/wsprivacy_policy.php"]]];
        [_viewForPandP addSubview:webData];
    }];
    [self disableSubViews:_viewForPandP];
}


#pragma mark: Disable SubView
-(void)disableSubViews:(UIView *)view
{
    for(UIView *subview in [self.view subviews])
    {
        if ((subview.tag==view.tag&&[subview isMemberOfClass:[UIView class]]))
        {
            
        }
        else
        {
            subview.userInteractionEnabled=NO;
            //subview.alpha=.5;
        }
    }
}

#pragma mark: Enable Subview
-(void)enableSubViews
{
    for(UIView *subview in [self.view subviews])
    {
        subview.userInteractionEnabled=YES;
        // subview.alpha=1;
    }
}

#pragma mark: view For Alert
-(void)viewForAlertUI
{
    [_viewForAlert removeFromSuperview];
    _viewForAlert =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForAlert.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForAlert.tag=20;
    [self.view addSubview:_viewForAlert];
    
    CGRect frameForViewForHelp=_viewForAlert.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForAlert.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForAlert.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForAlert addSubview:img];
        UILabel *lblAlert=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForAlert.frame.size.width,30)];
        lblAlert.text=_strAlertTitle;
        lblAlert.textAlignment=NSTextAlignmentCenter;
        lblAlert.textColor=[UIColor whiteColor];
        lblAlert.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForAlert addSubview:lblAlert];
        
        _btnCloseAlert=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseAlert.frame=CGRectMake(_viewForAlert.frame.size.width/2-60,CGRectGetMaxY(lblAlert.frame)+50,120,40);
        }
        else
        {
            _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,CGRectGetMaxY(lblAlert.frame)+50,100,40)];
        }
        if (_isLogin) {
            [_btnCloseAlert setTitle:@"Login" forState:UIControlStateNormal];
            
        }
        else
        {
            [_btnCloseAlert setTitle:@"Close" forState:UIControlStateNormal];
            
        }
        [_btnCloseAlert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseAlert.layer.borderWidth=2.0;
        _btnCloseAlert.layer.cornerRadius=4.0;
        _btnCloseAlert.layer.borderColor=[[UIColor orangeColor]CGColor];
        //[_btnVoteSeeMore addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [_btnCloseAlert addTarget:self action:@selector(viewForCloseAlert) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewForAlert addSubview:_btnCloseAlert];
    }];
}

#pragma mark-Close Comment View
-(void)viewForCloseAlert
{
    @try
    {
        for(UIView *subview in [_viewForAlert subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForVote=_viewForAlert.frame;
        frameForViewForVote=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            _viewForAlert.frame=frameForViewForVote;
        } completion:^(BOOL finished){
            
            [_viewForAlert removeFromSuperview];
            
            if (_isLogin)
            {
                SignInScreen *signIn=[[SignInScreen alloc]initWithNibName:@"SignInScreen" bundle:nil];
                [self.navigationController pushViewController:signIn animated:YES];
            }
           
            
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}
#pragma mark: User SignUp Server Call
-(void)SignUpServerCall
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *urlString=base_URL;
    
        NSString *strURL=[NSString stringWithFormat:@"wsregistration.php?email=%@&password=%@&dob=No&fname=%@&lname=%@&phone=%@",_txtEmail.text,_txtPass.text,_txtFName.text,_txtLName.text,_txtMob.text];
        
        urlString=[urlString stringByAppendingString:strURL];
        urlString=[urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *urlSignUp=[NSURL URLWithString:urlString];
        request=[NSMutableURLRequest requestWithURL:[urlSignUp standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"The Internet connection appears to be offline" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    request = nil;
    
    
    }

#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    [theAppDelegate stopSpinner];
    [self.view setUserInteractionEnabled:YES];
    
    dict =[[NSMutableDictionary alloc]initWithDictionary:response];
    
        NSString *strMSg=[dict valueForKey:@"message"];
        if ([strMSg isEqualToString:@"sus"])
        {
            NSLog(@"Sing up successfull");
            _strAlertTitle=@"Successfully Registered";
            _isLogin=YES;
            [self viewForAlertUI];
        }
        else if ([strMSg isEqualToString:@"already"])
        {
            NSLog(@"Already Registered");
            _strAlertTitle=@"Already registered with same Email";
            _isLogin=YES;
            [self viewForAlertUI];
        }
        else
        {
            _strAlertTitle=@"Sorry failed to register";
            _isLogin=NO;
            [self viewForAlertUI];
        }
    
        _txtEmail.text=@"";
        _txtPass.text=@"";
        _txtDob.text=@"";
        _txtFName.text=@"";
        _txtMob.text=@"";
        _txtLName.text=@"";
}
@end
