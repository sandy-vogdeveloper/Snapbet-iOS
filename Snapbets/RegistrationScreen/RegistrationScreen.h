//
//  RegistrationScreen.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 06/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderAndConstants.h"
#import "WebConnection1.h"
#import "Reachability.h"
#import "SignInScreen.h"

@interface RegistrationScreen : UIViewController<UITextFieldDelegate,NSURLConnectionDataDelegate,NSURLConnectionDelegate>
{
    WebConnection1 *connection;
    NSMutableDictionary *dict;
    CGRect vscreenSize;
    CGFloat vscreenHeight,vscreenWidth;
    UIToolbar *toolBarsDOB;
    NSString *strDOB;
    NSDate   *dteDOB;
    UIBarButtonItem *doneButton;
    UIToolbar *tbDOB;
    NSArray *arrtbDOB;
}

#pragma mark: UIView Properties declarations here
@property(nonatomic)IBOutlet UIView *viewForTandC,*viewForPandP,*viewForAlert;

#pragma mark:UIButton Properties declarations here
@property (nonatomic)IBOutlet UIButton *btnBack,*btnBackPress,*btnSubmit,*btnTermsCondi,*btnPrivyPolycy,*btnCloseTandC,*btnClosePandP,*btnCloseAlert;

@property(nonatomic)Reachability *reachbility;

#pragma mark:NSString properties declarations here
@property(nonatomic)NSString *strAlertTitle;

#pragma mark: UITextField Properties declarations here
@property (nonatomic)IBOutlet UITextField *txtEmail,*txtPass,*txtDob,*txtFName,*txtLName,*txtMob;

#pragma mark: UIImageView Properties declarations here
@property (nonatomic)IBOutlet UIImageView *imgTrangleBG,*imgLogo;

#pragma mark:- Picker view declarations here
@property (nonatomic)UIDatePicker *dteDOB1;

#pragma mark: Bool properties declarations here
@property(nonatomic)BOOL isKeyboardOnScreen,isLogin;

@end
