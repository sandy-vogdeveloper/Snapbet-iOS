//
//  SpecifiBetPage.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 28/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

/*
 A cool App. Store and download sweet memories of your loved ones, always forever. Happy moments, sour moments funny memories..
 Distribute & Share with family & friends
 
http://marvsystems.com/
 © 2016 Marv Systems (P) Ltd.
 Marv   Systems
 117/H2/84 1st Floor
 Pandu Nagar
 Kanpur Uttar Pradesh
 208005
 +91-8090842211
 info@marvsystems.com
 Lakshendra Singh 
 +91-8090842211
 lakshendra@gmail.com
 */

#import "SpecifiBetPage.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "SAVideoRangeSlider.h"
#import "PECropViewController.h"
#import "MainDashboardScreen.h"

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

@interface SpecifiBetPage ()<PECropViewControllerDelegate,FBSDKAppInviteDialogDelegate>

   //@property (nonatomic, strong) ALMoviePlayerController *moviePlayer;
    @property (nonatomic) CGRect defaultFrame;

#pragma mark - FB sharing property
@property(strong,nonatomic)IBOutlet FBSDKShareButton *shareButton,*shareButton1;
@property(strong,nonatomic)  FBSDKShareLinkContent *content;
@property(strong,nonatomic) FBSDKShareVideo *video;
@property (nonatomic)FBSDKShareVideoContent *vContent;

@property (strong, nonatomic) NSString *originalVideoPath;
@property (nonatomic) CGFloat startTime;
@property (nonatomic) CGFloat stopTime;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *myActivityIndicator;
@property (assign, nonatomic) CGFloat videoPlaybackPosition;
@property (weak, nonatomic) IBOutlet UIButton *trimButton;
@property (weak, nonatomic) IBOutlet UIView *videoPlayer;
@property (weak, nonatomic) IBOutlet UIView *videoLayer;
@property (strong, nonatomic) AVAssetExportSession *exportSession;
@property (strong, nonatomic) AVAsset *asset;

@property (strong, nonatomic) NSString *tempVideoPath;
@property (assign, nonatomic) BOOL restartOnPlay;

@end

@implementation SpecifiBetPage

- (void)viewDidLoad
{
    [super viewDidLoad];
    _isRemoveChlngrBet=NO;
    _isRemoveChlngBet=NO;
    _isLFromManyBet=NO;
    _isbtnLikePressed=NO;
    
    _arrMyVideoData=[[NSMutableArray alloc]init];
    _arrMyVideoData=[[NSMutableArray alloc]init];
    _videoURLArray=[[NSMutableArray alloc]init];
    _assetItems=[[NSMutableArray alloc]init];
    _arrWinner=[[NSMutableArray alloc]init];
    _isSnapSuccess=NO;
    yOffset=0.0;
    _isVideoPlayed=NO;
    [self getDataFromDB];
    [_dic removeAllObjects];
    
    _arrSeconds=[[NSMutableArray alloc]init];
    _arrMinute=[[NSMutableArray alloc]init];
    _arrHour=[[NSMutableArray alloc]init];
    [self loadInitialUI];
    [self specificBetPageServerAPI];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.tempVideoPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"tmpMov.mov"];
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark: load InitialUI
-(void)loadInitialUI
{
    _btnBack=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnJoinSnpBet=[[UIButton alloc]init];
    UIImageView *imgPkgBG=[[UIImageView alloc]init];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        _imgHeaderBG=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,screenWidth,200)];
        _asyHeaderBG=[[AsyncImageView alloc]initWithFrame:CGRectMake(0,0,screenWidth,200)];
        
        _btnJoinSnpBet.frame=CGRectMake(screenWidth-150,50,150,35);
        imgPkgBG.frame=CGRectMake(0,_imgHeaderBG.frame.size.height-100,150,100);
    }
    else
    {
        _imgHeaderBG=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,screenWidth,150)];
        _asyHeaderBG=[[AsyncImageView alloc]initWithFrame:CGRectMake(0,0,screenWidth,150)];
        _btnJoinSnpBet.frame=CGRectMake(screenWidth-100,50,100,25);
        imgPkgBG.frame=CGRectMake(0,75,130,75);
    
    }
    
    _imgHeaderBG.image=[UIImage imageNamed:@"icon_BG"];
    [self.view addSubview:_imgHeaderBG];
    
    
    imgPkgBG.image=[UIImage imageNamed:@"img_NewBGBet"];
    //[[UIColor blackColor]colorWithAlphaComponent:.7];
   
    if (_strBetImg.length>0)
    {
        NSString *strBURL=ImgBase_URL;
        _asyHeaderBG.backgroundColor = [UIColor clearColor];
        strBURL=[strBURL stringByAppendingString:_strBetImg];
        NSURL *url=[NSURL URLWithString:strBURL];
        [_asyHeaderBG loadImageFromURL:url];
        [_asyHeaderBG setContentMode:UIViewContentModeScaleAspectFill];
        [_asyHeaderBG sizeToFit];
        _asyHeaderBG.clipsToBounds = YES;
        [self.view addSubview:_asyHeaderBG];
    }
    [_btnJoinSnpBet setBackgroundImage:[UIImage imageNamed:@"icon_btnSnpbet"] forState:UIControlStateNormal];
    _btnJoinSnpBet.hidden=YES;
    [self.view addSubview:_btnJoinSnpBet];
    [self.view addSubview:imgPkgBG];
    
    UILabel *lblBigOne=[[UILabel alloc]initWithFrame:CGRectMake(5,75,125,30)];
    
    if ([_strThemeName isKindOfClass:[NSNull class]] || _strThemeName == nil)
    {
        _strThemeName=@"";
    }
    lblBigOne.text=_strThemeName;
    lblBigOne.textColor=[UIColor whiteColor];
    NSArray *arr=[_strThemeName componentsSeparatedByString:@" "];
    lblBigOne.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:26];
    
    if([arr count]==1)
    {
        lblBigOne.frame=CGRectMake(0,78,130,30);
        NSInteger count=_strThemeName.length;
        NSLog(@"%ld",(long)count);
        if (count==12||count<=12)
        {
            lblBigOne.frame=CGRectMake(0,100,130,30);
           if(screenWidth<325)
           {
               lblBigOne.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:18];
            }
           else{ lblBigOne.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:18];}
             lblBigOne.text=_strThemeName;
            lblBigOne.textAlignment=NSTextAlignmentCenter;
        }
        else
        {
            lblBigOne.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:28];
            [lblBigOne setNumberOfLines:2];
            [lblBigOne sizeToFit];
        }
    }
    else if([arr count]==2 || [arr count]==3 || [arr count]==4 || [arr count]==5)
    {
        lblBigOne.frame=CGRectMake(0,78,130,30);
        NSInteger count=_strThemeName.length;
        NSLog(@"%ld",(long)count);
        if (count==12||count<=12)
        {
            lblBigOne.frame=CGRectMake(0,100,130,30);
            if(screenWidth<325)
            {
                lblBigOne.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:18];
            }
            else
            {
                lblBigOne.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:18];
            }
                lblBigOne.text=_strThemeName;
                lblBigOne.textAlignment=NSTextAlignmentCenter;
        }
        else
        {
            lblBigOne.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:28];
            [lblBigOne setNumberOfLines:2];
            [lblBigOne sizeToFit];
        }
    }
    else
    {
        lblBigOne.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:28];
        [lblBigOne setNumberOfLines:2];
        [lblBigOne sizeToFit];
    }
    
    [self.view addSubview:lblBigOne];
    
    _btnBack.frame = CGRectMake(8,8,25,25);
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"icon_BackMove"] forState:UIControlStateNormal];
    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    _btnBackPress=[[UIButton alloc]initWithFrame:CGRectMake(0,0,55,30)];
    [_btnBackPress addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnBack];
    [self.view addSubview:_btnBackPress];
    
    _tblBet=[[UITableView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_imgHeaderBG.frame),screenWidth,screenHeight-_imgHeaderBG.frame.size.height)];
    _tblBet.delegate=self;
    _tblBet.dataSource=self;
    _tblBet.tableFooterView=[[UIView alloc]init];
    _tblBet.backgroundColor=[UIColor clearColor];
    [self.view addSubview:_tblBet];
    [_btnJoinSnpBet addTarget:self action:@selector(btnJoinSnapBetPressed:) forControlEvents:UIControlEventTouchUpInside];
}

-(IBAction)btnBackPressed:(id)sender
{
    if(theAppDelegate.isLodedFromAddDelegate)
    {
        theAppDelegate.isLodedFromAddDelegate=NO;
        MainDashboardScreen *dash=[[MainDashboardScreen alloc]initWithNibName:@"MainDashboardScreen" bundle:nil];
        [self.navigationController pushViewController:dash animated:NO];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(IBAction)btnJoinSnapBetPressed:(id)sender
{
    //[self viewForPostReportPopUPUI];
    [ self viewForPostResponsePopUPUI];
}

-(IBAction)btnCloseChallengePressed:(id)sender
{
    [self closeViewButonPressed];
}

-(IBAction)btnCloseVideoUIPressed:(id)sender
{
    [_playerViewController.player pause];
    
    [self closeViewButonPressed];
    [self specificBetPageServerAPI];
}
-(IBAction)btnFBSharePressed:(id)sender
{
    [self closeViewButonPressed];
}

-(IBAction)btnFbInvitePressed:(id)sender
{
   [self closeViewButonPressed];
    NSString *strBaseURL=ImgBase_URL;
    NSString *strVURL=vBase_URL;
    NSString *str;
    NSString *strVideo;
    
    if ([strShareUser isEqualToString:@"challenge"])
    {
        str=[_arrChallenge valueForKey:@"image"];
        strVideo=[_arrChallenge valueForKey:@"video"];
    }
    else if ([strShareUser isEqualToString:@"challenger"])
    {
        str=[[_arrChallenger valueForKey:@"image"]objectAtIndex:slectedShareIndex];
        strVideo=[[_arrChallenger valueForKey:@"video"]objectAtIndex:slectedShareIndex];
    }
    
    strBaseURL=[strBaseURL stringByAppendingString:str];
    strVURL=[strVURL stringByAppendingString:strVideo];
    
    NSString *strBase=ImgBase_URL;
    strBase=[strBase stringByAppendingString:str];
    
    FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init];
    //content.appLinkURL = [NSURL URLWithString:@"https://fb.me/1444996258928276"];
    content.appLinkURL = [NSURL URLWithString:@"https://fb.me/1445063282254907"];
   // content.promotionText=_strThemeName;
    //optionally set previewImageURL
    content.appInvitePreviewImageURL = [NSURL URLWithString:strBaseURL];
    
    // Present the dialog. Assumes self is a view controller
    // which implements the protocol `FBSDKAppInviteDialogDelegate`.
    [FBSDKAppInviteDialog showFromViewController:self
                                     withContent:content
                                        delegate:self];
}
- (void)appInviteDialog:	(FBSDKAppInviteDialog *)appInviteDialog
 didCompleteWithResults:	(NSDictionary *)results
{
    NSLog(@"%@",results);
}

-(void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error
{
    
    NSLog(@"%@",error);
}

-(IBAction)btnTwitterSharePressed:(id)sender
{    
    _loader = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_loader startAnimating];
    _loader.backgroundColor=[UIColor blackColor];
    _loader.center = self.view.center;
    [self.view addSubview:_loader];
    
    //[self closeViewButonPressed];
    [_loader startAnimating];
    [theAppDelegate showSpinnerInView:self.view];
    
    NSString *strBaseURL=ImgBase_URL;
    NSString *strVURL=vBase_URL;
    NSString *str;
    NSString *strVideo;
    
    if ([strShareUser isEqualToString:@"challenge"])
    {
        str=[_arrChallenge valueForKey:@"image"];
        strVideo=[_arrChallenge valueForKey:@"video"];
    }
    else if ([strShareUser isEqualToString:@"challenger"])
    {
        str=[[_arrChallenger valueForKey:@"image"]objectAtIndex:slectedShareIndex];
        strVideo=[[_arrChallenger valueForKey:@"video"]objectAtIndex:slectedShareIndex];
    }
    
    strBaseURL=[strBaseURL stringByAppendingString:str];
    strVURL=[strVURL stringByAppendingString:strVideo];
    
    NSString *strBase=ImgBase_URL;
    strBase=[strBase stringByAppendingString:str];
    
    [self saveImagesInLocalDirectory:strBaseURL imgNAME:str];
    
    /*SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    NSString *strShareText = [NSString stringWithFormat:@"View Snapbets to accept new Challenges"];
    
    [mySLComposerSheet setInitialText:strShareText];
    
    // [mySLComposerSheet addImage:[UIImage imageNamed:strShareImg]];
    
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                
                NSLog(@"Post Canceled");
                break;
            case SLComposeViewControllerResultDone:
                
                NSLog(@"Post Sucessful");
                break;
            default:
                break;
        }
    }];
    [self closeViewButonPressed];
    [self presentViewController:mySLComposerSheet animated:YES completion:nil];
     */
}

-(void)twiterSharing
{
    
    [theAppDelegate stopSpinner];
    SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    NSString *strShareText = [NSString stringWithFormat:@"%@ \n Check this out on the Snapbets app!",_strThemeName];
    
    [mySLComposerSheet setInitialText:strShareText];
    
    [mySLComposerSheet addImage:[UIImage imageNamed:strTwiterImagePath]];
    
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                
                NSLog(@"Post Canceled");
                break;
            case SLComposeViewControllerResultDone:
                
                NSLog(@"Post Sucessful");
                break;
            default:
                break;
        }
    }];
    [theAppDelegate stopSpinner];
    [_loader stopAnimating];
    [self closeViewButonPressed];
    [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    
}
-(void)saveImagesInLocalDirectory:(NSString *)imgUrl imgNAME:(NSString*)imgName
{
    NSString * documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *writablePath = [documentsDirectoryPath stringByAppendingPathComponent:imgName];
    
    if(![fileManager fileExistsAtPath:writablePath]){
        // file doesn't exist
        NSLog(@"file doesn't exist");
        //save Image From URL
        NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgUrl]];
        
        NSError *error = nil;
        [data writeToFile:[documentsDirectoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imgName]] options:NSAtomicWrite error:&error];
        
        if (error) {
            [_loader stopAnimating];
            NSLog(@"Error Writing File : %@",error);
        }else
        {
            [_loader stopAnimating];
            NSLog(@"Image %@ Saved SuccessFully",imgName);
            NSString *strPath=[documentsDirectoryPath stringByAppendingString:@"/"];
            strTwiterImagePath=[strPath stringByAppendingString:imgName];
            NSLog(@"%@",strPath);
            [self twiterSharing];
            
        }
    }
    else{
        // file exist
        NSLog(@"file exist");
        NSString *strPath=[documentsDirectoryPath stringByAppendingString:@"/"];
        strTwiterImagePath=[strPath stringByAppendingString:imgName];
        NSLog(@"%@",strPath);
        [self twiterSharing];
    }
}

-(IBAction)btnShareChalngPressed:(id)sender
{
    strShareUser=@"challenge";
     [self viewForShareUI];
}
-(IBAction)btnSharePressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    slectedShareIndex=btn.tag;
    strShareUser=@"challenger";
    [self viewForShareUI];
}

-(IBAction)btnVotePressed:(id)sender
{
    
    if (_isWinner)
    {
        _strVoteAlertTitle=@"Oops! you can’t vote as winner has already been declared!";
        [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.1];
    }
    else
    {
        UIButton *btn=(UIButton*)sender;
        NSInteger row=btn.tag;
        NSString *strTID=[[_arrChallenger valueForKey:@"uid"]objectAtIndex:row];
        NSMutableURLRequest *request;
        if (theAppDelegate.isConnected)
        {
            _isCCData=NO;
            _isJoinSnpbt=NO;
            _isChlngrVote=YES;
            _isJoinVideoUpload=NO;
            _isSnapVote=NO;
            _isSubmitReportPrsed=NO;
        
            [theAppDelegate showSpinnerInView:self.view];
            [self.view setUserInteractionEnabled:NO];
            NSString *strBaseURL=base_URL;
        
            NSString *urlString=[NSString stringWithFormat:@"wsaddchallenger_vote.php?uid=%@&sid=%@&tid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],_selectedBetID1,strTID];
        
            strBaseURL=[strBaseURL stringByAppendingString:urlString];
            NSURL *urlChallenger=[NSURL URLWithString:strBaseURL];
        
            request=[NSMutableURLRequest requestWithURL:[urlChallenger standardizedURL]];
        
            if(!connection1)
                connection1= [[WebConnection1 alloc] init];
                connection1.delegate = self;
                [connection1 makeConnection:request];
        }
        else
        {
            theAppDelegate.alert.message=@"Check Internet connection";
            [theAppDelegate.alert show];
        }
            request = nil;
    }
}
-(IBAction)btnChlngVotePressed:(id)sender
{
        UIButton *btn=(UIButton*)sender;
        NSInteger row=btn.tag; NSLog(@"%ld",(long)row);
        NSString *strChngUID=[_arrChallenge valueForKey:@"uid"];
        if(theAppDelegate.isMyBeetSelected && [strChngUID isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:0]])
        {
            strSID=[_arrChallenge valueForKey:@"sid"];
            _isRemoveChlngBet=YES;
            _isRemoveChlngrBet=NO;
            _isCCData=NO;
            _isJoinSnpbt=NO;
            _isChlngrVote=NO;
            _isJoinVideoUpload=NO;
            _isSnapVote=NO;
            _isSubmitReportPrsed=NO;
            if(_isWinner)
            {
                _strVoteAlertTitle=@"Oops! you cant remove this snapbet as winner has been declared!";
                [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.1];
            }
            else
            {
                [self viewfForYesNoAlertUI1];
            }
        }
        else if([strChngUID isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:0]])
        {
            strSID=[_arrChallenge valueForKey:@"sid"];
            _isRemoveChlngBet=YES;
            _isRemoveChlngrBet=NO;
            _isCCData=NO;
            _isJoinSnpbt=NO;
            _isChlngrVote=NO;
            _isJoinVideoUpload=NO;
            _isSnapVote=NO;
            _isSubmitReportPrsed=NO;
            if(_isWinner)
            {
                _strVoteAlertTitle=@"Oops! you cant remove this snapbet as winner has been declared!";
                [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.1];
            }
            else
            {
                [self viewfForYesNoAlertUI1];
            }
        }
        else
        {
            if (_isWinner)
            {
                _strVoteAlertTitle=@"Oops! you can’t vote as winner has already been declared!";
                [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.1];
            }
            else
            {
                NSMutableURLRequest *request;
                if (theAppDelegate.isConnected)
                {
                    _isCCData=NO;
                    _isJoinSnpbt=NO;
                    _isChlngrVote=NO;
                    _isJoinVideoUpload=NO;
                    _isSnapVote=YES;
                    _isSubmitReportPrsed=NO;
            
                    [theAppDelegate showSpinnerInView:self.view];
                    [self.view setUserInteractionEnabled:NO];
                    NSString *strBaseURL=base_URL;
                    NSString *urlString;
            
                    urlString=[NSString stringWithFormat:@"wsaddsnapvote.php?uid=%@&sid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],_selectedBetID1];
           
                    strBaseURL=[strBaseURL stringByAppendingString:urlString];
                    NSURL *urlChallenger=[NSURL URLWithString:strBaseURL];
                
                    request=[NSMutableURLRequest requestWithURL:[urlChallenger standardizedURL]];
            
                    if(!connection1)
                        connection1= [[WebConnection1 alloc] init];
                        connection1.delegate = self;
                    [connection1 makeConnection:request];
                }
                else
                {
                    theAppDelegate.alert.message=@"Check Internet connection";
                    [theAppDelegate.alert show];
                }
                request = nil;
            }
        }
}
-(IBAction)btnChlngerVotePressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    NSString *strTID=[[_arrChallenger valueForKey:@"uid"]objectAtIndex:row];
    strSID=[[_arrChallenger valueForKey:@"sid"]objectAtIndex:row];
    _strChlgerRemoveID=[[_arrChallenger valueForKey:@"uid"]objectAtIndex:row];
    NSString *strLUID=[theAppDelegate.arrUserLoginData objectAtIndex:0];
    
    if([_strChlgerRemoveID isEqualToString:strLUID])
    {
        _isRemoveChlngrBet=YES;
        _isRemoveChlngBet=NO;
        _isCCData=NO;
        _isJoinSnpbt=NO;
        _isChlngrVote=NO;
        _isJoinVideoUpload=NO;
        _isSnapVote=NO;
        _isSubmitReportPrsed=NO;

        if (_isWinner)
        {
            _strVoteAlertTitle=@"Oops! you cant remove this snapbet as winner has been declared!";
            [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.1];
        }
        else
        {
            [self viewfForYesNoAlertUI1];
        }
    }
    else
    {
        if (_isWinner)
        {
            _strVoteAlertTitle=@"Oops! you can’t vote as winner has already been declared!";
            [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.1];
        }
        else
        {
            NSMutableURLRequest *request;
            if (theAppDelegate.isConnected)
            {
                _isCCData=NO;
                _isJoinSnpbt=NO;
                _isChlngrVote=YES;
                _isJoinVideoUpload=NO;
                _isSnapVote=NO;
                _isSubmitReportPrsed=NO;
        
                [theAppDelegate showSpinnerInView:self.view];
                [self.view setUserInteractionEnabled:NO];
                NSString *strBaseURL=base_URL;
                
                NSString *urlString;
            
                urlString=[NSString stringWithFormat:@"wsaddchallenger_vote.php?uid=%@&sid=%@&tid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],strSID,strTID];
            
                strBaseURL=[strBaseURL stringByAppendingString:urlString];
                NSURL *urlChallenger=[NSURL URLWithString:strBaseURL];
                request=[NSMutableURLRequest requestWithURL:[urlChallenger standardizedURL]];
        
                if(!connection1)
                    connection1= [[WebConnection1 alloc] init];
                connection1.delegate = self;
                [connection1 makeConnection:request];
            }
            else
            {
                theAppDelegate.alert.message=@"Check Internet connection";
                [theAppDelegate.alert show];
            }
            request = nil;
        }
    }
}
-(IBAction)btnBetReportPressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    NSLog(@"%ld",(long)row);
    [self viewForPostReportPopUPUI];
}

-(IBAction)btnChooseVideoPressed:(id)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
    {
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take Photo / Video" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // take photo button tapped.
        [self takePhoto];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose Photo / Video" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // choose photo button tapped.
        [self ChoosePhotoFromExitsing];
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

#pragma mark: Choose From Gallery
-(void)ChoosePhotoFromExitsing
{
    UIImagePickerController *imageController= [[UIImagePickerController alloc]init];
    
    imageController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imageController.allowsEditing = YES;
    imageController.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    imageController.delegate = self;
    //[self presentViewController:imageController animated:YES completion:nil];
    [self performSelector:@selector(showGallery:) withObject:imageController afterDelay:1];
}

-(void)showGallery:(id)imageController
{
    [self presentViewController:imageController animated:YES completion:nil];
}
#pragma mark- TakePhotoFromCamera
-(void)takePhoto
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imageController = [[UIImagePickerController alloc] init];
        imageController.sourceType = UIImagePickerControllerSourceTypeCamera;
        // imageController.allowsEditing = NO;
        imageController.mediaTypes  = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
        imageController.delegate = self;
        imageController.allowsEditing = YES;
        if([arrPurchaseDates count])
        {
            NSDate *currDate = [NSDate date];
            NSDateFormatter *tempDateFormatter = [[NSDateFormatter alloc]init];
            [tempDateFormatter setDateFormat:@"dd-MMM-yy"];
            NSString *strCDate= [tempDateFormatter stringFromDate:currDate];            
            
            NSString *strExp=[arrExpiryDates objectAtIndex:0];
            if ([strExp isEqualToString:@"No Expiry"]) {
                theAppDelegate.strTrimingTime=@"90";
                theAppDelegate.GlobDurOfOrignalVideo=90;
                //imageController.videoMaximumDuration=90.0f;
            }
            else if ([strCDate isEqualToString:[arrExpiryDates objectAtIndex:0]])
            {
                theAppDelegate.strTrimingTime=@"15";
                theAppDelegate.GlobDurOfOrignalVideo=15;
                //imageController.videoMaximumDuration=15.0f;
            }
            else
            {
                theAppDelegate.strTrimingTime=@"90";
                theAppDelegate.GlobDurOfOrignalVideo=90;
                //imageController.videoMaximumDuration=90.0f;
                
            }
        }
        else
        {
            theAppDelegate.strTrimingTime=@"15";
            theAppDelegate.GlobDurOfOrignalVideo=15;
            //imageController.videoMaximumDuration=15.0f;
        }
        [self presentViewController:imageController animated:YES completion:nil];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Camera is not available" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

-(IBAction)btnPlayVideoPressedTbl:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    selectedVideoID=btn.tag;
    strSlectedBetTag=btn.tag;
    strSlectedBetTag=strSlectedBetTag+1;
    
    _strSelectedVideoURL=[[_arrChallenger valueForKey:@"video"]objectAtIndex:selectedVideoID];
        NSString *strVote=[[_arrChallenger valueForKey:@"alvote"] objectAtIndex:selectedVideoID];
        // NSString *strUID=[[_arrChallenger valueForKey:@"uid"]objectAtIndex:selectedVideoID];
        // NSString *strUid=[theAppDelegate.arrUserLoginData objectAtIndex:0];
    
            if ([strVote isEqualToString:@"Y"]) {
                _isAlreadyVote=YES;
            }
            else
            {
                _isAlreadyVote=NO;
            }
    
        _strSelectedThemeUser=@"Challenger";
    
        [self viewForVideoUI];
}

-(IBAction)btnPlayVideoPressedChlng:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    selectedVideoID=btn.tag;
    strSlectedBetTag=btn.tag;
    
        _strSelectedVideoURL=[_arrChallenge valueForKey:@"video"];
        NSString *strVote=[_arrChallenge valueForKey:@"alvote"];
        if ([strVote isEqualToString:@"Y"])
        {
            _isAlreadyVote=YES;
        }
        else
        {
            _isAlreadyVote=NO;
        }
        _strSelectedThemeUser=@"Challenge";
        [self viewForVideoUI];
    
}

-(IBAction)btnSubmitSnapbetPressed:(id)sender
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isJoinSnpbt=YES;
        _isCCData=NO;
        _isChlngrVote=NO;
        _isJoinVideoUpload=NO;
        _isSubmitReportPrsed=NO;
        _isRemoveChlngBet=NO;
        _isRemoveChlngrBet=NO;
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *urlJString=base_URL;
        NSString *strURL=[[NSString alloc]init];
        if([_strVideoName isEqualToString:@"No"])
        {
           strURL=[NSString stringWithFormat:@"wsaddjoinsnap.php?uid=%@&sid=%@&&video=%@&time=0.0 &url=www.fdf.df&image=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],_selectedBetID1,_strVideoName,_strUplodedImg];
        }
        else
        {
            strURL=[NSString stringWithFormat:@"wsaddjoinsnap.php?uid=%@&sid=%@&&video=%@&time=%f &url=www.fdf.df&image=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],_selectedBetID1,_strVideoName,videoTimeDuration,_strUplodedImg];
        }
        
        urlJString=[urlJString stringByAppendingString:strURL];
        urlJString=[urlJString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSURL *urlChallenger=[NSURL URLWithString:urlJString];
        
        request=[NSMutableURLRequest requestWithURL:[urlChallenger standardizedURL]];
        
        if(!connection1)
            connection1= [[WebConnection1 alloc] init];
        connection1.delegate = self;
        [connection1 makeConnection:request];
    }
    else
    {
        
    }
    request = nil;
}

-(IBAction)btnPriviousVideoPressed:(id)sender
{
    NSInteger sltedID=selectedVideoID;
    if(sltedID==0)
    {
        selectedVideoID=selectedVideoID+1;
    }
    else
    {
        sltedID=sltedID-1;
    }
    
    if (sltedID==0) {
        _lblNameVideo.text=[[_arrRelatedVData valueForKey:@"fname"] objectAtIndex:sltedID];
    }
    else
    {
        _lblNameVideo.text=[[_arrRelatedVData valueForKey:@"name"] objectAtIndex:sltedID];
    }
    _btnLikeVideo.tag=sltedID;
     NSString *strVote=[[_arrRelatedVData valueForKey:@"alvote"] objectAtIndex:sltedID];
    NSLog(@"%@",strVote);
    if([strVote isEqualToString:@"Y"])
    {
        [_btnLikeVideo setBackgroundImage:[UIImage imageNamed:@"icon_CMark"] forState:UIControlStateNormal];
        _btnLikeVideo.userInteractionEnabled=NO;
    }
    else
    {
        [_btnLikeVideo setBackgroundImage:[UIImage imageNamed:@"icon_LikeV"] forState:UIControlStateNormal];
        _btnLikeVideo.userInteractionEnabled=YES;
        
    }
    
    if([[[_arrRelatedVData valueForKey:@"uid"]objectAtIndex:sltedID] isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:0]])
    {
        _btnLikeVideo.hidden=YES;
    }
    else
    {
        _btnLikeVideo.frame=CGRectMake(_viewForVideoArea.frame.size.width-82,[UIScreen mainScreen].bounds.size.height/100*8,90,35);
        
       _lblNameVideo.frame=CGRectMake(0,[UIScreen mainScreen].bounds.size.height/100*10,_viewForVideo.frame.size.width,30);
        
        _btnLikeVideo.hidden=NO;
    }
    
    if (selectedVideoID<0|| selectedVideoID==0) {
        NSLog(@"You reached at first index");
    }
    else
    {
        NSString *strImage=[[_arrRelatedVData valueForKey:@"image"]objectAtIndex:selectedVideoID=selectedVideoID-1];
        
        _strSelectedVideoURL=[[_arrRelatedVData valueForKey:@"video"]objectAtIndex:selectedVideoID];
        if([_strSelectedVideoURL isEqualToString:@"No"])
        {
            _isVideoPlayed=YES;
            _btnPlayRltedIcon.hidden=YES;
            if ([strImage isKindOfClass:[NSNull class]]) {
                
                _btnPlayRltedIcon.hidden=YES;
                _imgVideoBG.backgroundColor=[UIColor blackColor];
                NSURL *imgURL=[NSURL URLWithString:@"No"];
                [_imgVideoBG loadImageFromURL:imgURL];
            }
            else
            {
                _btnPlayRltedIcon.hidden=YES;
                NSString *strBURL=ImgBase_URL;
                strBURL=[strBURL stringByAppendingString:strImage];
                NSURL *imgURL=[NSURL URLWithString:strBURL];
                [_imgVideoBG loadImageFromURL:imgURL];
            }
        }
        else
        {
                if ([strImage isKindOfClass:[NSNull class]])
                {
                        _imgVideoBG.backgroundColor=[UIColor blackColor];
                        NSURL *imgURL=[NSURL URLWithString:@"No"];
                        [_imgVideoBG loadImageFromURL:imgURL];
                }
                else
                {
                    _isVideoPlayed=NO;
                    _btnPlayRltedIcon.hidden=NO;
                    NSString *strBURL=ImgBase_URL;
                    strBURL=[strBURL stringByAppendingString:strImage];
                    NSURL *imgURL=[NSURL URLWithString:strBURL];
                    [_imgVideoBG loadImageFromURL:imgURL];
                }
        
            [self playVideo];
        }
    }
}

-(IBAction)btnNextVideoPressed:(id)sender
{
    NSInteger sltedID=selectedVideoID;
    if([_arrRelatedVData count]-1==sltedID)
    {
        
    }
    else
    {
        sltedID=sltedID+1;
    }
    
    if (sltedID==0) {
        _lblNameVideo.text=[[_arrRelatedVData valueForKey:@"fname"] objectAtIndex:sltedID=sltedID];
    }
    else
    {
        _lblNameVideo.text=[[_arrRelatedVData valueForKey:@"name"] objectAtIndex:sltedID];
    }
    
    NSString *strVote=[[_arrRelatedVData valueForKey:@"alvote"] objectAtIndex:sltedID];
    NSLog(@"%@",strVote);
    _btnLikeVideo.tag=sltedID;
    if([strVote isEqualToString:@"Y"])
    {
        [_btnLikeVideo setBackgroundImage:[UIImage imageNamed:@"icon_CMark"] forState:UIControlStateNormal];
        _btnLikeVideo.userInteractionEnabled=NO;
    }
    else
    {
        [_btnLikeVideo setBackgroundImage:[UIImage imageNamed:@"icon_LikeV"] forState:UIControlStateNormal];
        _btnLikeVideo.userInteractionEnabled=YES;
        
    }
    if([[[_arrRelatedVData valueForKey:@"uid"]objectAtIndex:sltedID] isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:0]])
    {
        _btnLikeVideo.hidden=YES;
    }
    
    else
    {
        _lblNameVideo.frame=CGRectMake(0,[UIScreen mainScreen].bounds.size.height/100*10,_viewForVideo.frame.size.width,30);
        _btnLikeVideo.frame=CGRectMake(_viewForVideoArea.frame.size.width-82,[UIScreen mainScreen].bounds.size.height/100*8,90,35);
        _btnLikeVideo.hidden=NO;
    }

    
    if ([_arrRelatedVData count]-1==selectedVideoID)
    {
        NSLog(@"You reached at last index");
    }
    else
    {
        NSString *strImage=[[_arrRelatedVData valueForKey:@"image"]objectAtIndex:selectedVideoID=selectedVideoID+1];
        
        _strSelectedVideoURL=[[_arrRelatedVData valueForKey:@"video"]objectAtIndex:selectedVideoID];
        if([_strSelectedVideoURL isEqualToString:@"No"])
        {
            _isVideoPlayed=YES;
            _btnPlayRltedIcon.hidden=YES;
            if ([strImage isKindOfClass:[NSNull class]]) {
                
                _imgVideoBG.backgroundColor=[UIColor blackColor];
                NSURL *imgURL=[NSURL URLWithString:@"No"];
                [_imgVideoBG loadImageFromURL:imgURL];
            }
            else
            {
                NSString *strBURL=ImgBase_URL;
                strBURL=[strBURL stringByAppendingString:strImage];
                NSURL *imgURL=[NSURL URLWithString:strBURL];
                [_imgVideoBG loadImageFromURL:imgURL];
            }
        }
        else
        {
            
                if ([strImage isKindOfClass:[NSNull class]]) {
                    
                    _imgVideoBG.backgroundColor=[UIColor blackColor];
                    NSURL *imgURL=[NSURL URLWithString:@"No"];
                    [_imgVideoBG loadImageFromURL:imgURL];
                }
                else
                {
                    _isVideoPlayed=NO;
                    _btnPlayRltedIcon.hidden=NO;
                    NSString *strBURL=ImgBase_URL;
                    strBURL=[strBURL stringByAppendingString:strImage];
                    NSURL *imgURL=[NSURL URLWithString:strBURL];
                    [_imgVideoBG loadImageFromURL:imgURL];
               
                }
            [self playVideo];
        }
    }
}

-(IBAction)btnChlngProfilePressed:(id)sender
{
    NSString *strStatus=[_arrChallenge valueForKey:@"privacy"];
    if([strStatus isKindOfClass:[NSNull class]])
    {
        strStatus=@"";
    }
    if ([strStatus isEqualToString:@"Y"])
    {
        _strVoteAlertTitle=@"Sorry this profile is private";
        [self viewForVoteUI];
    }
    else
    {
        theAppDelegate.isProfFromMenu=YES;
        NSLog(@"Vishal");
        ProfileScreen *prof=[[ProfileScreen alloc]initWithNibName:@"ProfileScreen" bundle:nil];
        
        NSString *strID=[_arrChallenge valueForKey:@"uid"];
        if ([[theAppDelegate.arrUserLoginData objectAtIndex:0] isEqualToString:strID]) {
            theAppDelegate.isProfFromMenu=YES;
        }
        else
        {
            prof.strUserID=strID;
            theAppDelegate.isProfFromMenu=NO;
        }
        [self.navigationController pushViewController:prof animated:YES];
    }
}

-(IBAction)btnChlngrProfilePressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    
    NSString *strUID=[[_arrChallenger valueForKey:@"uid"]objectAtIndex:row];
    
    NSString *strPrivacy=[[_arrChallenger valueForKey:@"privacy"]objectAtIndex:row];
    
    if([strPrivacy isKindOfClass:[NSNull class]])
    {
        strPrivacy=@"N/A";
    }
    if ([strPrivacy isEqualToString:@"Y"])
    {
        _strVoteAlertTitle=@"Sorry this profile is private";
        [self viewForVoteUI];
    }
    else
    {
        if ([strUID isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:0]]) {
            theAppDelegate.isProfFromMenu=YES;
        }
        else
        {
            theAppDelegate.isProfFromMenu=NO;
        }
        NSLog(@"HELLO");
        ProfileScreen *prof=[[ProfileScreen alloc]initWithNibName:@"ProfileScreen" bundle:nil];
        prof.strUserID=strUID;
        [self.navigationController pushViewController:prof animated:YES];
    }
}

-(IBAction)btnCloseVoteBtnPressed:(id)sender
{
    [self closeViewButonPressed];
    [self specificBetPageServerAPI];
}

-(IBAction)btnPlayRelatedVideos:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    _strSelectedVideoURL=[[_arrRelatedVData valueForKey:@"video"]objectAtIndex:row];
    _isLFromManyBet=YES;
    _btnLikeVideo.tag=row;
    slectedRIndexID=row;
    if(row==0)
    {
        _strSelectedThemeUser=@"Challenge";
        NSString *strName=[[_arrRelatedVData valueForKey:@"fname"]objectAtIndex:row];
        if ([strName isEqual:(id)[NSNull null]])
        {
            _lblNameVideo.text=@"N/A";
        }
        else
        {
            _lblNameVideo.text=@"";
            _lblNameVideo.text=strName;
        }
        
    }
    else
    {
        _strSelectedThemeUser=@"challenger";
        _lblNameVideo.frame=CGRectMake(0,[UIScreen mainScreen].bounds.size.height/100*10,_viewForVideo.frame.size.width,30);
        NSString *strName=[[_arrRelatedVData valueForKey:@"name"]objectAtIndex:row];
        
        
        if ([strName isEqual:(id)[NSNull null]]) {
            _lblNameVideo.text=@"N/A";
        }
        else
        {
            _lblNameVideo.text=@"";
            _lblNameVideo.text=strName;
        }
        
    }
    if([_strSelectedVideoURL isEqualToString:@"No"])
    {
        _btnPlayRltedIcon.hidden=YES;
    }
    else
    {
        _btnPlayRltedIcon.hidden=NO;
    }
    NSString *strImage=[[_arrRelatedVData valueForKey:@"image"]objectAtIndex:row];
    NSString *strVote=[[_arrRelatedVData valueForKey:@"alvote"]objectAtIndex:row];
    
    if ([strVote isEqualToString:@"N"])
    {
        [_btnLikeVideo setBackgroundImage:[UIImage imageNamed:@"icon_LikeV"] forState:UIControlStateNormal];
        _btnLikeVideo.userInteractionEnabled=YES;
    }
    else
    {
        [_btnLikeVideo setBackgroundImage:[UIImage imageNamed:@"icon_CMark"] forState:UIControlStateNormal];
        _btnLikeVideo.userInteractionEnabled=NO;
    }
    
  /*  _lblNameVideo=[[UILabel alloc]init];
    _lblNameVideo.frame=CGRectMake(0,[UIScreen mainScreen].bounds.size.height/100*18.5,_viewForVideo.frame.size.width,30);
    if (row==0)
    {
        _lblNameVideo.text=[[_arrRelatedVData valueForKey:@"fname"] objectAtIndex:row];
    }
    else
    {
        _lblNameVideo.text=[[_arrRelatedVData valueForKey:@"name"] objectAtIndex:row];
    }*/
    
    if([[[_arrRelatedVData valueForKey:@"uid"]objectAtIndex:row] isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:0]])
    {
        _btnLikeVideo.hidden=YES;
    }
    else
    {
        
          _btnLikeVideo.frame=CGRectMake(_viewForVideoArea.frame.size.width-82,[UIScreen mainScreen].bounds.size.height/100*8,90,35);
        _btnLikeVideo.hidden=NO;
    }
    
    if([_strSelectedVideoURL isEqualToString:@"No"])
    {
        if ([strImage isKindOfClass:[NSNull class]]) {
            
            
            _imgVideoBG.backgroundColor=[UIColor blackColor];
            NSURL *imgURL=[NSURL URLWithString:@"No"];
            [_imgVideoBG loadImageFromURL:imgURL];
        }
        else
        {
            NSString *strBURL=ImgBase_URL;
            strBURL=[strBURL stringByAppendingString:strImage];
            NSURL *imgURL=[NSURL URLWithString:strBURL];
            [_imgVideoBG loadImageFromURL:imgURL];
        }
       // _strVoteAlertTitle=@"Video Not Available";
        //[self viewForVoteUI1];
    }
    else
    {
        if ([strImage isKindOfClass:[NSNull class]]) {
            
            _imgVideoBG.backgroundColor=[UIColor blackColor];
            NSURL *imgURL=[NSURL URLWithString:@"No"];
            [_imgVideoBG loadImageFromURL:imgURL];
        }
        else
        {
            NSString *strBURL=ImgBase_URL;
            strBURL=[strBURL stringByAppendingString:strImage];
            NSURL *imgURL=[NSURL URLWithString:strBURL];
            [_imgVideoBG loadImageFromURL:imgURL];
        }

        [self playVideo];
    }
}

-(IBAction)btnLikePressed:(id)sender
{
    if (_isWinner)
    {
        _strVoteAlertTitle=@"Oops! you can’t vote as winner has already been declared!";
        [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.1];
        
    }
    else
    {
        
        if (_isVideoPlayed)
        {
            if (theAppDelegate.isConnected)
            {
                NSInteger RowID;
                UIButton *btn=(UIButton*)sender;
                RowID=btn.tag;
                NSMutableURLRequest *request;
                NSString *urlString;
                [self.view setUserInteractionEnabled:NO];
                NSString *strBaseURL=base_URL;
                [self ViewForDataLoadingAlert];
                
                if (RowID==0)
                {
                    _strSelectedThemeUser=@"Challenge";
                }
                else
                {
                    _strSelectedThemeUser=@"Challenger";
                }
                
                if ([_strSelectedThemeUser isEqualToString:@"Challenge"])
                {
                    NSLog(@"Challenge");
                    _isLikeVideo=YES;
                    _isVideoUpload=NO;
                    _strSelectedThemeUser=@"Challenger";
                    urlString =[NSString stringWithFormat:@"wsaddsnapvote.php?uid=%@&sid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],_selectedBetID1];
                }
                else
                {
                    if(_isLFromManyBet)
                    {
                        selectedVideoID=selectedVideoID-1;
                    }
                    else
                    {
                        
                    }
                    if (_isLFromManyBet)
                    {
                        selectedVideoID=slectedRIndexID-1;
                    }
                    NSString *strTID=[[_arrRelatedVData valueForKey:@"uid"]objectAtIndex:RowID];
                    NSString *strSID2=[[_arrRelatedVData valueForKey:@"sid"]objectAtIndex: RowID];
                
                    _isLikeVideo=YES;
                    _isVideoUpload=NO;
                
                    urlString=[NSString stringWithFormat:@"wsaddchallenger_vote.php?uid=%@&sid=%@&tid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],strSID2,strTID];
                }
                strBaseURL=[strBaseURL stringByAppendingString:urlString];
                NSURL *urlChallenger=[NSURL URLWithString:strBaseURL];
            
                request=[NSMutableURLRequest requestWithURL:[urlChallenger standardizedURL]];
            
                _connLikeVideo=[[NSURLConnection alloc]initWithRequest:request delegate:self];
            }
            else
            {
                theAppDelegate.alert.message=@"Check Internet connection";
                [theAppDelegate.alert show];
            }
        }
        else
        {
            _strVoteAlertTitle=@"First watch the video before voting!";
            [self viewForVoteUI1];
        }
    }
}

-(IBAction)btnChlngRptPressed:(UIButton *)sender
{
    UIButton *btn=(UIButton*)sender;
    selectedReptID=btn.tag;
    
    _ischlgrReport=NO;
    [self viewForPostReportPopUPUI];
}
-(IBAction)btnChlngerReportPressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    selectedReptID=btn.tag;
    
    _ischlgrReport=YES;
    [self viewForPostReportPopUPUI];
}

-(IBAction)btnReportPressed:(id)sender
{
    _isReport=YES;
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isCCData=NO;
        _isJoinSnpbt=NO;
        _isChlngrVote=NO;
        _isJoinVideoUpload=NO;
        _isSnapVote=NO;
        _isRemoveChlngrBet=NO;
        _isRemoveChlngBet=NO;
        _isSubmitReportPrsed=YES;
        
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *strTID,*strSID1,*strUIDSnap;
        if (_ischlgrReport) {
            strTID=[[_arrChallenger valueForKey:@"joinsanpid"]objectAtIndex:selectedReptID];
            strUIDSnap=[[_arrChallenger valueForKey:@"uid"]objectAtIndex:selectedReptID];
            strSID1=[[_arrChallenger valueForKey:@"sid"]objectAtIndex:selectedReptID];
        }
        else
        {
            strTID=[_arrChallenge valueForKey:@"uid"];
            strSID1=[_arrChallenge valueForKey:@"sid"];
        }
        
        NSString *urlString;

        if (_ischlgrReport)
        {
            urlString=[NSString stringWithFormat:@"wsaddsanpbetchallengerreport.php?uid=%@&sid=%@&cid=%@&comment=comment",[theAppDelegate.arrUserLoginData objectAtIndex:0],strSID1,strTID];
        }
        else
        {            
            urlString=[NSString stringWithFormat:@"wsaddsanpbetreport.php?uid=%@&sid=%@&comment=comment",[theAppDelegate.arrUserLoginData objectAtIndex:0],strSID1];
        }
        
        [self.view setUserInteractionEnabled:NO];
        NSString *strBaseURL=base_URL;
        
        
        strBaseURL=[strBaseURL stringByAppendingString:urlString];
        NSURL *urlChallenger=[NSURL URLWithString:strBaseURL];
        
        request=[NSMutableURLRequest requestWithURL:[urlChallenger standardizedURL]];

        
        if(!connection1)
            connection1= [[WebConnection1 alloc] init];
        connection1.delegate = self;
        [connection1 makeConnection:request];
    }
    else
    {
        theAppDelegate.alert.message=@"Check Internet connection";
        [theAppDelegate.alert show];
    }
    request = nil;

}

#pragma mark-Close Comment View
-(void)closeViewButonPressed
{
    @try
    {
        for(UIView *subview in [_viewForVideo subviews])
        {
            [_playerViewController.player pause];
            [subview removeFromSuperview];
        }
        for(UIView *subview in [_viewForReport subviews])
        {
            [subview removeFromSuperview];
        }
        for(UIView *subview in [_viewForPostResponse subviews])
        {
            [subview removeFromSuperview];
        }
        for(UIView *subview in [_viewForVote subviews])
        {
            [subview removeFromSuperview];
        }
        for(UIView *subview in [_viewForShare subviews])
        {
            [subview removeFromSuperview];
        }
        for(UIView *subview in [_viewForNoDataFound subviews])
        {
            [subview removeFromSuperview];
        }
        for(UIView *subview in [_viewForVideoTrim subviews])
        {
            [subview removeFromSuperview];
        }
        CGRect frameForViewForVideo=_viewForVideo.frame;
        CGRect frameForViewForReport=_viewForReport.frame;
        CGRect frameForViewForPostRes=_viewForPostResponse.frame;
        CGRect frameForViewForVote=_viewForVote.frame;
        CGRect frameForViewForShare=_viewForShare.frame;
        CGRect frameForViewForNDataFound=_viewForNoDataFound.frame;
        CGRect frameForViewForVideoTrim=_viewForVideoTrim.frame;
        
        frameForViewForVideo=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        frameForViewForReport=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        frameForViewForPostRes=CGRectMake(30,screenHeight/2,screenWidth-60,0);
        frameForViewForVote=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        frameForViewForShare=CGRectMake(30,screenHeight/2,screenWidth-60,0);
        frameForViewForNDataFound=CGRectMake(30,screenHeight/2,screenWidth-60,0);
        frameForViewForVideoTrim=CGRectMake(30,screenHeight/2,screenWidth-60,0);
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForVideo.frame=frameForViewForVideo;
            _viewForReport.frame=frameForViewForReport;
             _viewForNoDataFound.frame=frameForViewForReport;
        } completion:^(BOOL finished){
            [_viewForVideo removeFromSuperview];
            [_viewForReport removeFromSuperview];
            [_viewForPostResponse removeFromSuperview];
            [_viewForVote removeFromSuperview];
            [_viewForShare removeFromSuperview];
            [_viewForNoDataFound removeFromSuperview];
            [_viewForVideoTrim removeFromSuperview];
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark-Close Comment View
-(void)closeVoteView
{
    @try
    {
        for(UIView *subview in [_viewForVote1 subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForVote=_viewForVote1.frame;
        frameForViewForVote=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            _viewForVote1.frame=frameForViewForVote;
        } completion:^(BOOL finished)
        {
            [_viewForVote1 removeFromSuperview];
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception){
    } @finally{
    }
}

#pragma mark-Close Comment View
-(void)closeYesNoButonPressed
{
    @try
    {
        for(UIView *subview in [_viewForYNAlertUI subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForVideo=_viewForYNAlertUI.frame;
        frameForViewForVideo=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForYNAlertUI.frame=frameForViewForVideo;
            
        } completion:^(BOOL finished){
            [_viewForYNAlertUI removeFromSuperview];
            
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark-Close Winner PopUp
-(void)closeWinnerPopUpUI
{
    @try
    {
        for(UIView *subview in [_viewForWinnerPopUp subviews])
        {
            [subview removeFromSuperview];
        }
       
        CGRect frameForViewForVideo=_viewForWinnerPopUp.frame;
        
        frameForViewForVideo=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForWinnerPopUp.frame=frameForViewForVideo;
                    } completion:^(BOOL finished){
            [_viewForWinnerPopUp removeFromSuperview];
        
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
    } @finally {
        
    }
}
#pragma mark: view For VoteUI
-(void)viewfForYesNoAlertUI1
{
    [_viewForYNAlertUI removeFromSuperview];
    _viewForYNAlertUI =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForYNAlertUI.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForYNAlertUI.tag=20;
    [self.view addSubview:_viewForYNAlertUI];
    CGRect frameForViewForHelp=_viewForYNAlertUI.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForYNAlertUI.frame=frameForViewForHelp;
    } completion:^(BOOL finished)
     {
         vscreenSize=_viewForYNAlertUI.frame;
         vscreenHeight=vscreenSize.size.height;
         vscreenWidth=vscreenSize.size.width;
         
         UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForYNAlertUI.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
         img.image=[UIImage imageNamed:@"icon_Share"];
         [_viewForYNAlertUI addSubview:img];
         UILabel *lblAlrt=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+25,_viewForYNAlertUI.frame.size.width,30)];
         lblAlrt.text=@"Are you sure you want to remove your response?";
         lblAlrt.textAlignment=NSTextAlignmentCenter;
         lblAlrt.font=[UIFont fontWithName:@"Avenir-Roman" size:14];
         lblAlrt.textColor=[UIColor whiteColor];
         [_viewForYNAlertUI addSubview:lblAlrt];
         
         _btnVoteSeeMore=[[UIButton alloc]init];
         _btnVoteClose=[[UIButton alloc]init];
         if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
         {
             _btnYes=[[UIButton alloc]initWithFrame:CGRectMake(_viewForYNAlertUI.frame.size.width/2-60,CGRectGetMaxY(lblAlrt.frame)+50,120,40)];
             _btnNo=[[UIButton alloc]initWithFrame:CGRectMake(_viewForYNAlertUI.frame.size.width/2-60,CGRectGetMaxY(lblAlrt.frame)+50,120,40)];
         }
         else
         {
             _btnYes=[[UIButton alloc]initWithFrame:CGRectMake(40,CGRectGetMaxY(lblAlrt.frame)+50,screenWidth/2-80,45)];
            _btnNo=[[UIButton alloc]initWithFrame:CGRectMake(_viewForYNAlertUI.frame.size.width/2+40,CGRectGetMaxY(lblAlrt.frame)+50,screenWidth/2-80,45)];
         }
         [_btnYes setTitle:@"Yes" forState:UIControlStateNormal];
         [_btnNo setTitle:@"No" forState:UIControlStateNormal];
         
         [_btnYes setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         [_btnNo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         
         _btnYes.layer.borderWidth=2.0;
         _btnNo.layer.borderWidth=2.0;
         
         _btnYes.layer.borderColor=[[UIColor orangeColor]CGColor];
         _btnNo.layer.borderColor=[[UIColor orangeColor]CGColor];
         
         _btnYes.layer.cornerRadius=4.0;
         _btnNo.layer.cornerRadius=4.0;
         
         [_btnYes addTarget:self action:@selector(viewForButtonYesRemoveSnapPressed) forControlEvents:UIControlEventTouchUpInside];
         [_btnNo addTarget:self action:@selector(closeYesNoButonPressed) forControlEvents:UIControlEventTouchUpInside];
         
         [_viewForYNAlertUI addSubview:_btnYes];
         [_viewForYNAlertUI addSubview:_btnNo];
         
     }];
}


-(void)viewForButtonYesRemoveSnapPressed
{
    [self closeYesNoButonPressed];
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isCCData=NO;
        _isJoinSnpbt=NO;
        _isChlngrVote=NO;
        _isJoinVideoUpload=NO;
        _isSnapVote=NO;
        _isSubmitReportPrsed=NO;
        
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *strBaseURL=base_URL;
        
        NSString *urlString;
        
        if (_isRemoveChlngBet)
        {
            urlString=[NSString stringWithFormat:@"wssnapremove.php?sid=%@",strSID];
        }
        else if (_isRemoveChlngrBet)
        {
            urlString=[NSString stringWithFormat:@"wschallengeremove.php?uid=%@&sid=%@",_strChlgerRemoveID,strSID];
        }
        strBaseURL=[strBaseURL stringByAppendingString:urlString];
        NSURL *urlChallenger=[NSURL URLWithString:strBaseURL];
        
        request=[NSMutableURLRequest requestWithURL:[urlChallenger standardizedURL]];
        
        if(!connection1)
            connection1= [[WebConnection1 alloc] init];
        connection1.delegate = self;
        [connection1 makeConnection:request];
    }
    else
    {
        theAppDelegate.alert.message=@"Check Internet connection";
        [theAppDelegate.alert show];
    }
    request = nil;

}
#pragma mark: view For VoteUI
-(void)viewForNoDataFoundUI
{
    [_viewForNoDataFound removeFromSuperview];
    _viewForNoDataFound =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForNoDataFound.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForNoDataFound.tag=20;
    [self.view addSubview:_viewForNoDataFound];
    CGRect frameForViewForHelp=_viewForNoDataFound.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForNoDataFound.frame=frameForViewForHelp;
    } completion:^(BOOL finished)
     {
         vscreenSize=_viewForNoDataFound.frame;
         vscreenHeight=vscreenSize.size.height;
         vscreenWidth=vscreenSize.size.width;
         
         UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForNoDataFound.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
         img.image=[UIImage imageNamed:@"icon_Share"];
         [_viewForNoDataFound addSubview:img];
         UILabel *lblShare=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForNoDataFound.frame.size.width,30)];
         lblShare.text=_strVoteAlertTitle;
         lblShare.textAlignment=NSTextAlignmentCenter;
         lblShare.textColor=[UIColor whiteColor];
         
         if([_strVoteAlertTitle isEqualToString:@"Video should be 15 seconds or less"])
         {
             lblShare.font=[UIFont fontWithName:@"Avenir-Roman" size:18];
         }
         else
         {
             lblShare.font=[UIFont fontWithName:@"Avenir-Roman" size:23];
         }
         
         [_viewForNoDataFound addSubview:lblShare];
         
         _btnVoteSeeMore=[[UIButton alloc]init];
         _btnVoteClose=[[UIButton alloc]init];
         if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
         {
             _btnVoteClose.frame=CGRectMake(_viewForNoDataFound.frame.size.width/2-60,CGRectGetMaxY(lblShare.frame)+50,120,40);
         }
         else
         {
             _btnVoteClose=[[UIButton alloc]initWithFrame:CGRectMake(_viewForNoDataFound.frame.size.width/2-50,CGRectGetMaxY(lblShare.frame)+50,100,40)];
         }
         
         [_btnVoteClose setTitle:@"Close" forState:UIControlStateNormal];
         [_btnVoteClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         _btnVoteClose.layer.borderWidth=2.0;
         _btnVoteClose.layer.borderColor=[[UIColor orangeColor]CGColor];
         _btnVoteClose.layer.cornerRadius=4.0;
         [_btnVoteClose addTarget:self action:@selector(btnCloseVoteBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
         
         [_viewForNoDataFound addSubview:_btnVoteClose];
         
     }];
}
#pragma mark: view For VoteUI
-(void)viewForVoteUI
{
    [_viewForVote removeFromSuperview];
    _viewForVote =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForVote.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForVote.tag=20;
    [self.view addSubview:_viewForVote];
    CGRect frameForViewForHelp=_viewForVote.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForVote.frame=frameForViewForHelp;
    } completion:^(BOOL finished)
    {
        vscreenSize=_viewForVote.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForVote.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForVote addSubview:img];
        UILabel *lblShare=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForVote.frame.size.width,30)];
        lblShare.text=_strVoteAlertTitle;
        lblShare.textAlignment=NSTextAlignmentCenter;
        lblShare.textColor=[UIColor whiteColor];
        
        if([_strVoteAlertTitle isEqualToString:@"Video should be 15 seconds or less"]||[_strVoteAlertTitle isEqualToString:@"Snapbet uploaded successfully!"])
        {
            lblShare.font=[UIFont fontWithName:@"Avenir-Roman" size:18];
        }
        else if ([_strVoteAlertTitle isEqualToString:@"Bet has been removed successfully!"]||[_strVoteAlertTitle isEqualToString:@"Response submitted successfully"])
        {
            lblShare.font=[UIFont fontWithName:@"Avenir-Roman" size:16];
        }
        else if ([_strVoteAlertTitle isEqualToString:@" Oops! you can’t vote as winner has already been declared!"])
        {
            lblShare.frame=CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForVote.frame.size.width,0);
            lblShare.font=[UIFont fontWithName:@"Avenir-Roman" size:14];
            lblShare.textAlignment=NSTextAlignmentCenter;
            [lblShare setNumberOfLines:0];
            [lblShare sizeToFit];
        }
        else if([_strVoteAlertTitle isEqualToString:@"Oops! you cant remove this snapbet as winner has been declared!"])
        {
            lblShare.frame=CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForVote.frame.size.width,0);
            lblShare.font=[UIFont fontWithName:@"Avenir-Roman" size:14];
            lblShare.textAlignment=NSTextAlignmentCenter;
            [lblShare setNumberOfLines:0];
            [lblShare sizeToFit];
        }
        else
        {
            lblShare.font=[UIFont fontWithName:@"Avenir-Roman" size:23];
        }
        
        [_viewForVote addSubview:lblShare];
        
        _btnVoteSeeMore=[[UIButton alloc]init];
        _btnVoteClose=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {            
            _btnVoteClose.frame=CGRectMake(_viewForVote.frame.size.width/2-60,CGRectGetMaxY(lblShare.frame)+50,120,40);
        }
        else
        {
            _btnVoteClose=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVote.frame.size.width/2-50,CGRectGetMaxY(lblShare.frame)+50,100,40)];
        }
        
        [_btnVoteClose setTitle:@"Close" forState:UIControlStateNormal];
        [_btnVoteClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnVoteClose.layer.borderWidth=2.0;
        _btnVoteClose.layer.borderColor=[[UIColor orangeColor]CGColor];
        _btnVoteClose.layer.cornerRadius=4.0;
        [_btnVoteClose addTarget:self action:@selector(btnCloseVoteBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [_viewForVote addSubview:_btnVoteClose];
        
    }];
}

#pragma mark: view For Share
-(void)viewForVoteUI1
{
    [_viewForVote1 removeFromSuperview];
    _viewForVote1 =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForVote1.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForVote1.tag=20;
    [_viewForVideo addSubview:_viewForVote1];
   
    CGRect frameForViewForHelp=_viewForVote1.frame;
  
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForVote1.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForVote1.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForVote1.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForVote1 addSubview:img];
        UILabel *lblShare=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForVote1.frame.size.width,30)];
        lblShare.text=_strVoteAlertTitle;
        lblShare.textAlignment=NSTextAlignmentCenter;
        lblShare.textColor=[UIColor whiteColor];
        lblShare.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForVote1 addSubview:lblShare];
        
        _btnVoteSeeMore=[[UIButton alloc]init];
        _btnVoteClose=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnVoteSeeMore.frame=CGRectMake(70,CGRectGetMaxY(lblShare.frame)+50,_viewForVote1.frame.size.width/2-140,40);
            _btnVoteClose.frame=CGRectMake(_viewForVote1.frame.size.width/2-60,CGRectGetMaxY(lblShare.frame)+50,120,40);
        }
        else
        {
            _btnVoteSeeMore=[[UIButton alloc]initWithFrame:CGRectMake(20,CGRectGetMaxY(lblShare.frame)+50,_viewForVote1.frame.size.width/2-30,40)];
            _btnVoteClose=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVote1.frame.size.width/2-50,CGRectGetMaxY(lblShare.frame)+50,100,40)];
        }
        
        [_btnVoteSeeMore setTitle:@"See More" forState:UIControlStateNormal];
        [_btnVoteClose setTitle:@"Close" forState:UIControlStateNormal];
        
        [_btnVoteSeeMore setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnVoteClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        _btnVoteSeeMore.backgroundColor=[UIColor orangeColor];
        _btnVoteClose.layer.borderWidth=2.0;
        _btnVoteClose.layer.borderColor=[[UIColor orangeColor]CGColor];
        _btnVoteClose.layer.cornerRadius=4.0;
        _btnVoteSeeMore.layer.cornerRadius=4.0;
        
        //[_btnVoteSeeMore addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        [_btnVoteClose addTarget:self action:@selector(closeVoteView) forControlEvents:UIControlEventTouchUpInside];
        
        // [_viewForVote addSubview:_btnVoteSeeMore];
        [_viewForVote1 addSubview:_btnVoteClose];
        
    }];
}
#pragma mark: view For Share
-(void)viewForShareUI
{
    [_viewForShare removeFromSuperview];
    _viewForShare =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForShare.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForShare.tag=20;
    [self.view addSubview:_viewForShare];
    CGRect frameForViewForHelp=_viewForShare.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForShare.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForShare.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForShare.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForShare addSubview:img];
        UILabel *lblShare=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForShare.frame.size.width,30)];
        lblShare.text=@"Share";
        lblShare.textAlignment=NSTextAlignmentCenter;
        lblShare.textColor=[UIColor whiteColor];
        lblShare.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:25];
        [_viewForShare addSubview:lblShare];
        
        _btnShareFB=[[UIButton alloc]init];
        _btnShareTwit=[[UIButton alloc]init];
        _btnShareYoutube=[[UIButton alloc]init];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnShareFB.frame=CGRectMake(_viewForShare.frame.size.width/2-100,CGRectGetMaxY(lblShare.frame)+50,50,50);
            
            //_shareButton1 = [[FBSDKShareButton alloc] initWithFrame:CGRectMake(_viewForShare.frame.size.width/2-100,CGRectGetMaxY(lblShare.frame)+50,50,50)];
            //_shareButton = [[FBSDKShareButton alloc] initWithFrame:CGRectMake(0,0,50,50)];
            _btnShareTwit.frame=CGRectMake(_viewForShare.frame.size.width/2+50,CGRectGetMaxY(lblShare.frame)+50,50,50);
        }
        else
        {
            _btnShareFB.frame=CGRectMake(_viewForShare.frame.size.width/2-100,CGRectGetMaxY(lblShare.frame)+50,50,50);
            //_shareButton1 = [[FBSDKShareButton alloc] initWithFrame:CGRectMake(_viewForShare.frame.size.width/2-100,CGRectGetMaxY(lblShare.frame)+50,50,50)];
            //_shareButton = [[FBSDKShareButton alloc] initWithFrame:CGRectMake(0,0,50,50)];
            _btnShareTwit.frame=CGRectMake(_viewForShare.frame.size.width/2+50,CGRectGetMaxY(lblShare.frame)+50,50,50);
        }
        [_btnShareFB setBackgroundImage:[UIImage imageNamed:@"icon_FB"] forState:UIControlStateNormal];
        [_btnShareTwit setBackgroundImage:[UIImage imageNamed:@"icon_Twitter"] forState:UIControlStateNormal];
        
        [_btnShareFB addTarget:self action:@selector(btnFbInvitePressed:) forControlEvents:UIControlEventTouchUpInside];
        [_btnShareTwit addTarget:self action:@selector(btnTwitterSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        _btnShareFB.transform = CGAffineTransformMakeScale(0.1,0.1);
        _btnShareTwit.transform = CGAffineTransformMakeScale(0.1,0.1);
        
        [UIView beginAnimations:@"fadeInNewView" context:NULL];
        
        [UIView setAnimationDuration:1.0];
        _btnShareFB.transform = CGAffineTransformMakeScale(1,1);
        _btnShareTwit.transform = CGAffineTransformMakeScale(1,1);
        
        _btnShareFB.alpha = 1.0f;
        _btnShareTwit.alpha = 1.0f;
        
        [UIView commitAnimations];
        
         [_viewForShare addSubview:_btnShareFB];
        [_viewForShare addSubview:_btnShareTwit];
       /*
        _content = [[FBSDKShareLinkContent alloc] init];
        // NSURL *urlImage = [NSURL URLWithString:strShareURL];
        
        NSString *strBaseURL=ImgBase_URL;
        NSString *strVURL=vBase_URL;
        NSString *str;
        NSString *strVideo;
        
        if ([strShareUser isEqualToString:@"challenge"])
        {
            str=[_arrChallenge valueForKey:@"image"];
            strVideo=[_arrChallenge valueForKey:@"video"];
        }
        else if ([strShareUser isEqualToString:@"challenger"])
        {
            str=[[_arrChallenger valueForKey:@"image"]objectAtIndex:slectedShareIndex];
            strVideo=[[_arrChallenger valueForKey:@"video"]objectAtIndex:slectedShareIndex];
        }
        
        strBaseURL=[strBaseURL stringByAppendingString:str];
        strVURL=[strVURL stringByAppendingString:strVideo];
        
        if ([strVideo isEqualToString:@"No"])
        {
            NSLog(@"No Video");
            
            //_content.contentTitle=@"MY TEST APP";
            _content.contentURL = [NSURL URLWithString:strBaseURL];
            [_content setContentTitle:_strThemeName];
            [_content setImageURL:[NSURL URLWithString: strBaseURL]];
            _content.contentDescription=@"Check this out on the Snapbets app!";
            
        }
        else
        {
            if (strVideo.length>0)
            {
                NSURL *videoURL = [NSURL URLWithString:strVURL];
                FBSDKShareVideo *video = [[FBSDKShareVideo alloc] init];
                video.videoURL = videoURL;
                FBSDKShareVideoContent *content = [[FBSDKShareVideoContent alloc] init];
                content.video = video;
                
                _content.contentURL = [NSURL URLWithString:strVURL];
                [_content setContentTitle:[NSString stringWithFormat: @"%@", _strThemeName]];
                [_content setContentDescription:@"Check this out on the Snapbets app!"];
                [_content setImageURL:[NSURL URLWithString: strBaseURL]];
                
                [FBSDKShareDialog showFromViewController:self withContent:content delegate:nil];
            }
            else
            {
                _content.contentURL = [NSURL URLWithString:strBaseURL];
                [_content setContentTitle:_strThemeName];
                [_content setContentDescription:[NSString stringWithFormat: @"Check this out on the Snapbets app!"]];
                [_content setImageURL:[NSURL URLWithString: strBaseURL]];
            }
        }
        _shareButton1.shareContent = _content;
        [_shareButton1  addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        _shareButton1.backgroundColor=[UIColor clearColor];
        [_viewForShare addSubview:_shareButton1];
        
        _shareButton.shareContent = _content;
        [_shareButton setTitle:@"" forState:UIControlStateNormal];
        [_shareButton1 setTitle:@"" forState:UIControlStateNormal];
        
        [_shareButton  addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        _shareButton.layer.cornerRadius=_shareButton.frame.size.height/2;
        _shareButton1.layer.cornerRadius=_shareButton1.frame.size.height/2;
        
        [_shareButton setClipsToBounds:YES];
        [_shareButton1 setClipsToBounds:YES];
        [_shareButton1 addSubview:_shareButton];
        [_shareButton1 setBackgroundImage:[UIImage imageNamed:@"fbwhiteback.png"] forState:UIControlStateNormal];
        [_shareButton1 setBackgroundImage:[UIImage imageNamed:@"fbwhiteback.png"] forState:UIControlStateSelected];
        [_shareButton1 setBackgroundImage:[UIImage imageNamed:@"fbwhiteback.png"] forState:UIControlStateHighlighted];
        */
        _btnCloseShareUI=[[UIButton alloc]initWithFrame:CGRectMake(_viewForShare.frame.size.width-40,10,25,25)];
        [_btnCloseShareUI setBackgroundImage:[UIImage imageNamed:@"icon_Cross"] forState:UIControlStateNormal];
        [_btnCloseShareUI addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
        [_viewForShare addSubview:_btnCloseShareUI];
    }];

}

#pragma mark: view For Winner PopUp
-(void)viewForWinnerPopUpUI
{
    [_viewForWinnerPopUp removeFromSuperview];
    _viewForWinnerPopUp =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForWinnerPopUp.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForWinnerPopUp.tag=20;
    [self.view addSubview:_viewForWinnerPopUp];
    CGRect frameForViewForHelp=_viewForWinnerPopUp.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForWinnerPopUp.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForWinnerPopUp.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForWinnerPopUp.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForWinnerPopUp addSubview:img];
        UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForWinnerPopUp.frame.size.width,30)];
        lblTitle.text=_strWTitle;
        lblTitle.textAlignment=NSTextAlignmentCenter;
        lblTitle.textColor=[UIColor whiteColor];
        lblTitle.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:20];
        
        UILabel *lblSTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblTitle.frame)+10,_viewForWinnerPopUp.frame.size.width,30)];
        lblSTitle.text=_strWinSTitle;
        lblSTitle.textColor=[UIColor whiteColor];
        lblSTitle.textAlignment=NSTextAlignmentCenter;
        lblSTitle.font=[UIFont fontWithName:@"Avenir-Roman" size:16];
        [_viewForWinnerPopUp addSubview:lblTitle];
        [_viewForWinnerPopUp addSubview:lblSTitle];
        
        _btnCloseWinner=[[UIButton alloc]init];
        _btnCloseWinner=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseWinner.frame=CGRectMake(_viewForWinnerPopUp.frame.size.width/2-60,CGRectGetMaxY(lblSTitle.frame)+50,120,40);
        }
        else
        {
            _btnCloseWinner=[[UIButton alloc]initWithFrame:CGRectMake(_viewForWinnerPopUp.frame.size.width/2-50,CGRectGetMaxY(lblSTitle.frame)+50,100,40)];
        }
        
        [_btnCloseWinner setTitle:@"Close" forState:UIControlStateNormal];
        [_btnCloseWinner setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseWinner.layer.borderWidth=2.0;
        _btnCloseWinner.layer.borderColor=[[UIColor orangeColor]CGColor];
        _btnCloseWinner.layer.cornerRadius=4.0;
        [_btnCloseWinner addTarget:self action:@selector(closeWinnerPopUpUI) forControlEvents:UIControlEventTouchUpInside];
        [_viewForWinnerPopUp addSubview:_btnCloseWinner];
    }];
}

#pragma mark: Viewfor Play Video
-(void)viewForVideoUI
{
    [_viewForVideo removeFromSuperview];
    [_btnCloseVideoUI removeFromSuperview];
    
    _viewForVideo =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForVideo.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForVideo.tag=20;
    [self.view addSubview:_viewForVideo];
    
    CGRect frameForViewForHelp=_viewForVideo.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForVideo.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForVideo.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        _lblNameVideo=[[UILabel alloc]init];
        _btnLikeVideo=[[UIButton alloc]init];
        _viewForVideoArea=[[UIView alloc]init];
        _btnPrivious=[[UIButton alloc]init];
        _btnNext=[[UIButton alloc]init];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _lblNameVideo.frame=CGRectMake(_viewForVideo.frame.size.width/2-100,150,_viewForVideo.frame.size.width,30);
            if ([_strSelectedThemeUser isEqualToString:@"Challenge"])
            {
                _lblNameVideo.frame=CGRectMake(0,[UIScreen mainScreen].bounds.size.height/100*18.5,_viewForVideo.frame.size.width,30);
                _viewForVideoArea.frame=CGRectMake(70,[UIScreen mainScreen].bounds.size.height/100*25,_viewForVideo.frame.size.width-140,[UIScreen mainScreen].bounds.size.height/100*50);
                if(theAppDelegate.isMyBeetSelected)
                {
                    _btnLikeVideo=[[UIButton alloc]init];
                }
                else
                {
                    _btnLikeVideo.frame=CGRectMake(screenWidth-170,[UIScreen mainScreen].bounds.size.height/100*18,100,35);
                }
               
            }
            else
            {
                _lblNameVideo.frame=CGRectMake(0,_viewForVideo.frame.size.height/2-230,_viewForVideo.frame.size.width,30);
               _viewForVideoArea.frame=CGRectMake(70,_viewForVideo.frame.size.height/2-200,_viewForVideo.frame.size.width-140,400);
               
                _btnLikeVideo.frame=CGRectMake(screenWidth-170,_viewForVideo.frame.size.height/2-245,100,35);
                
            }
            
            _btnPrivious.frame=CGRectMake(150,CGRectGetMaxY(_viewForVideoArea.frame)+20,80,30);
            _btnNext.frame=CGRectMake(_viewForVideo.frame.size.width-230,CGRectGetMaxY(_viewForVideoArea.frame)+20,80,30);
           // _btnLikeVideo.frame=CGRectMake(screenWidth-170,150,100,35);
        }
        else
        {
            if ([_strSelectedThemeUser isEqualToString:@"Challenge"])
            {
                //_lblNameVideo.frame=CGRectMake(0,[UIScreen mainScreen].bounds.size.height/100*18.5,_viewForVideo.frame.size.width,30);
               // _viewForVideoArea.frame=CGRectMake(10,[UIScreen mainScreen].bounds.size.height/100*25,_viewForVideo.frame.size.width-20,[UIScreen mainScreen].bounds.size.height/100*50);
                
                _lblNameVideo.frame=CGRectMake(0,10,_viewForVideoArea.frame.size.width,30);
                _viewForVideoArea.frame=CGRectMake(10,[UIScreen mainScreen].bounds.size.height/100*15,_viewForVideo.frame.size.width-20,[UIScreen mainScreen].bounds.size.height/100*50);
                
                NSString *strID=[_arrChallenge valueForKey:@"uid"];
                if(theAppDelegate.isMyBeetSelected||[strID isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:0]])
                {
                    _btnLikeVideo=[[UIButton alloc]init];
                }
                else
                {
                    //_btnLikeVideo.frame=CGRectMake(_viewForVideoArea.frame.size.width-82,[UIScreen mainScreen].bounds.size.height/100*18,90,35);
                
                    _btnLikeVideo.frame=CGRectMake(_viewForVideoArea.frame.size.width-82,[UIScreen mainScreen].bounds.size.height/100*8,90,35);
                }
               
            }
            else
            {
                _lblNameVideo.frame=CGRectMake(0,10,_viewForVideoArea.frame.size.width,30);
                _viewForVideoArea.frame=CGRectMake(10,[UIScreen mainScreen].bounds.size.height/100*15,_viewForVideo.frame.size.width-20,[UIScreen mainScreen].bounds.size.height/100*50);
               
                NSString *strUID=[[_arrChallenger valueForKey:@"uid"]objectAtIndex:selectedVideoID];
                
                if ([strUID isKindOfClass:[NSNull class]]) {
                    strUID=@"";
                }
                
                if([strUID isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:0]])
                {
                    _btnLikeVideo=[[UIButton alloc]init];
                }
                else
                {
                  _btnLikeVideo.frame=CGRectMake(_viewForVideoArea.frame.size.width-82,[UIScreen mainScreen].bounds.size.height/100*8,90,35);
                }
                
            }
            
            _btnPrivious.frame=CGRectMake(10,CGRectGetMaxY(_viewForVideoArea.frame)+20,80,30);
            _btnNext.frame=CGRectMake(_viewForVideo.frame.size.width-90,CGRectGetMaxY(_viewForVideoArea.frame)+20,80,30);
        }
       
        _lblNameVideo.textAlignment=NSTextAlignmentCenter;
        _lblNameVideo.textColor=[UIColor whiteColor];
        _lblNameVideo.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:25];
        [_viewForVideo addSubview:_lblNameVideo];
        
        if(_isAlreadyVote)
        {
            [_btnLikeVideo setBackgroundImage:[UIImage imageNamed:@"icon_CMark"] forState:UIControlStateNormal];
            _btnLikeVideo.userInteractionEnabled=NO;
        }
        else
        {
           [_btnLikeVideo setBackgroundImage:[UIImage imageNamed:@"icon_LikeV"] forState:UIControlStateNormal];
            _btnLikeVideo.userInteractionEnabled=YES;
            
        }
        [_btnLikeVideo addTarget:self action:@selector(btnLikePressed:) forControlEvents:UIControlEventTouchUpInside];
        _btnLikeVideo.tag=strSlectedBetTag;
        _viewForVideoArea.backgroundColor=[UIColor colorWithRed:38/255.0 green:38/255.0 blue:38/255.0 alpha:1];
        _viewForVideoArea.layer.borderWidth=2.0;
        _viewForVideoArea.layer.borderColor=[[UIColor whiteColor]CGColor];
        [_viewForVideo addSubview:_viewForVideoArea];
        [_viewForVideo addSubview:_btnLikeVideo];
        
        [_imgVideoBG removeFromSuperview];
        _imgVideoBG=[[AsyncImageView alloc]initWithFrame:CGRectMake(0,0,_viewForVideoArea.frame.size.width,_viewForVideoArea.frame.size.height)];
        
        NSString *strBURL=ImgBase_URL;
        if ([_strSelectedThemeUser isEqualToString:@"Challenge"])
        {
            NSString *strFname=[_arrChallenge valueForKey:@"fname"];
            if ([strFname isKindOfClass:[NSNull class]]) {
                strFname=@"N/A";
            }
            _lblNameVideo.text=strFname;
            
                NSString *strUrl=[_arrChallenge valueForKey:@"image"];
            if([strUrl isEqualToString:@"No.png"])
            {
                UIImageView *img;
                [img removeFromSuperview];
                
                img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForVideoArea.frame.size.width/2-20,_viewForVideoArea.frame.size.height/2-30,40,40)];
                img.image=[UIImage imageNamed:@"img_AppIcon"];
                [_viewForVideoArea addSubview:img];
            }
            else
            {
                strBURL=[strBURL stringByAppendingString:strUrl];
                NSURL *imgURL=[NSURL URLWithString:strBURL];
                [_imgVideoBG loadImageFromURL:imgURL];
            }
        }
        else
        {
             _lblNameVideo.frame=CGRectMake(0,[UIScreen mainScreen].bounds.size.height/100*10,_viewForVideo.frame.size.width,30);
            NSString *strName=[[_arrChallenger valueForKey:@"name"]objectAtIndex:selectedVideoID];
            if ([strName isEqual:(id)[NSNull null]]) {
                _lblNameVideo.text=@"N/A";
            }
            else
            {
                _lblNameVideo.text=strName;
            }
        
            NSString *strUrl=[[_arrChallenger valueForKey:@"image"]objectAtIndex:selectedVideoID];
            strBURL=[strBURL stringByAppendingString:strUrl];
            NSURL *imgURL=[NSURL URLWithString:strBURL];
            
            [_imgVideoBG loadImageFromURL:imgURL];
        }
        
        _imgVideoBG.backgroundColor = [UIColor clearColor];
        [_imgVideoBG setContentMode:UIViewContentModeScaleAspectFill];
        [_imgVideoBG sizeToFit];
        _imgVideoBG.clipsToBounds = YES;
        
        [_viewForVideoArea addSubview:_imgVideoBG];
        
        [_btnPlayRltedIcon removeFromSuperview];
        _btnPlayRltedIcon=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoArea.frame.size.width/2-22,_viewForVideoArea.frame.size.height/2-22,44,44)];
        [_btnPlayRltedIcon setBackgroundImage:[UIImage imageNamed:@"img_AppIcon"] forState:UIControlStateNormal];
        [_btnPlayRltedIcon addTarget:self action:@selector(playVideo) forControlEvents:UIControlEventTouchUpInside];
        [_viewForVideoArea addSubview:_btnPlayRltedIcon];
        
        if(_strSelectedVideoURL.length==0||[_strSelectedVideoURL isEqualToString:@"No"])
        {
            _isVideoPlayed=YES;
            _btnPlayRltedIcon.hidden=YES;
        }
        UILabel *lblChallenge=[[UILabel alloc]initWithFrame:CGRectMake(_viewForVideoArea.frame.size.width-110,5,95,30)];
        lblChallenge.text=_strSelectedThemeUser;
        lblChallenge.textAlignment=NSTextAlignmentRight;
        lblChallenge.textColor=[UIColor whiteColor];
        lblChallenge.font=[UIFont fontWithName:@"Avenir-Medium" size:16];
        [_viewForVideoArea addSubview:lblChallenge];
        
        [_btnPrivious setTitle:@"Previous" forState:UIControlStateNormal];
        [_btnPrivious setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnPrivious.titleLabel.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
        _btnPrivious.layer.borderColor=[[UIColor orangeColor]CGColor];
        _btnPrivious.layer.borderWidth=1.0;
        _btnPrivious.layer.cornerRadius=5.0;
        [_viewForVideo addSubview:_btnPrivious];
        
        [_btnNext setTitle:@"Next" forState:UIControlStateNormal];
        [_btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnNext.titleLabel.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
        _btnNext.layer.borderColor=[[UIColor orangeColor]CGColor];
        _btnNext.layer.borderWidth=1.0;
        _btnNext.layer.cornerRadius=5.0;
        [_viewForVideo addSubview:_btnNext];
        
        [_btnPrivious addTarget:self action:@selector(btnPriviousVideoPressed:) forControlEvents:UIControlEventTouchUpInside];
        [_btnNext addTarget:self action:@selector(btnNextVideoPressed:) forControlEvents:UIControlEventTouchUpInside];        
        
        UIButton *btnVideo=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideo.frame.size.width-60,0,60,35)];
        [btnVideo addTarget:self action:@selector(btnCloseVideoUIPressed:) forControlEvents:UIControlEventTouchUpInside];
        [_viewForVideo addSubview:btnVideo];
        
        _btnCloseVideoUI=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideo.frame.size.width-40,10,25,25)];
        [_btnCloseVideoUI setBackgroundImage:[UIImage imageNamed:@"icon_Cross"] forState:UIControlStateNormal];
        [_btnCloseVideoUI addTarget:self action:@selector(btnCloseVideoUIPressed:) forControlEvents:UIControlEventTouchUpInside];
        [_viewForVideo addSubview:_btnCloseVideoUI];
        
       /* if ([_strSelectedThemeUser isEqualToString:@"Challenge"])
        {
            _btnNext.hidden=YES;
            _btnPrivious.hidden=YES;
            [_tblRelatedVideos removeFromSuperview];
        }
        else
        {*/
        
        
            if ([_arrChallenger count]==0)
            {
                _btnNext.hidden=YES;
                _btnPrivious.hidden=YES;
                [_tblRelatedVideos removeFromSuperview];
            }
            else
            {
                _btnNext.hidden=NO;
                _btnPrivious.hidden=NO;
            
                [_tblRelatedVideos removeFromSuperview];
                _tblRelatedVideos=[[UITableView alloc]initWithFrame:CGRectMake(5,CGRectGetMaxY(_btnPrivious.frame)-10,screenWidth-10,[UIScreen mainScreen].bounds.size.height/100*30)];
            
                [_tblRelatedVideos setEditing:false];
                _tblRelatedVideos.transform = CGAffineTransformMakeRotation(M_PI*1.5);
                _tblRelatedVideos.tableFooterView=[[UIView alloc]init];
                [_tblRelatedVideos setBackgroundColor:[UIColor clearColor]];
                _tblRelatedVideos.delegate=self;
                _tblRelatedVideos.dataSource=self;
                _tblRelatedVideos.frame=CGRectMake(5,CGRectGetMaxY(_btnPrivious.frame),screenWidth-10,[UIScreen mainScreen].bounds.size.height/100*30);
                [_viewForVideo addSubview:_tblRelatedVideos];
            }
    }];
}

#pragma mark-View For Report Video
-(void)viewForReportVideoUI
{
    [_viewForReportVideo removeFromSuperview];
    _viewForReportVideo =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForReportVideo.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForReportVideo.tag=20;
    [self.view addSubview:_viewForReportVideo];
    CGRect frameForViewForHelp=_viewForReportVideo.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForReportVideo.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForReport.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForReportVideo.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForReportVideo addSubview:img];
        
        UILabel *lblShare=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForReportVideo.frame.size.width,30)];
        if([_strVoteAlertTitle isEqualToString:@"This content has already been reported"])
        {
            if(screenWidth<325)
            {
              lblShare.font=[UIFont fontWithName:@"Avenir-Roman" size:18];
            }
            else
            {
                lblShare.font=[UIFont fontWithName:@"Avenir-Roman" size:20];
            }
        }
        else
        {
            lblShare.font=[UIFont fontWithName:@"Avenir-Roman" size:20];
        }
        lblShare.text=_strVoteAlertTitle;
        lblShare.textAlignment=NSTextAlignmentCenter;
        lblShare.textColor=[UIColor whiteColor];
        
        [_viewForReportVideo addSubview:lblShare];
        
        _btnCloseUpgradeNow=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseUpgradeNow.frame=CGRectMake(_viewForVote.frame.size.width/2-60,CGRectGetMaxY(lblShare.frame)+50,120,40);
        }
        else
        {
            _btnCloseUpgradeNow=[[UIButton alloc]initWithFrame:CGRectMake(_viewForReportVideo.frame.size.width/2-50,CGRectGetMaxY(lblShare.frame)+50,100,40)];
        }
        
        [_btnCloseUpgradeNow setTitle:@"Close" forState:UIControlStateNormal];
        [_btnCloseUpgradeNow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseUpgradeNow.layer.borderWidth=2.0;
        _btnCloseUpgradeNow.layer.borderColor=[[UIColor orangeColor]CGColor];
        _btnCloseUpgradeNow.layer.cornerRadius=4.0;
        [_btnCloseUpgradeNow addTarget:self action:@selector(closeUpgradeView) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewForReportVideo addSubview:_btnCloseUpgradeNow];
        
    }];
}

#pragma mark: Enable Subview
-(void)enableSubViews
{
    for(UIView *subview in [self.view subviews])
    {
        subview.userInteractionEnabled=YES;
        // subview.alpha=1;
    }
}

-(void)closeUpgradeView
{
    @try
    {
        [self enableSubViews];
        for(UIView *subview in [_viewForReportVideo subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForHelp=_viewForReportVideo.frame;
        
        frameForViewForHelp=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForReportVideo.frame=frameForViewForHelp;
            
        } completion:^(BOOL finished){
            [_viewForReportVideo removeFromSuperview];
            
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}


#pragma mark: Play selected challage video
-(void)playVideo
{
    _isVideoPlayed=YES;
    NSString *strFilePath=vBase_URL;
    strFilePath=[strFilePath stringByAppendingString:_strSelectedVideoURL];
    [_playerViewController.view removeFromSuperview];
   
    NSURL *vURL=[NSURL URLWithString:strFilePath];
    _playerViewController = [[AVPlayerViewController alloc] init];
    _playerViewController.player = [AVPlayer playerWithURL:vURL];
    _playerViewController.view.frame =CGRectMake(0,0,_viewForVideoArea.frame.size.width,_viewForVideoArea.frame.size.height);
     [_playerViewController.player play];
    _playerViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:_playerViewController animated:YES completion:nil];

    //[_viewForVideoArea addSubview:_playerViewController.view];
}

#pragma mark: Play selected challage video
-(void)playVideo1
{
    NSString *strFilePath=vBase_URL;
    strFilePath=[strFilePath stringByAppendingString:_strVideoName];
    [_playerViewController.view removeFromSuperview];
    
    NSURL *vURL=[NSURL URLWithString:strFilePath];
    _playerViewController = [[AVPlayerViewController alloc] init];
    _playerViewController.player = [AVPlayer playerWithURL:vURL];
    _playerViewController.view.frame =CGRectMake(0,0,_viewForPostVideo.frame.size.width,_viewForPostVideo.frame.size.height);
    [_playerViewController.player play];
    _playerViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:_playerViewController animated:YES completion:nil];

   // [_viewForPostVideo addSubview:_playerViewController.view];
}

- (BOOL)shouldAutorotate
{       return NO;     }

#pragma mark: Viewfor Buy single Theme Package
-(void)viewForPostResponsePopUPUI
{
    [_viewForPostResponse removeFromSuperview];
    _viewForPostResponse =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForPostResponse.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForPostResponse.tag=20;
    [self.view addSubview:_viewForPostResponse];
    CGRect frameForViewForHelp=_viewForPostResponse.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForPostResponse.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForPostResponse.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        [_viewForVote removeFromSuperview];
        UILabel *lblPurchase=[[UILabel alloc]init];
        _viewForPostVideo=[[UIView alloc]init];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            lblPurchase.frame=CGRectMake(0,200,_viewForPostResponse.frame.size.width,30);
            _viewForPostVideo.frame=CGRectMake(100,CGRectGetMaxY(lblPurchase.frame)+10,_viewForPostResponse.frame.size.width-200,250);
        }
        else
        {
            lblPurchase.frame=CGRectMake(0,100,_viewForPostResponse.frame.size.width,30);
            _viewForPostVideo.frame=CGRectMake(10,CGRectGetMaxY(lblPurchase.frame)+10,_viewForPostResponse.frame.size.width-20,200);
        }
        lblPurchase.text=@"Post your Challenge Response";
        lblPurchase.textAlignment=NSTextAlignmentCenter;
        lblPurchase.textColor=[UIColor whiteColor];
        lblPurchase.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:20];
        [_viewForPostResponse addSubview:lblPurchase];
        
        [_asyVideoThumbnil removeFromSuperview];
        _asyVideoThumbnil=[[AsyncImageView alloc]initWithFrame:CGRectMake(0,0,_viewForPostVideo.frame.size.width,_viewForPostVideo.frame.size.height)];
        [_asyVideoThumbnil setContentMode:UIViewContentModeScaleAspectFill];
        [_asyVideoThumbnil sizeToFit];
        _asyVideoThumbnil.clipsToBounds = YES;
        [_viewForPostVideo addSubview:_asyVideoThumbnil];
        
        _viewForPostVideo.backgroundColor=[UIColor colorWithRed:38/255.0 green:38/255.0 blue:38/255.0 alpha:1];
        _viewForPostVideo.layer.borderWidth=2.0;
        _viewForPostVideo.layer.borderColor=[[UIColor whiteColor]CGColor];
        [_viewForPostResponse addSubview:_viewForPostVideo];
        
        _btnPlayNewSnapbet=[[UIButton alloc]initWithFrame:CGRectMake(_viewForPostVideo.frame.size.width/2-32,_viewForPostVideo.frame.size.height/2-27,60,60)];
        [_btnPlayNewSnapbet setUserInteractionEnabled:YES];
        [_btnPlayNewSnapbet setBackgroundImage:[UIImage imageNamed:@"img_AppIcon"] forState:UIControlStateNormal];
        [_btnPlayNewSnapbet addTarget:self action:@selector(playVideo1) forControlEvents:UIControlEventTouchUpInside];
        _btnPlayNewSnapbet.hidden=NO;
        [_btnPlayNewSnapbet setUserInteractionEnabled:NO];
        [_viewForPostVideo addSubview:_btnPlayNewSnapbet];
        
        _btnUploadSnpbet=[[UIButton alloc]initWithFrame:CGRectMake(_viewForPostResponse.frame.size.width/2-70,CGRectGetMaxY(_viewForPostVideo.frame)+10,140,40)];
        [_btnUploadSnpbet setTitle:@"Upload Response" forState:UIControlStateNormal];
        [_btnUploadSnpbet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnUploadSnpbet.titleLabel.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        _btnUploadSnpbet.layer.cornerRadius=5.0;
        _btnUploadSnpbet.layer.borderColor=[[UIColor orangeColor]CGColor];
        _btnUploadSnpbet.layer.borderWidth=2.0;
        [_btnUploadSnpbet addTarget:self action:@selector(btnChooseVideoPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewForPostResponse addSubview:_btnUploadSnpbet];
        
        [_btnClsoePostRespnse removeFromSuperview];
        _btnClsoePostRespnse=[[UIButton alloc]initWithFrame:CGRectMake(_viewForPostResponse.frame.size.width-40,10,30,30)];
        [_btnClsoePostRespnse addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
        [_btnClsoePostRespnse setBackgroundImage:[UIImage imageNamed:@"icon_Cross"] forState:UIControlStateNormal];
        [_viewForPostResponse addSubview:_btnClsoePostRespnse];
    }];
}

#pragma mark: Viewfor Report POP UI
-(void)viewForPostReportPopUPUI
{
    [_viewForReport removeFromSuperview];
    _viewForReport =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForReport.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForReport.tag=20;
    [self.view addSubview:_viewForReport];
    CGRect frameForViewForHelp=_viewForReport.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForReport.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForReport.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForReport.frame.size.width/2-50,80,100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForReport addSubview:img];
        UILabel *lblInvite=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForReport.frame.size.width,30)];
        lblInvite.text=@"Report";
        lblInvite.textAlignment=NSTextAlignmentCenter;
        lblInvite.textColor=[UIColor whiteColor];
        lblInvite.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:25];
        [_viewForReport addSubview:lblInvite];
        
        UILabel *lblInvite1=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblInvite.frame)+10,_viewForReport.frame.size.width,30)];
        lblInvite1.text=@"Report this content for review";
        lblInvite1.textAlignment=NSTextAlignmentCenter;
        lblInvite1.textColor=[UIColor whiteColor];
        lblInvite1.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:18];
        [_viewForReport addSubview:lblInvite1];
        
        _btnViewReport=[[UIButton alloc]initWithFrame:CGRectMake(20,CGRectGetMaxY(lblInvite1.frame)+50,_viewForReport.frame.size.width/2-30,45)];
        _btnCloseReport=[[UIButton alloc]initWithFrame:CGRectMake(_viewForReport.frame.size.width/2+15,CGRectGetMaxY(lblInvite1.frame)+50,_viewForReport.frame.size.width/2-30,45)];
        
        [_btnViewReport setTitle:@"Report" forState:UIControlStateNormal];
        [_btnCloseReport setTitle:@"Cancel" forState:UIControlStateNormal];
        
        [_btnViewReport setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnCloseReport setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [_btnViewReport setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnCloseReport setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [_btnViewReport addTarget:self action:@selector(btnReportPressed:) forControlEvents:UIControlEventTouchUpInside];
        [_btnCloseReport addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
        
        _btnViewReport.layer.borderWidth=2.0;
        _btnCloseReport.layer.borderWidth=2.0;
        
        _btnViewReport.layer.borderColor=[[UIColor orangeColor]CGColor];
        _btnCloseReport.layer.borderColor=[[UIColor orangeColor]CGColor];
        
        _btnViewReport.layer.cornerRadius=5.0;
        _btnCloseReport.layer.cornerRadius=5.0;
        
        [_btnCloseReport addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
        
        _btnViewReport.backgroundColor=[UIColor orangeColor];
        
        [_viewForReport addSubview:_btnViewReport];
        [_viewForReport addSubview:_btnCloseReport];
    }];
}

/*------    TableView delegate and datasource declaration here   ------*/

#pragma mark: UITableview delegate and datasource methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

#pragma mark: UITableView delegate and datasource methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_tblBet)
    {
        if (section==0)
        {
            if ([_arrChallenge count]==0) {
                return 0;
            }
            else
            {
                return 1;
            }
            return 0;
        }
        else if (section==1)
        {
            return [_arrChallenger count];
        }
    }
    else if (tableView==_tblRelatedVideos)
    {
        if (section==0) {
            
        return [_arrChallenger count]+1;
        }
    }
    return 0;
}

//--------------------- cellforrow At Index TableView -----------------------//

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@""];
    if (cell == nil)
    {
        //  cell.transform= CGAffineTransformMakeRotation(M_PI/2);
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    tableView.separatorColor =UIColor.clearColor;
    cell.backgroundColor= [UIColor clearColor];
    [self.tblBet setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if (tableView==_tblBet)
    {
        UIView *viewForCell=[[UIView alloc]init];
        UIView *viewTemp=[[UIView alloc]init];
      //  UILabel *lblName=[[UILabel alloc]init];
        UIButton *btnChlngProfile=[[UIButton alloc]init];
        UIButton *btnChlngrProfile=[[UIButton alloc]init];
        
        UIButton *btnChlngVote=[[UIButton alloc]init];
        UIButton *btnChlngrVote=[[UIButton alloc]init];
        
        UIView *viewSept1=[[UIView alloc]init];
        UILabel *lblVote1=[[UILabel alloc]init];
        UIView *viewSept2=[[UIView alloc]init];
    
        UIButton *btnShare1=[[UIButton alloc]init];
        UIView *viewSept=[[UIView alloc]init];
        //UIButton *btnVote1=[[UIButton alloc]init];
    
        viewSept2.backgroundColor=[UIColor whiteColor];
        [viewForCell addSubview:viewSept2];
    
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            viewForCell.frame=CGRectMake(0,0,screenWidth,148);
            viewTemp.frame=CGRectMake(-2,0,200,148);
            viewSept1.frame=CGRectMake(CGRectGetMaxX(viewTemp.frame),0,2,viewForCell.frame.size.height);
            lblVote1.frame=CGRectMake(CGRectGetMaxX(viewSept1.frame),viewForCell.frame.size.height-35,viewForCell.frame .size.width-viewTemp.frame.size.width*2,35);
            viewSept2.frame=CGRectMake(CGRectGetMaxX(lblVote1.frame),viewForCell.frame.size.height-35,2,35);
            
           // lblName.frame=CGRectMake(0,viewTemp.frame.size.height-35,viewTemp.frame.size.width,35);
            
            btnShare1.frame=CGRectMake(CGRectGetMaxX(viewSept2.frame),viewForCell.frame.size.height-35,viewTemp.frame.size.width/2,35);
            viewSept.frame=CGRectMake(CGRectGetMaxX(btnShare1.frame),viewForCell.frame.size.height-35,2,35);
            
            if (indexPath.section==0)
            {
                btnChlngProfile.frame=CGRectMake(0,viewTemp.frame.size.height-35,viewTemp.frame.size.width,35);
                btnChlngVote.frame=CGRectMake(CGRectGetMaxX(viewSept.frame),viewForCell.frame.size.height-35,viewTemp.frame.size.width/2,35);
            }
            else if (indexPath.section==1)
            {
                btnChlngrProfile.frame=CGRectMake(0,viewTemp.frame.size.height-35,viewTemp.frame.size.width,35);
                btnChlngrVote.frame=CGRectMake(CGRectGetMaxX(viewSept.frame)+1,viewForCell.frame.size.height-35,viewTemp.frame.size.width/2,35);
            }
        }
        else
        {
            if (screenWidth<325)
            {
                viewForCell.frame=CGRectMake(0,0,screenWidth,98);
                viewTemp.frame=CGRectMake(-2,0,130,98);
                viewSept1.frame=CGRectMake(CGRectGetMaxX(viewTemp.frame),0,2,viewForCell.frame.size.height);
                lblVote1.frame=CGRectMake(CGRectGetMaxX(viewSept1.frame),viewForCell.frame.size.height-35,80,35);
                viewSept2.frame=CGRectMake(CGRectGetMaxX(lblVote1.frame),viewForCell.frame.size.height-35,2,35);
            
                //lblName.frame=CGRectMake(0,viewTemp.frame.size.height-25,viewTemp.frame.size.width,25);
                if (indexPath.section==0)
                {
                    btnChlngProfile.frame=CGRectMake(0,viewTemp.frame.size.height-25,viewTemp.frame.size.width,25);
                    
                }
                else if (indexPath.section==1)
                {
                    btnChlngrProfile.frame=CGRectMake(0,viewTemp.frame.size.height-25,viewTemp.frame.size.width,25);
                }
                btnShare1.frame=CGRectMake(CGRectGetMaxX(viewSept2.frame),viewForCell.frame.size.height-35,55,35);
                viewSept.frame=CGRectMake(CGRectGetMaxX(btnShare1.frame),viewForCell.frame.size.height-35,2,35);
                if (indexPath.section==0)
                {
                    btnChlngVote.frame=CGRectMake(CGRectGetMaxX(viewSept.frame),viewForCell.frame.size.height-35,55,35);
                }
                else if (indexPath.section==1)
                {
                    btnChlngrVote.frame=CGRectMake(CGRectGetMaxX(viewSept.frame),viewForCell.frame.size.height-35,55,35);
                }
            }
            else
            {
                viewForCell.frame=CGRectMake(0,0,screenWidth,98);
                viewTemp.frame=CGRectMake(-2,0,130,98);

                viewSept1.frame=CGRectMake(CGRectGetMaxX(viewTemp.frame),0,2,viewForCell.frame.size.height);
                lblVote1.frame=CGRectMake(CGRectGetMaxX(viewSept1.frame),viewForCell.frame.size.height-35,((viewForCell.frame.size.width-viewTemp.frame.size.width)/3)+35,35);
                viewSept2.frame=CGRectMake(CGRectGetMaxX(lblVote1.frame),viewForCell.frame.size.height-35,2,35);
                //lblName.frame=CGRectMake(0,viewTemp.frame.size.height-25,viewTemp.frame.size.width,25);
                if (indexPath.section==0)
                {
                    btnChlngProfile.frame=CGRectMake(0,viewTemp.frame.size.height-25,viewTemp.frame.size.width,25);
                }
                else if (indexPath.section==1)
                {
                    btnChlngrProfile.frame=CGRectMake(0,viewTemp.frame.size.height-25,viewTemp.frame.size.width,25);
                }
                btnShare1.frame=CGRectMake(CGRectGetMaxX(viewSept2.frame),viewForCell.frame.size.height-35,((viewForCell.frame.size.width-viewTemp.frame.size.width)/3)-17.5,35);
                viewSept.frame=CGRectMake(CGRectGetMaxX(btnShare1.frame),viewForCell.frame.size.height-35,2,35);
                if (indexPath.section==0)
                {
                btnChlngVote.frame=CGRectMake(CGRectGetMaxX(viewSept.frame),viewForCell.frame.size.height-35,((viewForCell.frame.size.width-viewTemp.frame.size.width)/3)-17.5,35);
                }
                else if (indexPath.section==1)
                {
                    btnChlngrVote.frame=CGRectMake(CGRectGetMaxX(viewSept.frame),viewForCell.frame.size.height-35,((viewForCell.frame.size.width-viewTemp.frame.size.width)/3)-17.5,35);
                }
            }
        }
        viewForCell.backgroundColor=[UIColor colorWithRed:243.0/255.0 green:144.0/255.0 blue:0/255.0 alpha:1];
        [cell.contentView addSubview:viewForCell];
        
        viewTemp.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.7];
        [viewForCell addSubview:viewTemp];
    
        _imgThumbnil=[[AsyncImageView alloc]initWithFrame:CGRectMake(0,0,viewTemp.frame.size.width,viewTemp.frame.size.height-25)];
        _imgThumbnil.backgroundColor = [UIColor clearColor];
        [viewTemp addSubview:_imgThumbnil];
    
        if (indexPath.section==0)
        {
            
            if(_isWinner)
            {
                if (_isChlngerWinner)
                {
                  
                }
                else
                {
                    UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,viewTemp.frame.size.width,viewTemp.frame.size.height)];
                    img.image=[UIImage imageNamed:@"img_WinTag"];
                    [viewTemp addSubview:img];

                }
            }
            
            UIButton *btnChlngPlay=[[UIButton alloc]initWithFrame:CGRectMake(viewTemp.frame.size.width/2-20,viewTemp.frame.size.height/2-30,40,40)];
            [btnChlngPlay setBackgroundImage:[UIImage imageNamed:@"img_AppIcon"] forState:UIControlStateNormal];
            [btnChlngPlay addTarget:self action:@selector(btnPlayVideoPressedChlng:) forControlEvents:UIControlEventTouchUpInside];
            btnChlngPlay.tag=indexPath.row;
            [viewTemp addSubview:btnChlngPlay];
            
            NSString *strVideo=[_arrChallenge valueForKey:@"video"];
            NSString *strImage=[_arrChallenge valueForKey:@"image"];
            if ([strImage isEqualToString:@""]&& strVideo.length>0) {
                btnChlngPlay.hidden=NO;
            }
            else if (strImage.length>0 && [strVideo isEqualToString:@"No"])
            {
               [btnChlngPlay setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            }
            else if (strImage.length>0 && [strVideo isEqualToString:@"(null)"])
            {
                [btnChlngPlay setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            }
        }
        else
        {
            UIImageView *img;
            if(indexPath.row==0)
            {
                if(_isWinner)
                {
                    if (_isChlngerWinner)
                    {
                        img=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,viewTemp.frame.size.width,viewTemp.frame.size.height)];
                        img.image=[UIImage imageNamed:@"img_WinTag"];
                        [viewTemp addSubview:img];
                    }
                }
            }
            UIButton *btnChlngrPlay=[[UIButton alloc]initWithFrame:CGRectMake(viewTemp.frame.size.width/2-20,viewTemp.frame.size.height/2-30,40,40)];
            [btnChlngrPlay setBackgroundImage:[UIImage imageNamed:@"img_AppIcon"] forState:UIControlStateNormal];
            [btnChlngrPlay addTarget:self action:@selector(btnPlayVideoPressedTbl:) forControlEvents:UIControlEventTouchUpInside];
            btnChlngrPlay.tag=indexPath.row;
            if (indexPath.row==0) {
                if (_isWinner) {
                    if (_isChlngerWinner)
                    {
                        btnChlngrPlay.frame=CGRectMake(img.frame.size.width/2-20,img.frame.size.height/2-30,40,40);
                        [img addSubview:btnChlngrPlay];
                    }
                }
                else
                {
                    [viewTemp addSubview:btnChlngrPlay];
                }
            }
            else
            {
                 [viewTemp addSubview:btnChlngrPlay];
            }
           
            NSString *strVideo=[[_arrChallenger valueForKey:@"video"]objectAtIndex:indexPath.row];
            NSString *strImage=[[_arrChallenger valueForKey:@"image"]objectAtIndex:indexPath.row];
            if (strImage.length>0&& [strVideo isEqualToString:@"No"])
            {
                [btnChlngrPlay setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            }
        }
        
        UILabel *lblChallngrsDetails=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(viewTemp.frame)+5,16,viewForCell.frame.size.width-130,0)];
    
        if (indexPath.section==0)
        {
            NSString *strName=[_arrChallenge valueForKey:@"fname"];
            
            if([strName isKindOfClass:[NSNull class]])
            {
                strName=@"N/A";
            }
            else if ([strName isEqualToString:@"Snapbet"])
            {
                strName=@"Snapbet Team";
            }
            
            [btnChlngProfile setTitle:strName forState:UIControlStateNormal];
            btnChlngProfile.tag=indexPath.row;
            //lblName.text=[_arrChallenge valueForKey:@"name"];
            lblChallngrsDetails.text=[_arrChallenge valueForKey:@"details"];
            NSString *strVote=@"Votes ";
            strVote=[strVote stringByAppendingString:[_arrChallenge valueForKey:@"vote"]];
            lblVote1.text=strVote;
            NSString *strBURL=ImgBase_URL;
            NSString *strImage=[_arrChallenge valueForKey:@"image"];
            if([strImage isEqualToString:@"(null)"])
            {
                
            }
            else
            {
                if([strImage isEqualToString:@"No.png"])
                {
                    UIImageView *img;
                    [img removeFromSuperview];
                    img=[[UIImageView alloc]initWithFrame:CGRectMake(viewTemp.frame.size.width/2-20,viewTemp.frame.size.height/2-30,40,40)];
                    img.image=[UIImage imageNamed:@"img_AppIcon"];
                    [cell.contentView addSubview:img];
                }
                else
                {
                    strBURL=[strBURL stringByAppendingString:strImage];
                    NSString *webStr1 = [NSString stringWithFormat:@"%@",strBURL];
                    NSURL *imageUrl1 = [[NSURL alloc] initWithString:[webStr1 stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
                    [_imgThumbnil loadImageFromURL:imageUrl1];
                    [_imgThumbnil setContentMode:UIViewContentModeScaleAspectFill];
                    [_imgThumbnil sizeToFit];
                    _imgThumbnil.clipsToBounds = YES;
                }
            }
            if ([strName isEqual:(id)[NSNull null]])
            {
                
            }
            else
            {
                [btnChlngProfile setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                btnChlngProfile.titleLabel.textAlignment=NSTextAlignmentCenter;
                btnChlngProfile.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.6];
                btnChlngProfile.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:14];
                [viewTemp addSubview:btnChlngProfile];
            }
        }
        else if (indexPath.section==1)
        {
            NSString *strVDesc=[[_arrChallenger valueForKey:@"name"]objectAtIndex:indexPath.row];
            if ([strVDesc isEqual: [NSNull null]]) {
                    strVDesc=@"N/A";
            }
            else if ([strVDesc isEqualToString:@"Snapbet"])
            {
                strVDesc=@"Snapbet Team";
            }
            [btnChlngrProfile setTitle:strVDesc forState:UIControlStateNormal];
            btnChlngrProfile.tag=indexPath.row;
            strVDesc=[strVDesc stringByAppendingString:@" has posted a response"];
            lblChallngrsDetails.text=strVDesc;
            NSString *strVote=@"Votes ";
            strVote=[strVote stringByAppendingString:[[_arrChallenger valueForKey:@"vote"]objectAtIndex:indexPath.row]];
        
            lblVote1.text=strVote;
        
            NSString *strBURL=ImgBase_URL;
            NSString *strImg=[[_arrChallenger valueForKey:@"image"]objectAtIndex:indexPath.row];
            strBURL=[strBURL stringByAppendingString:strImg];
            
            NSString *webStr1 = [NSString stringWithFormat:@"%@",strBURL];
            
            webStr1=[webStr1 stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            NSURL *imageUrl1 = [[NSURL alloc] initWithString:webStr1];
            
            if ([strImg isEqualToString:@"No"])
            {
               [_imgThumbnil loadImageFromURL:imageUrl1];
            }
            else
            {
                [_imgThumbnil loadImageFromURL:imageUrl1];
            }
            
            _imgThumbnil.contentMode = UIViewContentModeScaleToFill;
            _imgThumbnil.layer.masksToBounds=YES;
            
            [btnChlngrProfile setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            btnChlngrProfile.titleLabel.textAlignment=NSTextAlignmentCenter;
            btnChlngrProfile.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.6];
            btnChlngrProfile.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:14];
            [viewTemp addSubview:btnChlngrProfile];
            
            NSString *strImg1=[[_arrChallenger valueForKey:@"image"]objectAtIndex:indexPath.row];
            NSString *strVideo=[[_arrChallenger valueForKey:@"video"]objectAtIndex:indexPath.row];
            
            if ([strImg1 isEqualToString:@""]&& strVideo.length>0) {
                //btnChlngrProfile.hidden=NO;
            }
            else if (strImg1.length>0 && strVideo.length==0)
            {
               // btnChlngrProfile.hidden=YES;
            }
        }
        
        [btnChlngProfile addTarget:self action:@selector(btnChlngProfilePressed:) forControlEvents:UIControlEventTouchUpInside];
        [btnChlngrProfile addTarget:self action:@selector(btnChlngrProfilePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        lblChallngrsDetails.textColor=[UIColor blackColor];
        lblChallngrsDetails.font=[UIFont fontWithName:@"Avenir-Medium" size:11];
        [lblChallngrsDetails setNumberOfLines:3];
        [lblChallngrsDetails sizeToFit];
        [viewForCell addSubview:lblChallngrsDetails];
    
        viewSept1.backgroundColor=[UIColor whiteColor];
        [viewForCell addSubview:viewSept1];

        lblVote1.font=[UIFont fontWithName:@"Avenir-Roman" size:15];
        lblVote1.textColor=[UIColor whiteColor];
        lblVote1.textAlignment=NSTextAlignmentCenter;
        lblVote1.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.6];
        [viewForCell addSubview:lblVote1];
        
        [btnShare1 setTitle:@"Invite" forState:UIControlStateNormal];
        [btnShare1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnShare1.backgroundColor=[[UIColor  colorWithRed:166/255.0 green:87/255.0 blue:19/255.0 alpha:1]colorWithAlphaComponent:0.6];
        
        if (indexPath.section==0)
        {
           [btnShare1 addTarget:self action:@selector(btnShareChalngPressed:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            [btnShare1 addTarget:self action:@selector(btnSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        }
        btnShare1.tag=indexPath.row;
        btnShare1.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:15];
        [viewForCell addSubview:btnShare1];
    
        viewSept.backgroundColor=[UIColor whiteColor];
        [viewForCell addSubview:viewSept];
    
        if (indexPath.section==0) {
            if([_strSelectBUID isKindOfClass:[NSNull class]])
            {
                _strSelectBUID=[[NSString alloc]init];
            }
             NSString *strChngUID=[_arrChallenge valueForKey:@"uid"];
            if(theAppDelegate.isMyBeetSelected && [strChngUID isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:0]])
            {
                [btnChlngVote setTitle:@"Remove" forState:UIControlStateNormal];
            }
            else if([strChngUID isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:0]])
            {
                [btnChlngVote setTitle:@"Remove" forState:UIControlStateNormal];
            }
            else
            {
                [btnChlngVote setTitle:@"Vote" forState:UIControlStateNormal];
            }
            [btnChlngVote setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            btnChlngVote.backgroundColor=[[UIColor  colorWithRed:166/255.0 green:87/255.0 blue:19/255.0 alpha:1]colorWithAlphaComponent:0.6];
            [btnChlngVote addTarget:self action:@selector(btnChlngVotePressed:) forControlEvents:UIControlEventTouchUpInside];
            btnChlngVote.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:15];
            btnChlngVote.tag=indexPath.row;
            [viewForCell addSubview:btnChlngVote];
        }
        else if (indexPath.section==1)
        {
            NSString *strRowID=[[_arrChallenger valueForKey:@"uid"]objectAtIndex:indexPath.row];
            if([strRowID isKindOfClass:[NSNull class]])
            {
                strRowID=[[NSString alloc]init];
            }
            NSString *strLUID=[theAppDelegate.arrUserLoginData objectAtIndex:0];
            if([strRowID isEqualToString:strLUID])
            {
                [btnChlngrVote setTitle:@"Remove" forState:UIControlStateNormal];
            }
            else
            {
                [btnChlngrVote setTitle:@"Vote" forState:UIControlStateNormal];
            }
            [btnChlngrVote setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            btnChlngrVote.backgroundColor=[[UIColor  colorWithRed:166/255.0 green:87/255.0 blue:19/255.0 alpha:1]colorWithAlphaComponent:0.6];
            [btnChlngrVote addTarget:self action:@selector(btnChlngerVotePressed:) forControlEvents:UIControlEventTouchUpInside];
            btnChlngrVote.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:15];
            btnChlngrVote.tag=indexPath.row;
            [viewForCell addSubview:btnChlngrVote];
        }
    
        if (indexPath.section==0)
        {
            _btnMoreInfo=[[UIButton alloc]initWithFrame:CGRectMake(viewForCell.frame.size.width-20,5,15,15)];
            [_btnMoreInfo setBackgroundImage:[UIImage imageNamed:@"icon_MoreInfo"] forState:UIControlStateNormal];
            [_btnMoreInfo addTarget:self action:@selector(btnChlngRptPressed:) forControlEvents:UIControlEventTouchUpInside];
            _btnMoreInfo.tag=indexPath.row;
            [viewForCell addSubview:_btnMoreInfo];
        }
        else
        {
            _btnMoreInfo1=[[UIButton alloc]initWithFrame:CGRectMake(viewForCell.frame.size.width-20,5,15,15)];
            [_btnMoreInfo1 setBackgroundImage:[UIImage imageNamed:@"icon_MoreInfo"] forState:UIControlStateNormal];
            [_btnMoreInfo1 addTarget:self action:@selector(btnChlngerReportPressed:) forControlEvents:UIControlEventTouchUpInside];
            _btnMoreInfo1.tag=indexPath.row;
            [viewForCell addSubview:_btnMoreInfo1];
        }
    }
    else if (tableView==_tblRelatedVideos)
    {
        if (indexPath.section==0)
        {
            cell.backgroundColor=[UIColor clearColor];
            UILabel *lblName1; [lblName1 removeFromSuperview];
            UIImageView *imgTemp1; [imgTemp1 removeFromSuperview];
            UILabel *lblVote1;[lblVote1 removeFromSuperview];
            AsyncImageView *asyIMG; [asyIMG removeFromSuperview];
            
            lblName1=[[UILabel alloc]init];
            imgTemp1=[[UIImageView alloc]init];
            lblVote1=[[UILabel alloc]init];
            asyIMG=[[AsyncImageView alloc]init];
            
            if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                lblName1.frame=CGRectMake(105,30,150,30);
                imgTemp1=[[UIImageView alloc]initWithFrame:CGRectMake(70,5,100,90)];
                asyIMG=[[AsyncImageView alloc]initWithFrame:CGRectMake(70,5,100,90)];
                lblVote1.frame=CGRectMake(20,30,80,30);
            }
            else
            {
                lblName1.frame=CGRectMake(100,30,80,30);
                imgTemp1=[[UIImageView alloc]initWithFrame:CGRectMake(45,10,90,70)];
                asyIMG=[[AsyncImageView alloc]initWithFrame:CGRectMake(45,10,90,70)];
                lblVote1.frame=CGRectMake(-5,30,80,30);
            }
            
            lblName1.transform= CGAffineTransformMakeRotation(M_PI/2);
            imgTemp1.transform=CGAffineTransformMakeRotation(M_PI/2);
            lblVote1.transform=CGAffineTransformMakeRotation(M_PI/2);
            asyIMG.transform=CGAffineTransformMakeRotation(M_PI/2);
            
            _arrRelatedVData=[[NSMutableArray alloc]init];
            [_arrRelatedVData addObject:_arrChallenge];
            for (int i=0;i<[_arrChallenger count];i++)
            {
                
                [_arrRelatedVData addObject:[_arrChallenger objectAtIndex:i]];
            }
            
            NSString *strName;
            if(indexPath.row==0)
            {
               strName=[[_arrRelatedVData valueForKey:@"fname"]objectAtIndex:indexPath.row];
                
            }
            else
            {
                strName=[[_arrRelatedVData valueForKey:@"name"]objectAtIndex:indexPath.row];
                
            }
            if ([strName isEqual:(id)[NSNull null]])
            {
                 lblName1.text=@"N/A";
            }
            else
            {
                lblName1.text=strName;
            }
           
            lblName1.textColor=[UIColor whiteColor];
            lblName1.textAlignment=NSTextAlignmentCenter;
            lblName1.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
             [lblName1 sizeToFit];
            [cell.contentView addSubview:lblName1];
        
            NSString *strBURL=ImgBase_URL;
            NSString *strImg;
            
            strImg=[[_arrRelatedVData valueForKey:@"image"]objectAtIndex:indexPath.row];
            strBURL=[strBURL stringByAppendingString:strImg];
            
            [asyIMG loadImageFromURL:[NSURL URLWithString:strBURL]];
            [asyIMG setContentMode:UIViewContentModeScaleAspectFill];
            [asyIMG sizeToFit];
            asyIMG.clipsToBounds = YES;
            
            imgTemp1.layer.borderWidth=1.0;
            imgTemp1.layer.borderColor=[[UIColor whiteColor]CGColor];
            [cell.contentView addSubview:imgTemp1];
            [cell.contentView addSubview:asyIMG];
        
            NSString *strBaseVote;
            strBaseVote=[[_arrRelatedVData valueForKey:@"vote"]objectAtIndex:indexPath.row];
            
            strBaseVote=[strBaseVote stringByAppendingString:@" Votes"];
            lblVote1.text=strBaseVote;
                lblVote1.textColor=[UIColor whiteColor];
            lblVote1.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
            lblVote1.textAlignment=NSTextAlignmentCenter;
            [cell.contentView addSubview:lblVote1];
            
            UIButton *btnPlyRVideo=[[UIButton alloc]initWithFrame:CGRectMake(30,20,30,30)];
            
            btnPlyRVideo.tag=indexPath.row;
            [btnPlyRVideo addTarget:self action:@selector(btnPlayRelatedVideos:) forControlEvents:UIControlEventTouchUpInside];
            //[imgTemp1 bringSubviewToFront:btnPlyRVideo];
            [asyIMG addSubview:btnPlyRVideo];

            NSString *strVideoFound;
            strVideoFound=[[_arrRelatedVData valueForKey:@"video"]objectAtIndex:indexPath.row];
            if ([strVideoFound isEqualToString:@"No"]|| strVideoFound == (id)[NSNull null])
            {
                btnPlyRVideo.frame=CGRectMake(0,0,50,50);
                [btnPlyRVideo setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            }
            else
            {
                btnPlyRVideo.frame=CGRectMake(30,20,30,30);
                [btnPlyRVideo setBackgroundImage:[UIImage imageNamed:@"img_AppIcon"] forState:UIControlStateNormal];
            }
        }
        
    }
    return cell;
}

#pragma Mark-delegate methods
//--------------------- Tableview didselected index ---------------------//
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblRelatedVideos)
    {
        
            }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        return 150;
    }
    else
    {
        if (tableView==_tblRelatedVideos)
        {
            return 100;
        }
        else if (tableView==_tblBet)
        {
            return 100;
        }
        return 0;
    }
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    _lblTags=[[UILabel alloc] init];
    _lblTags.font=[UIFont fontWithName:@"AvenirNext-Regular" size:16];
    NSMutableArray *arrSectionList=[[NSMutableArray alloc]init];
    arrSectionList=[[NSMutableArray alloc]initWithObjects:@"The Challenge",@"The Challengers",nil];
    
    NSString *strSelectCat;
    for (int i=0;i<[arrSectionList count];i++)
    {
        strSelectCat=[arrSectionList objectAtIndex:section];
        _lblTags.text=strSelectCat;
    }
    _lblTags.textColor=[UIColor colorWithRed:214/255.0 green:96/255.0 blue:0/255.0 alpha:1];
    _lblTags.textAlignment=NSTextAlignmentCenter;
    _lblTags.backgroundColor=[[UIColor whiteColor]colorWithAlphaComponent:.6];
    CGSize sizeSelectCat=[strSelectCat sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-Regular" size:12]}];
    [_lblTags setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/100*20,25,sizeSelectCat.width, 30)];
    [_lblTags sizeToFit];
    return _lblTags;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==_tblRelatedVideos) {
        return 0;
    }
    else
    {
        return 20;
    }
    return 0;
}

#pragma mark: getnerate Thumbnil from video
-(void)generateImage
{
    NSString *strBase=vBase_URL;
    strBase=[strBase stringByAppendingString:_strVideoName];
    
    NSURL *partOneUrl = [NSURL URLWithString:strBase];
    AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL:partOneUrl options:nil];
    AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
    generate1.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 2);
    CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
    if(oneRef==NULL)
    {
        UIImage *one = [[UIImage alloc]init];
        one = [UIImage imageNamed:@"icon_ShareBG"];
        [self serviceCallForUploadThumbnail:[self getCompressedImage:one]];
    }
    else
    {
        UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
        [self serviceCallForUploadThumbnail:[self getCompressedImage:one]];
    }
}

-(void)loadAllChlngerThumbNail
{
    _arrChlngrThumbnilIMG=[[NSMutableArray alloc]init];
    for (int i=0;i<[_arrChallenger count];i++)
    {
        NSString *strBase_URL=vBase_URL;
        NSString *strVideo=[[_arrChallenger valueForKey:@"video"]objectAtIndex:i];
        strBase_URL=[strBase_URL stringByAppendingString:strVideo];
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL URLWithString:strBase_URL] options:nil];
        AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        generate.appliesPreferredTrackTransform=TRUE;
        NSError *err = NULL;
        CMTime time = CMTimeMake(1, 120);
        CGImageRef imgRef;
    
        imgRef=[generate copyCGImageAtTime:time actualTime:NULL error:&err];
        NSLog(@"err==%@, imageRef==%@", err, imgRef);
        if (imgRef==NULL)
        {
            [_arrChlngrThumbnilIMG insertObject:@"nil" atIndex:i];
        }
        else
        {
            [_arrChlngrThumbnilIMG insertObject:(__bridge id _Nonnull)(imgRef) atIndex:i];
        }
    }
}

#pragma mark: User SignIn Server Call
-(void)specificBetPageServerAPI
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isCCData=YES;
        _isJoinSnpbt=NO;
        _isChlngrVote=NO;
        _isJoinVideoUpload=NO;
        _isSubmitReportPrsed=NO;
        _isRemoveChlngrBet=NO;
        _isRemoveChlngBet=NO;
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *strBaseURL=base_URL;

        NSString *urlString=[NSString stringWithFormat:@"wssanpbetchallengerlist.php?id=%@&uid=%@",_selectedBetID1,[theAppDelegate.arrUserLoginData objectAtIndex:0]];
    
    strBaseURL=[strBaseURL stringByAppendingString:urlString];
    NSURL *urlChallenger=[NSURL URLWithString:strBaseURL];
    
    request=[NSMutableURLRequest requestWithURL:[urlChallenger standardizedURL]];
    
    if(!connection1)
        connection1= [[WebConnection1 alloc] init];
    connection1.delegate = self;
    [connection1 makeConnection:request];
     }
    else
    {
        theAppDelegate.alert.message=@"Check Internet connection";
        [theAppDelegate.alert show];
    }
    request = nil;
}

#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    [self.view setUserInteractionEnabled:YES];
    [theAppDelegate stopSpinner];
    if (_isbtnLikePressed)
    {
        
    }
    else
    {
       [self closeViewButonPressed];
    }
    
    dict =[[NSMutableDictionary alloc]initWithDictionary:response];
    
    NSString *strMSG=[dict valueForKey:@"message"];
    if ([strMSG isEqualToString:@"sus"])
    {
        if(_isCCData)
        {
            NSMutableArray *arrCC=[dict valueForKey:@"info"];
            _arrChallenger=[arrCC valueForKey:@"challengers"];
            _arrChallenge=[arrCC valueForKey:@"challenge"];
            _arrWinner=[arrCC valueForKey:@"Winner"];
            
            if (_isbtnLikePressed)
            {
                _isbtnLikePressed=NO;
                _arrRelatedVData=[[NSMutableArray alloc]init];
                [_arrRelatedVData addObject:_arrChallenge];
                
                if (_arrChallenger == (id)[NSNull null])
                {
                    _arrChallenger=[[NSMutableArray alloc]init];
                }
                for (int i=0;i<[_arrChallenger count];i++)
                {
                    
                    [_arrRelatedVData addObject:[_arrChallenger objectAtIndex:i]];
                }
            }
            else
            {
                if (_arrChallenger == (id)[NSNull null])
                {
                    _isWinner=NO;
                    _arrChallenger=[[NSMutableArray alloc]init];
                    if (theAppDelegate.isMyBeetSelected)
                    {
                        _btnJoinSnpBet.hidden=YES;
                    }
                    //_arrChallenge=[[NSMutableArray alloc]init];
                    [_tblBet reloadData];
                    [self checkJoinSnapbetPresentOrNot];
                    [self checkVideoExpiary];
                }
                else
                {
                    _isWinner=NO;
                    [_tblBet reloadData];
                    [self checkJoinSnapbetPresentOrNot];
                    [self checkVideoExpiary];
                }
                if([_arrWinner isEqual:(id)[NSNull null]])
                {
                
                }
                else
                {
                    NSString *strStatus=[[_arrWinner objectAtIndex:0]valueForKey:@"status"];
                    if ([strStatus isEqualToString:@"Yes"])
                    {
                    _isWinner=YES;
                    _arrWinnerUData=[[NSMutableArray alloc]init];
                    _arrWinnerUData=[_arrWinner objectAtIndex:0];
                    NSString *strWin=[_arrWinnerUData valueForKey:@"win"];
                    if ([strWin isEqualToString:@"join"])
                    {
                        _isChlngerWinner=YES;
                        NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
                        arrTemp=_arrChallenger;
                        _arrChallenger=[[NSMutableArray alloc]init];
                        [_arrChallenger addObject:_arrWinnerUData];
                        for (int i=0;i<[arrTemp count];i++)
                        {
                            [_arrChallenger addObject:[arrTemp objectAtIndex:i]];
                        }
                        [_tblBet reloadData];
                        NSString *strUID=[_arrChallenge valueForKey:@"uid"];
                        if([strUID isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:0]])
                        {
                            NSLog(@"Loged User");
                            _strWTitle=@"Congratulations!";
                            _strWinSTitle=@"You have win ";
                            [self viewForWinnerPopUp];
                        }
                        else
                        {
                            NSLog(@"Another User");
                            _strWTitle=[_arrChallenge valueForKey:@"fname"];
                            _strWTitle=[_strWTitle stringByAppendingString:@" Wins!"];
                            _strWinSTitle=[_arrChallenge valueForKey:@"fname"];
                            _strWinSTitle=[_strWinSTitle stringByAppendingString:@" has won "];
                            _strWinSTitle=[_strWinSTitle stringByAppendingString:_strThemeName];
                            //[self viewForWinnerPopUpUI];
                            _btnJoinSnpBet.hidden=YES;
                        }
                    }
                    else
                    {
                        _isChlngerWinner=NO;
                        [_tblBet reloadData];
                        NSString *strUID=[_arrChallenge valueForKey:@"uid"];
                        if([strUID isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:0]])
                        {
                            NSLog(@"Loged User");
                            _strWTitle=@"Congratulations!";
                            _strWinSTitle=@"You have win ";
                            [self viewForWinnerPopUp];
                            _btnJoinSnpBet.hidden=YES;
                        }
                        else
                        {
                            NSLog(@"Another User");
                            _strWTitle=[_arrChallenge valueForKey:@"fname"];
                            if([_strWTitle isKindOfClass:[NSNull class]])
                            {
                                _strWTitle=@"N/A";
                            }
                            _strWTitle=[_strWTitle stringByAppendingString:@" Wins!"];
                            
                            
                            _strWinSTitle=[_arrChallenge valueForKey:@"fname"];
                            if([_strWinSTitle isKindOfClass:[NSNull class]])
                            {
                                _strWinSTitle=@"N/A";
                            }
                            _strWinSTitle=[_strWinSTitle stringByAppendingString:@" has won "];
                            _strWinSTitle=[_strWinSTitle stringByAppendingString:_strThemeName];
                            
                            //[self viewForWinnerPopUpUI];
                            _btnJoinSnpBet.hidden=YES;
                        }
                    }
                }
                }
            }
        }
        else if (_isJoinSnpbt)
        {
            _strVoteAlertTitle=@"Snapbet uploaded successfully!";
            [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
           // [self specificBetPageServerAPI];
        }
        else if (_isRemoveChlngBet)
        {
            _strVoteAlertTitle=@"Bet has been removed successfully!";
            [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
        }
        else if(_isRemoveChlngrBet)
        {
            _strVoteAlertTitle=@"Bet has been removed successfully!";
            [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
            
        }
        else if (_isChlngrVote)
        {
            NSArray *arrVotes=[dict valueForKey:@"info"];
            NSString *strVote=[arrVotes valueForKey:@"challenger_vote_id"];
                if ([strVote isEqualToString:@"allready"])
                {
                    _strVoteAlertTitle=@"Vote has already been placed!";
                    [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
                }
                else
                {
                    _strVoteAlertTitle=@"Vote has been placed!";
                    [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
                    
                }
        }
        else if (_isSnapVote)
        {
            if(theAppDelegate.isMyBeetSelected)
            {
                NSString *strChngUID=[_arrChallenge valueForKey:@"uid"];
                if([strChngUID isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:0]])
                {
                    _strVoteAlertTitle=@"Bet has been removed successfully!";
                }
                else
                {
                    NSArray *arrVotes=[dict valueForKey:@"info"];
                    NSString *strVote=[arrVotes valueForKey:@"snap_vote_id"];
                    if ([strVote isEqualToString:@"allready"])
                    {
                        _strVoteAlertTitle=@"Vote has already been placed!";
                        
                    }
                    else
                    {
                        _strVoteAlertTitle=@"Vote has been placed!";
                        
                    }
                }
                [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
            }
            else
            {
                NSArray *arrVotes=[dict valueForKey:@"info"];
                NSString *strVote=[arrVotes valueForKey:@"snap_vote_id"];
                if ([strVote isEqualToString:@"allready"])
                {
                    _strVoteAlertTitle=@"Vote has already been placed!";
                    [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
                }
                else
                {
                    _strVoteAlertTitle=@"Vote has been placed!";
                    [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
                }
            }
           
        }
        else if (_isJoinVideoUpload)
        {

        }
        else if (_isReport == YES)
        {
            _isReport=NO;            
            NSString *strMSG=[dict valueForKey:@"message"];
            NSLog(@"%@",strMSG);
            _strVoteAlertTitle=@"Report has been sent!";
            
            [self performSelector:@selector(viewForReportVideoUI) withObject:nil afterDelay:0.5];
        }
    }
    else
    {
        if(_isChlngrVote)
        {
            _strVoteAlertTitle=@"Failed to Vote";
            [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
        }
        else if (_isReport)
        {
            if ([strMSG isEqualToString:@"allready"])
            {
                _strVoteAlertTitle=@"This content has already been reported";
                [self performSelector:@selector(viewForReportVideoUI) withObject:nil afterDelay:0.5];
            }
        }
        else if (_isRemoveChlngBet)
        {
            
        }
        else if (_isRemoveChlngrBet)
        {
            
        }
        else if (_isSnapVote)
        {
            if([strMSG isEqualToString:@"not found"])
            {
                _arrChallenger=[[NSMutableArray alloc]init];
                _arrChallenge=[[NSMutableArray alloc]init];
                [_tblBet reloadData];
            }
            else
            {
                _strVoteAlertTitle=@"Failed to Post Challenge";
                [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
            }
        }
        else if (_isJoinSnpbt)
        {
            _strVoteAlertTitle=@"Failed to Post Challenge";
           [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
        }
        else if (_isCCData)
        {
            if([strMSG isEqualToString:@"not found"])
            {
                _arrChallenger=[[NSMutableArray alloc]init];
                _arrChallenge=[[NSMutableArray alloc]init];
                [_tblBet reloadData];
            }
            else
            {
                _strVoteAlertTitle=@"Failed to load Data";
                [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
            }
        }
        else if (_isJoinVideoUpload)
        {
           _strVoteAlertTitle=@"Failed to upload video";
           [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
        }
    }
}

#pragma mark:Check snapbet already joined or Not
-(void)checkJoinSnapbetPresentOrNot
{
    if (theAppDelegate.isMyBeetSelected)
    {
        _btnJoinSnpBet.hidden=YES;
    }
    else
    {
        if ([_arrChallenge count])
        {           
            NSString *strID=[_arrChallenge valueForKey:@"uid"];
            if ([strID isEqualToString:[theAppDelegate.arrUserLoginData objectAtIndex:0]]) {
                _btnJoinSnpBet.hidden=YES;
            }
            else
            {
                _btnJoinSnpBet.hidden=NO;
            }
        }
        for (int i=0;i<[_arrChallenger count];i++)
        {
            NSString *str=[[_arrChallenger valueForKey:@"uid"]objectAtIndex:i];
            if ([str isEqual:(id)[NSNull null]]) {
                str=[[NSString alloc]init];
            }
            else
            {
                if ([[theAppDelegate.arrUserLoginData objectAtIndex:0] isEqualToString:str]) {
                    _btnJoinSnpBet.hidden=YES;
                    break;
                }
                else
                {
                  //  _btnJoinSnpBet.hidden=NO;
                    
                }
            }
        }
    }
}
#pragma mark:ViewForVideo selection
-(void)viewForVideoUpload
{
    [_viewForUpload removeFromSuperview];
    [_btnClose removeFromSuperview];
    _viewForUpload =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    
    _viewForUpload.backgroundColor=[UIColor blackColor];
    //_viewForUpload.alpha=1;
    _viewForUpload.layer.cornerRadius=5.f;
    _viewForUpload.layer.borderColor=[[UIColor whiteColor]CGColor];
    _viewForUpload.layer.borderWidth=2.0f;
    _viewForUpload.tag=20;
    [self.view addSubview:_viewForUpload];
    CGRect frameForViewForHelp=_viewForUpload.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForUpload.frame=frameForViewForHelp;
    } completion:^(BOOL finished)
     {
         vscreenSize=_viewForUpload.frame;
         vscreenHeight=vscreenSize.size.height;
         vscreenWidth=vscreenSize.size.width;
         
         _btnClose=[[UIButton alloc]initWithFrame:CGRectMake(_viewForUpload.frame.size.width-40,10,30,30)];
         [_btnClose setBackgroundImage:[UIImage imageNamed:@"img_Close1"] forState:UIControlStateNormal];
         [_btnClose addTarget:self action:@selector(closeUploadView:) forControlEvents:UIControlEventTouchUpInside];
         [_viewForUpload addSubview:_btnClose];
         
         [_collMyVideos removeFromSuperview];
         UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
         layout.minimumInteritemSpacing =4;
         layout.minimumLineSpacing =[UIScreen mainScreen].bounds.size.height/100;
         _collMyVideos=[[UICollectionView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_btnClose.frame)+10,_viewForUpload.frame.size.width,_viewForUpload.frame.size.height-80) collectionViewLayout:layout];
         [_collMyVideos setDataSource:self];
         [_collMyVideos setDelegate:self];
         [_collMyVideos setShowsVerticalScrollIndicator:NO];
         [_collMyVideos registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
         [_collMyVideos setBackgroundColor:[UIColor clearColor]];
         [_viewForUpload addSubview:_collMyVideos];
     }];
    //  [self disableSubViews:viewForUpload];
}

#pragma mark: Check Video Expiary
-(void)checkVideoExpiary
{
    
    //get Current date
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *strCrntDate=[dateformate stringFromDate:currentDate]; // CONVERT date to string
    NSLog(@"date :%@",strCrntDate);
    
    _arrChlngrVotes=[[NSMutableArray alloc]init];
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *strCurrentDate=[DateFormatter stringFromDate:[NSDate date]];
    NSLog(@"%@",strCurrentDate);
    NSString *strExpDate=[_arrChallenge valueForKey:@"expdate"];
    NSLog(@"%@",strExpDate);
    
    if ([_arrChallenger count])
    {
        for (int i=0;i<[_arrChallenger count];i++)
        {
            NSString *strVCount=[[_arrChallenger valueForKey:@"vote"]objectAtIndex:i];
            if ([strVCount isEqual:(id)[NSNull null]]) {
                strVCount=@"N/A";
            }
            else
            {
                [_arrChlngrVotes addObject:strVCount];
            }
        }
    }
   
    NSSet *numberSet = [NSSet setWithArray:_arrChlngrVotes];
    
    NSArray *sortedNumbers = [[numberSet allObjects] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO] ]];
    NSNumber *secondHighest;
    
    if ([sortedNumbers count] > 1){
        secondHighest = sortedNumbers[1];
    }
    
}
#pragma mark: ViewFor Spinner declaration
-(void)viewForSpinner
{
    [_viewForLoadingVideos removeFromSuperview];
    _viewForLoadingVideos=[[UIView alloc]initWithFrame:CGRectMake(screenWidth/2-100,screenHeight/3-40,200,150)];
    [_viewForUpload addSubview:_viewForLoadingVideos];
    [self.view setUserInteractionEnabled:NO];
    _viewForLoadingVideos.backgroundColor=[UIColor whiteColor];
    _viewForLoadingVideos.layer.cornerRadius=5.0;
    UILabel *lblLoad=[[UILabel alloc]initWithFrame:CGRectMake(0,5,_viewForLoadingVideos.frame.size.width,30)];
    lblLoad.text=@"Wait";
    lblLoad.textAlignment=NSTextAlignmentCenter;
    lblLoad.textColor=[UIColor blackColor];
    lblLoad.font=[UIFont boldSystemFontOfSize:14];
    [_viewForLoadingVideos addSubview:lblLoad];
    UILabel *lblTit=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblLoad.frame),_viewForLoadingVideos.frame.size.width,30)];
    lblTit.text=@"Loading local videos";
    lblTit.textColor=[UIColor blackColor];
    lblTit.font=[UIFont boldSystemFontOfSize:16];
    lblTit.textAlignment=NSTextAlignmentCenter;
    [_viewForLoadingVideos addSubview:lblTit];
    
    static const CGFloat activityIndicatorSize = 40.f;
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:   UIActivityIndicatorViewStyleWhiteLarge];
    _activityIndicator.alpha = 0.f;
    _activityIndicator.hidesWhenStopped = YES;
    [_activityIndicator setFrame:CGRectMake(_viewForLoadingVideos.frame.size.width/2-20,_viewForLoadingVideos.frame.size.height-60,activityIndicatorSize,activityIndicatorSize)];
    [self showLoadingIndicators];
}

- (void)showLoadingIndicators
{
    if (_isLikeVideo) {
         [_viewForVideo addSubview:_activityIndicator];
    }
    else
    {
         [_viewForLoader addSubview:_activityIndicator];
    }
   
    [_activityIndicator startAnimating];
    _activityIndicator.color=[UIColor blackColor];
    //_activityIndicator.backgroundColor=[UIColor blackColor];
    [UIView animateWithDuration:0.2f animations:^{
        _activityBackgroundView.alpha = 1.f;
        _activityIndicator.alpha = 1.f;
    }];
}

- (void)hideLoadingIndicators
{
    [UIView animateWithDuration:0.2f delay:0.0 options:0 animations:^{
        self.activityBackgroundView.alpha = 0.0f;
        self.activityIndicator.alpha = 0.f;
    } completion:^(BOOL finished) {
        [_btnUpload setUserInteractionEnabled:YES];
        [self.activityBackgroundView removeFromSuperview];
        [self.activityIndicator removeFromSuperview];
    }];
}

#pragma mark-Close Comment View
-(void)closeUploadView:(id)sender
{
    @try
    {
        [self viewDidLoad];
        [_btnClose removeFromSuperview];
        for(UIView *subview in [_viewForUpload subviews])
        {
            [subview removeFromSuperview];
            
            
            [self.btnClose removeFromSuperview];
        }
        CGRect frameForViewForHelp=_viewForUpload.frame;
        
        frameForViewForHelp=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForUpload.frame=frameForViewForHelp;
        } completion:^(BOOL finished)
         {
             [_viewForUpload removeFromSuperview];
             [_collMyVideos removeFromSuperview];
             [self viewForPostResponsePopUPUI];
         }];
    } @catch (NSException *exception)
    {
        
    } @finally    {
        
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *strCapturedVideo=info[UIImagePickerControllerMediaURL];
    NSLog(@"%@",strCapturedVideo);
    
   // NSURL *sourceMovieURL = [NSURL fileURLWithPath:@""];
   // AVURLAsset *sourceAsset = [AVURLAsset URLAssetWithURL:sourceMovieURL options:nil];
    //  CMTime duration = sourceAsset.duration;
       
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    //check the media type string so we can determine if its a video
    
    if ([mediaType isEqualToString:@"public.movie"])
    {
        NSLog(@"got a movie");
        
        selectedVdoURL =  [info objectForKey:UIImagePickerControllerMediaURL];
        strGOrgVPath =  [info objectForKey:UIImagePickerControllerMediaURL];
        
        theAppDelegate.strVideoURL=selectedVdoURL;
        self.asset = [AVAsset assetWithURL:selectedVdoURL];
        
        NSURL *videoURL=[info objectForKey:@"UIImagePickerControllerMediaURL"];
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
        
        NSTimeInterval durationInSeconds = 0.0;
        videoTimeDuration=0.0;
        
        if (asset)
        {
            durationInSeconds = CMTimeGetSeconds(asset.duration);
            durationOfVideo = CMTimeGetSeconds(asset.duration);
            theAppDelegate.durationOfOrignalVideo = CMTimeGetSeconds(asset.duration);
            
            NSLog(@"Duration Of Video--------------------------------%f",durationInSeconds);
            //[self btnSubmitPressed];
            [picker dismissViewControllerAnimated:YES completion:NULL];
            [self performSelector:@selector(viewForVideoTrimming) withObject:nil afterDelay:0.5];
        }
    }
    else
    {
        [picker dismissViewControllerAnimated:YES completion:^{
            
            // -------------- UIImageView for Profile ----------//
            
            _image=[[UIImage alloc]init];
            //_image = [info valueForKey:UIImagePickerControllerOriginalImage];
            _image = [self scaleAndRotateImage: [info valueForKey:UIImagePickerControllerOriginalImage]];
            [self openEditor:nil];
           /* NSData *dataImage = [[NSData alloc] init];
            dataImage = UIImagePNGRepresentation(_image);
            _strUplodedImg=[dataImage base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            videoTimeDuration=0.0;
            [self serviceCallForUploadPhoto1:[self getCompressedImage:_image]];*/
        }];
    }
}

- (UIImage *)scaleAndRotateImage:(UIImage *) image {
    int kMaxResolution = 1200;
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return imageCopy;
}

-(NSData*)getCompressedImage:(UIImage*)img
{
    //>213832
    //------------  Image Compression --------- //
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    int maxFileSize = 250*1024;
    //int maxFileSize = 640*480;
    NSData  *imageData = UIImageJPEGRepresentation(img, compression);
    NSLog(@"Initial Size of Image==>%li",(long)imageData.length);
    
    while ([imageData length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        imageData = UIImageJPEGRepresentation(img, compression);
    }
    
    NSLog(@"Compressed Size of Image==>%li",(long)imageData.length);
    //------------  Image Compression Algorithm--------- //
    
    return imageData;
}

-(void)btnOriginalPressed
{
    selectedVdoURL= strGOrgVPath;
    [self playMovie:selectedVdoURL];
}

#pragma mark - Actions

- (void)deleteTempFile
{
    NSURL *url = [NSURL fileURLWithPath:self.tempVideoPath];
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL exist = [fm fileExistsAtPath:url.path];
    NSError *err;
    if (exist) {
        [fm removeItemAtURL:url error:&err];
        NSLog(@"file deleted");
        if (err) {
            NSLog(@"file remove error, %@", err.localizedDescription );
        }
    } else {
        NSLog(@"no file by that name");
    }
}


- (IBAction)trimVideo:(id)sender
{
    [self deleteTempFile];
    
    _btnSubmit.alpha=1.0;
    _btnSubmit.userInteractionEnabled=YES;
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:self.asset];
    if ([compatiblePresets containsObject:AVAssetExportPresetMediumQuality]) {
        
        self.exportSession = [[AVAssetExportSession alloc]
                              initWithAsset:self.asset presetName:AVAssetExportPresetPassthrough];
        // Implementation continues.
        
        NSURL *furl = [NSURL fileURLWithPath:self.tempVideoPath];
        
        self.exportSession.outputURL = furl;
        self.exportSession.outputFileType = AVFileTypeQuickTimeMovie;
        
        CMTime start = CMTimeMakeWithSeconds(self.startTime, self.asset.duration.timescale);
        CMTime duration = CMTimeMakeWithSeconds(self.stopTime - self.startTime, self.asset.duration.timescale);
        CMTimeRange range = CMTimeRangeMake(start, duration);
        self.exportSession.timeRange = range;
        
        [self.exportSession exportAsynchronouslyWithCompletionHandler:^{
            
            switch ([self.exportSession status]) {
                case AVAssetExportSessionStatusFailed:
                    
                    NSLog(@"Export failed: %@", [[self.exportSession error] localizedDescription]);
                    break;
                case AVAssetExportSessionStatusCancelled:
                    
                    NSLog(@"Export canceled");
                    break;
                default:
                    NSLog(@"NONE");
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        selectedVdoURL=[NSURL fileURLWithPath:self.tempVideoPath];
                        [self playMovie:selectedVdoURL];
                    });
                    
                    break;
            }
        }];
        
    }
}

-(void)btnTrimPressed
{
    //[self deleteTmpFile];
    
    //    NSURL *videoFileUrl = [NSURL fileURLWithPath:self.originalVideoPath];
    
    AVAsset *anAsset = [[AVURLAsset alloc] initWithURL:selectedVdoURL options:nil];
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:anAsset];
    if ([compatiblePresets containsObject:AVAssetExportPresetMediumQuality]) {
        
        self.exportSession = [[AVAssetExportSession alloc]
                              initWithAsset:anAsset presetName:AVAssetExportPresetPassthrough];
        // Implementation continues.
        
        NSURL *furl = [NSURL fileURLWithPath:self.tempOrignalVideoPath];
        self.exportSession.outputURL = furl;
        self.exportSession.outputFileType = AVFileTypeQuickTimeMovie;
        
        CMTime start = CMTimeMakeWithSeconds(self.startTime, anAsset.duration.timescale);
        CMTime duration = CMTimeMakeWithSeconds(self.stopTime-self.startTime, anAsset.duration.timescale);
        CMTimeRange range = CMTimeRangeMake(start, duration);
        self.exportSession.timeRange = range;
        
        [self.exportSession exportAsynchronouslyWithCompletionHandler:^{
            
            switch ([self.exportSession status])
            {
                case AVAssetExportSessionStatusFailed:
                    selectedVdoURL=[NSURL fileURLWithPath:_tempOrignalVideoPath];
                    NSLog(@"Export failed: %@", [[self.exportSession error] localizedDescription]);
                    break;
                case AVAssetExportSessionStatusCancelled:
                    selectedVdoURL=[NSURL fileURLWithPath:self.tempOrignalVideoPath];
                    NSLog(@"Export canceled");
                    break;
                default:
                    NSLog(@"NONE");
                    dispatch_async(dispatch_get_main_queue(), ^
                                   {
                                       selectedVdoURL=[NSURL fileURLWithPath:self.tmpVideoPath];
                                       [self playMovie:selectedVdoURL];
                                   });
                    break;
            }
        }];
    }
}

-(void)playMovie: (NSURL *) url
{
    MPMoviePlayerViewController *theMovie = [[MPMoviePlayerViewController alloc] initWithContentURL:url];
    [self presentMoviePlayerViewControllerAnimated:theMovie];
    theMovie.moviePlayer.movieSourceType = MPMovieSourceTypeFile;
    [theMovie.moviePlayer play];
}

#pragma mark - ICGVideoTrimmerDelegate

- (void)trimmerView:(ICGVideoTrimmerView *)trimmerView didChangeLeftPosition:(CGFloat)startTime rightPosition:(CGFloat)endTime
{
    _restartOnPlay = YES;
    
    [self.trimmerView hideTracker:true];
    
    if (startTime != self.startTime) {
        //then it moved the left position, we should rearrange the bar
        [self seekVideoToPos:startTime];
    }
    else{ // right has changed
        [self seekVideoToPos:endTime];
    }
    self.startTime = startTime;
    self.stopTime = endTime;
    
    _lblTime.text = [NSString stringWithFormat:@"%.0f secs - %.0f secs", startTime, endTime];
    _lblTotalTime.text=[NSString stringWithFormat:@"Total Time : %.0f secs",endTime - startTime];
    durationOfVideo=endTime-startTime;
    _btnTrim.backgroundColor=[UIColor orangeColor];
}

- (void)seekVideoToPos:(CGFloat)pos
{
    self.videoPlaybackPosition = pos;
    //CMTime time = CMTimeMakeWithSeconds(self.videoPlaybackPosition, self.player.currentTime.timescale);
    //NSLog(@"seekVideoToPos time:%.2f", CMTimeGetSeconds(time));
   // [self.player seekToTime:time toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
}

#pragma mark - SAVideoRangeSliderDelegate
- (void)videoRange:(SAVideoRangeSlider *)videoRange didChangeLeftPosition:(CGFloat)leftPosition rightPosition:(CGFloat)rightPosition
{
    self.startTime = leftPosition;
    self.stopTime = rightPosition;
    _lblTime.text = [NSString stringWithFormat:@"%.0f secs - %.0f secs", leftPosition, rightPosition];
    _lblTotalTime.text=[NSString stringWithFormat:@"Total Time : %.0f secs",rightPosition - leftPosition];
    durationOfVideo=rightPosition-leftPosition;
    _btnTrim.backgroundColor=[UIColor orangeColor];
}


-(void)changeTrimingVideoTime
{
    //[self deleteTmpFile];
    
    
    AVAsset *anAsset = [[AVURLAsset alloc] initWithURL:selectedVdoURL options:nil];
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:anAsset];
    if ([compatiblePresets containsObject:AVAssetExportPresetMediumQuality]) {
        
        self.exportSession = [[AVAssetExportSession alloc]
                              initWithAsset:anAsset presetName:AVAssetExportPresetPassthrough];
        // Implementation continues.
        
        NSURL *furl = [NSURL fileURLWithPath:self.tmpVideoPath];
        
        self.exportSession.outputURL = furl;
        self.exportSession.outputFileType = AVFileTypeQuickTimeMovie;
        
        CMTime start = CMTimeMakeWithSeconds(self.startTime, anAsset.duration.timescale);
        CMTime duration = CMTimeMakeWithSeconds(self.stopTime-self.startTime, anAsset.duration.timescale);
        CMTimeRange range = CMTimeRangeMake(start, duration);
        self.exportSession.timeRange = range;
        
        [self.exportSession exportAsynchronouslyWithCompletionHandler:^{
            
            switch ([self.exportSession status]) {
                case AVAssetExportSessionStatusFailed:
                    NSLog(@"Export failed: %@", [[self.exportSession error] localizedDescription]);
                    break;
                case AVAssetExportSessionStatusCancelled:
                    NSLog(@"Export canceled");
                    break;
                default:
                    NSLog(@"NONE");
                    dispatch_async(dispatch_get_main_queue(), ^
                                   {
                                       selectedVdoURL=[NSURL fileURLWithPath:self.tmpVideoPath];
                                       //[self playMovie:selectedVdoURL];
                                   });
                    
                    break;
            }
        }];
    }
    
}


#pragma mark-View For Video Trimming
-(void)viewForVideoTrimming
{
    [_viewForVideoTrim removeFromSuperview];
    _viewForVideoTrim =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForVideoTrim.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForVideoTrim.tag=20;
    [self.view addSubview:_viewForVideoTrim];
    
    CGRect frameForViewForHelp=_viewForVideoTrim.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForVideoTrim.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForVideoTrim.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        _btnCloseVideoTrim=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoTrim.frame.size.width-40,10,30,30)];
        [_btnCloseVideoTrim setBackgroundImage:[UIImage imageNamed:@"icon_Cross"] forState:UIControlStateNormal];
        [_btnCloseVideoTrim addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
        _btnCloseVideoTrim.layer.borderColor=[[UIColor orangeColor]CGColor];
        [_viewForVideoTrim addSubview:_btnCloseVideoTrim];
        
        NSString *tempDir = NSTemporaryDirectory();
        self.tmpVideoPath = [tempDir stringByAppendingPathComponent:@"tmpMov.mov"];
        self.tempOrignalVideoPath = [tempDir stringByAppendingPathComponent:@"tmpMov.mov"];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            
            [_trimmerView removeFromSuperview];
            _trimmerView=[[ICGVideoTrimmerView alloc]initWithFrame:CGRectMake(0,200, self.view.frame.size.width, 100) asset:self.asset];
        }
        else
        {
            [_trimmerView removeFromSuperview];
            _trimmerView=[[ICGVideoTrimmerView alloc]initWithFrame:CGRectMake(0,100, self.view.frame.size.width, 80) asset:self.asset];
        }
        
        [_trimmerView removeFromSuperview];
        _trimmerView=[[ICGVideoTrimmerView alloc]initWithFrame:CGRectMake(0,100, self.view.frame.size.width, 80) asset:self.asset];
        [self.trimmerView setThemeColor:[UIColor lightGrayColor]];
        [self.trimmerView setAsset:self.asset];
        [self.trimmerView setShowsRulerView:YES];
        [self.trimmerView setRulerLabelInterval:5];
        [self.trimmerView setTrackerColor:[UIColor cyanColor]];
        [self.trimmerView setDelegate:self];
        [self.trimmerView resetSubviews];
        [self.trimmerView hideTracker:true];
        [self.trimButton setHidden:NO];
        [_viewForVideoTrim addSubview:self.trimmerView];
        
        
        _lblTime=[[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.trimmerView.frame)+15, _viewForVideoTrim.frame.size.width, 30)];
        _lblTime.textColor=[UIColor whiteColor];
        _lblTime.textAlignment=NSTextAlignmentCenter;
        _lblTime.text = [NSString stringWithFormat:@"0 secs - %.0f secs", durationOfVideo];
        _lblTime.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForVideoTrim addSubview:_lblTime];
        
        _lblTotalTime=[[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_lblTime.frame)+15, _viewForVideoTrim.frame.size.width, 30)];
        _lblTotalTime.textColor=[UIColor whiteColor];
        _lblTotalTime.text=[NSString stringWithFormat:@"Total Time : %.0f secs",durationOfVideo];
        _lblTotalTime.textAlignment=NSTextAlignmentCenter;
        _lblTotalTime.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForVideoTrim addSubview:_lblTotalTime];
        
        _btnOriginal=[[UIButton alloc]initWithFrame:CGRectMake(30,CGRectGetMaxY(_lblTotalTime.frame)+30,_viewForVideoTrim.frame.size.width/2-40,50)];
        //_btnTrim=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoTrim.frame.size.width/2+15,CGRectGetMaxY(_lblTotalTime.frame)+30,_viewForVideoTrim.frame.size.width/2-30,50)];
        _btnTrim=[[UIButton alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_lblTotalTime.frame)+30,_viewForVideoTrim.frame.size.width/2-40,50)];
        
        [_btnTrim setTitle:@"Trim" forState:UIControlStateNormal];
        
        [_btnTrim setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        _btnTrim.layer.borderWidth=2.0;
        _btnTrim.layer.borderColor=[[UIColor orangeColor]CGColor];
        
        _btnOriginal.backgroundColor=[UIColor orangeColor];
        
        _btnOriginal.layer.cornerRadius=5.0;
        _btnTrim.layer.cornerRadius=5.0;
        
        [_btnOriginal addTarget:self action:@selector(btnOriginalPressed) forControlEvents:UIControlEventTouchUpInside];
        
        [_btnTrim addTarget:self action:@selector(trimVideo:) forControlEvents:UIControlEventTouchUpInside];
        //[_viewForVideoTrim addSubview:_btnOriginal];
        [_viewForVideoTrim addSubview:_btnTrim];
        
        UILabel *lblBuy=[[UILabel alloc]initWithFrame:CGRectMake(0,10,_btnOriginal.frame.size.width,30)];
        lblBuy.text=@"Original";
        lblBuy.textAlignment=NSTextAlignmentCenter;
        lblBuy.textColor=[UIColor whiteColor];
        lblBuy.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_btnOriginal addSubview:lblBuy];
        
        
        _btnSubmit=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnSubmit.frame=CGRectMake(_viewForVideoTrim.frame.size.width/2+15,CGRectGetMaxY(_lblTotalTime.frame)+30,_viewForVideoTrim.frame.size.width/2-30,50);
        }
        else
        {
            _btnSubmit=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoTrim.frame.size.width/2+15,CGRectGetMaxY(_lblTotalTime.frame)+30,_viewForVideoTrim.frame.size.width/2-30,50)];
        }
        [_btnSubmit setTitle:@"Submit" forState:UIControlStateNormal];
        [_btnSubmit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnSubmit.layer.cornerRadius=4.0;
        _btnSubmit.backgroundColor=[UIColor orangeColor];
        if (theAppDelegate.GlobDurOfOrignalVideo == durationOfVideo || durationOfVideo < theAppDelegate.GlobDurOfOrignalVideo)
        {
            
        }
        else
        {
            _btnSubmit.alpha=0.7;
            _btnSubmit.userInteractionEnabled=NO;
        }
        [_btnSubmit addTarget:self action:@selector(btnSubmitPressed) forControlEvents:UIControlEventTouchUpInside];
        [_viewForVideoTrim addSubview:_btnSubmit];
        
        UILabel *lblInfo=[[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_btnSubmit.frame)+15, _viewForVideoTrim.frame.size.width, 30)];
        lblInfo.textColor=[UIColor whiteColor];
        lblInfo.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        lblInfo.textAlignment=NSTextAlignmentCenter;
        if ([arrPurchaseDates count])
        {
            lblInfo.text=[NSString stringWithFormat:@"Select video 90 seconds or less"];
        }
        else
        {
            lblInfo.text=[NSString stringWithFormat:@"Select video 15 seconds or less"];
        }
        [_viewForVideoTrim addSubview:lblInfo];
        
        
    }];
}

-(void)btnSubmitPressed
{
    @try
    {
        NSDate *today=[NSDate date];
        NSDateFormatter *format=[[NSDateFormatter alloc]init];
        [format setDateFormat:@"dd-MMM-yy"];
        
        [self enableSubViews];
       /* for(UIView *subview in [_viewForVideoTrim subviews])
        {
            [subview removeFromSuperview];
        }*/
       // CGRect frameForViewForHelp=_viewForVideoTrim.frame;
       // frameForViewForHelp=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        [UIView animateWithDuration:0.5f animations:^{
            //_viewForVideoTrim.frame=frameForViewForHelp;
            
        } completion:^(BOOL finished)
         {
             AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:selectedVdoURL options:nil];
             durationOfVideo = CMTimeGetSeconds(asset.duration);
             if ((durationOfVideo > 15.2 || durationOfVideo == 16) && [arrPurchaseDates count] == 0 )
             {
                 _isSnapSuccess=NO;
                 _strVoteAlertTitle=@"Video should be 15 seconds or less";
                 [self performSelector:@selector(viewForTrimingVideoAlertUI) withObject:nil afterDelay:0.5];
             }
             else if ([arrPurchaseDates count] && (durationOfVideo > 90.2 || durationOfVideo == 91))
             {
                 _isSnapSuccess=NO;
                 _strVoteAlertTitle=@"Video should be 90 seconds or less";
                 [self performSelector:@selector(viewForTrimingVideoAlertUI) withObject:nil afterDelay:0.5];
             }
             else if([arrPurchaseDates count])
             {
                 if ([arrExpiryDates count] == 1)
                 {
                     NSString *strExpireDate = [arrExpiryDates objectAtIndex:0];
                     NSDate *expireDate=[format dateFromString:strExpireDate];
                     NSComparisonResult result1=[today compare:expireDate];
                     if (result1 == NSOrderedDescending)
                     {
                         _isSnapSuccess=NO;
                         _strVoteAlertTitle=@"Video should be 15 seconds or less";
                         [self performSelector:@selector(viewForTrimingVideoAlertUI) withObject:nil afterDelay:0.5];
                     }
                     else
                     {
                         [_viewForVideoTrim removeFromSuperview];
                         [self loadVideoToServer];
                     }
                 }
                 else
                 {
                     NSString *strExpireDate1 = [arrExpiryDates objectAtIndex:0];
                     NSString *strExpireDate2 = [arrExpiryDates objectAtIndex:0];
                     
                     NSDate *expireDate1=[format dateFromString:strExpireDate1];
                     NSDate *expireDate2=[format dateFromString:strExpireDate2];
                     
                     NSComparisonResult result1=[expireDate1 compare:expireDate2];
                     if (result1 == NSOrderedSame)
                     {
                         NSComparisonResult result1=[today compare:expireDate1];
                         if (result1 == NSOrderedDescending)
                         {
                             _isSnapSuccess=NO;
                             _strVoteAlertTitle=@"Video should be 15 seconds or less";
                             [self performSelector:@selector(viewForAlert) withObject:nil afterDelay:0.5];
                         }
                         else
                         {
                             [_viewForVideoTrim removeFromSuperview];
                             [self loadVideoToServer];
                         }
                     }
                     else if (result1 == NSOrderedAscending)
                     {
                         NSComparisonResult result1=[today compare:expireDate2];
                         if (result1 == NSOrderedDescending)
                         {
                             _isSnapSuccess=NO;
                             _strVoteAlertTitle=@"Video should be 15 seconds or less";
                             [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
                         }
                         else
                         {
                             //[self loadVideoToServer];
                         }
                     }
                     else
                     {
                         NSComparisonResult result1=[today compare:expireDate1];
                         if (result1 == NSOrderedDescending)
                         {
                             _isSnapSuccess=NO;
                             _strVoteAlertTitle=@"Video should be 15 seconds or less";
                             [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
                         }
                         else
                         {
                             [_viewForVideoTrim removeFromSuperview];
                             [self loadVideoToServer];
                         }
                     }
                 }
             }
             else
             {
                 [_viewForVideoTrim removeFromSuperview];
                 [self loadVideoToServer];
             }
             
         }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}
-(void)loadVideoToServer
{
    //videoTimeDuration=durationInSeconds;
    NSData *data = [NSData dataWithContentsOfURL:selectedVdoURL];
    
    if(data==nil)
    {
        _strVideoName=[[NSString alloc]init];
    }
    else
    {
        NSString *urlString = @"http://54.67.95.152/snapbets/aws/uploadvideofile.php?";
        NSString *filename = @"aa";
        NSMutableURLRequest *request=[[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        NSMutableData *postbody = [NSMutableData data];
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.MP4\"\r\n", filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[NSData dataWithData:data]];
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:postbody];
    
        if (theAppDelegate.isConnected)
        {
            _isCCData=NO;
            _isJoinSnpbt=NO;
            _isChlngrVote=NO;
            _isJoinVideoUpload=YES;
            _isVideoUpload=YES;
            _isLikeVideo=NO;
            _isSubmitReportPrsed=NO;
        
            [self ViewForDataLoadingAlert];
            _connUploadVideo=[[NSURLConnection alloc]initWithRequest:request delegate:self];
        }
        else
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Sorry"   message:@"Please check internet connection" preferredStyle:UIAlertControllerStyleAlert];
        
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
        
            [self presentViewController:alertController animated:YES completion:nil];
    }
 }
}
#pragma mark: UploadFirstImage
-(void)serviceCallForUploadPhoto1:(NSData*)imgData
{
    if ([theAppDelegate isConnected])
    {
        NSString *strBase=base_URL;
        NSString *url=[NSString stringWithFormat:@"uploadimage_ios.php"];
        strBase=[strBase stringByAppendingString:url];
        
        NSLog(@"Image upload URL :%@",url);
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strBase]];
        
        NSData *imageData = imgData;
        
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"unique-consistent-string";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"imageCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"Some Caption"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        if(imageData)
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=imagename.png\r\n",@"userfile"] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[NSData dataWithData:imageData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        // [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        NSHTTPURLResponse* response =[[NSHTTPURLResponse alloc] init];
        NSError* error = [[NSError alloc] init];
        
        //synchronous filling of data from HTTP POST response
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        //convert data into string
        NSString   *responseString = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];
        
        NSLog(@"Response String %@",responseString);
        
        NSError *error1;
        NSMutableDictionary *mainDict=[NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error1];
        
        NSString *strMSG=[mainDict valueForKey:@"message"];
        NSArray *arr=[[NSArray alloc]init];
        arr=[mainDict valueForKey:@"info"];
        responseString=[arr description];
        
        if ([strMSG isEqualToString:@"sus"])
        {
            _strVideoName=@"No";
            _strUplodedImg=responseString;
            NSString *strBURL=ImgBase_URL;
            strBURL=[strBURL stringByAppendingString:responseString];
            [_asyVideoThumbnil loadImageFromURL:[NSURL URLWithString:strBURL]];

            _btnUploadSnpbet.hidden=YES;
            //_btnPlayNewSnapbet.hidden=YES;
           // [_btnPlayNewSnapbet setUserInteractionEnabled:NO];
            
            _lblInfoSubmitSnap=[[UILabel alloc]initWithFrame:CGRectMake(30,CGRectGetMaxY(_viewForPostVideo.frame)+5,screenWidth-60,60)];
            _lblInfoSubmitSnap.text=@"are you ready to submit this response?";
            _lblInfoSubmitSnap.textColor=[UIColor whiteColor];
            _lblInfoSubmitSnap.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
            [_lblInfoSubmitSnap setNumberOfLines:2];
            _lblInfoSubmitSnap.textAlignment=NSTextAlignmentCenter;
            [_viewForPostResponse addSubview:_lblInfoSubmitSnap];
            
            _btnSubmitSnapbet=[[UIButton alloc]initWithFrame:CGRectMake(_viewForPostResponse.frame.size.width/2-70,CGRectGetMaxY(_lblInfoSubmitSnap.frame)+10,140,40)];
            [_btnSubmitSnapbet setTitle:@"Submit Snapbet" forState:UIControlStateNormal];
            [_btnSubmitSnapbet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            _btnSubmitSnapbet.titleLabel.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
            _btnSubmitSnapbet.layer.cornerRadius=5.0;
            _btnSubmitSnapbet.layer.borderColor=[[UIColor orangeColor]CGColor];
            _btnSubmitSnapbet.layer.borderWidth=2.0;
            [_btnSubmitSnapbet addTarget:self action:@selector(btnSubmitSnapbetPressed:) forControlEvents:UIControlEventTouchUpInside];
            [_viewForPostResponse addSubview:_btnSubmitSnapbet];
        }
        else
        {
            //_imgProfile.image=[UIImage imageNamed:@""];
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:@"Failed to upload picture to server." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            
            //[[[UIAlertView alloc]initWithTitle:@"Failed" message:@"Failed to upload picture to server." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
    }
    else
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"No Internet connection"
                                              message:@""
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"ok", @"Cancel action")
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"Cancel action");
                                       }];
        
        
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    [theAppDelegate stopSpinner];
    [self.view setUserInteractionEnabled:YES];
}

#pragma mark: UploadFirstImage
-(void)serviceCallForUploadThumbnail:(NSData*)imgData
{
    NSString *strBase=base_URL;
    NSString *url=[NSString stringWithFormat:@"uploadimage_ios.php"];
    strBase=[strBase stringByAppendingString:url];
    
    NSLog(@"Image upload URL :%@",url);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strBase]];
    NSData *imageData = imgData;
    
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"imageCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"Some Caption"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    if(imageData)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=imagename.png\r\n",@"userfile"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    // [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    NSHTTPURLResponse* response =[[NSHTTPURLResponse alloc] init];
    NSError* error = [[NSError alloc] init];
    
    //synchronous filling of data from HTTP POST response
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    //convert data into string
    NSString   *responseString = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];
    [_btnPlayNewSnapbet setUserInteractionEnabled:YES];
    NSLog(@"Response String %@",responseString);
    
    NSError *error1;
    NSMutableDictionary *mainDict=[NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error1];
    NSArray *arr=[[NSArray alloc]init];
    arr=[mainDict valueForKey:@"info"];
    responseString=[arr description];
    
    if (responseString.length>0)
    {
        _strUplodedImg=responseString;
        NSString *strBURL=ImgBase_URL;
        strBURL=[strBURL stringByAppendingString:responseString];
        [_asyVideoThumbnil loadImageFromURL:[NSURL URLWithString:strBURL]];

    }
    else
    {
        //_imgProfile.image=[UIImage imageNamed:@""];
        
        _strVideoName=@"";
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:@"Failed to upload Thumbil." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        
        //[[[UIAlertView alloc]initWithTitle:@"Failed" message:@"Failed to upload picture to server." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    [self.view setUserInteractionEnabled:YES];
}


#pragma mark: Viewfor Loading alert
-(void)ViewForDataLoadingAlert
{
                [_viewForLoader removeFromSuperview];
                _viewForLoader=[[UIView alloc]initWithFrame:CGRectMake(_viewForPostResponse.frame.size.width/2-100,_viewForPostResponse.frame.size.height/3-40,200,150)];
                [self.view setUserInteractionEnabled:NO];
                _viewForLoader.backgroundColor=[UIColor whiteColor];
                _viewForLoader.layer.cornerRadius=5.0;
                UILabel *lblLoad=[[UILabel alloc]initWithFrame:CGRectMake(0,5,_viewForLoader.frame.size.width,30)  ];
                lblLoad.text=@"Wait";
                lblLoad.textAlignment=NSTextAlignmentCenter;
                lblLoad.textColor=[UIColor blackColor];
                lblLoad.font=[UIFont boldSystemFontOfSize:14];
                [_viewForLoader addSubview:lblLoad];
                UILabel *lblTit=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblLoad.frame),_viewForLoader.frame.size.width,30)];
    
                if (_isLikeVideo)
                {
                    
                }
                else
                {
                    lblTit.text=@"Video Uploading";
                    
                }
                lblTit.textColor=[UIColor blackColor];
                lblTit.font=[UIFont boldSystemFontOfSize:16];
                lblTit.textAlignment=NSTextAlignmentCenter;
                [_viewForLoader addSubview:lblTit];
                
                static const CGFloat activityIndicatorSize = 40.f;
                
                _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:   UIActivityIndicatorViewStyleWhiteLarge];
                _activityIndicator.alpha = 0.f;
                _activityIndicator.hidesWhenStopped = YES;
                [_activityIndicator setFrame:CGRectMake(_viewForLoader.frame.size.width/2-20,_viewForLoader.frame.size.height-60,activityIndicatorSize,activityIndicatorSize)];
                [_viewForPostResponse addSubview:_viewForLoader];
                [self showLoadingIndicators];
}

#pragma mark:- Collection view delegate, datasource method declarations here...!!
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_videoURLArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UILabel *lbl in cell.subviews)
    {
        if ([lbl isKindOfClass:[UILabel class]])
        {
            [lbl removeFromSuperview];
        }
    }
    
    for (UIImageView *img in cell.contentView.subviews) {
        if ([img isKindOfClass:[UIImageView class]]) {
            [img removeFromSuperview];
        }
    }
    UIImageView *imgPoster=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,cell.frame.size.width,cell.frame.size.height)];
    //imgPoster.contentMode=UIViewContentModeScaleAspectFill;
    
    NSData *imgData=[_arrMyVideoData objectAtIndex:indexPath.row];
    imgPoster.image=[UIImage imageWithData:imgData];
    // [imgPoster setClipsToBounds:YES];
    NSLog(@"%ld",(long)indexPath.row);
    imgPoster.layer.borderWidth=0.5;
    imgPoster.layer.borderColor=[[UIColor whiteColor]CGColor];
    [cell.contentView addSubview:imgPoster];
    
    
    NSString *strSecond=[_arrSeconds objectAtIndex:indexPath.row];
    NSString *strMinute=[_arrMinute objectAtIndex:indexPath.row];
    NSString *strHour=[_arrHour objectAtIndex:indexPath.row];
    
    if ([strSecond isEqualToString:@"0"]) {
        strSecond=@"00";
    }
    if ([strMinute isEqualToString:@"0"]) {
        strMinute=@"00:";
        strMinute=[strMinute stringByAppendingString:strSecond];
    }
    else if (strMinute.length>0&&[strHour isEqualToString:@"0"])
    {
        strMinute=[strMinute stringByAppendingString:@":"];
        strMinute=[strMinute stringByAppendingString:strSecond];
    }
    
    if ([strHour isEqualToString:@"0"]) {
        strHour=@"";
    }
    
    if (strHour.length>0) {
        strHour=[strHour stringByAppendingString:@":"];
        
        strHour=[strHour stringByAppendingString:strMinute];
        strHour=[strHour stringByAppendingString:strSecond];
    }
    UILabel *lblTime=[[UILabel alloc]initWithFrame:CGRectMake(imgPoster.frame.size.width-50,imgPoster.frame.size.height-30,40,30)];
    if (strHour.length>0) {
        lblTime.text=strHour;
    }
    else
    {
        lblTime.text=strMinute;
    }
    lblTime.textColor=[UIColor whiteColor];
    lblTime.font=[UIFont systemFontOfSize:14];
    [imgPoster addSubview:lblTime];
    
    
    //  cell.layer.borderColor=[[UIColor whiteColor]CGColor];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSelector:@selector(closeUploadView:) withObject:nil afterDelay:0];
}

#pragma mark: Cell Sizes Declarations here
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (screenWidth<325)
    {
        return CGSizeMake(100,80);
        
    }
    else
    {
        return CGSizeMake(100,100);
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5,[UIScreen mainScreen].bounds.size.width/100/20,0,3); // top, left, bottom,
}

//-----  cell line spacing declarations here -----//
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if (screenWidth<325)
    {
        return 5.0;
    }
    else
    {
        return 5.0;
    }
    return 0;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y < yOffset) {
        
        // scrolls down.
        yOffset = scrollView.contentOffset.y;
    }
    else
    {
        // scrolls up.
        yOffset = scrollView.contentOffset.y;
        // Your Action goes here...
    }
    
}


#pragma mark- Connection  Methods
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _webdata =[[NSMutableData alloc] init];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_webdata appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    @try
    {
            NSError *error;
            [_viewForLoader removeFromSuperview];
            [self.view setUserInteractionEnabled:YES];
            [self hideLoadingIndicators];
        
            NSString *strMSG;
            if (_isVideoUpload)
            {
                _dictVideo=[NSJSONSerialization JSONObjectWithData:_webdata options:NSJSONReadingMutableContainers error:&error];
                strMSG=[_dictVideo valueForKey:@"message"];
            }
            else if (_isLikeVideo)
            {
                _dictLike=[NSJSONSerialization JSONObjectWithData:_webdata options:NSJSONReadingMutableContainers error:&error];
                 strMSG=[_dictLike valueForKey:@"message"];
            }
        
        
            if([strMSG isEqualToString:@"sus"])
            {
                if (connection==_connUploadVideo)
                {
           
                    if (_isVideoUpload)
                    {
                        _strVideoName=[_dictVideo valueForKey:@"info"];
                        [self generateImage];
                        _btnPlayNewSnapbet.hidden=NO;
                        [_btnPlayNewSnapbet setUserInteractionEnabled:YES];
                        
                        _btnUploadSnpbet.hidden=YES;
                       // [_btnPlayNewSnapbet setUserInteractionEnabled:NO];
                        _lblInfoSubmitSnap=[[UILabel alloc]initWithFrame:CGRectMake(30,CGRectGetMaxY(_viewForPostVideo.frame)+5,screenWidth-60,60)];
                        _lblInfoSubmitSnap.text=@"are you ready to submit this response?";
                        _lblInfoSubmitSnap.textColor=[UIColor whiteColor];
                        _lblInfoSubmitSnap.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
                        [_lblInfoSubmitSnap setNumberOfLines:2];
                        _lblInfoSubmitSnap.textAlignment=NSTextAlignmentCenter;
                        [_viewForPostResponse addSubview:_lblInfoSubmitSnap];
            
                        _btnSubmitSnapbet=[[UIButton alloc]initWithFrame:CGRectMake(_viewForPostResponse.frame.size.width/2-70,CGRectGetMaxY(_lblInfoSubmitSnap.frame)+10,140,40)];
                        [_btnSubmitSnapbet setTitle:@"Submit Snapbet" forState:UIControlStateNormal];
                        [_btnSubmitSnapbet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        _btnSubmitSnapbet.titleLabel.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
                        _btnSubmitSnapbet.layer.cornerRadius=5.0;
                        _btnSubmitSnapbet.layer.borderColor=[[UIColor orangeColor]CGColor];
                        _btnSubmitSnapbet.layer.borderWidth=2.0;
                        [_btnSubmitSnapbet addTarget:self action:@selector(btnSubmitSnapbetPressed:) forControlEvents:UIControlEventTouchUpInside];
                        [_viewForPostResponse addSubview:_btnSubmitSnapbet];
                    }
                }
                else if (connection==_connLikeVideo)
                {
                    if (_isLikeVideo)
                    {
                        if ([_strSelectedThemeUser isEqualToString:@"Challenge"])
                        {
                                NSArray *arrVotes=[_dictLike valueForKey:@"info"];
                                NSString *strVote=[arrVotes valueForKey:@"snap_vote_id"];
                                if ([strVote isEqualToString:@"allready"])
                                {
                                    //_strVoteAlertTitle=@"Vote has been placed already";
                                    //[self performSelector:@selector(viewForVoteUI1) withObject:nil afterDelay:0.5];
                                }
                                else
                                {
                                    [_btnLikeVideo setBackgroundImage:[UIImage imageNamed:@"icon_CMark"] forState:UIControlStateNormal];
                                    [_btnLikeVideo setUserInteractionEnabled:NO];
                                    if([_strSelectedThemeUser isEqualToString:@"Challenge"])
                                    {
                                        
                                    }
                                    else
                                    {
                                        _isbtnLikePressed=YES;
                                        [self specificBetPageServerAPI];
                                    }
                                    //_strVoteAlertTitle=@"Vote has been placed!";
                                    //[self performSelector:@selector(viewForVoteUI1) withObject:nil afterDelay:0.5];
                                }
                        }
                        else
                        {
                            NSArray *arrVotes=[_dictLike valueForKey:@"info"];
                            NSString *strVote=[arrVotes valueForKey:@"challenger_vote_id"];
                            if ([strVote isEqualToString:@"allready"])
                            {
                                //_strVoteAlertTitle=@"Vote has been placed already";
                                //[self performSelector:@selector(viewForVoteUI1) withObject:nil afterDelay:0.5];
                            }
                            else
                            {
                                [_btnLikeVideo setBackgroundImage:[UIImage imageNamed:@"icon_CMark"] forState:UIControlStateNormal];
                                [_btnLikeVideo setUserInteractionEnabled:NO];
                                _isbtnLikePressed=YES;
                                 [self specificBetPageServerAPI];
                               // _strVoteAlertTitle=@"Vote has been placed!";
                               // [self performSelector:@selector(viewForVoteUI1) withObject:nil afterDelay:0.5];
                            }
                        }
                    }
                }
            }
            else
            {
                if (_isVideoUpload)
                {
                    _strVoteAlertTitle=@"Failed to upload video";
                    [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
                }
                else if (_isLikeVideo)
                {
                    _strVoteAlertTitle=@"Failed to Like video";
                    [self performSelector:@selector(viewForVoteUI) withObject:nil afterDelay:0.5];
                }
                    _strVideoName=@"";
            }
    }
     @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark-Get Data From Database
-(void)getDataFromDB
{
    NSString *selectQuery;
    DBManager *objDBManager=[DBManager getSharedInstance];
    NSMutableArray *arrData=[[NSMutableArray alloc]init];
    arrPurchaseDates=[[NSMutableArray alloc]init];
    arrExpiryDates=[[NSMutableArray alloc]init];
    
    selectQuery=[NSString stringWithFormat:@"select * from tblInApp where UID='%@'",[theAppDelegate.arrUserLoginData objectAtIndex:0]];
    
    NSArray *arrTemp=[objDBManager selectTableDataWithQuery:selectQuery];
    NSLog(@"%@",arrTemp);
    
    if ([arrTemp count])
    {
        for (int i=0; i<[arrTemp count]; i++)
        {
            arrData=[arrTemp objectAtIndex:i];
            NSString *strPurchaseDate=[arrData objectAtIndex:3];
            NSString *strExpiryDate=[arrData objectAtIndex:4];
            
            [arrPurchaseDates addObject:strPurchaseDate];
            [arrExpiryDates addObject:strExpiryDate];
        }
    }
}

- (IBAction)openEditor:(id)sender
{
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image =_image;
    UIImage *image = _image;
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    CGFloat length = MIN(width, height);
    controller.imageCropRect = CGRectMake((width - length) / 2,
                                          (height - length) / 2,
                                          length,
                                          length);
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    
    [self presentViewController:navigationController animated:YES completion:NULL];
}

#pragma mark - PECropViewControllerDelegate methods

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    croppedIImage=croppedImage;
    [controller dismissViewControllerAnimated:YES completion:NULL];
    [theAppDelegate showSpinnerInView:self.view];
    [self performSelector:@selector(UploadImage) withObject:nil afterDelay:0.2];
    
    //NSData *imageData=UIImageJPEGRepresentation(croppedImage, 0.1);
    
}

-(void)UploadImage
{
    NSData *dataImage = [[NSData alloc] init];
    dataImage = UIImagePNGRepresentation(croppedIImage);
    _strUplodedImg=[dataImage base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    videoTimeDuration=0.0;
    [self serviceCallForUploadPhoto1:[self getCompressedImage:croppedIImage]];
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark: view For Alert
-(void)viewForTrimingVideoAlertUI
{
    [_viewForTrimAlertUI removeFromSuperview];
    _viewForTrimAlertUI =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForTrimAlertUI.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForTrimAlertUI.tag=20;
    [self.view addSubview:_viewForTrimAlertUI];
    
    CGRect frameForViewForHelp=_viewForTrimAlertUI.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForTrimAlertUI.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForTrimAlertUI.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForTrimAlertUI.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForTrimAlertUI addSubview:img];
        UILabel *lblAlert=[[UILabel alloc]init];
        lblAlert.frame=CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForTrimAlertUI.frame.size.width,30);
        lblAlert.text=_strVoteAlertTitle;//_strVoteAlertTitle
        lblAlert.textAlignment=NSTextAlignmentCenter;
        lblAlert.textColor=[UIColor whiteColor];
        lblAlert.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForTrimAlertUI addSubview:lblAlert];
        
        _btnCloseAlert=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseAlert.frame=CGRectMake(_viewForTrimAlertUI.frame.size.width/2-60,CGRectGetMaxY(lblAlert.frame)+50,120,40);
        }
        else
        {
            _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForTrimAlertUI.frame.size.width/2-50,CGRectGetMaxY(lblAlert.frame)+50,100,40)];
        }
        [_btnCloseAlert setTitle:@"Close" forState:UIControlStateNormal];
        [_btnCloseAlert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseAlert.layer.borderWidth=2.0;
        _btnCloseAlert.layer.cornerRadius=4.0;
        _btnCloseAlert.layer.borderColor=[[UIColor orangeColor]CGColor];
        //[_btnVoteSeeMore addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        [_btnCloseAlert addTarget:self action:@selector(viewForCloseTrimAlert) forControlEvents:UIControlEventTouchUpInside];
        [_viewForTrimAlertUI addSubview:_btnCloseAlert];
    }];
}
#pragma mark-Close Comment View
-(void)viewForCloseTrimAlert
{
    @try
    {
        for(UIView *subview in [_viewForTrimAlertUI subviews])
        {
            [subview removeFromSuperview];
        }
        
        
        CGRect frameForViewAlert=_viewForTrimAlertUI.frame;
        
        frameForViewAlert=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForTrimAlertUI.frame=frameForViewAlert;
            
        } completion:^(BOOL finished){
            [_viewForTrimAlertUI removeFromSuperview];
            
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}
@end
