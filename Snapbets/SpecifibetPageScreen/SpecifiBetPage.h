//
//  SpecifiBetPage.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 28/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderAndConstants.h"
#import "WebConnection1.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "AsyncImageView.h"
#import <MediaPlayer/MediaPlayer.h>
#import "ProfileScreen.h"
#import <Social/Social.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <QuartzCore/QuartzCore.h>

#import "ICGVideoTrimmerView.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>

@interface SpecifiBetPage : UIViewController<UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,WebRequestResult1,UIPopoverControllerDelegate,ICGVideoTrimmerDelegate>
{
    UIImage *croppedIImage;
    NSURL *selectedVdoURL,*strGOrgVPath;
    NSTimeInterval durationOfVideo;
     WebConnection1 *connection1;
    NSMutableDictionary *dict;
    CGFloat yOffset;
    CGRect vscreenSize;
    CGFloat vscreenHeight,vscreenWidth;
    NSInteger selectedVideoID,strSlectedBetTag;
    NSTimeInterval videoTimeDuration;
    NSInteger selectedReptID,slectedRIndexID;
    NSString *strSID;
    NSMutableArray *arrPurchaseDates,*arrExpiryDates;
    NSUInteger slectedShareIndex;
    NSString *strShareUser, *strTwiterImagePath;
}

@property(strong,nonatomic)IBOutlet UIActivityIndicatorView *loader;

@property (nonatomic) IBOutlet ICGVideoTrimmerView *trimmerView;

#pragma mark: UIView Properties declarations here
@property(nonatomic)IBOutlet UIView *viewChallenge,*viewForVideo,*viewForReport,*viewForPostResponse,*viewForVote,*viewForShare,*viewForVideoArea,*viewForPostVideo,*viewForLoader,*viewForVote1,*viewForWinnerPopUp,*viewForYNAlertUI,*viewForNoDataFound,*viewForVideoTrim,*viewForTrimAlertUI;

#pragma mark: ImageView declarations here
@property (nonatomic)AsyncImageView *imgThumbnil,*asyHeaderBG,*asyVideoThumbnil,*imgVideoBG;
@property (nonatomic)IBOutlet UIImageView *imgHeaderBG,*imgTitleBG;

#pragma mark:UIButton Properties declarations here
@property(nonatomic)IBOutlet UIButton *btnBack,*btnBackPress,*btnPlay,*btnVote,*btnShare,*btnMoreInfo,*btnMoreInfo1,*btnJoinSnpBet,*btnNext,*btnPrivious,*btnCloseVideoUI,*btnViewReport,*btnCloseReport,*btnClsoePostRespnse,*btnVoteSeeMore,*btnVoteClose,*btnShareFB,*btnShareTwit,*btnShareYoutube,*btnClose,*btnUpload,*btnUploadSnpbet,*btnSubmitSnapbet,*btnPlayNewSnapbet,*btnLikeVideo,*btnCloseShareUI,*btnCloseWinner,*btnCloseUpgradeNow,*btnYes,*btnNo,*btnCloseVideoTrim,*btnTrim,*btnOriginal,*btnSubmit,*btnCancel,*btnCancel1,*btnSend1,*btnCloseAlert;

@property (nonatomic)IBOutlet UIButton *btnPlayRltedIcon;

#pragma mark: ALPlayverViewController Properties declarations here
@property (nonatomic) AVPlayerViewController *playerViewController;

#pragma mark: UITableView Properties declarations here
@property(nonatomic)IBOutlet UITableView *tblBet,*tblRelatedVideos;

#pragma mark: UILabel Properties declarations here
@property(nonatomic)IBOutlet UILabel *lblNameVideo,*lblChallenge,*lblVote,*lblChallengers,*lblTags,*lblInfoSubmitSnap,*lblTime,*lblTotalTime;

#pragma mark: UIView Properties declarations here
@property (nonatomic)IBOutlet UIView *viewForUpload,*viewForLoadingVideos,*viewForReportVideo;
@property (nonatomic, strong) UIView *activityBackgroundView;

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

#pragma mark: NSMutable Array Properties declarations here
@property (nonatomic)NSMutableArray *arrMyVideoData,*arrChlngrVotes,*arrRelatedVData;

@property(nonatomic)NSMutableArray *arrSeconds,*arrMinute,*arrHour,*arrVideosList,*arrChallenge,*arrChallenger,*arrChlngThumbnilIMG,*arrChlngrThumbnilIMG,*arrWinner,*arrWinnerUData;

@property (nonatomic, strong) NSURL *videoURL;
@property (nonatomic, strong) NSMutableArray *videoURLArray;
@property (nonatomic, strong) NSMutableArray *assetItems;
@property (nonatomic, strong) NSMutableDictionary *dic;
@property (nonatomic, strong) NSMutableURLRequest *reqVideoUpload;

#pragma mark: NSString Properties declarations here
@property(nonatomic)NSString *strSelectedThemeUser,*strVideoName,*strThemeName,*selectedBetID1,*strSelectedVideoURL,*strBetImg,*strUplodedImg,*strSelectedBetUID,*strVoteAlertTitle,*strWTitle,*strWinSTitle,*strSelectBUID,*strChlgerRemoveID;
@property (strong, nonatomic) NSString *tmpVideoPath,*strAlertTitle,*tempOrignalVideoPath;
;

#pragma mark: UICollectionView Properties declarations here
@property (nonatomic)IBOutlet UICollectionView *collMyVideos;

#pragma mark: Bool Properties declarations here
@property (nonatomic) BOOL isCCData,isJoinSnpbt,isChlngrVote,isJoinVideoUpload,isSnapVote,isVideoUpload,isLikeVideo,isAlreadyVote,isVideoPlayed,isSubmitReportPrsed,ischlgrReport,isWinner,isChlngerWinner,isReport,isRemoveChlngBet,isRemoveChlngrBet,isSnapSuccess,isLFromManyBet,isbtnLikePressed;

#pragma mark: UIImage properties declarations here
@property (nonatomic) UIImage *image;


#pragma mark- Connetion Objects
@property(nonatomic) NSMutableData *webdata;
@property (nonatomic)NSURLConnection *connUploadVideo,*connLikeVideo;
@property (nonatomic)NSMutableDictionary *dictVideo,*dictLike;

@end
