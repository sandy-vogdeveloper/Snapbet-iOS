//
//  ViewController.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 06/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderAndConstants.h"
#import "RegistrationScreen/RegistrationScreen.h"
#import "SignInScreen/SignInScreen.h"
#import "SearchScreen/SearchScreen.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "WebConnection1.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "APPChildViewController.h"
#import "DBManager.h"

@interface ViewController : UIViewController<WebRequestResult1>

{
    WebConnection1 *connection;
    NSMutableDictionary *dict;
    NSInteger curntIndexPge;
}

@property (nonatomic)DBManager *objDBManager;

#pragma mark: UIButton Properties declarations here
@property (nonatomic)IBOutlet UIButton *btnForFB,*btnForSignIn;

#pragma mark: UIView Properties declaratiosn here
@property (nonatomic)IBOutlet UIView *viewForTutorial,*viewForAlert,*viewForBlockedAlert;

#pragma mark: UILabel Properties declarations here
@property (nonatomic)IBOutlet UILabel *lblTitle;

#pragma mark: UIImageView properties declarations here
@property (nonatomic)IBOutlet UIImageView *imgMainBG,*imgTrangleBG;

#pragma mark: UIButton Properties declarations here
@property (nonatomic)IBOutlet UIButton *btnSignUp,*btnSignIn,*btnFB,*btnClose,*btnNext1,*btnCloseAlert;

#pragma mark: NSURL declarations here
@property(nonatomic)NSURL *urlFB;

#pragma mark: NSString Properties declarations here
@property(nonatomic)NSString *strFBMail,*strFBID,*strFBName,*strAlertTitle;

#pragma mark- UIPageViewController Properties
//@property (strong, nonatomic) UIPageViewController *pageController;

#pragma mark: NSMutableArray Properties declarations here
@property (nonatomic)NSMutableArray *arrLoginInfo;

@property (nonatomic)BOOL isfbLogin,isStatus;

@end

