//
//  AppDelegate.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 06/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//https://we.tl/XmQayqSNyc
/*
 if ((application.applicationState ==  UIApplicationStateBackground))
 {
 
 NSLog(@"===========================");
 NSLog(@"App was in BACKGROUND...");
 
 NSLog(@"In didReceiveRemoteNotification badge number is %ld",(long)[UIApplication sharedApplication].applicationIconBadgeNumber);
 [UIApplication sharedApplication].applicationIconBadgeNumber += [[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] integerValue];
 NSLog(@"In didReceiveRemoteNotification badge number is %ld",(long)[UIApplication sharedApplication].applicationIconBadgeNumber);
 
 }

 */
#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "MainDashboardScreen.h"
#import "SpecifiBetPage.h"
#import "ViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <Social/Social.h>

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface AppDelegate ()<UNUserNotificationCenterDelegate>

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{    
    NSLog(@"%f",screenWidth);
    NSLog(@"%f",screenHeight);
    _isLodedFromAddDelegate=NO;
    _isStartFromAppDelegate=YES;
    _isPoolRefresh=NO;
    _isAlreadyPurchased=NO;
    _isFromSearchProcess=NO;
    _isUserBlocked=NO;
    _isAppBackground=NO;
    _isNotiArrival=NO;
    _isFromBG=NO;
    _strTrimingTime=@"0";
    [UIApplication sharedApplication].applicationIconBadgeNumber=0;
    // Override point for customization after application launch.
    _arrUserLoginData=[[NSMutableArray alloc]init];
    [self createAllTablesIfNotExist];
    UIDevice *device = [UIDevice currentDevice];
    _isMyBeetSelected=NO;
    _isFirstTime=YES;
    _strDeviceID=[[device identifierForVendor]UUIDString];
    NSLog(@"%@",_strDeviceID);
    
    [self registerForRemoteNotification];
    
    //-------- Check Internet Connection
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkChanged:) name:kReachabilityChangedNotification object:nil];
    _reachbility= [Reachability reachabilityForInternetConnection];
    [_reachbility startNotifier];
    [self CheckDBData];
    NetworkStatus remoteHostStatus = [_reachbility currentReachabilityStatus];
    _alert=[[UIAlertView alloc]initWithTitle:@"" message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
    
    if(remoteHostStatus==NotReachable)
    { NSLog(@"not Connected");
        _isConnected=NO;
    }
    else    {
        NSLog(@"Connected");
        _isConnected=YES;
    }
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
    
    
    return YES;
}
#pragma mark - Class Methods

/**
 Notification Registration
 */
- (void)registerForRemoteNotification {
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")||SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.1")) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error )
            {
                [[UIApplication sharedApplication] registerForRemoteNotifications];
                [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound |    UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
                [[UIApplication sharedApplication] registerForRemoteNotifications];
                
            }
        }];
    }
    else {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    _isAppBackground=YES;
    //Minimun keepAliveTimeout is 600 seconds
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    
    if (_isAppBackground)
    {
        _isFromBG=YES;
    }
    else
    {
        _isFromBG=NO;
    }
    _isAppBackground=NO;
    _objDBManager=[DBManager getSharedInstance];
    
    NSArray *arrDBData=[_objDBManager selectTableDataWithQuery:@"select * from tblUser"];
    if ([arrDBData count]==0)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ViewController *vc=[storyboard instantiateViewControllerWithIdentifier:@"Loginview"];
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:vc];
        self.window.rootViewController=nav;
    }
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [UIApplication sharedApplication].applicationIconBadgeNumber=0;
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}

#pragma mark: Network Changed checking function
-(void)networkChanged:(NSNotification *)notification
{
    NetworkStatus remoteHostStatus=[_reachbility currentReachabilityStatus];
    if (remoteHostStatus==NotReachable)
    {
        NSLog(@"Not Connected");
        _isConnected=NO;
    }
    
    else
    {
        NSLog(@"Connected");
        _isConnected=YES;
    }
}

- (BOOL)application: (UIApplication *)application
            openURL: (NSURL *)url
  sourceApplication: (NSString *)sourceApplication
         annotation: (id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url                                                                                                     sourceApplication:sourceApplication                                                                                                            annotation:annotation];
}

#pragma mark: Check Data in DB Or Not
-(void)CheckDBData
{
    _objDBManager=[DBManager getSharedInstance];
    
    NSArray *arrDBData=[_objDBManager selectTableDataWithQuery:@"select * from tblUser"];
    if ([arrDBData count])
    {
        _arrUserLoginData=[[arrDBData objectAtIndex:0] mutableCopy];
        NSLog(@"%@",_arrUserLoginData);
        NSString *strCheckPImg=[_arrUserLoginData objectAtIndex:7];
        
        if ([strCheckPImg isEqualToString:@"No"])
        {
            if ([[_arrUserLoginData objectAtIndex:5] isEqualToString:@"NO"])
            {
                
            }
            else
            {
                NSString *strBURL=ImgBase_URL;
                strBURL=[strBURL stringByAppendingString:[_arrUserLoginData objectAtIndex:5]];
                _strProfileImgURL=strBURL;
            }
        }
        else if ([strCheckPImg isEqualToString:@"Yes"])
        {
            _strProfileImgURL=strCheckPImg;
        }
        [self checkInappSubscription];
        
        MainDashboardScreen *dash=[[MainDashboardScreen alloc]initWithNibName:@"MainDashboardScreen" bundle:nil];
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:dash];
        self.window.rootViewController=nav;
    }
    [self.window makeKeyAndVisible];
}

#pragma mark-Get Data From Database
-(void)checkInappSubscription
{
    NSString *selectQuery;
    DBManager *objDBManager=[DBManager getSharedInstance];
    NSMutableArray *arrData=[[NSMutableArray alloc]init];
    NSMutableArray *arrPurchaseDates=[[NSMutableArray alloc]init];
    NSMutableArray *arrExpiryDates=[[NSMutableArray alloc]init];
    
    selectQuery=[NSString stringWithFormat:@"select * from tblInApp where UID='%@'",[_arrUserLoginData objectAtIndex:0]];
    
    NSArray *arrTemp=[objDBManager selectTableDataWithQuery:selectQuery];
    NSLog(@"%@",arrTemp);
    
    if ([arrTemp count])
    {
        for (int i=0; i<[arrTemp count]; i++)
        {
            arrData=[arrTemp objectAtIndex:i];
            NSString *strPurchaseDate=[arrData objectAtIndex:3];
            NSString *strExpiryDate=[arrData objectAtIndex:4];
            
            [arrPurchaseDates addObject:strPurchaseDate];
            [arrExpiryDates addObject:strExpiryDate];
            
            NSDate *currDate = [NSDate date];
            NSDateFormatter *tempDateFormatter = [[NSDateFormatter alloc]init];
            [tempDateFormatter setDateFormat:@"dd-MMM-yy"];
            NSString *strCDate= [tempDateFormatter stringFromDate:currDate];

            if ([strCDate isEqualToString:[arrExpiryDates objectAtIndex:0]]) {
                 [_objDBManager deleteFromTableWithQuery:@"delete from tblInApp"];
                
            }
            else
            {
                _isAlreadyPurchased=YES;
            }
        }
    }
}


#pragma mark- Activity Indicator Methods
-(void)showSpinnerInView:(UIView*)view
{
    [_viewForSpinner removeFromSuperview];
    _viewForSpinner=[[UILabel alloc]initWithFrame:CGRectMake(screenWidth/2-35, screenHeight/2-30, 70, 70)];
    _viewForSpinner.layer.cornerRadius=35.0;
    _viewForSpinner.layer.borderWidth=2;
    _viewForSpinner.layer.masksToBounds=YES;
    _viewForSpinner.layer.borderColor=[UIColor whiteColor].CGColor;
    _viewForSpinner.backgroundColor=[UIColor blackColor];
    
    [view addSubview:_viewForSpinner];
    
    _activityIndicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _activityIndicator.frame=CGRectMake(_viewForSpinner.frame.size.width/2-15,_viewForSpinner.frame.size.height/2-15, 30, 30);
    [_viewForSpinner addSubview:_activityIndicator];
    
    [_activityIndicator startAnimating];
    [[UIApplication sharedApplication]beginIgnoringInteractionEvents];
}

#pragma mark: Stop Spinner
-(void)stopSpinner
{
    [_viewForSpinner removeFromSuperview];
    [[UIApplication sharedApplication]endIgnoringInteractionEvents];
}
-(void)showSilentAlertWithTitle:(NSString*)title description:(NSString*)description inView:(UIView*)inView
{
    [_viewForSilentAlert removeFromSuperview];
    _viewForSilentAlert=[[UIView alloc]initWithFrame:CGRectMake(10,screenHeight/2-75, screenWidth-20, 150)];
}

#pragma mark: show Spinner in view
-(void)showSpinnerInView:(UIView*)view withMsg:(NSString*)msg
{
    [_viewForSpinner removeFromSuperview];
    _viewForSpinner=[[UILabel alloc]initWithFrame:CGRectMake(screenWidth/2-100, screenHeight/2-100,200,150)];
    _viewForSpinner.layer.cornerRadius=35.0;
    _viewForSpinner.layer.borderWidth=2;
    _viewForSpinner.layer.masksToBounds=YES;
    _viewForSpinner.layer.borderColor=[UIColor blackColor].CGColor;
    _viewForSpinner.backgroundColor=[UIColor orangeColor];
    [view addSubview:_viewForSpinner];
    
    _activityIndicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    _activityIndicator.frame=CGRectMake(_viewForSpinner.frame.size.width/2-15,20, 30, 30);
    [_viewForSpinner addSubview:_activityIndicator];
    UITextView *lblMsg=[[UITextView alloc]initWithFrame:CGRectMake(10,60,_viewForSpinner.frame.size.width-20,80)];
    lblMsg.textAlignment=NSTextAlignmentCenter;
    lblMsg.textColor=[UIColor colorWithRed:125.0/255.0 green:177.0/255.0 blue:97.0/255.0 alpha:1];
    lblMsg.text=msg;
    lblMsg.backgroundColor=[UIColor whiteColor];
    lblMsg.font=[UIFont fontWithName:@"Avenir-Light" size:15];
    lblMsg.editable=NO;
    lblMsg.selectable=NO;
    [_viewForSpinner addSubview:lblMsg];
    
    [_activityIndicator startAnimating];
    [[UIApplication sharedApplication]beginIgnoringInteractionEvents];
    
    _timerForSpinnerInterval=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(stopSpinnerAfterWaiting:) userInfo:nil repeats:YES];
    _intCounterValue=0;
}

-(void)stopSpinnerAfterWaiting:(NSTimer*)timer
{
    if (_intCounterValue>=20)
    {
        [self stopSpinner];
        [_timerForSpinnerInterval invalidate];
        _timerForSpinnerInterval=nil;
        _intCounterValue=0;
    }
    _intCounterValue++;
    // NSLog(@"Counter :%ld ",_intCounterValue);
}

#pragma mark - Remote Notification Delegate // <= iOS 9.x

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *strDevicetoken = [[NSString alloc]initWithFormat:@"%@",[[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    NSLog(@"Device Token = %@",strDevicetoken);
    self.strDeviceToken = strDevicetoken;
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier
forRemoteNotification:(NSDictionary *)notification completionHandler:(void(^)())completionHandler
{
    NSLog(@"Received push notification: %@, identifier: %@", notification, identifier); // iOS 8
    completionHandler();
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"Push Notification Information : %@",userInfo);
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"%@ = %@", NSStringFromSelector(_cmd), error);
    NSLog(@"Error = %@",error);
}

#pragma mark - UNUserNotificationCenter Delegate // >= iOS 10

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
        NSLog(@"User Info = %@",notification.request.content.userInfo);
        _arrNotificationData=[[NSMutableArray alloc]init];
        NSDictionary *dictData=[[NSDictionary alloc]init];
        dictData=notification.request.content.userInfo;
        NSLog(@"%@",dictData);
        _arrNotificationData=[dictData valueForKey:@"aps"];
    
        NSString *strType=[_arrNotificationData valueForKey:@"type"];
        NSLog(@"%@",strType);
        [self saveNotificationToDB];
        /*if ([strType isEqualToString:@"snap"])
        {
            SpecifiBetPage *specificBet=[[SpecifiBetPage alloc]initWithNibName:@"SpecifiBetPage" bundle:nil];
            specificBet.strThemeName=[_arrNotificationData valueForKey:@"name"];
            specificBet.selectedBetID1=[_arrNotificationData valueForKey:@"sid"];
            specificBet.strBetImg=[_arrNotificationData valueForKey:@"image"];
            specificBet.strSelectBUID=[_arrUserLoginData objectAtIndex:0];
        
            UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:specificBet];
            self.window.rootViewController=nav;
        }
        else if([strType isEqualToString:@"invite"])
        {
            SpecifiBetPage *specificBet=[[SpecifiBetPage alloc]initWithNibName:@"SpecifiBetPage" bundle:nil];
            specificBet.strThemeName=[_arrNotificationData valueForKey:@"name"];
            specificBet.selectedBetID1=[_arrNotificationData valueForKey:@"sid"];
            specificBet.strBetImg=[_arrNotificationData valueForKey:@"image"];
            specificBet.strSelectBUID=[_arrUserLoginData objectAtIndex:0];
            
            UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:specificBet];
            self.window.rootViewController=nav;
        }
        else if ([strType isEqualToString:@"vote"])
        {
            SpecifiBetPage *specificBet=[[SpecifiBetPage alloc]initWithNibName:@"SpecifiBetPage" bundle:nil];
            specificBet.strThemeName=[_arrNotificationData valueForKey:@"name"];
            specificBet.selectedBetID1=[_arrNotificationData valueForKey:@"sid"];
            specificBet.strBetImg=[_arrNotificationData valueForKey:@"image"];
            specificBet.strSelectBUID=[_arrUserLoginData objectAtIndex:0];
            
            UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:specificBet];
            self.window.rootViewController=nav;
        }
    
        else if ([strType isEqualToString:@"add_fd"])
        {
    
        }
        else if([strType isEqualToString:@"join_snap"])
        {
            SpecifiBetPage *specificBet=[[SpecifiBetPage alloc]initWithNibName:@"SpecifiBetPage" bundle:nil];
            specificBet.strThemeName=[_arrNotificationData valueForKey:@"name"];
            specificBet.selectedBetID1=[_arrNotificationData valueForKey:@"sid"];
            specificBet.strSelectBUID=[_arrUserLoginData objectAtIndex:0];
            
            UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:specificBet];
            self.window.rootViewController=nav;

        }
        else
        {
            SpecifiBetPage *specificBet=[[SpecifiBetPage alloc]initWithNibName:@"SpecifiBetPage" bundle:nil];
            specificBet.strThemeName=[_arrNotificationData valueForKey:@"name"];
            specificBet.selectedBetID1=[_arrNotificationData valueForKey:@"sid"];
            specificBet.strBetImg=[_arrNotificationData valueForKey:@"image"];
            specificBet.strSelectBUID=[_arrUserLoginData objectAtIndex:0];
            
            UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:specificBet];
            self.window.rootViewController=nav;
        }*/
        completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler
{
    NSLog(@"User Info = %@",response.notification.request.content.userInfo);
    
    _arrNotificationData=[[NSMutableArray alloc]init];
    NSDictionary *dictData=[[NSDictionary alloc]init];
    dictData=response.notification.request.content.userInfo;
    NSLog(@"%@",dictData);
    _arrNotificationData=[dictData valueForKey:@"aps"];
    
    NSString *strType=[_arrNotificationData valueForKey:@"type"];
    
    if (_isFromBG)
    {
        [self saveNotificationToDB];
    }
    if ([strType isEqualToString:@"snap"])
    {
        SpecifiBetPage *specificBet=[[SpecifiBetPage alloc]initWithNibName:@"SpecifiBetPage" bundle:nil];
        _isLodedFromAddDelegate=YES;
        specificBet.strThemeName=[_arrNotificationData valueForKey:@"name"];
        specificBet.selectedBetID1=[_arrNotificationData valueForKey:@"sid"];
        specificBet.strBetImg=[_arrNotificationData valueForKey:@"image"];
        specificBet.strSelectBUID=[_arrUserLoginData objectAtIndex:0];
        
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:specificBet];
        self.window.rootViewController=nav;
    }
    else if([strType isEqualToString:@"invite"])
    {
        
        SpecifiBetPage *specificBet=[[SpecifiBetPage alloc]initWithNibName:@"SpecifiBetPage" bundle:nil];
        _isLodedFromAddDelegate=YES;
        specificBet.strThemeName=[_arrNotificationData valueForKey:@"name"];
        specificBet.selectedBetID1=[_arrNotificationData valueForKey:@"sid"];
        specificBet.strBetImg=[_arrNotificationData valueForKey:@"image"];
        specificBet.strSelectBUID=[_arrUserLoginData objectAtIndex:0];
        
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:specificBet];
        self.window.rootViewController=nav;
    }
    else if ([strType isEqualToString:@"vote"])
    {
        SpecifiBetPage *specificBet=[[SpecifiBetPage alloc]initWithNibName:@"SpecifiBetPage" bundle:nil];
        _isLodedFromAddDelegate=YES;
        specificBet.strThemeName=[_arrNotificationData valueForKey:@"name"];
        specificBet.selectedBetID1=[_arrNotificationData valueForKey:@"sid"];
        specificBet.strBetImg=[_arrNotificationData valueForKey:@"image"];
        specificBet.strSelectBUID=[_arrUserLoginData objectAtIndex:0];
        
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:specificBet];
        self.window.rootViewController=nav;
    }
    
    else if ([strType isEqualToString:@"add_fd"])
    {
        
    }
    else if([strType isEqualToString:@"join_snap"])
    {
        SpecifiBetPage *specificBet=[[SpecifiBetPage alloc]initWithNibName:@"SpecifiBetPage" bundle:nil];
        _isLodedFromAddDelegate=YES;
        specificBet.strThemeName=[_arrNotificationData valueForKey:@"name"];
        specificBet.selectedBetID1=[_arrNotificationData valueForKey:@"sid"];
        specificBet.strSelectBUID=[_arrUserLoginData objectAtIndex:0];
        
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:specificBet];
        self.window.rootViewController=nav;
    }
    else
    {
        SpecifiBetPage *specificBet=[[SpecifiBetPage alloc]initWithNibName:@"SpecifiBetPage" bundle:nil];
        _isLodedFromAddDelegate=YES;
        specificBet.strThemeName=[_arrNotificationData valueForKey:@"name"];
        specificBet.selectedBetID1=[_arrNotificationData valueForKey:@"sid"];
        specificBet.strBetImg=[_arrNotificationData valueForKey:@"image"];
        specificBet.strSelectBUID=[_arrUserLoginData objectAtIndex:0];
        
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:specificBet];
        self.window.rootViewController=nav;
    }

    completionHandler();
}

-(void)saveNotificationToDB
{
    _objDBManager=[DBManager getSharedInstance];
    NSString *strUID,*strSID,*strName,*strType,*strDetails,*strImg,*strBGImg,*strAlert;
    NSLog(@"%@",_arrUserLoginData);
    strUID=[_arrUserLoginData objectAtIndex:0];
    strSID=[_arrNotificationData valueForKey:@"sid"];
    strName=[_arrNotificationData valueForKey:@"name"];
    strType=[_arrNotificationData valueForKey:@"type"];
    strDetails=[_arrNotificationData valueForKey:@"details"];
    strBGImg=[_arrNotificationData valueForKey:@"bgimage"];
    strImg=[_arrNotificationData valueForKey:@"image"];
    strAlert=[_arrNotificationData valueForKey:@"alert"];
    
    if ([strSID isKindOfClass:[NSNull class]] || strSID == nil)
    {
        strSID=@"";
    }
    if ([strName isKindOfClass:[NSNull class]] || strName==nil) {
        strName=@"";
    }
    if ([strType isKindOfClass:[NSNull class]] || strType==nil) {
        strType=@"";
    }
    if ([strDetails isKindOfClass:[NSNull class]] || strDetails==nil) {
        strDetails=@"";
    }
    if ([strBGImg isKindOfClass:[NSNull class]] || strBGImg==nil) {
        strBGImg=@"";
    }
    if ([strImg isKindOfClass:[NSNull class]] || strImg==nil) {
        strImg=@"";
    }
    if ([strAlert isKindOfClass:[NSNull class]] || strAlert==nil) {
        
        strAlert=@"";
    }
    
     NSMutableArray *arrTempStorage;
    
     NSArray *arrDBData=[_objDBManager selectTableDataWithQuery:@"select * from tblNotifications"];
    
    arrTempStorage=[arrDBData mutableCopy];

    if (arrDBData.count)
    {
        
        NSLog(@"Database Count %lu",(unsigned long)arrDBData.count);
        NSString *strDelQuery=[NSString stringWithFormat:@"delete from tblNotifications"];
        [_objDBManager deleteFromTableWithQuery:strDelQuery];
    }
    
    [theAppDelegate.arrUserLoginData addObject:strImg];
    NSString *queryString=[NSString stringWithFormat:@"insert into tblNotifications values('%@','%@','%@','%@','%@','%@','%@','%@')",strUID,strSID,strName,strType,strDetails,strBGImg,strAlert,strImg];
    
    if ([_objDBManager insertDataWithQuery:queryString])
    {
        
    }
    else
    {
        
    }

    for (int aa=0;aa<[arrTempStorage count];aa++)
    {
         NSString *strUID1,*strSID1,*strName1,*strType1,*strDetails1,*strImg1,*strBGImg1,*strAlert1;
        
        strUID1=[[arrTempStorage objectAtIndex:aa]objectAtIndex:0];
        strSID1=[[arrTempStorage objectAtIndex:aa]objectAtIndex:1];
        strName1=[[arrTempStorage objectAtIndex:aa]objectAtIndex:2];
        strType1=[[arrTempStorage objectAtIndex:aa]objectAtIndex:3];
        strDetails1=[[arrTempStorage objectAtIndex:aa]objectAtIndex:4];
        strImg1=[[arrTempStorage objectAtIndex:aa]objectAtIndex:5];
        strBGImg1=[[arrTempStorage objectAtIndex:aa]objectAtIndex:6];
        strAlert1=[[arrTempStorage objectAtIndex:aa]objectAtIndex:7];
        
        NSString *strInsertQuery=[NSString stringWithFormat:@"insert into tblNotifications values('%@','%@','%@','%@','%@','%@','%@','%@')",strUID1,strSID1,strName1,strType1,strDetails1,strImg1,strBGImg1,strAlert1];
        NSLog(@"%@",strInsertQuery);
        [_objDBManager insertDataWithQuery:strInsertQuery];
    }
}

#pragma mark create all table's
-(void)createAllTablesIfNotExist
{
    //--------------------------DataBase handling Coding-------------------//
    _objDBManager=[DBManager getSharedInstance];
    NSString *sql_stmt;
    
    sql_stmt=@"create table if not exists tblUser(UID TEXT primary key,EMAIL text,FNAME text,LNAME text,CONTACT text,IMAGE text,DID text,ISFBLOGIN text)";
    
    if (![_objDBManager createTableWithQuery:sql_stmt])
    {
        //  NSLog(@"error while creating table User");
    }
    
    sql_stmt=@"create table if not exists tblInApp(UID TEXT, PRODUCT_ID TEXT,PRODUCT_NAME text,PURCHASE_DATE text,EXPIRY DATE text)";
    //    sql_stmt=@"drop table tblInApp";
    
    if (![_objDBManager createTableWithQuery:sql_stmt])
    {
        //  NSLog(@"error while creating table User");
    }
    
    sql_stmt=@"create table if not exists tblInAppThemePurchase(UID TEXT, PRODUCT_ID TEXT,PRODUCT_NAME text,PURCHASE_DATE text,EXPIRY DATE text,ThemeID text)";
    //    sql_stmt=@"drop table tblInApp";
    
    if (![_objDBManager createTableWithQuery:sql_stmt])
    {
        //  NSLog(@"error while creating table User");
    }
    
    sql_stmt=@"create table if not exists tblThemeInApp(UID TEXT, PRODUCT_ID TEXT,THEME_ID TEXT ,PRODUCT_NAME text,PURCHASE_DATE text,EXPIRY DATE text)";
    //    sql_stmt=@"drop table tblInApp";
    
    if (![_objDBManager createTableWithQuery:sql_stmt])
    {
        //  NSLog(@"error while creating table User");
    }
    
    sql_stmt=@"create table if not exists tblNotifications(UID text,SID text,NAME text, TYPE text,DETAILS text,BGIMAGE text,ALERT text,IMAGE text)";
    //    sql_stmt=@"drop table tblInApp";
    
    if (![_objDBManager createTableWithQuery:sql_stmt])
    {
        //  NSLog(@"error while creating table User");
    }
}
@end
