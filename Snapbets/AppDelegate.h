
//
//  AppDelegate.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 06/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Reachability.h"
#import "HeaderAndConstants.h"
#import "DBManager.h"
#import <UserNotifications/UserNotifications.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>

@property (nonatomic)NSTimeInterval durationOfOrignalVideo,GlobDurOfOrignalVideo;
@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property(nonatomic)Reachability *reachbility;
@property (nonatomic)DBManager *objDBManager;
@property (nonatomic)NSMutableArray *arrUserLoginData,*arrNotificationData;

#pragma mark: BOOL Properties declarations here
@property(nonatomic)BOOL isConnected,isProfFromMenu,isMyBeetSelected,isSearch,isSend,isFirstTime,isStartFromAppDelegate,isPoolRefresh,isAlreadyPurchased,isFromSearchProcess,isUserBlocked,isAppBackground,isNotiArrival;

#pragma mark: UIImageView forFacebook Image
@property(nonatomic)NSData *imgData;

#pragma mark: UIAlertView properties declarations here
@property(nonatomic)UIAlertView *alert;

#pragma mark: NSString properties declarations here
@property(strong,nonatomic)NSString *strDeviceToken,*strProfileImgURL;
@property (nonatomic) NSString *strDeviceID,*strVideoName,*strUplodedImg,*strSelectedThmeID,*strInAppPurchase,*strVTitle,*strVDescription,*strVideoURL;
@property (nonatomic)NSString *strTrimingTime;

#pragma mark- Activitity Indicator Properties
@property (nonatomic)UIView *viewForSpinner,*viewForSilentAlert;
@property (nonatomic)UIActivityIndicatorView *activityIndicator;
@property (nonatomic)NSTimer *timerForSpinnerInterval;
@property(nonatomic)NSInteger intCounterValue;

@property (nonatomic)BOOL isLodedFromAddDelegate,isFromBG;
#pragma mark: Methods Declarations Here
-(void)showSpinnerInView:(UIView*)view;
-(void)stopSpinner;
@end

