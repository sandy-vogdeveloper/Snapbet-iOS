//
//  SignInScreen.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 07/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "SignInScreen.h"

@interface SignInScreen ()

@end

@implementation SignInScreen

- (void)viewDidLoad {
    [super viewDidLoad];
   
    /*----  Load Initial UI ------*/
        [self loadMainUI];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark: Load InitialUI
-(void)loadMainUI
{
    /*-----------  Button For Menu Selection for SidebarViewController-----------*/
    
    /*-----------  Button For Menu Selection for SidebarViewController-----------*/
    _btnBack=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBack.frame = CGRectMake(5,8,25,25);
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"iconBackArrow"] forState:UIControlStateNormal];

    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    /*----------- Button Pressed Event Fired and goto SidebarViewController--------*/
    _btnBackPress=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBackPress.frame = CGRectMake(0,0,105,55);
    [_btnBackPress addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnBack];
    [self.view addSubview:_btnBackPress];
    
    UIView *viewForMail;
    UIView *viewForPassword;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
         _imgTrangleBG=[[UIImageView alloc]initWithFrame:CGRectMake(40,60,screenWidth-80,240)];
        viewForMail=[[UIView alloc]initWithFrame:CGRectMake(60,90,screenWidth-120,40)];
        viewForPassword=[[UIView alloc]initWithFrame:CGRectMake(60,CGRectGetMaxY(viewForMail.frame)+20,screenWidth-120,40)];
        _btnLogin=[[UIButton alloc]initWithFrame:CGRectMake(60,CGRectGetMaxY(viewForPassword.frame)+20,screenWidth-120,40)];
    }
    else
    {
         _imgTrangleBG=[[UIImageView alloc]initWithFrame:CGRectMake(10,60,screenWidth-20,240)];
        viewForMail=[[UIView alloc]initWithFrame:CGRectMake(20,90,screenWidth-40,40)];
        viewForPassword=[[UIView alloc]initWithFrame:CGRectMake(20,CGRectGetMaxY(viewForMail.frame)+20,screenWidth-40,40)];
        _btnLogin=[[UIButton alloc]initWithFrame:CGRectMake(20,CGRectGetMaxY(viewForPassword.frame)+20,screenWidth-40,40)];
    }
   
    _imgTrangleBG.image=[UIImage imageNamed:@"img_Trangle"];
    [self.view addSubview:_imgTrangleBG];
    
    
    [viewForMail setBackgroundColor:[UIColor whiteColor]];
    [viewForPassword setBackgroundColor:[UIColor whiteColor]];
    
    viewForMail.layer.cornerRadius=4.0;
    viewForPassword.layer.cornerRadius=4.0;
    
    [self.view addSubview:viewForMail];
    [self.view addSubview:viewForPassword];
    
    _txtEmail=[[UITextField alloc]initWithFrame:CGRectMake(10,5,viewForMail.frame.size.width-10,30)];
    _txtPassword=[[UITextField alloc]initWithFrame:CGRectMake(10,5,viewForPassword.frame.size.width-10,30)];
    [viewForMail addSubview:_txtEmail];
    [viewForPassword addSubview:_txtPassword];
    
    [self getTextFieldWithProperties:_txtEmail placeholder:@"Email" forLogin:YES];
    [self getTextFieldWithProperties:_txtPassword placeholder:@"Password" forLogin:YES];
    
    [_btnLogin addTarget:self action:@selector(btnLoginPressed:) forControlEvents:UIControlEventTouchUpInside];
    _btnLogin.backgroundColor=[UIColor blackColor];
    _btnLogin.layer.cornerRadius=4.0;
    [self.view addSubview:_btnLogin];
    
    UILabel *lblLogin=[[UILabel alloc]initWithFrame:CGRectMake(10,0,_btnLogin.frame.size.width-10,40)];
    lblLogin.text=@"Login";
    lblLogin.textColor=[UIColor whiteColor];
    lblLogin.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
    [_btnLogin addSubview:lblLogin];
    
    UIImageView *iconSignInFrwd=[[UIImageView alloc]initWithFrame:CGRectMake(_btnLogin.frame.size.width-30,10,20,20)];
    iconSignInFrwd.image=[UIImage imageNamed:@"icon_forward"];
    [_btnLogin addSubview:iconSignInFrwd];
    
    _btnFrgtPassword=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth/2-70,CGRectGetMaxY(_btnLogin.frame)+10,140,30)];
    [_btnFrgtPassword setTitle:@"Forgot Password!" forState:UIControlStateNormal];
    [_btnFrgtPassword setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _btnFrgtPassword.titleLabel.font=[UIFont fontWithName:@"Avenir-HeavyOblique" size:16];
    [_btnFrgtPassword addTarget:self action:@selector(btnForgetPasswordPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnFrgtPassword];
    
    
    viewForMail.alpha = 0.0f;
    viewForMail.transform = CGAffineTransformMakeScale(0.1,0.1);
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:1.0];
    viewForMail.transform = CGAffineTransformMakeScale(1,1);
    viewForMail.alpha = 1.0f;
    [UIView commitAnimations];
    
    viewForPassword.alpha = 0.0f;
    viewForPassword.transform = CGAffineTransformMakeScale(0.1,0.1);
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:1.0];
    viewForPassword.transform = CGAffineTransformMakeScale(1,1);
    viewForPassword.alpha = 1.0f;
    [UIView commitAnimations];
    
    _txtEmail.alpha = 0.0f;
    _txtEmail.transform = CGAffineTransformMakeScale(0.1,0.1);
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:1.0];
    _txtEmail.transform = CGAffineTransformMakeScale(1,1);
    _txtEmail.alpha = 1.0f;
    [UIView commitAnimations];
    
    _txtPassword.alpha = 0.0f;
    _txtPassword.transform = CGAffineTransformMakeScale(0.1,0.1);
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:1.0];
    _txtPassword.transform = CGAffineTransformMakeScale(1,1);
    _txtPassword.alpha = 1.0f;
    [UIView commitAnimations];
    
    _btnLogin.alpha = 0.0f;
    _btnLogin.transform = CGAffineTransformMakeScale(0.1,0.1);
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:1.0];
    _btnLogin.transform = CGAffineTransformMakeScale(1,1);
    _btnLogin.alpha = 1.0f;
    [UIView commitAnimations];
    
}


#pragma mark: Set Proerties for TextFields
-(UITextField*)getTextFieldWithProperties:(UITextField*)textField placeholder:(NSString*)placeholder forLogin:(BOOL)forLogin
{
    //------------- Add properties to textfield ------------- //
    //  textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.placeholder =placeholder;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.keyboardType=UIKeyboardAppearanceDefault;
    
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.keyboardAppearance=UIKeyboardAppearanceLight;
    [textField addTarget:self action:@selector(resignTxt:) forControlEvents:UIControlEventEditingDidEndOnExit];
    _txtEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    _txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    _txtEmail.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
    _txtPassword.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
    
    _txtEmail.textAlignment=NSTextAlignmentLeft;
    _txtPassword.textAlignment=NSTextAlignmentLeft;
    
    _txtEmail.textColor=[UIColor blackColor];
    _txtPassword.textColor=[UIColor blackColor];
    
    //----------- Validate return keys -------------------- //
    if (forLogin && textField==_txtPassword)
    {        _txtPassword.returnKeyType=UIReturnKeyDone;      }
    
    else if (forLogin && textField ==_txtEmail)
    {        _txtEmail.returnKeyType=UIReturnKeyNext;    }
    
    //----------- Validate secure key entry for password field ----//
    
    if (textField==_txtPassword)
    {        textField.secureTextEntry=YES;    }
    
    //---------- Validate keyboard type for email address --------//
    
    if (textField==_txtEmail)
    {        textField.keyboardType=UIKeyboardTypeEmailAddress;    }
    
    return textField;//------- Return fully updated textfield
}

#pragma mark- TextField DelegateMethods
-(void)resignTxt:(UITextField*)txt
{
    //----------- Validate textfield return action ------ //
    if (txt==_txtEmail)
    {   [_txtPassword becomeFirstResponder];
    }
    else if(txt==_txtPassword)
    {
        
    }
    else
    {   [txt resignFirstResponder];    }
    //------------------------------------------------ //
}

#pragma mark: TextField Validation Here
-(BOOL)isValidTextfield
{
    NSString *strMsg;
    if(_txtEmail.text!=nil && _txtEmail.text.length>0)
    {
        if (_txtPassword.text!=nil && _txtPassword.text.length>0)
        {
            return YES;
        }
        else
        {
            strMsg=NSLocalizedString(@"Please enter Password.",nil);
        }
    }
    else
    {
        strMsg=NSLocalizedString(@"Enter the details.",nil);
    }
    
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Warning"  message:strMsg  preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    return NO;
}

#pragma mark: UIButton pressed delegate declaration here
-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnLoginPressed:(id)sender
{
        if ([self isValidTextfield]) {
       [self LoginServerCall];
    }
}
-(IBAction)btnForgetPasswordPressed:(id)sender
{
    ForgetPasswordScreen *frg=[[ForgetPasswordScreen alloc]initWithNibName:@"ForgetPasswordScreen" bundle:nil];
    [self.navigationController pushViewController:frg animated:YES];
}
#pragma mark: User SignIn Server Call
-(void)LoginServerCall
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        if (theAppDelegate.strDeviceToken.length==0)
        {
            theAppDelegate.strDeviceToken=theAppDelegate.strDeviceID;
        }
    [theAppDelegate showSpinnerInView:self.view];
    [self.view setUserInteractionEnabled:NO];
    NSString *urlString=base_URL;
        
    NSString *strURL=[NSString stringWithFormat:@"wslogin.php?email=%@&password=%@&did=%@&dtype=I",_txtEmail.text,_txtPassword.text,theAppDelegate.strDeviceToken];
    urlString=[urlString stringByAppendingString:strURL];
    NSURL *urlSignIn=[NSURL URLWithString:urlString];
    request=[NSMutableURLRequest requestWithURL:[urlSignIn standardizedURL]];
    
    if(!connection)
        connection= [[WebConnection1 alloc] init];
    connection.delegate = self;
    [connection makeConnection:request];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"The Internet connection appears to be offline" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];

    }
    request = nil;
}

#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    [theAppDelegate stopSpinner];
    [self.view setUserInteractionEnabled:YES];
    // [theAppDelegate stopSpinner];
    
    dict =[[NSMutableDictionary alloc]initWithDictionary:response];
    NSString *strMsg=[dict valueForKey:@"message"];
    NSLog(@"%@",strMsg);
    if ([strMsg isEqualToString:@"sus"])
    {
       _arrLoginInfo=[dict valueForKey:@"info"];
        NSString *strBlock=[_arrLoginInfo valueForKey:@"block"];
        
        if ([strBlock isEqualToString:@"N"])
        {
            [self saveDatatoDatabase];
            
            MainDashboardScreen *mainDash=[[MainDashboardScreen alloc]initWithNibName:@"MainDashboardScreen" bundle:nil];
            [self.navigationController pushViewController:mainDash animated:YES];

        }
        else
        {
            [self performSelector:@selector(viewForBlockedAlertUI) withObject:nil afterDelay:0.5];
        }
        
    }
    else
    {
        _strAlertTitle=@"Invalid login details";
        [self viewForAlertUI];
    }
    _txtEmail.text=@"";
    _txtPassword.text=@"";
}

#pragma mark: Save data to DB
-(void)saveDatatoDatabase
{
    NSArray *arrDBTemp=[[NSMutableArray alloc]init];
    theAppDelegate.arrUserLoginData=[[NSMutableArray alloc]init];
    objDBManager=[DBManager getSharedInstance];
    arrDBTemp=[objDBManager selectTableDataWithQuery:@"select *from tblUser"];
    
    if ([arrDBTemp count])
    {
        [objDBManager deleteFromTableWithQuery:@"delete from tblUser"];
    }
    
    NSString *strUID,*strEmail,*strFName,*strLName,*strContact,*strImg,*strFB;

        strUID=[_arrLoginInfo valueForKey:@"id"];
        strEmail=[_arrLoginInfo valueForKey:@"email"];
        strFName=[_arrLoginInfo valueForKey:@"fname"];
        strLName=[_arrLoginInfo valueForKey:@"lname"];
        strContact=[_arrLoginInfo valueForKey:@"contact"];
        strImg=[_arrLoginInfo valueForKey:@"image"];
        strFB=@"No";
    
    [theAppDelegate.arrUserLoginData addObject:strUID];
    [theAppDelegate.arrUserLoginData addObject:strEmail];
    [theAppDelegate.arrUserLoginData addObject:strFName];
    [theAppDelegate.arrUserLoginData addObject:strLName];
    [theAppDelegate.arrUserLoginData addObject:strContact];
    [theAppDelegate.arrUserLoginData addObject:strImg];
    [theAppDelegate.arrUserLoginData addObject:theAppDelegate.strDeviceToken];
    [theAppDelegate.arrUserLoginData addObject:strFB];
    
    NSLog(@"%@",theAppDelegate.arrUserLoginData);
    
    NSString *strBaseURL;
    if([strImg isEqualToString:@"NO"])
    {
            strBaseURL=@"NO";
    }
    else
    {
        strBaseURL=ImgBase_URL;
        strBaseURL=[strBaseURL stringByAppendingString:strImg];
    }

    [theAppDelegate.arrUserLoginData addObject:strImg];
    NSString *queryString=[NSString stringWithFormat:@"insert into tblUser values('%@','%@','%@','%@','%@','%@','%@','%@')",strUID,strEmail,strFName,strLName,strContact,strBaseURL,theAppDelegate.strDeviceToken,strFB];
    
    if ([objDBManager insertDataWithQuery:queryString])
    {
        
    }
    else
    {
        
    }
}

#pragma mark: view For Alert
-(void)viewForAlertUI
{
    [_viewForAlert removeFromSuperview];
    _viewForAlert =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForAlert.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForAlert.tag=20;
    [self.view addSubview:_viewForAlert];
    
    CGRect frameForViewForHelp=_viewForAlert.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForAlert.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForAlert.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForAlert addSubview:img];
        UILabel *lblAlert=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForAlert.frame.size.width,30)];
        lblAlert.text=_strAlertTitle;
        lblAlert.textAlignment=NSTextAlignmentCenter;
        lblAlert.textColor=[UIColor whiteColor];
        lblAlert.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForAlert addSubview:lblAlert];
        
        _btnCloseAlert=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseAlert.frame=CGRectMake(_viewForAlert.frame.size.width/2-60,CGRectGetMaxY(lblAlert.frame)+50,120,40);
        }
        else
        {
            _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,CGRectGetMaxY(lblAlert.frame)+50,100,40)];
        }
        
        [_btnCloseAlert setTitle:@"Close" forState:UIControlStateNormal];
        
        
        [_btnCloseAlert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseAlert.layer.borderWidth=2.0;
        _btnCloseAlert.layer.cornerRadius=4.0;
        _btnCloseAlert.layer.borderColor=[[UIColor orangeColor]CGColor];
        //[_btnVoteSeeMore addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [_btnCloseAlert addTarget:self action:@selector(viewForCloseAlert) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewForAlert addSubview:_btnCloseAlert];
    }];
}

#pragma mark-Close Comment View
-(void)viewForCloseAlert
{
    @try
    {
        for(UIView *subview in [_viewForAlert subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForVote=_viewForAlert.frame;
        frameForViewForVote=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            _viewForAlert.frame=frameForViewForVote;
        } completion:^(BOOL finished){
            
            [_viewForAlert removeFromSuperview];
            
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}


#pragma mark: view For Block User Alert
-(void)viewForBlockedAlertUI
{
    [_viewForBlockedAlert removeFromSuperview];
    _viewForBlockedAlert =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForBlockedAlert.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForBlockedAlert.tag=20;
    [self.view addSubview:_viewForBlockedAlert];
    
    CGRect frameForViewForHelp=_viewForBlockedAlert.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForBlockedAlert.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForBlockedAlert.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForBlockedAlert.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForBlockedAlert addSubview:img];
        
        UILabel *lblAlert=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForBlockedAlert.frame.size.width,30)];
        lblAlert.text=@"Your account has blocked";
        lblAlert.textAlignment=NSTextAlignmentCenter;
        lblAlert.textColor=[UIColor whiteColor];
        lblAlert.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:18];
        //[lblAlert sizeToFit];
        [_viewForBlockedAlert addSubview:lblAlert];
        
        UILabel *lblAlert1=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblAlert.frame)+5,_viewForBlockedAlert.frame.size.width,30)];
        lblAlert1.text=@"Please contact to admin";
        lblAlert1.textAlignment=NSTextAlignmentCenter;
        lblAlert1.textColor=[UIColor whiteColor];
        lblAlert1.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        //[lblAlert1 sizeToFit];
        [_viewForBlockedAlert addSubview:lblAlert1];
        
        _btnCloseAlert=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseAlert.frame=CGRectMake(_viewForBlockedAlert.frame.size.width/2-60,CGRectGetMaxY(lblAlert1.frame)+50,120,40);
        }
        else
        {
            _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForBlockedAlert.frame.size.width/2-50,CGRectGetMaxY(lblAlert1.frame)+50,100,40)];
        }
        
        [_btnCloseAlert setTitle:@"Close" forState:UIControlStateNormal];
        
        
        [_btnCloseAlert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseAlert.layer.borderWidth=2.0;
        _btnCloseAlert.layer.cornerRadius=4.0;
        _btnCloseAlert.layer.borderColor=[[UIColor orangeColor]CGColor];
        //[_btnVoteSeeMore addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [_btnCloseAlert addTarget:self action:@selector(viewForCloseAlert1) forControlEvents:UIControlEventTouchUpInside];
        [_viewForBlockedAlert addSubview:_btnCloseAlert];
    }];
}

#pragma mark-Close Comment View
-(void)viewForCloseAlert1
{
    @try
    {
        for(UIView *subview in [_viewForBlockedAlert subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForVote=_viewForBlockedAlert.frame;
        frameForViewForVote=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            _viewForBlockedAlert.frame=frameForViewForVote;
        } completion:^(BOOL finished){
            
            [_viewForBlockedAlert removeFromSuperview];
            
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}
@end
