//
//  SignInScreen.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 07/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderAndConstants.h"
#import "WebConnection1.h"
#import "MainDashboardScreen.h"
#import "ForgetPasswordScreen.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "DBManager.h"

@interface SignInScreen : UIViewController<WebRequestResult1,NSURLConnectionDelegate,NSURLConnectionDataDelegate>
{
    WebConnection1 *connection;
    NSMutableDictionary *dict;
    DBManager *objDBManager;
    CGRect vscreenSize;
    CGFloat vscreenHeight,vscreenWidth;
}

#pragma mark: UIView properties declarations here
@property (nonatomic)IBOutlet UIView *viewForAlert,*viewForBlockedAlert;

#pragma mark: UIButton Properties dclarations here
@property(nonatomic)IBOutlet UIButton *btnBack,*btnBackPress,*btnLogin,*btnFrgtPassword,*btnCloseAlert;

#pragma mark:UIImageView properties declaratiosn here
@property (nonatomic)IBOutlet UIImageView *imgTrangleBG;

#pragma mark:UITextField Properties declarations here
@property (nonatomic)IBOutlet UITextField *txtEmail,*txtPassword;

@property (nonatomic) NSMutableArray *arrLoginInfo;

@property (nonatomic)NSString *strAlertTitle;

@end
