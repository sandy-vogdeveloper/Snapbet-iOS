
#ifndef Snapbets_HeaderAndConstants_h
#define Snapbets_HeaderAndConstants_h

//------------ Screen Size ------------------------ //
#define screenSize [UIScreen mainScreen].bounds
#define screenWidth screenSize.size.width
#define screenHeight screenSize.size.height

//----------------- Class Shared Instance ----------//
#define theAppDelegate  ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define theVNRequestOperations  [VNRequestOperation sharedInstance]

//-----------------    Font Declarations Here  ----------//

//------------------ Color Codes ------------------ //
#define vnColorHeader [UIColor colorWithRed:67.0/255.0 green:120.0/255.0 blue:177.0/255.0 alpha:1];

//------------- Messages -------------//
#define connectionErrorMsg @"The Internet connection to be offline."

//----- Service Request Strings ------//
#define GET_Method @"GET"
#define POST_Method @"POST"
#define base_URL @"http://54.67.95.152/snapbets/aws/"
#define vBase_URL @"http://54.67.95.152/snapbets/video/"
#define ImgBase_URL @"http://54.67.95.152/snapbets/image/"

#endif
