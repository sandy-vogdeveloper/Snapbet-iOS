//
//  SpecificThemePackgScreen.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 10/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderAndConstants.h"
#import "WebConnection1.h"
#import "AsyncImageView.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "MBProgressHUD.h"
#import "IAPHelper.h"
#import "InAppRageIAPHelper.h"
#import "DBManager.h"

@interface SpecificThemePackgScreen : UIViewController<AVPlayerViewControllerDelegate,UITableViewDelegate,UITableViewDataSource,WebRequestResult1,UIScrollViewDelegate,UIGestureRecognizerDelegate>
{
    CGRect vscreenSize;
    CGFloat vscreenHeight,vscreenWidth;
    WebConnection1 *connection;
    NSMutableDictionary *dict;
    NSInteger selectedVideoIndex;
    NSUInteger singleSelectionID;
    MBProgressHUD *hud;
    int productId;
    NSInteger wordCount;
    UIFont *fnt;
    DBManager *objDBManager;
}

@property (nonatomic) AVPlayerViewController *playerViewController;

#pragma mark: UIView properties declarations here
@property (nonatomic)IBOutlet UIView *viewForBuySingleTheme,*viewForBuyAll,*viewForInvite,*viewForPostResponse,*viewForReport,*viewForUpgradeAccount,*viewForVideoScreen,*viewForVideoArea,*viewForAlert;

#pragma mark: UITableView properties declarations here
@property(nonatomic)IBOutlet UITableView *tblChallenges;

#pragma mark: UIImageView properties declaratios here
@property(nonatomic)IBOutlet UIImageView *imgHeaderBG,*imgHeadingTitle,*imgFuture;

#pragma mark: UILabel Properties declarations here
@property(nonatomic)IBOutlet UILabel *lblChallenge,*lblHedgTitle;

#pragma mark: UIButton properties declarations here
@property(nonatomic)IBOutlet UIButton *btnPlay,*btnBuySingle,*btnBack,*btnBackPress,*btnSBuySnpbet,*btnClseBuySnpbet,*btnBuyAll,*btnViewInvite,*btnCloseInvite,*btnClsoePostRespnse,*btnCloseUpgradeUI,*btnUpgrade,*btnCloseVideoScreen,*btnCloseAlert;

#pragma mark: NSMutableArray properties declarations here
@property (nonatomic)NSMutableArray *arrTblData,*arrVideoURL;

@property(nonatomic) AsyncImageView *asynchronusImage,*asynchronusThemeImg,*asynchronusImgHeaderBG,*asySelectedBG;

#pragma mark: NSString properties declarations here
@property (nonatomic) NSString *strAlertTitle;
@property(nonatomic)NSString *strSelectedThmeID,*strThName,*strTDesc,*strTImage,*strIAPStatus;

#pragma mark: Bool Properties declarations here
@property (nonatomic)BOOL ispurchaseList,isSinglePurchase,isBuyAllList;

#pragma mark:-ScrollView Declaration here
@property(strong,nonatomic)UIView *viewForcontent,*viewForcontent1;
@property(strong,nonatomic)UIScrollView *scrollView,*scrollView1;
@property(strong,nonatomic)UIView *viewContent,*viewContent1;
@end
