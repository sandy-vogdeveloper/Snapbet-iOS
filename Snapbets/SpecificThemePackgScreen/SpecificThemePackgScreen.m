//
//  SpecificThemePackgScreen.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 10/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "SpecificThemePackgScreen.h"

@interface SpecificThemePackgScreen ()

@end

@implementation SpecificThemePackgScreen

- (void)viewDidLoad
{
    [super viewDidLoad];
    _arrVideoURL=[[NSMutableArray alloc]init];
    _arrTblData=[[NSMutableArray alloc]init];
    SKPaymentTransaction * transaction;
    NSLog(@"%ld",(long)transaction.transactionState);
    if([_strIAPStatus isEqualToString:@"free"])
    {
        
    }
    else
    {
        if (theAppDelegate.isAlreadyPurchased) {
        
        }
        else
        {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productsLoaded:) name:kProductsLoadedNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:kProductPurchasedNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(productPurchaseFailed:) name:kProductPurchaseFailedNotification object: nil];
        
            if ([InAppRageIAPHelper sharedHelper].products == nil) {
            
                [[InAppRageIAPHelper sharedHelper] requestProducts];
                hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.labelText =@"Loading";
                [self performSelector:@selector(timeout:) withObject:nil afterDelay:30.0];
            }
        }
    }
    
    [self loadInitialUI];
    [self GetThemeDetailsAPI];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark: UILoad InitialUI
-(void)loadInitialUI
{
    _btnBack=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        _asynchronusImgHeaderBG=[[AsyncImageView alloc] initWithFrame:CGRectMake(0,0,screenWidth,250)];
    }
    else
    {
        _asynchronusImgHeaderBG=[[AsyncImageView alloc] initWithFrame:CGRectMake(0,0,screenWidth,200)];
    }
    _asynchronusImgHeaderBG.backgroundColor = [UIColor clearColor];
    NSString *imgUrlString1 =_strTImage;
    
    NSString *webStr = [NSString stringWithFormat:@"%@",imgUrlString1];
    webStr=[webStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *imageUrl = [[NSURL alloc] initWithString:webStr];
    [_asynchronusImgHeaderBG setContentMode:UIViewContentModeScaleAspectFill];
    [_asynchronusImgHeaderBG setClipsToBounds:YES];
    [_asynchronusImgHeaderBG loadImageFromURL:imageUrl];
    [_asynchronusImgHeaderBG sizeToFit];
    [self.view addSubview:_asynchronusImgHeaderBG];
    _btnBack.frame = CGRectMake(5,8,25,25);
    
    //  _imgHeaderBG.image=[UIImage imageNamed:_strTImage];
    //  [self.view addSubview:_imgHeaderBG];
    
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"icon_BackMove"] forState:UIControlStateNormal];
    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    _btnBackPress=[[UIButton alloc]initWithFrame:CGRectMake(0,0,55,30)];
    [_btnBackPress addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnBack];
    [self.view addSubview:_btnBackPress];
    
    
    _btnBuyAll=[[UIButton alloc]init];
    _imgFuture=[[UIImageView alloc]initWithFrame:CGRectMake(0,_asynchronusImgHeaderBG.frame.size.height-130,screenWidth/3,130)];
    UILabel *lblFuture=[[UILabel alloc]init];
    _imgHeadingTitle=[[UIImageView alloc]init];
    _lblHedgTitle=[[UILabel alloc]init];
    _lblHedgTitle.text=_strTDesc;
    NSLog(@"%lu",(unsigned long)_strTDesc.length);
    _lblHedgTitle.textColor=[UIColor whiteColor];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        _btnBuyAll.frame=CGRectMake(_asynchronusImgHeaderBG.frame.size.width-100,_asynchronusImgHeaderBG.frame.size.height/2,100,35);
        _imgFuture.frame=CGRectMake(0,_asynchronusImgHeaderBG.frame.size.height-130,_asynchronusImgHeaderBG.frame.size.width/3.5,130);
        lblFuture.frame=CGRectMake(0,_imgFuture.frame.size.height/2-20,_imgFuture.frame.size.width,40);
        lblFuture.text=_strThName;
        lblFuture.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:35];
        _imgHeadingTitle.frame=CGRectMake(CGRectGetMaxX(_imgFuture.frame),_asynchronusImgHeaderBG.frame.size.height-70,screenWidth-_imgFuture.frame.size.width,70);
        
        _lblHedgTitle.frame=CGRectMake(0,5,_imgHeadingTitle.frame.size.width,0);
        _lblHedgTitle.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:15];
        [_lblHedgTitle setNumberOfLines:0];
        [_lblHedgTitle sizeToFit];
    }
    else
    {
        _btnBuyAll.frame=CGRectMake(_asynchronusImgHeaderBG.frame.size.width-100,_asynchronusImgHeaderBG.frame.size.height/2.5,100,35);
        _imgFuture.frame=CGRectMake(0,_asynchronusImgHeaderBG.frame.size.height-80,screenWidth/3,80);
        
        NSArray *words = [_strThName componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        wordCount = [words count];
        
            lblFuture.frame=CGRectMake(0,0,_imgFuture.frame.size.width,0);
        
        lblFuture.text=_strThName;
        lblFuture.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:30];
        [lblFuture setNumberOfLines:0];
        [lblFuture sizeToFit];
        _imgHeadingTitle.frame=CGRectMake(CGRectGetMaxX(_imgFuture.frame),_asynchronusImgHeaderBG.frame.size.height-50,screenWidth-_imgFuture.frame.size.width,50);
        _lblHedgTitle.frame=CGRectMake(0,5,_imgHeadingTitle.frame.size.width,70);
        fnt=[UIFont fontWithName:@"AvenirNext-DemiBold" size:11];
        _lblHedgTitle.font=fnt;
        [_lblHedgTitle setNumberOfLines:3];
        [_lblHedgTitle sizeToFit];
    }
    
    if([_strIAPStatus isEqualToString:@"free"])
    {
        _btnBuyAll.hidden=YES;
    }
    else
    {
        _btnBuyAll.hidden=NO;
        [_btnBuyAll setBackgroundImage:[UIImage imageNamed:@"icon_btnBuyAll1"] forState:UIControlStateNormal];
        [_btnBuyAll addTarget:self action:@selector(btnBuyAllPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_btnBuyAll];
    }
    
    
    _imgFuture.image=[UIImage imageNamed:@"icon_PkgBG"];
    _imgFuture.alpha=.6;
    [self.view addSubview:_imgFuture];
    
    if(wordCount>3||wordCount==3)
    {
        _scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0,_asynchronusImgHeaderBG.frame.size.height-80,screenWidth/3,80)];
       // [self.view addSubview:_scrollView];
       // [imgFuture bringSubviewToFront:_scrollView];
        _viewForcontent=[[UIView alloc]initWithFrame:CGRectMake(0,0,_imgFuture.frame.size.width,40)];
       // [_scrollView addSubview:_viewForcontent];
        
        _scrollView.contentSize=CGSizeMake(_imgFuture.frame.size.width,_imgFuture.frame.size.height+40);
        
        _viewContent=[[UIView alloc]initWithFrame:CGRectMake(0,0,_imgFuture.frame.size.width,_imgFuture.frame.size.height)];
        
       // [_viewForcontent addSubview:_viewContent];
        lblFuture.frame=CGRectMake(0,0,_imgFuture.frame.size.width,_imgFuture.frame.size.height);
    }

    
    lblFuture.textColor=[UIColor whiteColor];
    lblFuture.textAlignment=NSTextAlignmentCenter;
    
    if (wordCount>3||wordCount==3) {
         lblFuture.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:15];
        [_imgFuture addSubview:lblFuture];
    }
    else
    {
      [_imgFuture addSubview:lblFuture];
    }
    
    
    _imgHeadingTitle.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.6];
    [self.view addSubview:_imgHeadingTitle];
   
    NSInteger intLength=_strTDesc.length;
    if(intLength>150)
    {
//        _scrollView1=[[UIScrollView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imgFuture.frame),_asynchronusImgHeaderBG.frame.size.height-50,screenWidth-imgFuture.frame.size.width,50)];
//        [self.view addSubview:_scrollView1];
//        [imgHeadingTitle bringSubviewToFront:_scrollView];
//        _viewForcontent1=[[UIView alloc]initWithFrame:CGRectMake(0,0,imgHeadingTitle.frame.size.width,80)];
//        [_scrollView1 addSubview:_viewForcontent1];
//        _scrollView1.contentSize=CGSizeMake(imgFuture.frame.size.width,imgHeadingTitle.frame.size.height+60);
//        
//        _viewContent1=[[UIView alloc]initWithFrame:CGRectMake(0,0,imgHeadingTitle.frame.size.width,imgHeadingTitle.frame.size.height)];
//        
//        [_viewForcontent1 addSubview:_viewContent1];
         [_imgHeadingTitle addSubview:_lblHedgTitle];
        
        UIColor *redc=[UIColor redColor];
        
        NSString *readMoreText = @" ... More";
        NSInteger lengthForString = _strTDesc.length;
        if (lengthForString >=140)
        {
            NSInteger lengthForVisibleString = [self fitString:_lblHedgTitle.text intoLabel:_lblHedgTitle];
            NSMutableString *mutableString = [[NSMutableString alloc] initWithString:_lblHedgTitle.text];
            NSString *trimmedString = [mutableString stringByReplacingCharactersInRange:NSMakeRange(lengthForVisibleString, (_lblHedgTitle.text.length - lengthForVisibleString)) withString:@""];
            NSInteger readMoreLength = readMoreText.length;
            NSString *trimmedForReadMore = [trimmedString stringByReplacingCharactersInRange:NSMakeRange((trimmedString.length - readMoreLength), readMoreLength) withString:@""];
            NSMutableAttributedString *answerAttributed = [[NSMutableAttributedString alloc] initWithString:trimmedForReadMore attributes:@{
                                                                                                                                            NSFontAttributeName : _lblHedgTitle.font
                                                                                                                                            }];
            NSMutableAttributedString *readMoreAttributed = [[NSMutableAttributedString alloc] initWithString:readMoreText attributes:@{
                                                                                                                                        NSFontAttributeName :fnt,
                                                                                                                                        NSForegroundColorAttributeName : redc
                                                                                                                                        }];

            //[answerAttributed addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(130,4)];
            
            
            [answerAttributed appendAttributedString:readMoreAttributed];
            _lblHedgTitle.attributedText = answerAttributed;
            [_lblHedgTitle setUserInteractionEnabled:YES];
        
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureUpdated:)];
            tapGesture.delegate=self;
            tapGesture.cancelsTouchesInView = NO;
            [[self view] addGestureRecognizer:tapGesture];
        }
        else {
            
            NSLog(@"No need for 'Read More'...");
        }
    }
    else
    {
         [_imgHeadingTitle addSubview:_lblHedgTitle];
    }
    
    UILabel *lblChallenges=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_asynchronusImgHeaderBG.frame)+5,screenWidth,30)];
    lblChallenges.text=@"Challenges";
    lblChallenges.textColor=[UIColor orangeColor];
    lblChallenges.textAlignment=NSTextAlignmentCenter;
    lblChallenges.font=[UIFont fontWithName:@"Avenir Medium" size:20];
    [self.view addSubview:lblChallenges];
    
    _tblChallenges=[[UITableView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblChallenges.frame),screenWidth,screenHeight-230)];
    _tblChallenges.delegate=self;
    _tblChallenges.dataSource=self;
    _tblChallenges.tableFooterView=[[UIView alloc]init];
    [self.view addSubview:_tblChallenges];
}

- (NSUInteger)fitString:(NSString *)string intoLabel:(UILabel *)label
{
    UIFont *font           = label.font;
    NSLineBreakMode mode   = label.lineBreakMode;
    
    CGFloat labelWidth     = label.frame.size.width;
    CGFloat labelHeight    = label.frame.size.height;
    CGSize  sizeConstraint = CGSizeMake(labelWidth, CGFLOAT_MAX);
    
    
    NSDictionary *attributes = @{ NSFontAttributeName : font };
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:string attributes:attributes];
    CGRect boundingRect = [attributedText boundingRectWithSize:sizeConstraint options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    {
        if (boundingRect.size.height > labelHeight)
        {
            NSUInteger index = 0;
            NSUInteger prev;
            NSCharacterSet *characterSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
            
            do
            {
                prev = index;
                if (mode == NSLineBreakByCharWrapping)
                    index++;
                else
                    index = [string rangeOfCharacterFromSet:characterSet options:0 range:NSMakeRange(index + 1, [string length] - index - 1)].location;
            }
            
            while (index != NSNotFound && index < [string length] && [[string substringToIndex:index] boundingRectWithSize:sizeConstraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.height <= labelHeight);
            return prev;
        }
    }
    
    return [string length];
}
- (IBAction)tapGestureUpdated:(UIGestureRecognizer *)recognizer
{
    _lblHedgTitle.frame=CGRectMake(0,5,_imgHeadingTitle.frame.size.width,0);
    _lblHedgTitle.text=_strTDesc;
    [_lblHedgTitle setNumberOfLines:0];
    [_lblHedgTitle sizeToFit];
    
    CGSize maxSize = CGSizeMake(_lblHedgTitle.bounds.size.width, CGFLOAT_MAX);
   
    /*NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont systemFontOfSize:12], NSFontAttributeName,
                                [NSParagraphStyle defaultParagraphStyle], NSParagraphStyleAttributeName,
                                nil];*/
    CGRect textSize =[_strTDesc boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: fnt} context:nil];
    
    NSLog(@"Ok");
            _scrollView1=[[UIScrollView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_imgFuture.frame),_asynchronusImgHeaderBG.frame.size.height-50,screenWidth-_imgFuture.frame.size.width,50)];
            [self.view addSubview:_scrollView1];
            [_imgHeadingTitle bringSubviewToFront:_scrollView];
            _viewForcontent1=[[UIView alloc]initWithFrame:CGRectMake(0,0,_imgHeadingTitle.frame.size.width, textSize.size.height)];
            [_scrollView1 addSubview:_viewForcontent1];
            _scrollView1.contentSize=CGSizeMake(_imgFuture.frame.size.width,textSize.size.height);
    
            _viewContent1=[[UIView alloc]initWithFrame:CGRectMake(0,0,_imgHeadingTitle.frame.size.width,_imgHeadingTitle.frame.size.height)];
            [_viewForcontent1 addSubview:_viewContent1];
            [_viewForcontent1 addSubview:_lblHedgTitle];
}
#pragma mark: UIButton Pressed delegate properties declaratiosn here
-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnBuySinglePressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    singleSelectionID=btn.tag;
    
    if ([_strIAPStatus isEqualToString:@"free"])
    {
        [self buySingSnapbetTheme];
    }
    else
    {
        [self viewForPurchaseSingleTheme];
    }
}

-(IBAction)btnBuyAllPressed:(id)sender
{
    [self viewForPurchaseAllThemePkg];
    //[self viewForInvitePopUpUI];
    //[self viewForUpgradeAccountPopUpUI];
}

-(IBAction)btnPlayVideoPressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    selectedVideoIndex=btn.tag;
        NSLog(@"%ld",(long)selectedVideoIndex);
    
   /* if(theAppDelegate.isAlreadyPurchased)
    {
        [self viewForVideoScreenPopUpUI];
    }
    else
    {
        
    }*/
}
#pragma mark-Close Comment View
-(void)closeViewButonPressed
{
    @try
    {
         for(UIView *subview in [_viewForBuySingleTheme subviews])
        {
            [subview removeFromSuperview];
        }
        for(UIView *subview in [_viewForBuyAll subviews])
        {
            [subview removeFromSuperview];
        }
        for(UIView *subview in [_viewForInvite subviews])
        {
            [subview removeFromSuperview];
        }
        for(UIView *subview in [_viewForPostResponse subviews])
        {
            [subview removeFromSuperview];
        }
        
        for(UIView *subview in [_viewForUpgradeAccount subviews])
        {
            [subview removeFromSuperview];
        }
        for(UIView *subview in [_viewForVideoScreen subviews])
        {
            _lblHedgTitle.hidden=NO;
            [_playerViewController.player pause];
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForHelp=_viewForBuySingleTheme.frame;
        CGRect frameForViewForBuyAll=_viewForBuyAll.frame;
        CGRect frameForViewForInvite=_viewForInvite.frame;
        CGRect frameForViewForPostResponse=_viewForPostResponse.frame;
        CGRect frameForViewForUpgrade=_viewForUpgradeAccount.frame;
        CGRect frameForViewForVideo=_viewForVideoScreen.frame;
        
        frameForViewForHelp=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        frameForViewForBuyAll=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        frameForViewForInvite=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        frameForViewForPostResponse=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        frameForViewForUpgrade=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        frameForViewForVideo=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForBuySingleTheme.frame=frameForViewForHelp;
            _viewForBuyAll.frame=frameForViewForBuyAll;
            _viewForInvite.frame=frameForViewForBuyAll;
            _viewForPostResponse.frame=frameForViewForBuyAll;
            _viewForUpgradeAccount.frame=frameForViewForUpgrade;
            _viewForVideoScreen.frame=frameForViewForVideo;
            
        } completion:^(BOOL finished){
            [_viewForBuySingleTheme removeFromSuperview];
            [_viewForBuyAll removeFromSuperview];
            [_viewForInvite removeFromSuperview];
            [_viewForPostResponse removeFromSuperview];
            [_viewForUpgradeAccount removeFromSuperview];
            [_viewForVideoScreen removeFromSuperview];
                  //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark: Viewfor Buy single Theme Package
-(void)viewForPurchaseSingleTheme
{
    [_viewForBuySingleTheme removeFromSuperview];
    _viewForBuySingleTheme =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForBuySingleTheme.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForBuySingleTheme.tag=20;
    [self.view addSubview:_viewForBuySingleTheme];
    CGRect frameForViewForHelp=_viewForBuySingleTheme.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForBuySingleTheme.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForBuySingleTheme.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForBuySingleTheme.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForBuySingleTheme addSubview:img];
        UILabel *lblPurchase=[[UILabel alloc]initWithFrame:CGRectMake(10,CGRectGetMaxY(img.frame)+20,_viewForBuySingleTheme.frame.size.width-20,30)];
        lblPurchase.text=@"Single Snapbet Purchase";
        lblPurchase.textAlignment=NSTextAlignmentCenter;
        lblPurchase.textColor=[UIColor whiteColor];
        lblPurchase.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:22];
        [_viewForBuySingleTheme addSubview:lblPurchase];
        
        UILabel *lblPurchase1=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblPurchase.frame)+10,_viewForBuySingleTheme.frame.size.width,30)];
        lblPurchase1.text=@"Would you like to purchase this Snapbet?";
        lblPurchase1.textAlignment=NSTextAlignmentCenter;
        lblPurchase1.textColor=[UIColor whiteColor];
        lblPurchase1.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForBuySingleTheme addSubview:lblPurchase1];
        
        _btnSBuySnpbet=[[UIButton alloc]initWithFrame:CGRectMake(20,CGRectGetMaxY(lblPurchase1.frame)+50,_viewForBuySingleTheme.frame.size.width/2-30,45)];
        _btnClseBuySnpbet=[[UIButton alloc]initWithFrame:CGRectMake(_viewForBuySingleTheme.frame.size.width/2+15,CGRectGetMaxY(lblPurchase1.frame)+50,_viewForBuySingleTheme.frame.size.width/2-30,45)];
      
         [_btnClseBuySnpbet setTitle:@"Cancel" forState:UIControlStateNormal];
        
        [_btnSBuySnpbet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnClseBuySnpbet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        _btnSBuySnpbet.backgroundColor=[UIColor orangeColor];
        
        _btnClseBuySnpbet.layer.borderWidth=2.0;
        _btnClseBuySnpbet.layer.borderColor=[[UIColor orangeColor]CGColor];
        
        _btnSBuySnpbet.layer.cornerRadius=5.0;
        _btnClseBuySnpbet.layer.cornerRadius=5.0;

        [_btnClseBuySnpbet addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
      
            [_btnSBuySnpbet addTarget:self action:@selector(btnBuySinglePkgValuePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewForBuySingleTheme addSubview:_btnSBuySnpbet];
        [_viewForBuySingleTheme addSubview:_btnClseBuySnpbet];
        
        UILabel *lblBuy=[[UILabel alloc]initWithFrame:CGRectMake(0,0,_btnSBuySnpbet.frame.size.width,30)];
        lblBuy.text=@"Buy Snapbet";
        lblBuy.textAlignment=NSTextAlignmentCenter;
        lblBuy.textColor=[UIColor whiteColor];
        lblBuy.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_btnSBuySnpbet addSubview:lblBuy];
        
        UILabel *lblBuyPrice=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblBuy.frame)-10,_btnSBuySnpbet.frame.size.width,30)];
        lblBuyPrice.text=@"$2.99";
        lblBuyPrice.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        lblBuyPrice.textAlignment=NSTextAlignmentCenter;
        lblBuyPrice.textColor=[UIColor whiteColor];
        [_btnSBuySnpbet addSubview:lblBuyPrice];
    }];
}


#pragma mark: Viewfor Buy single Theme Package
-(void)viewForPurchaseAllThemePkg
{
    [_viewForBuyAll removeFromSuperview];
    _viewForBuyAll =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForBuyAll.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForBuyAll.tag=20;
    [self.view addSubview:_viewForBuyAll];
    CGRect frameForViewForHelp=_viewForBuyAll.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForBuyAll.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForBuyAll.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForBuyAll.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForBuyAll addSubview:img];
        UILabel *lblPurchase=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForBuyAll.frame.size.width,30)];
        lblPurchase.text=@"Theme Purchase";
        lblPurchase.textAlignment=NSTextAlignmentCenter;
        lblPurchase.textColor=[UIColor whiteColor];
        lblPurchase.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:25];
        [_viewForBuyAll addSubview:lblPurchase];
        
        UILabel *lblPurchase1=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblPurchase.frame)+10,_viewForBuyAll.frame.size.width,30)];
        lblPurchase1.text=@"Would you like to purchase this theme?";
        lblPurchase1.textAlignment=NSTextAlignmentCenter;
        lblPurchase1.textColor=[UIColor whiteColor];
        lblPurchase1.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForBuyAll addSubview:lblPurchase1];
        
        _btnSBuySnpbet=[[UIButton alloc]initWithFrame:CGRectMake(20,CGRectGetMaxY(lblPurchase1.frame)+50,_viewForBuyAll.frame.size.width/2-30,45)];
        _btnClseBuySnpbet=[[UIButton alloc]initWithFrame:CGRectMake(_viewForBuyAll.frame.size.width/2+15,CGRectGetMaxY(lblPurchase1.frame)+50,_viewForBuyAll.frame.size.width/2-30,45)];
        
        [_btnClseBuySnpbet setTitle:@"Cancel" forState:UIControlStateNormal];
        
        [_btnSBuySnpbet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnClseBuySnpbet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        _btnSBuySnpbet.backgroundColor=[UIColor orangeColor];
        
        _btnClseBuySnpbet.layer.borderWidth=2.0;
        _btnClseBuySnpbet.layer.borderColor=[[UIColor orangeColor]CGColor];
        
        _btnSBuySnpbet.layer.cornerRadius=5.0;
        _btnClseBuySnpbet.layer.cornerRadius=5.0;
        
        [_btnClseBuySnpbet addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
        [_btnSBuySnpbet addTarget:self action:@selector(btnBuyAllPkgValuePressed:) forControlEvents:UIControlEventTouchUpInside];
        [_viewForBuyAll addSubview:_btnSBuySnpbet];
        [_viewForBuyAll addSubview:_btnClseBuySnpbet];
        
        UILabel *lblBuy=[[UILabel alloc]initWithFrame:CGRectMake(0,0,_btnSBuySnpbet.frame.size.width,30)];
        lblBuy.text=@"Buy Package";
        lblBuy.textAlignment=NSTextAlignmentCenter;
        lblBuy.textColor=[UIColor whiteColor];
        lblBuy.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_btnSBuySnpbet addSubview:lblBuy];
        
        UILabel *lblBuyPrice=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblBuy.frame)-10,_btnSBuySnpbet.frame.size.width,30)];
        lblBuyPrice.text=@"$5.99";
        lblBuyPrice.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        lblBuyPrice.textAlignment=NSTextAlignmentCenter;
        lblBuyPrice.textColor=[UIColor whiteColor];
        [_btnSBuySnpbet addSubview:lblBuyPrice];
    }];
}

#pragma mark: Viewfor Buy single Theme Package
-(void)viewForUpgradeAccountPopUpUI
{
    [_viewForUpgradeAccount removeFromSuperview];
    _viewForUpgradeAccount =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForUpgradeAccount.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForUpgradeAccount.tag=20;
    [self.view addSubview:_viewForUpgradeAccount];
    CGRect frameForViewForHelp=_viewForUpgradeAccount.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForUpgradeAccount.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForUpgradeAccount.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForUpgradeAccount.frame.size.width/2-50,80,100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForUpgradeAccount addSubview:img];
        UILabel *lblPurchase=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForUpgradeAccount.frame.size.width,30)];
        lblPurchase.text=@"Upgrade Account";
        lblPurchase.textAlignment=NSTextAlignmentCenter;
        lblPurchase.textColor=[UIColor whiteColor];
        lblPurchase.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:25];
        [_viewForUpgradeAccount addSubview:lblPurchase];
        
        UILabel *lblPurchase1=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblPurchase.frame)+10,_viewForUpgradeAccount.frame.size.width,30)];
        lblPurchase1.text=@"Upgrade your account";
        lblPurchase1.textAlignment=NSTextAlignmentCenter;
        lblPurchase1.textColor=[UIColor whiteColor];
        lblPurchase1.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForUpgradeAccount addSubview:lblPurchase1];
        
        _btnUpgrade=[[UIButton alloc]initWithFrame:CGRectMake(20,CGRectGetMaxY(lblPurchase1.frame)+50,_viewForUpgradeAccount.frame.size.width/2-30,45)];
        _btnCloseUpgradeUI=[[UIButton alloc]initWithFrame:CGRectMake(_viewForUpgradeAccount.frame.size.width/2+15,CGRectGetMaxY(lblPurchase1.frame)+50,_viewForUpgradeAccount.frame.size.width/2-30,45)];
        
        [_btnCloseUpgradeUI setTitle:@"Cancel" forState:UIControlStateNormal];
        
        [_btnCloseUpgradeUI setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        _btnCloseUpgradeUI.layer.borderWidth=2.0;
        _btnCloseUpgradeUI.layer.borderColor=[[UIColor orangeColor]CGColor];
       
        _btnUpgrade.backgroundColor=[UIColor orangeColor];
       
        _btnUpgrade.layer.cornerRadius=5.0;
        _btnCloseUpgradeUI.layer.cornerRadius=5.0;
        
        [_btnCloseUpgradeUI addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
        [_viewForUpgradeAccount addSubview:_btnUpgrade];
        [_viewForUpgradeAccount addSubview:_btnCloseUpgradeUI];
        
        UILabel *lblBuy=[[UILabel alloc]initWithFrame:CGRectMake(0,0,_btnUpgrade.frame.size.width,30)];
        lblBuy.text=@"Upgrade";
        lblBuy.textAlignment=NSTextAlignmentCenter;
        lblBuy.textColor=[UIColor whiteColor];
        lblBuy.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_btnUpgrade addSubview:lblBuy];
        
        UILabel *lblBuyPrice=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblBuy.frame)-10,_btnUpgrade.frame.size.width,30)];
        lblBuyPrice.text=@"$10.99";
        lblBuyPrice.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        lblBuyPrice.textAlignment=NSTextAlignmentCenter;
        lblBuyPrice.textColor=[UIColor whiteColor];
        [_btnUpgrade addSubview:lblBuyPrice];
    }];
}
#pragma mark: Viewfor Buy single Theme Package
-(void)viewForInvitePopUpUI
{
    [_viewForInvite removeFromSuperview];
    _viewForInvite =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForInvite.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForInvite.tag=20;
    [self.view addSubview:_viewForInvite];
    CGRect frameForViewForHelp=_viewForInvite.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForInvite.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForInvite.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForInvite.frame.size.width/2-50,80,100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForInvite addSubview:img];
        UILabel *lblInvite=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForInvite.frame.size.width,30)];
        lblInvite.text=@"You Have an Invite!";
        lblInvite.textAlignment=NSTextAlignmentCenter;
        lblInvite.textColor=[UIColor whiteColor];
        lblInvite.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:25];
        [_viewForInvite addSubview:lblInvite];
        
        UILabel *lblInvite1=[[UILabel alloc]initWithFrame:CGRectMake(40,CGRectGetMaxY(lblInvite.frame)+10,_viewForInvite.frame.size.width-80,0)];
        lblInvite1.text=@"You have been invited to a new Snapbet!";
        lblInvite1.textAlignment=NSTextAlignmentCenter;
        lblInvite1.textColor=[UIColor whiteColor];
        lblInvite1.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [lblInvite1 setNumberOfLines:0];
        [lblInvite1 sizeToFit];
        [_viewForInvite addSubview:lblInvite1];
        
        _btnViewInvite=[[UIButton alloc]initWithFrame:CGRectMake(20,CGRectGetMaxY(lblInvite1.frame)+50,_viewForInvite.frame.size.width/2-30,45)];
        _btnCloseInvite=[[UIButton alloc]initWithFrame:CGRectMake(_viewForInvite.frame.size.width/2+15,CGRectGetMaxY(lblInvite1.frame)+50,_viewForInvite.frame.size.width/2-30,45)];
        
        [_btnViewInvite setTitle:@"View" forState:UIControlStateNormal];
        [_btnCloseInvite setTitle:@"Close" forState:UIControlStateNormal];
        
        [_btnViewInvite setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnCloseInvite setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [_btnViewInvite setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnCloseInvite setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
       
        [_btnCloseInvite addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
        
        _btnViewInvite.layer.borderWidth=2.0;
        _btnCloseInvite.layer.borderWidth=2.0;
        
        _btnViewInvite.layer.borderColor=[[UIColor orangeColor]CGColor];
        _btnCloseInvite.layer.borderColor=[[UIColor orangeColor]CGColor];
        
        _btnViewInvite.layer.cornerRadius=5.0;
        _btnCloseInvite.layer.cornerRadius=5.0;
        
        [_btnClseBuySnpbet addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewForInvite addSubview:_btnViewInvite];
        [_viewForInvite addSubview:_btnCloseInvite];
        
    }];
}

#pragma mark: Viewfor Play Video popUP UI
-(void)viewForVideoScreenPopUpUI
{
    [_viewForVideoScreen removeFromSuperview];
    _viewForVideoScreen =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForVideoScreen.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForVideoScreen.tag=20;
    [self.view addSubview:_viewForVideoScreen];
    CGRect frameForViewForHelp=_viewForVideoScreen.frame;
   
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForVideoScreen.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForVideoScreen.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        _lblHedgTitle.hidden=YES;
        UILabel *lblHTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,[UIScreen mainScreen].bounds.size.height/100*19,_viewForVideoScreen.frame.size.width,30)];
        lblHTitle.text=[[_arrTblData valueForKey:@"v_name"]objectAtIndex:selectedVideoIndex];
        lblHTitle.textAlignment=NSTextAlignmentCenter;
        lblHTitle.textColor=[UIColor whiteColor];
        lblHTitle.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:25];
        [_viewForVideoScreen addSubview:lblHTitle];
        
        _viewForVideoArea=[[UIView alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _viewForVideoArea.frame=CGRectMake(60,[UIScreen mainScreen].bounds.size.height/100*25,_viewForVideoScreen.frame.size.width-120,[UIScreen mainScreen].bounds.size.height/100*50);
        }
        else
        {
            _viewForVideoArea.frame=CGRectMake(10,[UIScreen mainScreen].bounds.size.height/100*25,_viewForVideoScreen.frame.size.width-20,[UIScreen mainScreen].bounds.size.height/100*50);
        }
        _viewForVideoArea.backgroundColor=[UIColor colorWithRed:38/255.0 green:38/255.0 blue:38/255.0 alpha:1];
        _viewForVideoArea.layer.borderWidth=2.0;
        _viewForVideoArea.layer.borderColor=[[UIColor whiteColor]CGColor];
        [_viewForVideoScreen addSubview:_viewForVideoArea];
        
        _asySelectedBG=[[AsyncImageView alloc]initWithFrame:CGRectMake(0,0,_viewForVideoArea.frame.size.width,_viewForVideoArea.frame.size.height)];
        _asynchronusImgHeaderBG.backgroundColor = [UIColor clearColor];
        NSString *imgUrlString1 =[[_arrTblData valueForKey:@"image"]objectAtIndex:selectedVideoIndex];
        NSString *imgBaseURL=ImgBase_URL;
        imgBaseURL=[imgBaseURL stringByAppendingString:imgUrlString1];
        
        NSString *webStr = [NSString stringWithFormat:@"%@",imgBaseURL];
        webStr=[webStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *imageUrl = [[NSURL alloc] initWithString:webStr];
        
        [_asySelectedBG setContentMode:UIViewContentModeScaleAspectFill];
        [_asySelectedBG setClipsToBounds:YES];
        [_asySelectedBG loadImageFromURL:imageUrl];
        [_asySelectedBG sizeToFit];
        [_viewForVideoArea addSubview:_asySelectedBG];
        
        UIButton *btnIcon=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoArea.frame.size.width/2-25,_viewForVideoArea.frame.size.height/2-22,50,44)];
        [btnIcon setBackgroundImage:[UIImage imageNamed:@"img_AppIcon"] forState:UIControlStateNormal];
        [btnIcon addTarget:self action:@selector(playVideo) forControlEvents:UIControlEventTouchUpInside];
        [_viewForVideoArea addSubview:btnIcon];
        
        UIButton *btnPrviCont=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoScreen.frame.size.width/2-50,CGRectGetMaxY(_viewForVideoArea.frame)+25,30,20)];
        [btnPrviCont setBackgroundImage:[UIImage imageNamed:@"icon_Privious"] forState:UIControlStateNormal];
       // [_viewForVideoScreen addSubview:btnPrviCont];
        
        UIButton *btnPlyPause=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoScreen.frame.size.width/2-12,CGRectGetMaxY(_viewForVideoArea.frame)+25,24,20)];
        [btnPlyPause setBackgroundImage:[UIImage imageNamed:@"icon_Pause"] forState:UIControlStateNormal];
        ///[_viewForVideoScreen addSubview:btnPlyPause];
        
        UIButton *btnNxtCont=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(btnPlyPause.frame)+8,CGRectGetMaxY(_viewForVideoArea.frame)+25,30,20)];
        [btnNxtCont setBackgroundImage:[UIImage imageNamed:@"icon_Next"] forState:UIControlStateNormal];
        //[_viewForVideoScreen addSubview:btnNxtCont];
        
        _btnCloseVideoScreen=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoScreen.frame.size.width-40,20,30,30)];
        [_btnCloseVideoScreen setBackgroundImage:[UIImage imageNamed:@"icon_Cross"] forState:UIControlStateNormal];
        [_btnCloseVideoScreen addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
        [_viewForVideoScreen addSubview:_btnCloseVideoScreen];
    }];
}

#pragma mark: Play selected challage video
-(void)playVideo
{
    [_playerViewController.view removeFromSuperview];
    NSString *strVBase=vBase_URL;
    NSString *strVideoName=[_arrVideoURL objectAtIndex:selectedVideoIndex];
    strVBase=[strVBase stringByAppendingString:strVideoName];
    strVBase=[strVBase stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *vURL=[NSURL URLWithString:strVBase];
    _playerViewController = [[AVPlayerViewController alloc] init];
    _playerViewController.player = [AVPlayer playerWithURL:vURL];
    _playerViewController.view.frame =CGRectMake(0,0,_viewForVideoArea.frame.size.width,_viewForVideoArea.frame.size.height);
    //[_playerViewController setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_playerViewController.player play];
    
    _playerViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:_playerViewController animated:YES completion:nil];
    //[_viewForVideoArea addSubview:_playerViewController.view];
    
}

#pragma mark: view For Alert
-(void)viewForAlertUI
{
    [_viewForAlert removeFromSuperview];
    _viewForAlert =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForAlert.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForAlert.tag=20;
    [self.view addSubview:_viewForAlert];
    
    CGRect frameForViewForHelp=_viewForAlert.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForAlert.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForAlert.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForAlert addSubview:img];
        UILabel *lblAlert=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForAlert.frame.size.width,30)];
        lblAlert.text=_strAlertTitle;
        lblAlert.textAlignment=NSTextAlignmentCenter;
        lblAlert.textColor=[UIColor whiteColor];
        lblAlert.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForAlert addSubview:lblAlert];
        
        _btnCloseAlert=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseAlert.frame=CGRectMake(_viewForAlert.frame.size.width/2-60,CGRectGetMaxY(lblAlert.frame)+50,120,40);
        }
        else
        {
            _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,CGRectGetMaxY(lblAlert.frame)+50,100,40)];
        }
        [_btnCloseAlert setTitle:@"Close" forState:UIControlStateNormal];
         [_btnCloseAlert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         _btnCloseAlert.layer.borderWidth=2.0;
         _btnCloseAlert.layer.cornerRadius=4.0;
        _btnCloseAlert.layer.borderColor=[[UIColor orangeColor]CGColor];
        //[_btnVoteSeeMore addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        [_btnCloseAlert addTarget:self action:@selector(viewForCloseAlert) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewForAlert addSubview:_btnCloseAlert];
    }];
}

#pragma mark-Close Comment View
-(void)viewForCloseAlert
{
    @try
    {
        for(UIView *subview in [_viewForAlert subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewAlert=_viewForAlert.frame;
        
        
        frameForViewAlert=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForAlert.frame=frameForViewAlert;
            
        } completion:^(BOOL finished){
            [_viewForAlert removeFromSuperview];
            
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

/*--------------     Bottom TableView delegate and datasource declaration here     -----------*/
#pragma mark- TableView DataSource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_arrTblData count];
}

//--------------------- cellforrow At Index TableView -----------------------//

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@""];
    if (cell == nil)
    {
        //  cell.transform= CGAffineTransformMakeRotation(M_PI/2);
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    
    @try
    {
        tableView.separatorColor =UIColor.clearColor;
        cell.backgroundColor= [UIColor clearColor];
        [self.tblChallenges setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        UIView *viewForCell=[[UIView alloc]init];
         UIView *viewFirstBG=[[UIView alloc]init];
        UIView *viewForCell1=[[UIView alloc]init];
        UIImageView *imgTemp=[[UIImageView alloc]init];
        _btnBuySingle=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            viewForCell.frame=CGRectMake(-5,0,screenWidth+10,150);
            viewFirstBG.frame=CGRectMake(0,0,screenWidth/3,150 );
            viewForCell1.frame=CGRectMake(CGRectGetMaxX(viewFirstBG.frame),0,screenWidth/1.4,150);
            imgTemp.frame=CGRectMake(0,viewForCell1.frame.size.height-30,screenWidth/1.4-200,30);
            _btnBuySingle.frame=CGRectMake(CGRectGetMaxX(imgTemp.frame),viewForCell1.frame.size.height-30,200,30);
        }
        else
        {
             viewForCell.frame=CGRectMake(-5,0,screenWidth+10,100);
             viewFirstBG.frame=CGRectMake(0,0,screenWidth/3,100);
             viewForCell1.frame=CGRectMake(CGRectGetMaxX(viewFirstBG.frame),0,screenWidth-100,100);
             imgTemp.frame=CGRectMake(0,viewForCell1.frame.size.height-30,screenWidth/1.4-100,30);
            _btnBuySingle.frame=CGRectMake(CGRectGetMaxX(imgTemp.frame),viewForCell1.frame.size.height-30,100,30);
        }
        
        viewForCell.backgroundColor=[UIColor colorWithRed:243.0/255.0 green:144.0/255.0 blue:0/255.0 alpha:1];
        viewForCell.layer.borderWidth=1.0;
        viewForCell.layer.borderColor=[[UIColor whiteColor]CGColor];
        [cell.contentView addSubview:viewForCell];
    
       
        viewFirstBG.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.7];
        viewFirstBG.layer.borderWidth=1.0;
        viewFirstBG.layer.borderColor=[[UIColor whiteColor]CGColor];
    
        [viewForCell addSubview:viewFirstBG];
        
        
        NSString *imgUrlString1 =[[_arrTblData valueForKey:@"image"]objectAtIndex:indexPath.row];
        _asynchronusImage=[[AsyncImageView alloc] initWithFrame:CGRectMake(0,0,screenWidth/3,100)];
        [viewForCell addSubview:_asynchronusImage];
        
        _btnPlay=[[UIButton alloc]initWithFrame:CGRectMake(viewFirstBG.frame.size.width/2-25,viewFirstBG.frame.size.height/2-25,50,50)];
        [_btnPlay setBackgroundImage:[UIImage imageNamed:@"img_AppIcon"] forState:UIControlStateNormal];
        [_btnPlay addTarget:self action:@selector(btnPlayVideoPressed:) forControlEvents:UIControlEventTouchUpInside];
        _btnPlay.tag=indexPath.row;
        
        if (imgUrlString1==(id)[NSNull null])
        {
            imgUrlString1=@"";
        }
        else if (imgUrlString1.length>0)
        {
            NSString *strBase=ImgBase_URL;
            strBase=[strBase stringByAppendingString:imgUrlString1];
            
            [_asynchronusImage setContentMode:UIViewContentModeScaleToFill];
            [_asynchronusImage sizeToFit];
            [_asynchronusImage loadImageFromURL:[NSURL URLWithString:strBase]];
            [_asynchronusImage setClipsToBounds:YES];
            [_asynchronusImage addSubview:_btnPlay];
        }
        else
        {
             [viewForCell addSubview:_btnPlay];
        }
        
        UILabel *lblVideoName=[[UILabel alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _asynchronusImage.frame=CGRectMake(0,0,screenWidth/3,viewForCell.frame.size.height-40);
            lblVideoName.frame=CGRectMake(0,viewFirstBG.frame.size.height-40,viewFirstBG.frame.size.width,40);
        }
        else
        {
            lblVideoName=[[UILabel alloc]initWithFrame:CGRectMake(0,viewFirstBG.frame.size.height-25,viewFirstBG.frame.size.width,25)];
        }
        
        lblVideoName.text=[[_arrTblData valueForKey:@"v_name"]objectAtIndex:indexPath.row];
        lblVideoName.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:14];
        lblVideoName.textColor=[UIColor whiteColor];
        lblVideoName.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7];
        lblVideoName.textAlignment=NSTextAlignmentCenter;
        [viewFirstBG addSubview:lblVideoName];
    
        
        viewForCell1.backgroundColor=[UIColor colorWithRed:243.0/255.0 green:144.0/255.0 blue:0/255.0 alpha:1];
        viewForCell1.layer.borderWidth=1.0;
        viewForCell1.layer.borderColor=[[UIColor whiteColor]CGColor];
        [viewForCell addSubview:viewForCell1];
    
        UILabel *lblDisChallenge=[[UILabel alloc]initWithFrame:CGRectMake(5,5,viewForCell1.frame.size.width-25,0)];
        NSString *strDetils=[[_arrTblData valueForKey:@"v_details"]objectAtIndex:indexPath.row];
        if ([strDetils isEqual: [NSNull null]])
        {
            lblDisChallenge.text=@"N/P";
        }
        else
        {
            lblDisChallenge.text=strDetils;
        }
        lblDisChallenge.textColor=[UIColor blackColor];
        lblDisChallenge.font=[UIFont fontWithName:@"Avenir-Roman" size:15];
        [lblDisChallenge setNumberOfLines:3];
        [lblDisChallenge sizeToFit];
        [viewForCell1 addSubview:lblDisChallenge];
        
        imgTemp.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7];
        //[viewForCell1 addSubview:imgTemp];
        
        if (theAppDelegate.isAlreadyPurchased) {
            [_btnBuySingle setTitle:@"Select" forState:UIControlStateNormal];
                [_btnBuySingle addTarget:self action:@selector(buySingSnapbetTheme) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            if([_strIAPStatus isEqualToString:@"free"])
            {
                [_btnBuySingle setTitle:@"Free" forState:UIControlStateNormal];
            }
            else
            {
                [_btnBuySingle setTitle:@"Buy Single" forState:UIControlStateNormal];
            }
            [_btnBuySingle addTarget:self action:@selector(btnBuySinglePressed:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        
        [_btnBuySingle setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnBuySingle.titleLabel.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
        _btnBuySingle.backgroundColor=[[UIColor colorWithRed:166/255.0 green:87/255.0 blue:19/255.0 alpha:1]colorWithAlphaComponent:0.7];
            _btnBuySingle.tag=indexPath.row;
        //[viewForCell1 addSubview:_btnBuySingle];
        
        return cell;
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}

#pragma Mark-delegate methods
//--------------------- Tableview didselected index ---------------------//
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        return 150;
    }
    else
    {
    return 100;
    }
}

#pragma mark: FBLogin Server call
-(void)GetThemeDetailsAPI
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isBuyAllList=NO;
        _ispurchaseList=YES;
        _isSinglePurchase=NO;
        
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
        NSString *strUrlString=base_URL;
        NSString *strURL=[NSString stringWithFormat:@"wsviewspecifictheme.php?theme_id=%@",_strSelectedThmeID];

        strUrlString=[strUrlString stringByAppendingString:strURL];
        NSURL *url = [NSURL URLWithString:[strUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        request=[NSMutableURLRequest requestWithURL:[url standardizedURL]];
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
    }
    request = nil;
}

#pragma mark: FBLogin Server call
-(void)buySingSnapbetTheme
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isBuyAllList=NO;
        _ispurchaseList=NO;
        _isSinglePurchase=YES;
        
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
        NSString *strUrlString=base_URL;
        NSString *strURL=[NSString stringWithFormat:@"wsthemepurchase.php?uid=%@&tid=%@&vid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],_strSelectedThmeID,[[_arrTblData valueForKey:@"id"]objectAtIndex:singleSelectionID]];
        
        strUrlString=[strUrlString stringByAppendingString:strURL];
        strUrlString=[strUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:strUrlString];
        request=[NSMutableURLRequest requestWithURL:[url standardizedURL]];
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        
    }
    request = nil;
}

#pragma mark: Button buy package pressed
-(IBAction)btnBuyAllPkgValuePressed:(id)sender
{
    SKProduct *product;
    
    product=[[InAppRageIAPHelper sharedHelper].products objectAtIndex:0];
    if([product isKindOfClass:[NSNull class]])
    {
        
    }
    else
    {
        NSLog(@"Buying %@...", product.productIdentifier);
        [[InAppRageIAPHelper sharedHelper] buyProduct:product];
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.labelText =@"Buying...";
        [self performSelector:@selector(timeout:) withObject:nil afterDelay:60*5];
        [self buyAllSnapbetTheme];
    }
}

#pragma mark: Button buy single package pressed
-(IBAction)btnBuySinglePkgValuePressed:(id)sender
{
    
    //UIButton *btn=(UIButton*)sender;
  //  NSInteger row=btn.tag;
//NSString *StrThemeID=[[_arrTblData valueForKey:@""]objectAtIndex:row];
    SKProduct *product;
    product=[[InAppRageIAPHelper sharedHelper].products objectAtIndex:1];
    NSLog(@"Buying %@...", product.productIdentifier);
    [[InAppRageIAPHelper sharedHelper] buyProduct:product];
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.labelText =@"Buying...";
    [self performSelector:@selector(timeout:) withObject:nil afterDelay:60*5];
    [self buySingSnapbetTheme];
}

#pragma mark: BuyAll Snapbet Server call
-(void)buyAllSnapbetTheme
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isBuyAllList=YES;
        _ispurchaseList=NO;
        _isSinglePurchase=NO;
        
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
        NSString *strUrlString=base_URL;
        NSString *strURL=[NSString stringWithFormat:@"wsthemepurchase.php?uid=%@&tid=%@&vid=All",[theAppDelegate.arrUserLoginData objectAtIndex:0],_strSelectedThmeID];
                                  
        strUrlString=[strUrlString stringByAppendingString:strURL];
        strUrlString=[strUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:strUrlString];
        request=[NSMutableURLRequest requestWithURL:[url standardizedURL]];
        if(!connection)
            connection= [[WebConnection1 alloc] init];
            connection.delegate = self;
            [connection makeConnection:request];
    }
    else
    {
       
    }
    request = nil;
}
                          
#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    [theAppDelegate stopSpinner];
    [self.view setUserInteractionEnabled:YES];
    dict =[[NSMutableDictionary alloc]initWithDictionary:response];
    NSString *strMsg=[dict valueForKey:@"message"];
    if ([strMsg isEqualToString:@"sus"])
    {
        if (_ispurchaseList) {
            
        _arrTblData=[dict valueForKey:@"info"];
        _arrVideoURL=[_arrTblData valueForKey:@"videourl"];
            if([_arrVideoURL count]==0||_arrVideoURL==(id)[NSNull null])
            {
                _btnBuyAll.hidden=YES;
            }
            else
            {
                _btnBuyAll.hidden=NO;
            }
        [_tblChallenges reloadData];
        }
        else if (_isSinglePurchase)
        {
            [self closeViewButonPressed];
            if(theAppDelegate.isAlreadyPurchased)
            {
                _strAlertTitle=@"Theme selection done";
                [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:1];
            }
            NSLog(@"Single Purchase Successfully");
        }
        else if (_isBuyAllList)
        {
            [self closeViewButonPressed];
            if(theAppDelegate.isAlreadyPurchased)
            {
                _strAlertTitle=@"Theme selection done";
                [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:1];
            }
            NSLog(@"Purchase All Successfully");
        }
    }
    else
    {
        if (_ispurchaseList)
        {
            _btnBuyAll.hidden=YES;
            _strAlertTitle=@"No record found";
            [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:1];
        }
        else if (_isSinglePurchase)
        {
            [self closeViewButonPressed];
            _strAlertTitle=@"Failed to purchase";
            [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:1];
        }
        else if (_isBuyAllList)
        {
            if([strMsg isEqualToString:@"all ready"])
            {
                [self closeViewButonPressed];
                _strAlertTitle=@"All ready purchased";
                [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:1];
            }
            else
            {
                [self closeViewButonPressed];
                _strAlertTitle=@"Failed to purchase";
                [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:1];
            }
            
        }

    }
}

#pragma mark: InApp Purchases product loading methods
- (void)productsLoaded:(NSNotification *)notification
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
}

- (void)productPurchased:(NSNotification *)notification
{
    objDBManager=[DBManager getSharedInstance];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    NSString *productIdentifier = (NSString *) notification.object;
    NSLog(@"Purchased: %@", productIdentifier);
    NSDate *currDate = [NSDate date];
    NSDateFormatter *tempDateFormatter = [[NSDateFormatter alloc]init];
    [tempDateFormatter setDateFormat:@"dd-MMM-yy"];
    NSString *strMStartDate= [tempDateFormatter stringFromDate:currDate];
    NSLog(@"%@",strMStartDate);
    
    NSDate *expiryDate=[currDate dateByAddingTimeInterval:30*(60*60*24)];
    NSString *strExpireDate=[tempDateFormatter stringFromDate:expiryDate];
    NSLog(@"%@",strExpireDate);
    
    NSString *strQuery;
    if ([notification.name isEqualToString:@"ProductPurchased"])
    {
        if ([productIdentifier isEqualToString:@"com.Snapbet_10"]) {
        
            strQuery=[NSString stringWithFormat:@"insert into tblInApp values('%@','%@','single','%@','%@')",[theAppDelegate.arrUserLoginData objectAtIndex:0],productIdentifier,strMStartDate,strExpireDate];
            
            if ([objDBManager insertDataWithQuery:strQuery])
            {
                
            }
            else
            {
                
            }

        }
        else if ([productIdentifier isEqualToString:@"com.Snapbet_10"])
        {
            strQuery=[NSString stringWithFormat:@"insert into tblInApp values('%@','%@','All','%@','%@')",[theAppDelegate.arrUserLoginData objectAtIndex:0],productIdentifier,strMStartDate,strExpireDate];
            
            if ([objDBManager insertDataWithQuery:strQuery])
            {
                
            }
            else
            {
                
            }
        }
        _strAlertTitle=@"Theme purchased successfully";
        [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:1];
        NSDate *currDate = [NSDate date];
        NSDateFormatter *tempDateFormatter = [[NSDateFormatter alloc]init];
        [tempDateFormatter setDateFormat:@"dd-MMM-yy"];
        strMStartDate= [tempDateFormatter stringFromDate:currDate];
    }
    [self performSelector:@selector(dismissHUD:) withObject:nil afterDelay:0.0];
}

- (void)productPurchaseFailed:(NSNotification *)notification
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    
    SKPaymentTransaction * transaction = (SKPaymentTransaction *) notification.object;
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        theAppDelegate.alert.title=@"Error";
        theAppDelegate.alert.message=transaction.error.localizedDescription;
        theAppDelegate.alert.show;
        [self performSelector:@selector(dismissHUD:) withObject:nil afterDelay:0.0];
    }
}


- (void)timeout:(id)arg
{
    hud.labelText =@"Timeout!";
    hud.detailsLabelText =@"Please try again later.";
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
    hud.mode = MBProgressHUDModeCustomView;
    [self performSelector:@selector(dismissHUD:) withObject:nil afterDelay:3.0];
    [self performSelector:@selector(showErrorMessage) withObject:nil afterDelay:3.0];
}

- (void)dismissHUD:(id)arg
{
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    hud = nil;
}

- (void) showErrorMessage
{
    theAppDelegate.alert.title=@"Error";
    theAppDelegate.alert.message=@"Please try again later";
    theAppDelegate.alert.show;
}
@end
