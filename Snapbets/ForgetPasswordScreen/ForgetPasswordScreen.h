//
//  ForgetPasswordScreen.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 06/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderAndConstants.h"
#import "WebConnection1.h"
#import "AppDelegate.h"
#import "SignInScreen.h"

@interface ForgetPasswordScreen : UIViewController<WebRequestResult1>

{
    WebConnection1 *connection;
    NSMutableDictionary *dict;
    CGRect vscreenSize;
    CGFloat vscreenHeight,vscreenWidth;
    
}
#pragma mark: UIView Properties declarations here
@property (nonatomic)IBOutlet UIView *viewForMail,*viewForAlert;

#pragma mark: UIImageView Properties declarations here
@property (nonatomic)IBOutlet UIImageView *imgTrangleBG;

#pragma mark: UIButton Properties declarations here
@property (nonatomic)IBOutlet UIButton *btnSubmit,*btnBack,*btnBackPress,*btnLogin,*btnCloseAlert;

#pragma mark: UITextField Properties declarations here
@property (nonatomic)IBOutlet UITextField *txtEmail;

@property (nonatomic)NSString *strAlertTitle;

@property (nonatomic)BOOL isAPIDone;

@end
