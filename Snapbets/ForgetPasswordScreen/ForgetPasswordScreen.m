//
//  ForgetPasswordScreen.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 06/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "ForgetPasswordScreen.h"

@interface ForgetPasswordScreen ()

@end

@implementation ForgetPasswordScreen

- (void)viewDidLoad
{
    [super viewDidLoad];
    _isAPIDone=NO;
    /*----  Load Initial UI     -----*/
            [self loadMainUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark: Load InitialUI
-(void)loadMainUI
{
    /*-----------  Button For Menu Selection for SidebarViewController-----------*/
    
    
    _btnBack=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBack.frame = CGRectMake(5,8,25,25);
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"iconBackArrow"] forState:UIControlStateNormal];
    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnBack];  
    
    /*----------- Button Pressed Event Fired and goto SidebarViewController--------*/
    _btnBackPress=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBackPress.frame = CGRectMake(0,0,105,55);
    [_btnBackPress addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnBackPress];
    
    [_viewForMail setBackgroundColor:[UIColor whiteColor]];
    _viewForMail.layer.cornerRadius=5.0;
    
    //_txtEmail=[[UITextField alloc]initWithFrame:CGRectMake(10,0,_viewForMail.frame.size.width-10,35)];
    [_txtEmail setUserInteractionEnabled:YES];
    [_viewForMail addSubview:_txtEmail];
    
    [self getTextFieldWithProperties:_txtEmail placeholder:@"Email" forLogin:YES];
    
    [_btnLogin addTarget:self action:@selector(btnSendPressed:) forControlEvents:UIControlEventTouchUpInside];
    _btnLogin.backgroundColor=[UIColor blackColor];
    _btnLogin.layer.cornerRadius=5.0;
   /// [self.view addSubview:_btnLogin];
    
    UILabel *lblLogin=[[UILabel alloc]initWithFrame:CGRectMake(10,0,_btnLogin.frame.size.width-10,40)];
    lblLogin.text=@"Send";
    lblLogin.textColor=[UIColor whiteColor];
    lblLogin.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
    //[_btnLogin addSubview:lblLogin];
    
   /* UIImageView *iconSignInFrwd=[[UIImageView alloc]initWithFrame:CGRectMake(_btnLogin.frame.size.width-30,10,20,20)];
    iconSignInFrwd.image=[UIImage imageNamed:@"icon_forward"];
    [_btnLogin addSubview:iconSignInFrwd];*/
}


#pragma mark: Set Proerties for TextFields
-(UITextField*)getTextFieldWithProperties:(UITextField*)textField placeholder:(NSString*)placeholder forLogin:(BOOL)forLogin
{
    //------------- Add properties to textfield ------------- //
    //  textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.placeholder =placeholder;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.keyboardType=UIKeyboardAppearanceDefault;
    
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.keyboardAppearance=UIKeyboardAppearanceLight;
    [textField addTarget:self action:@selector(resignTxt:) forControlEvents:UIControlEventEditingDidEndOnExit];
    _txtEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    _txtEmail.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
    
    _txtEmail.textAlignment=NSTextAlignmentLeft;
    
    _txtEmail.textColor=[UIColor blackColor];
    
    //----------- Validate return keys -------------------- //
    if (forLogin && textField==_txtEmail)
    {        _txtEmail.returnKeyType=UIReturnKeyDone;      }
    
    //---------- Validate keyboard type for email address --------//
    
    if (textField==_txtEmail)
    {        textField.keyboardType=UIKeyboardTypeEmailAddress;    }
    
    return textField;//------- Return fully updated textfield
}

#pragma mark- TextField DelegateMethods
-(void)resignTxt:(UITextField*)txt
{
    //----------- Validate textfield return action ------ //
    if (txt==_txtEmail)
    {
        
    }
    else
    {
        [txt resignFirstResponder];
    }
    //------------------------------------------------ //
}

#pragma mark: TextField Validation Here
-(BOOL)isValidTextfield
{
    NSString *strMsg;
    if(_txtEmail.text!=nil && _txtEmail.text.length>0)
    {
        return YES;
    }
    else
    {
        strMsg=NSLocalizedString(@"Please enter Email ID.",nil);
    }
    
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Warning"  message:strMsg  preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    return NO;
}

#pragma mark: UIButton pressed delegate declaration here
-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)btnSendPressed:(id)sender
{
    if ([self isValidTextfield]) {
        [self ForgetPassServerCall];
    }
}

#pragma mark: view For Alert
-(void)viewForAlertUI
{
    [_viewForAlert removeFromSuperview];
    _viewForAlert =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForAlert.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForAlert.tag=20;
    [self.view addSubview:_viewForAlert];
    
    CGRect frameForViewForHelp=_viewForAlert.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForAlert.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForAlert.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]init];
       
           img.frame=CGRectMake(_viewForAlert.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100);
        
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForAlert addSubview:img];
        UILabel *lblAlert=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForAlert.frame.size.width,30)];
        lblAlert.text=_strAlertTitle;
        lblAlert.textAlignment=NSTextAlignmentCenter;
        lblAlert.textColor=[UIColor whiteColor];
        lblAlert.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForAlert addSubview:lblAlert];
        
        
        _btnCloseAlert=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseAlert.frame=CGRectMake(_viewForAlert.frame.size.width/2-60,CGRectGetMaxY(lblAlert.frame)+50,120,40);
        }
        else
        {
            if ([_strAlertTitle isEqualToString:@"Password sent!"]) {
                UILabel *lblMSG=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblAlert.frame)+10,screenWidth,0)];
                lblMSG.text=@"We have sent a password to your registered email. Please re-login using email and password.";
                [lblMSG setNumberOfLines:0];
                [lblMSG sizeToFit];
                lblMSG.textAlignment=NSTextAlignmentCenter;
                lblMSG.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:14];
                lblMSG.textColor=[UIColor whiteColor];
                [_viewForAlert addSubview:lblMSG];
                _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,CGRectGetMaxY(lblMSG.frame)+50,100,40)];
            }
            else
            {
            _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,CGRectGetMaxY(lblAlert.frame)+50,100,40)];
            }
            
        }
        if (_isAPIDone) {
            [_btnCloseAlert setTitle:@"Login" forState:UIControlStateNormal];
            
        }
        else
        {
            [_btnCloseAlert setTitle:@"Close" forState:UIControlStateNormal];
            
        }
        [_btnCloseAlert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseAlert.layer.borderWidth=2.0;
        _btnCloseAlert.layer.cornerRadius=4.0;
        _btnCloseAlert.layer.borderColor=[[UIColor orangeColor]CGColor];
        //[_btnVoteSeeMore addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [_btnCloseAlert addTarget:self action:@selector(viewForCloseAlert) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewForAlert addSubview:_btnCloseAlert];
    }];
}

#pragma mark-Close Comment View
-(void)viewForCloseAlert
{
    @try
    {
        for(UIView *subview in [_viewForAlert subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForVote=_viewForAlert.frame;
        frameForViewForVote=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            _viewForAlert.frame=frameForViewForVote;
        } completion:^(BOOL finished){
            
            [_viewForAlert removeFromSuperview];
            
            if (_isAPIDone)
            {
                SignInScreen *signIn=[[SignInScreen alloc]initWithNibName:@"SignInScreen" bundle:nil];
                [self.navigationController pushViewController:signIn animated:YES];
            }
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark: Forget Password Server Call
-(void)ForgetPassServerCall
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        
        NSString *urlString=base_URL;
        NSString *strURL=[NSString stringWithFormat:@"wsforgotpwd.php?email=%@",_txtEmail.text];
         urlString=[urlString stringByAppendingString:strURL];
       
        urlString=[urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *urlFrgtPass=[NSURL URLWithString:urlString];
        request=[NSMutableURLRequest requestWithURL:[urlFrgtPass standardizedURL]];
             
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        
    }
    request = nil;
}

#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    [theAppDelegate stopSpinner];
    [self.view setUserInteractionEnabled:YES];
   // [theAppDelegate stopSpinner];
    
    dict =[[NSMutableDictionary alloc]initWithDictionary:response];
    NSString *strMsg=[dict valueForKey:@"message"];
    if ([strMsg isEqualToString:@"sus"])
    {
        _isAPIDone=YES;
        _strAlertTitle=@"Password sent!";
        [self viewForAlertUI];
    }
    else
    {
        _isAPIDone=NO;
        _strAlertTitle=@"Failed to update password";
        [self viewForAlertUI];
    }
    _txtEmail.text=@"";
}
@end
