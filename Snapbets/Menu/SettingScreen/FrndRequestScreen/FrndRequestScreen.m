//
//  FrndRequestScreen.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 14/01/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

#import "FrndRequestScreen.h"

@interface FrndRequestScreen ()

@end

@implementation FrndRequestScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    _arrFrndReqs=[[NSMutableArray alloc]init];
    _isgetDefaultList=NO;
    _isFAccept=NO;
    _isFReject=NO;
    
    [self loadInitialUI];
    [self serverCallForFrndReqsuts];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}
#pragma mark: Load InitialUI
-(void)loadInitialUI
{
    _btnBack=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBack.frame = CGRectMake(5,8,25,25);
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"iconBackArrow.png"] forState:UIControlStateNormal];
    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    _btnBackPress=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBackPress=[[UIButton alloc]initWithFrame:CGRectMake(0,0,55,30)];
    [_btnBackPress addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnBack];
    [self.view addSubview:_btnBackPress];
    
    UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(40,8,screenWidth-80,30)];
    lblTitle.text=@"Friend Request List";
    lblTitle.font=[UIFont fontWithName:@"Avenir-Roman" size:18];
    lblTitle.textColor=[UIColor whiteColor];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:lblTitle];
    
    _tblFReqst=[[UITableView alloc]initWithFrame:CGRectMake(0,50,screenWidth,screenHeight-50)];
    _tblFReqst.delegate=self;
    _tblFReqst.dataSource=self;
    _tblFReqst.tableFooterView=[[UIView alloc]init];
    _tblFReqst.backgroundColor=[UIColor clearColor];
    [self.view addSubview:_tblFReqst];
}

#pragma mark: UIButton Pressed delegate declarations here
-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnAcceptPressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    
    _isgetDefaultList=NO;
    _isFAccept=YES;
    _isFReject=NO;
    NSString *strRID=[[_arrFrndReqs valueForKey:@"uid"]objectAtIndex:row];
    NSString *strStatus=@"A";
    strStatusURL=[NSString stringWithFormat:@"wsfriendacceptreject.php?senderid=%@&receiverid=%@&status=%@",strRID,[theAppDelegate.arrUserLoginData objectAtIndex:0],strStatus];

    [self functionForAcceptReject];
}

-(IBAction)btnRejectPressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    _isgetDefaultList=NO;
    _isFAccept=NO;
    _isFReject=YES;
    
    NSString *strRID=[[_arrFrndReqs valueForKey:@"uid"]objectAtIndex:row];
    NSString *strStatus=@"R";
    strStatusURL=[NSString stringWithFormat:@"wsfriendacceptreject.php?senderid=%@&receiverid=%@&status=%@",strRID,[theAppDelegate.arrUserLoginData objectAtIndex:0],strStatus];
    [self functionForAcceptReject];
}

-(void)functionForAcceptReject
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *urlString=base_URL;
        NSLog(@"%@",[theAppDelegate.arrUserLoginData objectAtIndex:5]);
        NSLog(@"%@",[theAppDelegate.arrUserLoginData objectAtIndex:6]);
        
        urlString=[urlString stringByAppendingString:strStatusURL];
        NSURL *urlSignIn=[NSURL URLWithString:urlString];
        request=[NSMutableURLRequest requestWithURL:[urlSignIn standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"The Internet connection appears to be offline" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    request = nil;
}
/*------    TableView delegate and datasource declaration here   -------*/

#pragma mark- TableView DataSource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_arrFrndReqs count];
}

//--------------------- cellforrow At Index TableView -----------------------//

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@""];
    if (cell == nil)
    {
        //  cell.transform= CGAffineTransformMakeRotation(M_PI/2);
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    UILabel *lblName=[[UILabel alloc]initWithFrame:CGRectMake(5,5,screenWidth-10,30)];
    lblName.text=[[_arrFrndReqs valueForKey:@"name"]objectAtIndex:indexPath.row];
    lblName.font=[UIFont fontWithName:@"Avenir-Roman" size:16];
    [cell.contentView addSubview:lblName];
    
    _btnAccept=[[UIButton alloc]initWithFrame:CGRectMake(10,CGRectGetMaxY(lblName.frame)+5,screenWidth/2-20,35)];
    [_btnAccept setTitle:@"Accept" forState:UIControlStateNormal];
    [_btnAccept setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _btnAccept.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:18];
    _btnAccept.backgroundColor=[UIColor orangeColor];
    _btnAccept.layer.cornerRadius=5.0;
    [_btnAccept addTarget:self action:@selector(btnAcceptPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.contentView addSubview:_btnAccept];

    _btnReject=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth/2+10,CGRectGetMaxY(lblName.frame)+5,screenWidth/2-20,35)];
    [_btnReject setTitle:@"Reject" forState:UIControlStateNormal];
    [_btnReject setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _btnReject.titleLabel.font=[UIFont fontWithName:@"Avenir-Roman" size:16];
    _btnReject.backgroundColor=[UIColor orangeColor];
    _btnReject.layer.cornerRadius=5.0;
    [_btnReject addTarget:self action:@selector(btnRejectPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.contentView addSubview:_btnReject];
    
    cell.backgroundColor=[[UIColor whiteColor]colorWithAlphaComponent:.6];
    UIView *viewForCellSept=[[UIView alloc]initWithFrame:CGRectMake(0,79,screenWidth,1)];
    viewForCellSept.backgroundColor=[UIColor darkGrayColor];
    [cell.contentView addSubview:viewForCellSept];
    return cell;
}

#pragma Mark-delegate methods
//--------------------- Tableview didselected index ---------------------//
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}


#pragma mark: User SignIn Server Call
-(void)serverCallForFrndReqsuts
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isgetDefaultList=YES;
        _isFAccept=NO;
        _isFReject=NO;
        
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *urlString=base_URL;
        NSLog(@"%@",[theAppDelegate.arrUserLoginData objectAtIndex:5]);
        NSLog(@"%@",[theAppDelegate.arrUserLoginData objectAtIndex:6]);
        
        NSString *strURL=[NSString stringWithFormat:@"wsfriendrequestlist.php?uid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0]];
        urlString=[urlString stringByAppendingString:strURL];
        NSURL *urlSignIn=[NSURL URLWithString:urlString];
        request=[NSMutableURLRequest requestWithURL:[urlSignIn standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"The Internet connection appears to be offline" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    request = nil;
}

#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    [theAppDelegate stopSpinner];
    [self.view setUserInteractionEnabled:YES];
    // [theAppDelegate stopSpinner];
    
    dict =[[NSMutableDictionary alloc]initWithDictionary:response];
    NSString *strMsg=[dict valueForKey:@"message"];
    NSLog(@"%@",strMsg);
    if ([strMsg isEqualToString:@"sus"])
    {
        if(_isgetDefaultList)
        {
            NSMutableArray *arrInfo=[dict valueForKey:@"info"];
            NSLog(@"%@",arrInfo);
            
            if([arrInfo isKindOfClass:[NSNull class]])
            {
                arrInfo=[[NSMutableArray alloc]init];
                _arrFrndReqs=[[NSMutableArray alloc]init];
                [_tblFReqst reloadData];
            }
            else
            {
                _arrFrndReqs=arrInfo;
                NSLog(@"%@",_arrFrndReqs);
                [_tblFReqst reloadData];
            }
        }
        else if (_isFAccept)
        {
            _strAlertTitle=@"Added in friend list!!";
            [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:0.5];
        }
        else if (_isFReject)
        {
            _strAlertTitle=@"Request rejected !!";
            [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:0.5];
        }
    }
}

#pragma mark: view For Alert
-(void)viewForAlertUI
{
    [_viewForAlert removeFromSuperview];
    _viewForAlert =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForAlert.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForAlert.tag=20;
    [self.view addSubview:_viewForAlert];
    
    CGRect frameForViewForHelp=_viewForAlert.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForAlert.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForAlert.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForAlert addSubview:img];
        UILabel *lblAlert=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForAlert.frame.size.width,30)];
        lblAlert.text=_strAlertTitle;
        lblAlert.textAlignment=NSTextAlignmentCenter;
        lblAlert.textColor=[UIColor whiteColor];
        lblAlert.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForAlert addSubview:lblAlert];
        
        _btnCloseAlert=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseAlert.frame=CGRectMake(_viewForAlert.frame.size.width/2-60,CGRectGetMaxY(lblAlert.frame)+50,120,40);
        }
        else
        {
            _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,CGRectGetMaxY(lblAlert.frame)+50,100,40)];
        }
        
        [_btnCloseAlert setTitle:@"Close" forState:UIControlStateNormal];
        [_btnCloseAlert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseAlert.layer.borderWidth=2.0;
        _btnCloseAlert.layer.cornerRadius=4.0;
        _btnCloseAlert.layer.borderColor=[[UIColor orangeColor]CGColor];
        [_btnCloseAlert addTarget:self action:@selector(viewForCloseAlert) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewForAlert addSubview:_btnCloseAlert];
    }];
}

#pragma mark-Close Comment View
-(void)viewForCloseAlert
{
     [self serverCallForFrndReqsuts];
    @try
    {
        for(UIView *subview in [_viewForAlert subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForVote=_viewForAlert.frame;
        frameForViewForVote=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            _viewForAlert.frame=frameForViewForVote;
        } completion:^(BOOL finished){
            
            [_viewForAlert removeFromSuperview];
           
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

@end
