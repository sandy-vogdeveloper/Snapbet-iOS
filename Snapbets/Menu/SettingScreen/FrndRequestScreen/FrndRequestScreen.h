//
//  FrndRequestScreen.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 14/01/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderAndConstants.h"
#import "WebConnection1.h"
#import "AppDelegate.h"

@interface FrndRequestScreen : UIViewController<UITableViewDataSource,UITableViewDelegate,WebRequestResult1>

{
    WebConnection1 *connection;
    NSMutableDictionary *dict;
    NSString *strStatusURL;
    CGRect vscreenSize;
    CGFloat vscreenHeight,vscreenWidth;
}

#pragma mark: UIView properties declarations here
@property (nonatomic)IBOutlet UIView *viewForAlert;

#pragma mark: UIButton properties declarations here
@property (nonatomic)IBOutlet UIButton *btnBack,*btnBackPress,*btnAccept,*btnReject,*btnCloseAlert;

#pragma mark: UITableView properties declarations here
@property (nonatomic)IBOutlet UITableView *tblFReqst;

#pragma mark: NSMutableArray properties declarations here
@property (nonatomic)NSMutableArray *arrFrndReqs;

#pragma mark: NSString properties declarations here
@property (nonatomic)NSString *strAlertTitle;

#pragma mark:BOOL properties declaraitons here
@property (nonatomic)BOOL isgetDefaultList,isFAccept,isFReject;
@end
