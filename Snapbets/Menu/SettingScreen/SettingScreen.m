//
//  SettingScreen.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 08/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "SettingScreen.h"

@interface SettingScreen ()

@end

@implementation SettingScreen

- (void)viewDidLoad
{
    [super viewDidLoad];
    /*---   Load Initial UI ---*/
        [self loadInitialUI];
        [self ShowDefaultSettingAPI];
    _isAFrndDisable=YES;
    _isSOn=NO;
    _isSOn1=NO;
    _isSOn2=NO;
    _isSOn3=NO;
    
    _arrSectionList=[[NSMutableArray alloc]initWithObjects:@"   Account",@"   Friends",@"   Notifications",@"   Privacy",@"   Blocked Users",@"   Help",@"   Accounts",nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark:load InitialUI
-(void)loadInitialUI
{
    _btnBack=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBack.frame = CGRectMake(5,8,25,25);
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"iconBackArrow.png"] forState:UIControlStateNormal];
    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    _btnBackPress=[[UIButton alloc]initWithFrame:CGRectMake(0,0,55,30)];
    [_btnBackPress addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];    
    [self.view addSubview:_btnBack];
    [self.view addSubview:_btnBackPress];
    
    UIImageView *imgBG=[[UIImageView alloc]init];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        imgBG.frame=CGRectMake(0,44,screenWidth,screenHeight-117);
    }
    else
    {
        imgBG.frame=CGRectMake(0,44,screenWidth,screenHeight-90);
    }
    
    imgBG.image=[UIImage imageNamed:@"img_notificationBG"];
    [self.view addSubview:imgBG];
    
        //Button Properties declarations here
    [_btnLogOut addTarget:self action:@selector(btnLogOutPressed:) forControlEvents:UIControlEventTouchUpInside];
    _tblSetting=[[UITableView alloc]initWithFrame:CGRectMake(0,45,screenWidth,screenHeight-105)];
    _tblSetting.dataSource=self;
    _tblSetting.delegate=self;
    _tblSetting.tableFooterView=[[UIView alloc]init];
    _tblSetting.backgroundColor=[UIColor clearColor];
    [self.view addSubview:_tblSetting];
}
-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark-Close Comment View
-(void)closeTTandPPViewPressed
{
    @try
    {
        _viewForTandC.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
        _viewForPandP.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
        
        for(UIView *subview in [_viewForTandC subviews])
        {
            [subview removeFromSuperview];
        }
        for(UIView *subview in [_viewForPandP subviews])
        {
            [subview removeFromSuperview];
        }
        for(UIView *subview in [_viewForSocialLinking subviews])
        {
            [subview removeFromSuperview];
        }
        CGRect frameForViewForTT=_viewForTandC.frame;
        CGRect frameForViewSocial=_viewForSocialLinking.frame;
        CGRect frameForViewPandP=_viewForPandP.frame;
        
        frameForViewPandP=CGRectMake(30,screenHeight/2,screenWidth-60,0);
        frameForViewForTT=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        frameForViewSocial=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        [UIView animateWithDuration:0.5f animations:^{
            _viewForTandC.frame=frameForViewForTT;
            _viewForSocialLinking.frame=frameForViewSocial;
            _viewForPandP.frame=frameForViewPandP;
            
                 } completion:^(BOOL finished){
            [_viewForTandC removeFromSuperview];
            [_viewForSocialLinking removeFromSuperview];
            [_viewForPandP removeFromSuperview];
            //[self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}
#pragma mark-Close SocialLinking View
-(void)closeSocialLinkingView
{
    @try
    {
        _viewForSocialLinking.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
        
        for(UIView *subview in [_viewForSocialLinking subviews])
        {
            [subview removeFromSuperview];
        }
       
        CGRect frameForViewSocial=_viewForSocialLinking.frame;
        frameForViewSocial=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForSocialLinking.frame=frameForViewSocial;
        } completion:^(BOOL finished){
           
            [_viewForSocialLinking removeFromSuperview];
            //[self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}
#pragma mark: view For Terms & Condition
-(void)viewForTermsAndCondition
{
    [_viewForTandC removeFromSuperview];
    _viewForTandC =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForTandC.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForTandC.tag=20;
    [self.view addSubview:_viewForTandC];
    CGRect frameForViewForHelp=_viewForTandC.frame;
    
    frameForViewForHelp=CGRectMake(10,40,screenWidth-20,screenHeight-80);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForTandC.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        _viewForTandC.backgroundColor=[UIColor clearColor];
        vscreenSize=_viewForTandC.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,_viewForTandC.frame.size.width,_viewForTandC.frame.size.height)];
        img.image=[UIImage imageNamed:@"img_T&C"];
        [_viewForTandC addSubview:img];
        
        UILabel *lblTerms=[[UILabel alloc]initWithFrame:CGRectMake(0,10,img.frame.size.width,50)];
        lblTerms.text=@"Terms of Service";
        lblTerms.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:35];
        lblTerms.textColor=[UIColor whiteColor];
        lblTerms.textAlignment=NSTextAlignmentCenter;
        [img addSubview:lblTerms];
        
        UIView *viewSept=[[UIView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblTerms.frame)-5,img.frame.size.width,2)];
        [viewSept setBackgroundColor:[UIColor colorWithRed:237/255.0 green:124/255.0 blue:8/255.0 alpha:1]];
        [img addSubview:viewSept];
        
        UILabel *lbltermsOfService=[[UILabel alloc]initWithFrame:CGRectMake(0,img.frame.size.height/2.5-15,img.frame.size.width,30)];
        lbltermsOfService.text=@"Terms of Service is here";
        lbltermsOfService.textColor=[UIColor whiteColor];
        lbltermsOfService.textAlignment=NSTextAlignmentCenter;
        lbltermsOfService.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [img addSubview:lbltermsOfService];        
        
        _btnCloseTandC=[[UIButton alloc]initWithFrame:CGRectMake(_viewForTandC.frame.size.width-60,_viewForTandC.frame.size.height-60,40,40)];
        [_btnCloseTandC setBackgroundImage:[UIImage imageNamed:@"btn_Close"] forState:UIControlStateNormal];
        [_btnCloseTandC addTarget:self action:@selector(closeTTandPPViewPressed) forControlEvents:UIControlEventTouchUpInside];
        [_viewForTandC addSubview:_btnCloseTandC];
        
        UIWebView *webData=[[UIWebView alloc]initWithFrame:CGRectMake(10,CGRectGetMaxY(viewSept.frame)+5,_viewForTandC.frame.size.width-20,_viewForTandC.frame.size.height-130)];
        webData.backgroundColor=[UIColor clearColor];
        NSString *urlString=base_URL;
        NSString *strURL=[NSString stringWithFormat:@"wsterm_condation.php"];
        urlString=[urlString stringByAppendingString:strURL];
        
        [webData loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
        [_viewForTandC addSubview:webData];
    }];
}

#pragma mark: view For privacy policy
-(void)viewForPrivcyAndPolicy
{
    [_viewForPandP removeFromSuperview];
    _viewForPandP =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForPandP.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForPandP.tag=20;
    [self.view addSubview:_viewForPandP];
    CGRect frameForViewForHelp=_viewForPandP.frame;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        frameForViewForHelp=CGRectMake(10,70,screenWidth-20,screenHeight-80);
    }
    else
    {
        frameForViewForHelp=CGRectMake(10,40,screenWidth-20,screenHeight-80);
    }
    [UIView animateWithDuration:0.5f animations:^{
        _viewForPandP.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        _viewForPandP.backgroundColor=[UIColor clearColor];
        vscreenSize=_viewForPandP.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,_viewForPandP.frame.size.width,_viewForPandP.frame.size.height)];
        img.image=[UIImage imageNamed:@"img_T&C"];
        [_viewForPandP addSubview:img];
        
        UILabel *lblPolicy=[[UILabel alloc]initWithFrame:CGRectMake(0,10,img.frame.size.width,50)];
        lblPolicy.text=@"Privacy Policy";
        lblPolicy.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:35];
        lblPolicy.textColor=[UIColor whiteColor];
        lblPolicy.textAlignment=NSTextAlignmentCenter;
        [img addSubview:lblPolicy];
        
        UIView *viewSept=[[UIView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblPolicy.frame)-5,img.frame.size.width,2)];
        [viewSept setBackgroundColor:[UIColor colorWithRed:237/255.0 green:124/255.0 blue:8/255.0 alpha:1]];
        [img addSubview:viewSept];
        
        
        UILabel *lblPP=[[UILabel alloc]initWithFrame:CGRectMake(0,img.frame.size.height/2.5-15,img.frame.size.width,30)];
        lblPP.text=@"Privacy Policy is here";
        lblPP.textColor=[UIColor whiteColor];
        lblPP.textAlignment=NSTextAlignmentCenter;
        lblPP.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [img addSubview:lblPP];
        
        _btnClosePandP=[[UIButton alloc]initWithFrame:CGRectMake(_viewForPandP.frame.size.width-60,_viewForPandP.frame.size.height-60,40,40)];
        [_btnClosePandP setBackgroundImage:[UIImage imageNamed:@"btn_Close"] forState:UIControlStateNormal];
        [_btnClosePandP addTarget:self action:@selector(closeTTandPPViewPressed) forControlEvents:UIControlEventTouchUpInside];
        [_viewForPandP addSubview:_btnClosePandP];
        
        UIWebView *webData=[[UIWebView alloc]initWithFrame:CGRectMake(10,CGRectGetMaxY(viewSept.frame)+5,_viewForPandP.frame.size.width-20,_viewForPandP.frame.size.height-130)];
        webData.backgroundColor=[UIColor clearColor];
        NSString *urlString=base_URL;
        NSString *strURL=[NSString stringWithFormat:@"wsprivacy_policy.php"];
        urlString=[urlString stringByAppendingString:strURL];
        
        [webData loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
        [_viewForPandP addSubview:webData];
    }];
}


#pragma mark: view For Share
-(void)viewForSocialLinkingUI
{
    [_viewForSocialLinking removeFromSuperview];
    _viewForSocialLinking =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForSocialLinking.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForSocialLinking.tag=20;
    [self.view addSubview:_viewForSocialLinking];
    CGRect frameForViewForHelp=_viewForSocialLinking.frame;
    
    frameForViewForHelp=CGRectMake(5,20,screenWidth-10,screenHeight-40);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForSocialLinking.frame=frameForViewForHelp;
    } completion:^(BOOL finished)
    {
        _viewForSocialLinking.backgroundColor=[UIColor clearColor];
        vscreenSize=_viewForSocialLinking.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,_viewForSocialLinking.frame.size.width,_viewForSocialLinking.frame.size.height)];
        img.image=[UIImage imageNamed:@"img_T&C"];
        [_viewForSocialLinking addSubview:img];
        
        _btnCloseTandC=[[UIButton alloc]initWithFrame:CGRectMake(_viewForSocialLinking.frame.size.width-60,_viewForSocialLinking.frame.size.height-50,40,40)];
        [_btnCloseTandC setBackgroundImage:[UIImage imageNamed:@"btn_Close"] forState:UIControlStateNormal];
        [_btnCloseTandC addTarget:self action:@selector(closeSocialLinkingView) forControlEvents:UIControlEventTouchUpInside];
        [_viewForSocialLinking addSubview:_btnCloseTandC];
        UIWebView *webData=[[UIWebView alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            webData.frame=CGRectMake(10,60,_viewForSocialLinking.frame.size.width-20,_viewForSocialLinking.frame.size.height-120);
        }
        else
        {
            webData.frame=CGRectMake(10,30,_viewForSocialLinking.frame.size.width-20,_viewForSocialLinking.frame.size.height-85);
        }
        webData.backgroundColor=[UIColor clearColor];
        
        NSString *strURL=[NSString stringWithFormat:@"%@", _strSocialURL];
        
        [webData loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strURL]]];
        [_viewForSocialLinking addSubview:webData];
    }];
}

#pragma mark Help View
-(void)showHelpView
{
    [_viewForTutorial removeFromSuperview];
    [_btnClose removeFromSuperview];
    [_btnNext removeFromSuperview];
    
    _viewForTutorial =[[UIView alloc]initWithFrame:CGRectMake(0,0,screenWidth,screenHeight)];
    _viewForTutorial.backgroundColor=[UIColor blackColor];
    _viewForTutorial.alpha=1;
    _viewForTutorial.tag=20;
    [self.view addSubview:_viewForTutorial];
    
//    UIView *viewGray=[[UIView alloc]initWithFrame:CGRectMake(0,0,screenWidth,screenHeight-100)];
//    viewGray.backgroundColor=[UIColor whiteColor];
//    [_viewForTutorial addSubview:viewGray];
    
    CGRect frameForViewForHelp=_viewForTutorial.frame;
    frameForViewForHelp=CGRectMake(0,0, screenWidth, screenHeight);
    
    [UIView animateWithDuration:0.5f animations:^{
        _viewForTutorial.frame=frameForViewForHelp;
    } completion:^(BOOL finished)
     {
         vscreenSize=_viewForTutorial.frame;
         vscreenHeight=vscreenSize.size.height;
         vscreenWidth=vscreenSize.size.width;
         
         _btnClose=[[UIButton alloc]initWithFrame:CGRectMake(20,screenHeight-40,70,30)];
         [_btnClose setTitle:@"Skip" forState:UIControlStateNormal];
         _btnClose.backgroundColor=[UIColor orangeColor];
         
         [_btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         _btnClose.layer.borderColor=[UIColor orangeColor].CGColor;
         _btnClose.layer.borderWidth=2;
         
         self.btnClose.layer.cornerRadius = 2;
         self.btnClose.clipsToBounds = YES;
         [_btnClose addTarget:self action:@selector(closeHelpView) forControlEvents:UIControlEventTouchUpInside];
         [self.view addSubview:_btnClose];
                 
         self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
         
         self.pageController.dataSource = self;
         [[self.pageController view] setFrame:CGRectMake(0,0, vscreenWidth, vscreenHeight-50)];
         
         APPChildViewController *initialViewController = [self viewControllerAtIndex:0];
         NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
         [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
         [self addChildViewController:self.pageController];
         [_viewForTutorial addSubview:[self.pageController view]];
         
         [self.pageController didMoveToParentViewController:self];
         
         _btnNext=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-80,screenHeight-40,70,30)];
         [_btnNext setTitle:@"Next" forState:UIControlStateNormal];
         
         [_btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         _btnNext.layer.borderColor=[UIColor orangeColor].CGColor;
         _btnNext.layer.borderWidth=2;
         _btnNext.backgroundColor=[UIColor orangeColor];
         self.btnNext.layer.cornerRadius = 2;
         self.btnNext.clipsToBounds = YES;
         [_btnNext addTarget:self action:@selector(btnNextPressed) forControlEvents:UIControlEventTouchUpInside];
         [_btnNext setUserInteractionEnabled:YES];
         [_viewForTutorial bringSubviewToFront:_btnNext];
         [_viewForTutorial addSubview:_btnNext];
         
         UIView *currentView=[self.view viewWithTag:447];
         currentView.alpha=1;
         
     }];
    
    [self disableSubViews:_viewForTutorial];
}
-(void)closeHelpView
{
    for(UIView *subview in [_viewForTutorial subviews])
    {
        [subview removeFromSuperview];
        [self.btnClose removeFromSuperview];
        [_btnNext removeFromSuperview];
    }
    
    CGRect frameForViewForHelp=_viewForTutorial.frame;
    frameForViewForHelp=CGRectMake(0, screenHeight/2,screenWidth,0);
    
    [UIView animateWithDuration:0.5f animations:^{
        _viewForTutorial.frame=frameForViewForHelp;
    } completion:^(BOOL finished)
     {
         [_viewForTutorial removeFromSuperview];
         [self enableSubViews];
     }];
}

-(UIViewController*)randomVC
{
    UIViewController *vc = [[UIViewController alloc] init];
    UIColor *color = [UIColor colorWithRed:arc4random_uniform(255)/255.0 green:arc4random_uniform(255)/255.0 blue:arc4random_uniform(255)/255.0 alpha:1];
    vc.view.backgroundColor = color;
    return vc;
}

-(void)btnNextPressed
{
    for(UIView *subview in [_viewForTutorial subviews])
    {
        NSLog(@"%@",subview);
        
        NSLog(@"Button Index------------------------------%ld",(long)curntIndexPge);
    }
    
    CGRect frameForViewForHelp=_viewForTutorial.frame;
    
    [UIView animateWithDuration:0.5f animations:^{
        _viewForTutorial.frame=frameForViewForHelp;
    } completion:^(BOOL finished)
     {
         if (curntIndexPge < 3)
         {
             curntIndexPge =curntIndexPge+1;
         }
         else
         {
             if (curntIndexPge == 3)
             {
                 // [_btnNext removeFromSuperview];
                 
                 /* _btnFinish=[[UIButton alloc]initWithFrame:CGRectMake(80, screenHeight-90,screenWidth-160, 40)];
                  [_btnFinish setTitle:@"Finish" forState:UIControlStateNormal];
                  [_btnFinish addTarget:self action:@selector(btnFinishPressed) forControlEvents:UIControlEventTouchUpInside];
                  _btnFinish.layer.cornerRadius=18;
                  _btnFinish.layer.masksToBounds=YES;
                  _btnFinish.titleLabel.font=[UIFont fontWithName:@"OpenSans" size:13];
                  _btnFinish.backgroundColor=[UIColor colorWithRed:145/255.0 green:169/255.0 blue:222/255.0 alpha:1.0];
                  [self.view addSubview:_btnFinish];*/
             }
         }
         APPChildViewController *startingViewController = [self viewControllerAtIndex:curntIndexPge];
         NSArray *viewControllers = @[startingViewController];
         [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
     }];
    
}


-(IBAction)btnLogOutPressed:(id)sender
{
    [self LogOutServerCall];
}
#pragma mark- Page View Methods

#pragma mark- Page View Methods

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [(APPChildViewController *)viewController index];
    if (index == 0) {
        return nil;
    }
    index--;
    curntIndexPge=index;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(APPChildViewController *)viewController index];
    index++;
    curntIndexPge=index;
    if (index == 4)
    {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (APPChildViewController *)viewControllerAtIndex:(NSUInteger)index
{
    curntIndexPge=index;
    APPChildViewController *cvc = [[APPChildViewController alloc] initWithNibName:@"APPChildViewController" bundle:nil];
    UIPageControl *pageControlAppearance = [UIPageControl appearanceWhenContainedIn:[UIPageViewController class], nil];
    pageControlAppearance.pageIndicatorTintColor = [UIColor darkGrayColor];
    pageControlAppearance.currentPageIndicatorTintColor = [UIColor whiteColor];
    
    if (index==0)
    {
        cvc.strImgName=@"t1.1";
        
    }
    else if (index==1)
    {
        cvc.strImgName=@"t2";
        
    }
    else if (index==2)
    {
        cvc.strImgName=@"t3.1";
        
        
    }
    else if (index==3)
    {
        cvc.strImgName=@"t4";
        
    }
  
    
    cvc.index = index;
    return cvc;
}


- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return 4;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return curntIndexPge;
}
-(void)disableSubViews:(UIView *)view
{
    for(UIView *subview in [self.view subviews])
    {
        if ((subview.tag==view.tag&&[subview isMemberOfClass:[UIView class]]))
        {}
        else
        {
            subview.userInteractionEnabled=NO;
            subview.alpha=0.3;
        }
    }
}
-(void)enableSubViews
{
    for(UIView *subview in [self.view subviews])
    {
        subview.userInteractionEnabled=YES;
        subview.alpha=1;
    }
}

#pragma mark: UITableview delegate and datasource methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_arrSectionList count];
}
#pragma mark- TableView DataSource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return 1;
    }
    else if (section==1)
    {
        if(_isAFrndDisable)
        {
            return 3;
        }
        else
        {
            return 2;
        }
    }
    else if (section==2||section==3||section==4)
    {
        return 1;
    }
    else if (section==5)
    {
        return 4;
    }
    if(section==6)
    {
        return 2;
    }
    return 0;
}

//--------------------- cellforrow At Index TableView -----------------------//

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@""];
    if (cell == nil)
    {
        //  cell.transform= CGAffineTransformMakeRotation(M_PI/2);
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    UILabel *lblCellTitle=[[UILabel alloc]initWithFrame:CGRectMake(5,2,screenWidth-10,30)];
    UIImageView *imgForward=[[UIImageView alloc]init];
    imgForward.frame=CGRectMake(screenWidth-40,8,20,20);
    
    if (indexPath.section==0)
    {
        lblCellTitle.text=@"  Update Profile";
        imgForward.image=[UIImage imageNamed:@"icon_BlckFrwd"];
    }
    else if (indexPath.section==1)
    {
        if (indexPath.row==0)
        {
            lblCellTitle.text=@"  Auto-accept new friend";
            UIView *viewForCellSept=[[UIView alloc]initWithFrame:CGRectMake(0,34,screenWidth,1.0)];
            viewForCellSept.backgroundColor=[UIColor orangeColor];
            [cell.contentView addSubview:viewForCellSept];
            
            [_switchAutoAccNFriend removeFromSuperview];
            _switchAutoAccNFriend=[[UISwitch alloc]initWithFrame:CGRectMake(screenWidth-50,2,50,25)];
            _switchAutoAccNFriend.onTintColor=[UIColor orangeColor];
            _switchAutoAccNFriend.transform=CGAffineTransformMakeScale(0.75,0.75);
            [cell.contentView addSubview:_switchAutoAccNFriend];
            _switchAutoAccNFriend.tag=indexPath.row;
            
            if (!_isSOn) {
                [_switchAutoAccNFriend setOn:YES animated:YES];
            }
            else
            {
                [_switchAutoAccNFriend setOn:NO animated:YES];
            }
            
            [cell.contentView addSubview:imgForward];
            imgForward.image=[UIImage imageNamed:@""];
        }
        else
        {
            if(_isAFrndDisable)
            {
                if(indexPath.row==1)
                {
                lblCellTitle.text=@"  Friend Request List";
                
                }
                else
                {
                    lblCellTitle.text=@"  Add all Facebook friends";
                    
                    [_switchAllFbFrnds removeFromSuperview];
                    _switchAllFbFrnds=[[UISwitch alloc]initWithFrame:CGRectMake(screenWidth-50,2,50,25)];
                    _switchAllFbFrnds.onTintColor=[UIColor orangeColor];
                    _switchAllFbFrnds.transform=CGAffineTransformMakeScale(0.75,0.75);
                    [cell.contentView addSubview:_switchAllFbFrnds];
                    _switchAllFbFrnds.tag=indexPath.row;
                    
                    if (_isSOn1)
                    {
                        [_switchAllFbFrnds setOn:YES animated:YES];
                    }
                    else
                    {
                        [_switchAllFbFrnds setOn:NO animated:YES];
                    }
                    [cell.contentView addSubview:imgForward];
                    imgForward.image=[UIImage imageNamed:@""];
                }
            }
            else
            {
                lblCellTitle.text=@"  Add all Facebook friends";
                
                [_switchAllFbFrnds removeFromSuperview];
                _switchAllFbFrnds=[[UISwitch alloc]initWithFrame:CGRectMake(screenWidth-50,2,50,25)];
                _switchAllFbFrnds.onTintColor=[UIColor orangeColor];
                _switchAllFbFrnds.transform=CGAffineTransformMakeScale(0.75,0.75);
                [cell.contentView addSubview:_switchAllFbFrnds];
                _switchAllFbFrnds.tag=indexPath.row;
                
                if (_isSOn1) {
                    [_switchAllFbFrnds setOn:YES animated:YES];
                }
                else
                {
                    [_switchAllFbFrnds setOn:NO animated:YES];
                }
                [cell.contentView addSubview:imgForward];
                imgForward.image=[UIImage imageNamed:@""];
            }
            
        }
        
       
    }
    else if (indexPath.section==2)
    {
        lblCellTitle.text=@"  Silent Notifications";
        imgForward.image=[UIImage imageNamed:@""];
        
        [_switchSilenceNoti removeFromSuperview];
        _switchSilenceNoti=[[UISwitch alloc]initWithFrame:CGRectMake(screenWidth-50,2,50,25)];
        _switchSilenceNoti.onTintColor=[UIColor orangeColor];
        _switchSilenceNoti.transform=CGAffineTransformMakeScale(0.75,0.75);
        _switchSilenceNoti.tag=indexPath.row;
       
        if (_isSOn2) {
            [_switchSilenceNoti setOn:YES animated:YES];
        }
        else
        {
            [_switchSilenceNoti setOn:NO animated:YES];
        }
        [cell.contentView addSubview:_switchSilenceNoti];
        
        [cell.contentView addSubview:imgForward];
    }
    else if (indexPath.section==3)
    {
        lblCellTitle.text=@"  Set Profile to Private";
        imgForward.image=[UIImage imageNamed:@""];
        
        [_switchProfileToPrivate removeFromSuperview];
        _switchProfileToPrivate=[[UISwitch alloc]initWithFrame:CGRectMake(screenWidth-50,2,50,25)];
        _switchProfileToPrivate.onTintColor=[UIColor orangeColor];
        _switchProfileToPrivate.transform=CGAffineTransformMakeScale(0.75,0.75);
        [cell.contentView addSubview:_switchProfileToPrivate];
        
        if (_isSOn3) {
            [_switchProfileToPrivate setOn:YES animated:YES];
        }
        else
        {
             [_switchProfileToPrivate setOn:NO animated:YES];
        }
        _switchProfileToPrivate.tag=indexPath.row;
    }
    else if (indexPath.section==4)
    {
        lblCellTitle.text=@"  Blocked Users";
        imgForward.image=[UIImage imageNamed:@"icon_BlckFrwd"];
       // _switchSetting.hidden=YES;
    }
    else if (indexPath.section==5)
    {
        if (indexPath.row==0) {
            lblCellTitle.text=@"  Tutorial";
            UIView *viewForCellSept=[[UIView alloc]initWithFrame:CGRectMake(0,34,screenWidth,1.0)];
            viewForCellSept.backgroundColor=[UIColor orangeColor];
            [cell.contentView addSubview:viewForCellSept];
        }
        else if(indexPath.row==1)
        {
            lblCellTitle.text=@"  Terms and Conditions";
        }
        else if (indexPath.row==2)
        {
            lblCellTitle.text=@"  Privacy Policy";
        }
        else if (indexPath.row==3)
        {
            lblCellTitle.text=@"  Feedback";
        }
        UIView *viewForCellSept=[[UIView alloc]initWithFrame:CGRectMake(0,34,screenWidth,1)];
        viewForCellSept.backgroundColor=[UIColor darkGrayColor];
        imgForward.image=[UIImage imageNamed:@"icon_BlckFrwd"];
       // _switchSetting.hidden=YES;
    }
    else
    {
        //_switchSetting.hidden=YES;
        UIImageView *imgFBTwit=[[UIImageView alloc]initWithFrame:CGRectMake(10,8,20,20)];
        lblCellTitle.frame=CGRectMake(CGRectGetMaxX(imgFBTwit.frame)+5,5,cell.frame.size.width,30);
        if (indexPath.row==0) {
            lblCellTitle.text=@"  Facebook";
            
           imgFBTwit.image=[UIImage imageNamed:@"img_SetFB"];
           
            UIView *viewForCellSept=[[UIView alloc]initWithFrame:CGRectMake(0,34,screenWidth,1.0)];
            viewForCellSept.backgroundColor=[UIColor orangeColor];
            [cell.contentView addSubview:viewForCellSept];
        }
        else if (indexPath.row==1)
        {
            lblCellTitle.text=@"  Twitter";
            imgFBTwit.image=[UIImage imageNamed:@"img_SetTwit"];
            UIView *viewForCellSept=[[UIView alloc]initWithFrame:CGRectMake(0,34,screenWidth,1.0)];
            viewForCellSept.backgroundColor=[UIColor orangeColor];
            [cell.contentView addSubview:viewForCellSept];
        }
       
         [cell.contentView addSubview:imgFBTwit];
       
        lblCellTitle.textColor=[UIColor blackColor];
        imgForward.image=[UIImage imageNamed:@"icon_BlckFrwd"];
        UILabel *lblConnect=[[UILabel alloc]initWithFrame:CGRectMake(screenWidth-100,3,100,30)];
        lblConnect.text=@"Connect";
        lblConnect.textColor=[UIColor blackColor];
        lblConnect.font=[UIFont fontWithName:@"AvenirNext-Regular" size:15];
        [cell.contentView addSubview:lblConnect];
    }
    
    [_switchAutoAccNFriend addTarget:self action:@selector(updateSwitch1:)forControlEvents:UIControlEventValueChanged];
    [_switchAllFbFrnds addTarget:self action:@selector(updateSwitch2:)forControlEvents:UIControlEventValueChanged];
    [_switchProfileToPrivate addTarget:self action:@selector(updateSwitch4:)forControlEvents:UIControlEventValueChanged];
    [_switchSilenceNoti addTarget:self action:@selector(updateSwitch3:)forControlEvents:UIControlEventValueChanged];

    lblCellTitle.font=[UIFont fontWithName:@"AvenirNext-Regular" size:15];
    
    [cell.contentView addSubview:lblCellTitle];
    cell.backgroundColor=[UIColor clearColor];
    return cell;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    _lblTags=[[UILabel alloc] init];
    _lblTags.font=[UIFont fontWithName:@"AvenirNext-Regular" size:16];
    NSString *strSelectCat;
    for (int i=0;i<[_arrSectionList count];i++)
    {
        strSelectCat=[_arrSectionList objectAtIndex:section];
        _lblTags.text=[strSelectCat uppercaseString];
    }
    _lblTags.textColor=[UIColor blackColor];
    _lblTags.backgroundColor=[[UIColor whiteColor]colorWithAlphaComponent:.6];
    CGSize sizeSelectCat=[strSelectCat sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-Regular" size:16]}];
    [_lblTags setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/100*20,25,sizeSelectCat.width, 30)];
    [_lblTags sizeToFit];
    return _lblTags;
}
#pragma Mark-delegate methods
//--------------------- Tableview didselected index ---------------------//
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        UpdateProfileScreen *updteProf=[[UpdateProfileScreen alloc]initWithNibName:@"UpdateProfileScreen" bundle:nil];
        [self.navigationController pushViewController:updteProf animated:YES];
        
    }
    else if (indexPath.section==1){
        
        if(_isAFrndDisable)
        {
            if(indexPath.row==1)
            {
                FrndRequestScreen *frnd=[[FrndRequestScreen alloc]initWithNibName:@"FrndRequestScreen" bundle:nil];
                [self.navigationController pushViewController:frnd animated:YES];
            }
        }
        NSLog(@"%ld",(long)indexPath.row);
    }
    else if (indexPath.section==2){
                NSLog(@"%ld",(long)indexPath.row);
    }
    else if (indexPath.section==3){
                NSLog(@"%ld",(long)indexPath.row);
    }
    else if (indexPath.section==4)
    {
        BlockedUserListScreen *blockU=[[BlockedUserListScreen alloc]initWithNibName:@"BlockedUserListScreen" bundle:nil];
        [self.navigationController pushViewController:blockU animated:YES];
        
        NSLog(@"%ld",(long)indexPath.row);}
    else if (indexPath.section==5)
    {
        NSLog(@"%ld",(long)indexPath.row);
        if(indexPath.row==0)
        {
            [self showHelpView];
        }
        else if (indexPath.row==1)
        {
            [self viewForTermsAndCondition];
        }
        else if(indexPath.row==2)
        {
            [self viewForPrivcyAndPolicy];
        }
        else
        {
            [self sendEmailToAdmin];
        }
    }
    else if (indexPath.section==6)
    {
        if (indexPath.row==0)
        {
            _strSocialURL=@"https://www.facebook.com/";
            [self viewForSocialLinkingUI];
        }
        else if (indexPath.row==1)
        {
            _strSocialURL=@"https://twitter.com/";
            [self viewForSocialLinkingUI];

        }
        NSLog(@"%ld",(long)indexPath.row);
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}

- (void)updateSwitch1:(id)sender
{
    UISwitch *switcher = (UISwitch *)sender;
    NSInteger row=switcher.tag;
    _strSelectedSetting=@"autofbfriend";
    NSLog(@"%ld",(long)row);
    if (switcher.on)
    {
        _isSOn=YES;
        _isAFrndDisable=YES;
        [switcher setOn: NO animated:YES];
        //??? = NO;
        _strYesNo=@"N";
        NSLog(@"OFF");
    }
    else
    {
        [switcher setOn: YES animated: YES];
        //??? = YES;
        _isSOn=NO;
        _strYesNo=@"Y";
        _isAFrndDisable=NO;
        NSLog(@"ON");
    }
    [self SettingAPIServerCall];
}

- (void)updateSwitch2:(id)sender
{
    UISwitch *switcher = (UISwitch *)sender;
    NSInteger row=switcher.tag;
    _strSelectedSetting=@"fbfriends";
    NSLog(@"%ld",(long)row);
    if (switcher.on)
    {
        _isSOn1=NO;
        [switcher setOn: NO animated:YES];
        //??? = NO;
        _strYesNo=@"N";
        NSLog(@"OFF");
    }
    else
    {
        _isSOn1=YES;
        [switcher setOn: YES animated: YES];
        //??? = YES;
        _strYesNo=@"Y";
        NSLog(@"ON");
    }
        [self SettingAPIServerCall];
}
- (void)updateSwitch3:(id)sender
{
    UISwitch *switcher = (UISwitch *)sender;
    NSInteger row=switcher.tag;
    _strSelectedSetting=@"notification";
    NSLog(@"%ld",(long)row);
    if (switcher.on)
    {
        _isSOn2=NO;
        [switcher setOn: NO animated:YES];
        //??? = NO;
        NSLog(@"OFF");
        _strYesNo=@"N";
    }
    else
    {
        _isSOn2=YES;
        [switcher setOn: YES animated: YES];
        //??? = YES;
        _strYesNo=@"Y";
        NSLog(@"ON");
    }
        [self SettingAPIServerCall];
}
- (void)updateSwitch4:(id)sender
{
    UISwitch *switcher = (UISwitch *)sender;
    NSInteger row=switcher.tag;
    _strSelectedSetting=@"privacy";
    NSLog(@"%ld",(long)row);
    if (switcher.on)
    {
        [switcher setOn: NO animated:YES];
        //??? = NO;
        _isSOn3=NO;
        _strYesNo=@"N";
        NSLog(@"OFF");
    }
    else
    {
        _isSOn3=YES;
        [switcher setOn: YES animated: YES];
        //??? = YES;
       
        _strYesNo=@"Y";
        NSLog(@"ON");
    }
        [self SettingAPIServerCall];
}

#pragma mark: ByDefaultSettingAPI
-(void)ShowDefaultSettingAPI
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isLogOut=NO;
        _isSettingUpdate=NO;
        _isDSetting=YES;
        
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *urlString=base_URL;
        NSLog(@"%@",[theAppDelegate.arrUserLoginData objectAtIndex:5]);
        
        NSLog(@"%@",[theAppDelegate.arrUserLoginData objectAtIndex:6]);
        
        NSString *strURL=[NSString stringWithFormat:@"wsdisplaysetting.php?uid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0]];
        urlString=[urlString stringByAppendingString:strURL];
        NSURL *urlSignIn=[NSURL URLWithString:urlString];
        request=[NSMutableURLRequest requestWithURL:[urlSignIn standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"The Internet connection appears to be offline" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    request = nil;

}
#pragma mark: Update Setting  Server Call
-(void)SettingAPIServerCall
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isLogOut=NO;
        _isSettingUpdate=YES;
        _isDSetting=NO;
        
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *urlString=base_URL;
        NSLog(@"%@",[theAppDelegate.arrUserLoginData objectAtIndex:5]);
        NSLog(@"%@",[theAppDelegate.arrUserLoginData objectAtIndex:6]);
        
        NSString *strURL=[NSString stringWithFormat:@"wssetting.php?uid=%@&sname=%@&svalue=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],_strSelectedSetting,_strYesNo];
        urlString=[urlString stringByAppendingString:strURL];
        NSURL *urlSignIn=[NSURL URLWithString:urlString];
        request=[NSMutableURLRequest requestWithURL:[urlSignIn standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"The Internet connection appears to be offline" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    request = nil;
}

#pragma mark: User SignIn Server Call
-(void)LogOutServerCall
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isLogOut=YES;
        _isSettingUpdate=NO;
        _isDSetting=NO;
        
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *urlString=base_URL;
        NSLog(@"%@",[theAppDelegate.arrUserLoginData objectAtIndex:5]);
        NSLog(@"%@",[theAppDelegate.arrUserLoginData objectAtIndex:6]);
        
        NSString *strURL=[NSString stringWithFormat:@"wslogout.php?uid=%@&did=%@&dtype=I",[theAppDelegate.arrUserLoginData objectAtIndex:0],[theAppDelegate.arrUserLoginData objectAtIndex:6]];
        urlString=[urlString stringByAppendingString:strURL];
        NSURL *urlSignIn=[NSURL URLWithString:urlString];
        request=[NSMutableURLRequest requestWithURL:[urlSignIn standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"The Internet connection appears to be offline" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    request = nil;
}

#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    [theAppDelegate stopSpinner];
    [self.view setUserInteractionEnabled:YES];
    // [theAppDelegate stopSpinner];
    
    dict =[[NSMutableDictionary alloc]initWithDictionary:response];
    NSString *strMsg=[dict valueForKey:@"message"];
    NSLog(@"%@",strMsg);
    if ([strMsg isEqualToString:@"sus"])
    {
        if(_isLogOut)
        {
            objDBManager=[DBManager getSharedInstance];
            theAppDelegate.isStartFromAppDelegate=YES;
            [objDBManager deleteFromTableWithQuery:@"delete from tblUser"];
            [objDBManager deleteFromTableWithQuery:@"delete from tblNotifications"];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            ViewController *vc=[storyboard instantiateViewControllerWithIdentifier:@"Loginview"];
            UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:vc];
            [self presentViewController:nav animated:YES completion:nil];
        }
        else if (_isSettingUpdate)
        {
            [_tblSetting reloadData];
        }
        else if (_isDSetting)
        {
            _arrDefaultSetting=[[NSMutableArray alloc]init];
            _arrDefaultSetting=[dict valueForKey:@"info"];
            if ([_arrDefaultSetting isEqual:(id)[NSNull null]])
            {
                _arrDefaultSetting=[[NSMutableArray alloc]init];
            }
            else
            {
                _strAutofbfriend=[_arrDefaultSetting valueForKey:@"autofbfriend"];
                if([_strAutofbfriend isEqualToString:@"Y"])
                {
                    _isSOn=NO;
                    _isAFrndDisable=NO;
                }
                else
                {
                    _isSOn=YES;
                    _isAFrndDisable=YES;
                }
                _strFBfriends=[_arrDefaultSetting valueForKey:@"fbfriends"];
                if ([_strFBfriends isEqualToString:@"Y"]) {
                    _isSOn1=YES;
                }
                else
                {
                    _isSOn1=NO;
                }
                _strNotification=[_arrDefaultSetting valueForKey:@"notification"];
                
                if ([_strNotification isEqualToString:@"Y"]) {
                    _isSOn2=YES;
                }
                else
                {
                    _isSOn2=NO;
                }
                _strPrivacy=[_arrDefaultSetting valueForKey:@"privacy"];
                if ([_strPrivacy isEqualToString:@"Y"])
                {
                    _isSOn3=YES;
                }
                else
                {
                    _isSOn3=NO;
                }
            }
            
            [_tblSetting reloadData];
        }
    }
    else
    {
        if (_isLogOut) {
            
        }
        else if (_isSettingUpdate){}
        else if (_isDSetting) {}
    }
}

- (void)sendEmailToAdmin
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        [mail setSubject:@"Sample Subject"];
        [mail setMessageBody:@"Here is some main text in the email!" isHTML:NO];
        [mail setToRecipients:@[@"feedback@10foldinc.com"]];
        [self presentViewController:mail animated:YES completion:NULL];
    }
    else
    {
        NSLog(@"This device cannot send email");
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
