//
//  UpdateProfileScreen.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 26/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "HeaderAndConstants.h"
#import "UpdatePassword.h"
#import "UpdatePDetails.h"
#import "WebConnection1.h"
#import "AsyncImageView.h"
#import "DBManager.h"

@interface UpdateProfileScreen : UIViewController<UITableViewDelegate,UITableViewDataSource,WebRequestResult1,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    WebConnection1 *connection;
    NSMutableDictionary *dict;
    DBManager *objDBManager;
}

@property (nonatomic) AsyncImageView *asynchronusImage;
#pragma mark: UIButton Properties declarations here
@property(nonatomic)IBOutlet UIButton *btnBack,*btnBackPress,*btnUpdateImage;

@property (nonatomic) NSString *strSelectedImg,*strProfileImg;

#pragma mark:UIImageView Properties declaratios here
@property(nonatomic)IBOutlet UIImageView *imgProfile;

#pragma mark: UITableView Properties declarations here
@property (nonatomic)IBOutlet UITableView *tblInfo;

@property(nonatomic) UIImage *image;
@end
