//
//  UpdateProfileScreen.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 26/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "UpdateProfileScreen.h"

@interface UpdateProfileScreen ()

@end

@implementation UpdateProfileScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    
    objDBManager=[DBManager getSharedInstance];
    NSArray *arrUData=[objDBManager selectTableDataWithQuery:@"select *from tblUser"];
    
    if ([arrUData count]) {
        theAppDelegate.arrUserLoginData=[[NSMutableArray alloc]init];
        theAppDelegate.arrUserLoginData=[[arrUData objectAtIndex:0] mutableCopy];
    }
    [self loadInitialUI];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)prefersStatusBarHidden
{
    return true;
}
#pragma mark: Load Initial UI
-(void)loadInitialUI
{
    UIImageView *ImgBG=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,screenWidth,screenHeight)];
    ImgBG.image=[UIImage imageNamed:@"img_notificationBG.png"];
    ImgBG.alpha=0.5;
    [self.view addSubview:ImgBG];
    
    _btnBack=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBack.frame = CGRectMake(5,8,25,25);
    
    UIView *viewHeader=[[UIView alloc]initWithFrame:CGRectMake(0,0,screenWidth,45)];
    viewHeader.backgroundColor=[UIColor colorWithRed:240/255.0 green:137/255.0 blue:0/255.0 alpha:1];
    [self.view addSubview:viewHeader];
    
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"iconBackArrow.png"] forState:UIControlStateNormal];
    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    _btnBackPress=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBackPress=[[UIButton alloc]initWithFrame:CGRectMake(0,0,55,30)];
    [_btnBackPress addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnBack];
    [self.view addSubview:_btnBackPress];
    
    UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,9,screenWidth,30)];
    lblTitle.text=@"Update Profile";
    lblTitle.textColor=[UIColor whiteColor];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.font=[UIFont fontWithName:@"Avenir Light" size:25];
    [self.view addSubview:lblTitle];
    
    // -------------- UIImageView for Profile ----------//
    _imgProfile = [[UIImageView alloc] initWithFrame:CGRectMake(screenWidth/2-75,CGRectGetMaxY(viewHeader.frame)+20,150,150)];
    _imgProfile.backgroundColor =[UIColor grayColor];
    _imgProfile.layer.cornerRadius = _imgProfile.frame.size.width/2;
    _imgProfile.layer.masksToBounds = YES;
    _imgProfile.image = [UIImage imageNamed:@"profileIcon"];
    [self.view addSubview:_imgProfile];
    
    if([[theAppDelegate.arrUserLoginData objectAtIndex:5] isEqualToString:@"NO"])
    {
        // -------------- UIImageView for Profile ----------//
        _imgProfile = [[UIImageView alloc] initWithFrame:CGRectMake(screenWidth/2-75,CGRectGetMaxY(viewHeader.frame)+20,150,150)];
        _imgProfile.backgroundColor =[UIColor grayColor];
        _imgProfile.layer.cornerRadius = _imgProfile.frame.size.width/2;
        _imgProfile.layer.masksToBounds = YES;
        _imgProfile.image = [UIImage imageNamed:@"profileIcon"];
        [self.view addSubview:_imgProfile];
    }
    else
    {        
        // ---------- Loading Main Image ---------//
        _asynchronusImage = [[AsyncImageView alloc] initWithFrame:CGRectMake(screenWidth/2-75,CGRectGetMaxY(viewHeader.frame)+20,150,150)];
        //NSString *strBaseURL=ImgBase_URL;
        NSString *imgUrlString =[theAppDelegate.arrUserLoginData objectAtIndex:5];
        
        if([imgUrlString containsString:@"http://graph.facebook.com/"])
        {
            NSArray *arr=[imgUrlString componentsSeparatedByString:@"http://graph.facebook.com/"];
            NSLog(@"%@",arr);
            NSString *strString=[arr objectAtIndex:1];
            NSArray *arr1=[strString componentsSeparatedByString:@"/"];
            NSLog(@"%@",arr1);
            imgUrlString=[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?width=500&height=500",[arr1 objectAtIndex:0]];
        }
        //strBaseURL=[strBaseURL stringByAppendingString:imgUrlString];
        _asynchronusImage.layer.cornerRadius = _asynchronusImage.frame.size.width/2;
        _asynchronusImage.clipsToBounds = YES;
        _asynchronusImage.contentMode = UIViewContentModeScaleAspectFit;
        _asynchronusImage.layer.masksToBounds = YES;
        _asynchronusImage.layer.borderColor=[UIColor clearColor].CGColor;
        
        NSURL *imageUrl = [[NSURL alloc] initWithString:imgUrlString];
        [_asynchronusImage loadImageFromURL:imageUrl];
        [self.view addSubview:_asynchronusImage];
    }
    
    _btnUpdateImage=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth/2-50,CGRectGetMaxY(_imgProfile.frame),100,30)];
    [_btnUpdateImage addTarget:self action:@selector(btnUpdateImagePressed:)forControlEvents:UIControlEventTouchUpInside];
    [_btnUpdateImage setTitle:@"Change" forState:UIControlStateNormal];
    [_btnUpdateImage setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _btnUpdateImage.titleLabel.font=[UIFont fontWithName:@"Avenir Light" size:14];
    [self.view addSubview:_btnUpdateImage];
    
    _tblInfo=[[UITableView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_imgProfile.frame)+30,screenWidth,150)];
    _tblInfo.backgroundColor=[UIColor clearColor];
    _tblInfo.tableFooterView=[[UIView alloc]init];
    _tblInfo.scrollEnabled=NO;
    _tblInfo.delegate=self;
    _tblInfo.dataSource=self;
    [self.view addSubview:_tblInfo];
}

#pragma mark: UIButton Pressed delegate declaratios here
-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnUpdateImagePressed:(id)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // take photo button tapped.
        [self takePhoto];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // choose photo button tapped.
        [self ChoosePhotoFromExitsing];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Delete Photo" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        // Distructive button tapped.
        //[self deletePhoto];
        
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

#pragma mark: Choose From Gallery
-(void)ChoosePhotoFromExitsing
{
    UIImagePickerController *imageController= [[UIImagePickerController alloc]init];
    imageController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    imageController.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    imageController.allowsEditing = YES;
    imageController.delegate = self;
    //[self presentViewController:imageController animated:YES completion:nil];
    [self performSelector:@selector(showGallery:) withObject:imageController afterDelay:1];
}

-(void)showGallery:(id)imageController
{
    [self presentViewController:imageController animated:YES completion:nil];
}
#pragma mark- TakePhotoFromCamera
-(void)takePhoto
{
if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
{
    UIImagePickerController *imageController = [[UIImagePickerController alloc] init];
    imageController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imageController.mediaTypes  = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
    imageController.delegate = self;
    [self presentViewController:imageController animated:YES completion:nil];
}
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Camera is not available" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [theAppDelegate showSpinnerInView:self.view];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        // -------------- UIImageView for Profile ----------//
    
        _image=[[UIImage alloc]init];
            //_image = [info valueForKey:UIImagePickerControllerOriginalImage];
        _image = [self scaleAndRotateImage:[info valueForKey:UIImagePickerControllerOriginalImage]];
            _imgProfile.image =_image;
            
            NSData *dataImage = [[NSData alloc] init];
            dataImage = UIImagePNGRepresentation(_image);
            _strSelectedImg=[dataImage base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            
            [self serviceCallForUploadPhoto1:[self getCompressedImage:_image]];
        
        //NSData *imageData=UIImageJPEGRepresentation(image, 0.1);
    }];
}

- (UIImage *)scaleAndRotateImage:(UIImage *) image {
    int kMaxResolution = 1500;
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}
-(NSData*)getCompressedImage:(UIImage*)img
{
    //>213832
    //------------  Image Compression --------- //
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    //int maxFileSize = 250*1024;
    int maxFileSize = 540*480;
    NSData  *imageData = UIImageJPEGRepresentation(img, compression);
    NSLog(@"Initial Size of Image==>%li",(long)imageData.length);
    
    while ([imageData length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        imageData = UIImageJPEGRepresentation(img, compression);
    }
    
    NSLog(@"Compressed Size of Image==>%li",(long)imageData.length);
    //------------  Image Compression Algorithm--------- //
    
    return imageData;
}

#pragma mark: UploadFirstImage
-(void)serviceCallForUploadPhoto1:(NSData*)imgData
{
    if ([theAppDelegate isConnected])
    {
       
        NSString *strBase=base_URL;
        NSString *url=[NSString stringWithFormat:@"uploadimage_ios.php"];
        strBase=[strBase stringByAppendingString:url];
        
        NSLog(@"Image upload URL :%@",url);
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strBase]];
        
        NSData *imageData = imgData;
        
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"unique-consistent-string";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"imageCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"Some Caption"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        if(imageData)
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=imagename.png\r\n",@"userfile"] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[NSData dataWithData:imageData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        // [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        NSHTTPURLResponse* response =[[NSHTTPURLResponse alloc] init];
        NSError* error = [[NSError alloc] init];
        
        //synchronous filling of data from HTTP POST response
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        
        //convert data into string
        NSString   *responseString = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];        
        NSLog(@"Response String %@",responseString);
        
        NSError *error1;
        NSMutableDictionary *mainDict=[NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error1];
        
        NSArray *arr=[[NSArray alloc]init];
        arr=[mainDict valueForKey:@"info"];
        responseString=[arr description];
        
        if (responseString.length>0)
        {
            _strProfileImg=responseString;
            NSLog(@"%@",responseString);
            NSString *strBase=ImgBase_URL;
            strBase=[strBase stringByAppendingString:responseString];
            [_asynchronusImage loadImageFromURL:[NSURL URLWithString:strBase]];
            [self updateProfImageServerCall];
        }
        else
        {
            //_imgProfile.image=[UIImage imageNamed:@""];
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:@"Failed to upload picture to server." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            //[[[UIAlertView alloc]initWithTitle:@"Failed" message:@"Failed to upload picture to server." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
    }
    else
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"No Internet connection"
                                              message:@""
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"ok", @"Cancel action")
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"Cancel action");
                                       }];
        
        
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    [theAppDelegate stopSpinner];
    [self.view setUserInteractionEnabled:YES];
}


#pragma mark- TableView DataSource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

//--------------------- cellforrow At Index TableView -----------------------//

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@""];
    if (cell == nil)
    {
        //  cell.transform= CGAffineTransformMakeRotation(M_PI/2);
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    
    tableView.separatorColor =UIColor.clearColor;
    cell.backgroundColor= [UIColor clearColor];
    [self.tblInfo setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;


    UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(10,5,150,30)];
    if (indexPath.row==0) {
        lblTitle.text=@"Change Profile";
    }
    else
    {
        lblTitle.text=@"Change Password";
    }
    lblTitle.textColor=[UIColor blackColor];
    lblTitle.font=[UIFont fontWithName:@"AvenirNext-Regular" size:16];
    [cell.contentView addSubview:lblTitle];
    cell.backgroundColor=[UIColor whiteColor];
    
    UIView *viewSept=[[UIView alloc]initWithFrame:CGRectMake(0,50,screenWidth,1)];
    viewSept.backgroundColor=[UIColor blackColor];
    [cell.contentView addSubview:viewSept];
        return cell;
}

#pragma Mark-delegate methods
//--------------------- Tableview didselected index ---------------------//
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0){
        UpdatePDetails *updte=[[UpdatePDetails alloc]initWithNibName:@"UpdatePDetails" bundle:nil];
        [self presentViewController:updte animated:YES completion:nil];
    }
    else if(indexPath.row==1){
        UpdatePassword *updteP=[[UpdatePassword alloc]initWithNibName:@"UpdatePassword" bundle:nil];
        [self presentViewController:updteP animated:YES completion:nil];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 51;
}

#pragma mark: User SignIn Server Call
-(void)updateProfImageServerCall
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *urlString=base_URL;
        NSLog(@"%@",[theAppDelegate.arrUserLoginData objectAtIndex:5]);
        
        NSLog(@"%@",[theAppDelegate.arrUserLoginData objectAtIndex:6]);
        
        NSString *strURL=[NSString stringWithFormat:@"wsupdateprofileimage.php?uid=%@&image=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],_strProfileImg];
        
        urlString=[urlString stringByAppendingString:strURL];
        NSURL *urlSignIn=[NSURL URLWithString:urlString];
        request=[NSMutableURLRequest requestWithURL:[urlSignIn standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"The Internet connection appears to be offline" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    request = nil;
}

#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    [theAppDelegate stopSpinner];
    [self.view setUserInteractionEnabled:YES];
    
    dict =[[NSMutableDictionary alloc]initWithDictionary:response];
    NSString *strMsg=[dict valueForKey:@"message"];
    NSLog(@"%@",strMsg);
    if ([strMsg isEqualToString:@"sus"])
    {
        
        [self updateDB];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Failed to update profile Image" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark: Update Database
-(void)updateDB
{
    objDBManager=[DBManager getSharedInstance];
    
    NSString *strBase=ImgBase_URL;
    strBase=[strBase stringByAppendingString:_strProfileImg];
    NSString*update=[NSString stringWithFormat:@"update tblUser Set IMAGE='%@' Where UID='%@'",strBase,[theAppDelegate.arrUserLoginData objectAtIndex:0]];
    
    BOOL check=[objDBManager updateDataWithQuery:update];
    if (check) {
        NSLog(@"Update to Database");
    }
    else
    {
        NSLog(@"Failed to update Profile Image");
    }
    
    
}
@end
