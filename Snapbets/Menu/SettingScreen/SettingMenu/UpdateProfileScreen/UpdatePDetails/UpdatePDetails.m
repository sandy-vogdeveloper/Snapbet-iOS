//
//  UpdatePDetails.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 24/10/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "UpdatePDetails.h"
#define kOFFSET_FOR_KEYBOARD 90.0
@interface UpdatePDetails ()
{
    UIBarButtonItem *doneButtons;
    UIToolbar *keyboardToolBar1;
}
@end

@implementation UpdatePDetails

- (void)viewDidLoad {
    [super viewDidLoad];
    objDBManager=[DBManager getSharedInstance];
    NSArray *arrTemp=[[NSArray alloc]init];
    arrTemp=[objDBManager selectTableDataWithQuery:@"select *from tblUser"];
    _arrDBData=[[arrTemp objectAtIndex:0]mutableCopy];
    [self loadInitialUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}
#pragma mark: Load InitialUI
-(void)loadInitialUI
{
    UIImageView *ImgBG=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,screenWidth,screenHeight)];
    ImgBG.image=[UIImage imageNamed:@"img_notificationBG.png"];
    ImgBG.alpha=0.8;
    [self.view addSubview:ImgBG];
    
    UIView *viewHeader=[[UIView alloc]initWithFrame:CGRectMake(0,0,screenWidth,45)];
    viewHeader.backgroundColor=[UIColor colorWithRed:240/255.0 green:137/255.0 blue:0/255.0 alpha:1];
    [self.view addSubview:viewHeader];
    
    _btnBack=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBack.frame = CGRectMake(5,8,25,25);
    
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"iconBackArrow.png"] forState:UIControlStateNormal];
    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    _btnBackPress=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBackPress=[[UIButton alloc]initWithFrame:CGRectMake(0,0,55,30)];
    [_btnBackPress addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnBack];
    [self.view addSubview:_btnBackPress];
    
   
    
    UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,5,screenWidth,30)];
    lblTitle.text=@"Update Profile";
    lblTitle.textColor=[UIColor whiteColor];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.font=[UIFont fontWithName:@"Avenir Light" size:25];
    [self.view addSubview:lblTitle];
    UIView *viewForMail=[[UIView alloc]init];
    UIView *viewForFName=[[UIView alloc]init];
    UIView *viewForLName=[[UIView alloc]init];
    UIView *viewForContact=[[UIView alloc]init];
    UIView *viewForDOB=[[UIView alloc]init];
    
    
    UIImageView *imgTrangle=[[UIImageView alloc]initWithFrame:CGRectMake(screenWidth/2-15,50,30,30)];
    imgTrangle.image=[UIImage imageNamed:@"img_Diagonal"];
    [self.view addSubview:imgTrangle];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        _imgTrangleBG=[[UIImageView alloc]initWithFrame:CGRectMake(100,CGRectGetMaxY(imgTrangle.frame),screenWidth-200,380)];
        viewForMail.frame=CGRectMake(120,110,screenWidth-240,40);
        viewForFName.frame=CGRectMake(120,CGRectGetMaxY(viewForMail.frame)+20,screenWidth-240,40);
        viewForLName.frame=CGRectMake(120,CGRectGetMaxY(viewForFName.frame)+20,screenWidth-240,40);
        viewForContact.frame=CGRectMake(120,CGRectGetMaxY(viewForLName.frame)+20,screenWidth-240,40);
        viewForDOB.frame=CGRectMake(120,CGRectGetMaxY(viewForContact.frame)+20,screenWidth-240,40);
        
        _btnSubmit=[[UIButton alloc]initWithFrame:CGRectMake(120,CGRectGetMaxY(viewForDOB.frame)+10,screenWidth-240,40)];
    }
    else
    {
        _imgTrangleBG=[[UIImageView alloc]initWithFrame:CGRectMake(10,CGRectGetMaxY(imgTrangle.frame),screenWidth-20,345)];
        viewForMail.frame=CGRectMake(20,90,screenWidth-40,40);
        viewForFName.frame=CGRectMake(20,CGRectGetMaxY(viewForMail.frame)+17,screenWidth-40,40);
        viewForLName.frame=CGRectMake(20,CGRectGetMaxY(viewForFName.frame)+17,screenWidth-40,40);
        viewForContact.frame=CGRectMake(20,CGRectGetMaxY(viewForLName.frame)+17,screenWidth-40,40);
        viewForDOB.frame=CGRectMake(20,CGRectGetMaxY(viewForContact.frame)+17,screenWidth-40,40);
        
        _btnSubmit=[[UIButton alloc]initWithFrame:CGRectMake(20,CGRectGetMaxY(viewForDOB.frame)+10,screenWidth-40,40)];
    }
    _imgTrangleBG.image=[UIImage imageNamed:@"img_NSnap"];
    [self.view addSubview:_imgTrangleBG];
    
    [viewForMail setBackgroundColor:[UIColor blackColor]];
    [viewForFName setBackgroundColor:[UIColor blackColor]];
    [viewForLName setBackgroundColor:[UIColor blackColor]];
    [viewForContact setBackgroundColor:[UIColor blackColor]];
    [viewForDOB setBackgroundColor:[UIColor blackColor]];
    
    viewForMail.layer.cornerRadius=4.0;
    viewForFName.layer.cornerRadius=4.0;
    viewForLName.layer.cornerRadius=4.0;
    viewForContact.layer.cornerRadius=4.0;
    viewForDOB.layer.cornerRadius=4.0;
    
    [self.view addSubview:viewForMail];
    [self.view addSubview:viewForFName];
    [self.view addSubview:viewForLName];
    [self.view addSubview:viewForContact];
    [self.view addSubview:viewForDOB];
    
    _txtEmail=[[UITextField alloc]initWithFrame:CGRectMake(10,5,viewForMail.frame.size.width-10,30)];
    _txtFName=[[UITextField alloc]initWithFrame:CGRectMake(10,5,viewForFName.frame.size.width-10,30)];
    _txtLName=[[UITextField alloc]initWithFrame:CGRectMake(10,5,viewForLName.frame.size.width-10,30)];
    _txtMob=[[UITextField alloc]initWithFrame:CGRectMake(10,5,viewForContact.frame.size.width-10,30)];
    _txtDob=[[UITextField alloc]initWithFrame:CGRectMake(10,5,viewForDOB.frame.size.width-10,30)];
    
    [_txtEmail setUserInteractionEnabled:NO];
    
    [viewForMail addSubview:_txtEmail];
    [viewForFName addSubview:_txtFName];
    [viewForLName addSubview:_txtLName];
    [viewForContact addSubview:_txtMob];
    [viewForDOB addSubview:_txtDob];
    
    [self getTextFieldWithProperties:_txtEmail placeholder:[_arrDBData objectAtIndex:1] forLogin:YES];
    [self getTextFieldWithProperties:_txtFName placeholder:[_arrDBData objectAtIndex:2] forLogin:YES];
    [self getTextFieldWithProperties:_txtLName placeholder:[_arrDBData objectAtIndex:3] forLogin:YES];
    [self getTextFieldWithProperties:_txtMob placeholder:[_arrDBData objectAtIndex:4] forLogin:YES];
    [self getTextFieldWithProperties:_txtDob placeholder:@"DOB" forLogin:YES];
    
    tbDOB = [[UIToolbar alloc] initWithFrame:CGRectMake(0,screenHeight/2,screenWidth,25)];
    
    tbDOB.barStyle = UIBarStyleDefault;
    [tbDOB setItems: [NSArray arrayWithObjects:
                      [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                      [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(btnDOBDonePressed:)],
                      nil]];
    [tbDOB setBarStyle:UIBarStyleBlackOpaque];
    arrtbDOB= [NSArray arrayWithObjects:
               doneButton, nil];
    _txtDob.inputAccessoryView =tbDOB;
    [self.txtDob setInputView:_dteDOB1];
    _dteDOB1 = [[UIDatePicker alloc]init];
    [_dteDOB1 setDate:[NSDate date]];
    _dteDOB1.datePickerMode = UIDatePickerModeDate;
    _dteDOB1.enabled=true;
    _dteDOB1.date = [NSDate date];
    [_dteDOB1 addTarget:self action:@selector(txtDOBPressed:)forControlEvents:UIControlEventValueChanged];
    [_txtDob setInputView:_dteDOB1];
    
    [_btnSubmit setBackgroundColor:[UIColor whiteColor]];
    _btnSubmit.layer.cornerRadius=4.0;
    [self.view addSubview:_btnSubmit];
    
    UILabel *lblSignUp=[[UILabel alloc]initWithFrame:CGRectMake(10,0,_btnSubmit.frame.size.width-10,40)];
    lblSignUp.text=@"Update Profile";
    lblSignUp.textColor=[UIColor grayColor];
    lblSignUp.textAlignment=NSTextAlignmentCenter;
    lblSignUp.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
    [_btnSubmit addSubview:lblSignUp];
    [_btnSubmit addTarget:self action:@selector(btnUpdatePressed:) forControlEvents:UIControlEventTouchUpInside];
    UIImageView *iconSignInFrwd=[[UIImageView alloc]initWithFrame:CGRectMake(_btnSubmit.frame.size.width-30,10,20,20)];
    iconSignInFrwd.image=[UIImage imageNamed:@"icon_BlckFrwd"];
    [_btnSubmit addSubview:iconSignInFrwd];
}

#pragma mark: Set Proerties for TextFields
-(UITextField*)getTextFieldWithProperties:(UITextField*)textField placeholder:(NSString*)placeholder forLogin:(BOOL)forLogin
{
    //------------- Add properties to textfield ------------- //
    //  textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.placeholder =placeholder;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.keyboardType=UIKeyboardAppearanceDefault;
    
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.keyboardAppearance=UIKeyboardAppearanceLight;
    [textField addTarget:self action:@selector(resignTxt:) forControlEvents:UIControlEventEditingDidEndOnExit];
    _txtMob.keyboardType=UIKeyboardTypePhonePad;
    
    
    
    
    _txtEmail.text =[_arrDBData objectAtIndex:1];
    _txtFName.text = [_arrDBData objectAtIndex:2];
    _txtLName.text = [_arrDBData objectAtIndex:3];
    _txtMob.text =[_arrDBData objectAtIndex:4];
    _txtDob.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"DOB" attributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
    UIFont *fnt=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
    _txtEmail.font=fnt;
    _txtPass.font=fnt;
    _txtFName.font=fnt;
    _txtLName.font=fnt;
    _txtMob.font=fnt;
    _txtDob.font=fnt;
    
    _txtEmail.textAlignment=NSTextAlignmentLeft;
    _txtFName.textAlignment=NSTextAlignmentLeft;
    _txtLName.textAlignment=NSTextAlignmentLeft;
    _txtMob.textAlignment=NSTextAlignmentLeft;
    _txtDob.textAlignment=NSTextAlignmentLeft;
    
    _txtEmail.textColor=[[UIColor whiteColor]colorWithAlphaComponent:0.8];
    _txtFName.textColor=[[UIColor whiteColor]colorWithAlphaComponent:0.8];
    _txtLName.textColor=[[UIColor whiteColor]colorWithAlphaComponent:0.8];
    _txtMob.textColor=[[UIColor whiteColor]colorWithAlphaComponent:0.8];
     _txtDob.textColor=[[UIColor whiteColor]colorWithAlphaComponent:0.8];
    
    _imgLogo.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView animateWithDuration:0.5
                     animations:^{
                         _imgLogo.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
                     } completion:^(BOOL finished) {
                     }];
    
    //----------- Validate return keys -------------------- //
    if (forLogin && textField==_txtDob) {
        _txtDob.returnKeyType=UIReturnKeyDone;
    }
    else if (forLogin && textField==_txtMob)
    {
        _txtMob.returnKeyType=UIReturnKeyNext;
    }
    else if (forLogin && textField==_txtLName)
    {
        _txtLName.returnKeyType=UIReturnKeyNext;
    }
    else if (forLogin && textField==_txtFName)
    {
        _txtFName.returnKeyType=UIReturnKeyNext;
    }
    else if (forLogin && textField ==_txtEmail)
    {        _txtEmail.returnKeyType=UIReturnKeyNext;    }
    
    
    //---------- Validate keyboard type for email address --------//
    
    if (textField==_txtEmail)
    {        textField.keyboardType=UIKeyboardTypeEmailAddress;    }
    
    keyboardToolBar1 = [[UIToolbar alloc] initWithFrame:CGRectMake(0,screenHeight/2,screenWidth,30)];
    keyboardToolBar1.barStyle = UIBarStyleDefault;
    [keyboardToolBar1 setItems: [NSArray arrayWithObjects:
                                 [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                 [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneActionPkg)],
                                 nil]];
    _txtMob.inputAccessoryView = keyboardToolBar1;
    
    
    return textField;//------- Return fully updated textfield
}

-(void)doneActionPkg
{
    [_txtMob resignFirstResponder];
}
#pragma mark- TextField DelegateMethods
-(void)resignTxt:(UITextField*)txt
{
    //----------- Validate textfield return action ------ //
    if (txt==_txtEmail)
    {   [_txtFName becomeFirstResponder];       }
    
    else if (txt==_txtFName)
    {      [_txtLName becomeFirstResponder];    }
    else if (txt==_txtLName)
    {   [_txtMob becomeFirstResponder];         }
    else if (txt==_txtMob)
    {        [_txtPass becomeFirstResponder];   }    
    else if (txt==_txtDob)
    {                                           }
    else
    {   [txt resignFirstResponder];             }
    //------------------------------------------------ //
}

#pragma mark- Keyboard Show Hide Methods
-(void)keyboardWillShow:(NSNotification*)notification {
    // Animate the current view out of the way
    if (self.view.frame.origin.y == 0)
    {
        [self setViewMovedUp:YES];
    }
    //    else if (self.view.frame.origin.y < 0)
    //    {
    //        [self setViewMovedUp:NO];
    //    }
}
-(void)keyboardWillHide:(NSNotification*)notification {
    //    if (self.view.frame.origin.y >= 0)
    //    {
    //        [self setViewMovedUp:YES];
    //    }
    //    else
    if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}


#pragma mark- Keyboard movedup Methods
//---------- KeyBoard Movedup methods -----------------------//
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        _isKeyboardOnScreen=YES;
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        _isKeyboardOnScreen=NO;
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

-(void)dismisskeyboradafterEndInput:(UITextField*)sender
{
    [sender resignFirstResponder];
    
    // NSString *firstStr=_txtName.text;
    //  NSString *secondStr=_txtPass.text;
    
    if([_txtEmail.text length] ==0)
    {
    }
    if ([_txtFName.text length]==0) {
    }
    if ([_txtLName.text length]==0) {
    }
    if ([_txtMob.text length]==0) {
    }
    if ([_txtPass.text length]==0) {
    }
    if ([_txtDob.text length]==0) {
    }
}


#pragma mark: TextField Validation Here
-(BOOL)isValidTextfield
{
    NSString *strMsg;
    if(_txtEmail.text!=nil && _txtEmail.text.length>0)
    {
        if (_txtFName.text!=nil && _txtFName.text.length>0)
        {
            if (_txtLName.text!=nil && _txtLName.text.length>0)
            {
                if (_txtMob.text!=nil && _txtMob.text.length>0)
                {
                    if (_txtDob.text!=nil && _txtDob.text.length>0)
                    {
                        return true;
                    }
                    else
                    {
                        strMsg=NSLocalizedString(@"Please enter DOB.",nil);
                    }
                }
                else
                {
                    strMsg=NSLocalizedString(@"Please enter Mobile Number.",nil);
                }
            }
            else
            {
                strMsg=NSLocalizedString(@"Please enter Last Name.",nil);
            }
        }
        else
        {
            strMsg=NSLocalizedString(@"Please enter First Name.",nil);
        }
    }
    else
    {
        strMsg=NSLocalizedString(@"Please enter Email ID.",nil);
    }
    
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Warning"  message:strMsg  preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    return NO;
}

#pragma mark: Text DOB Pressed
-(IBAction)txtDOBPressed:(id)sender
{
    _dteDOB1.datePickerMode=YES;
    _dteDOB1.selected=YES;
    
    UIDatePicker *picker = (UIDatePicker*)_txtDob.inputView;
    [picker setMaximumDate:[NSDate date]];
    NSDate *date = picker.date;
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateformate stringFromDate:date];
    _txtDob.text = [NSString stringWithFormat:@"%@",dateString];
    _txtDob.textColor=[[UIColor whiteColor]colorWithAlphaComponent:0.8];
}

#pragma mark: UIButton Pressed delegate declarations here
-(IBAction)btnDOBDonePressed:(id)sender
{
    [_txtDob resignFirstResponder];
    [tbDOB removeFromSuperview];
    self.view.userInteractionEnabled=YES;
}
#pragma mark: Button Pressed delegate declarations here
-(IBAction)btnBackPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)btnUpdatePressed:(id)sender
{
        [self updateProfileServerCall];
}

#pragma mark: User SignIn Server Call
-(void)updateProfileServerCall
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *urlString=base_URL;
        NSLog(@"%@",[theAppDelegate.arrUserLoginData objectAtIndex:5]);
        
        NSLog(@"%@",[theAppDelegate.arrUserLoginData objectAtIndex:6]);
        
        NSString *strFName=[_txtFName.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSString *strLName=[_txtLName.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSString *strEmail=[_txtEmail.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        if (_txtDob.text.length==0) {
            _txtDob.text=@"";
        }
        NSString *strURL=[NSString stringWithFormat:@"wsprofileupdate.php?uid=%@&fname=%@&lname=%@&email=%@&contact=%@&dob=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],strFName,strLName,strEmail,_txtMob.text,_txtDob.text];
        
        urlString=[urlString stringByAppendingString:strURL];
        NSURL *urlSignIn=[NSURL URLWithString:urlString];
        request=[NSMutableURLRequest requestWithURL:[urlSignIn standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"The Internet connection appears to be offline" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    request = nil;
}

#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    [theAppDelegate stopSpinner];
    [self.view setUserInteractionEnabled:YES];
    // [theAppDelegate stopSpinner];
    
    dict =[[NSMutableDictionary alloc]initWithDictionary:response];
    NSString *strMsg=[dict valueForKey:@"message"];
    NSLog(@"%@",strMsg);
    if ([strMsg isEqualToString:@"sus"])
    {
        _arrLoginInfo=[[NSMutableArray alloc]init];
        _arrLoginInfo=[dict valueForKey:@"info"];
        [self saveDatatoDatabase];
        _strAlertTitle=@"Profile updated successfully";
        [self viewForAlertPopUp];
    }
    else
    {
        _strAlertTitle=@"Failed to update profile";
        [self viewForAlertPopUp];
    }
}

#pragma mark: Save data to DB
-(void)saveDatatoDatabase
{
    NSArray *arrDBTemp=[[NSMutableArray alloc]init];
    theAppDelegate.arrUserLoginData=[[NSMutableArray alloc]init];
    objDBManager=[DBManager getSharedInstance];
    arrDBTemp=[objDBManager selectTableDataWithQuery:@"select *from tblUser"];
    NSString *strFBLogin=[[arrDBTemp objectAtIndex:0]objectAtIndex:7];
    
    if ([arrDBTemp count])
    {
        [objDBManager deleteFromTableWithQuery:@"delete from tblUser"];
    }
    
    NSString *strUID,*strEmail,*strFName,*strLName,*strContact,*strImg;
    
    strUID=[_arrLoginInfo valueForKey:@"id"];
    strEmail=[_arrLoginInfo valueForKey:@"email"];
    strFName=[_arrLoginInfo valueForKey:@"fname"];
    strLName=[_arrLoginInfo valueForKey:@"lname"];
    strContact=[_arrLoginInfo valueForKey:@"contact"];
    strImg=[_arrLoginInfo valueForKey:@"image"];
    
    [theAppDelegate.arrUserLoginData addObject:strUID];
    [theAppDelegate.arrUserLoginData addObject:strEmail];
    [theAppDelegate.arrUserLoginData addObject:strFName];
    [theAppDelegate.arrUserLoginData addObject:strLName];
    [theAppDelegate.arrUserLoginData addObject:strContact];
    [theAppDelegate.arrUserLoginData addObject:strImg];
    [theAppDelegate.arrUserLoginData addObject:theAppDelegate.strDeviceToken];
     [theAppDelegate.arrUserLoginData addObject:strFBLogin];
    NSLog(@"%@",theAppDelegate.arrUserLoginData);
    
    [theAppDelegate.arrUserLoginData addObject:strImg];
    NSString *queryString=[NSString stringWithFormat:@"insert into tblUser values('%@','%@','%@','%@','%@','%@','%@','%@')",strUID,strEmail,strFName,strLName,strContact,strImg,theAppDelegate.strDeviceToken,strFBLogin];
    
    if ([objDBManager insertDataWithQuery:queryString])
    {
        
    }
    else
    {
    }
}

#pragma mark: view For Alert
-(void)viewForAlertPopUp
{
    [_viewForAlert removeFromSuperview];
    _viewForAlert =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForAlert.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForAlert.tag=20;
    [self.view addSubview:_viewForAlert];
    
    CGRect frameForViewForHelp=_viewForAlert.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForAlert.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForAlert.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UILabel *lblAlert=[[UILabel alloc]initWithFrame:CGRectMake(0,_viewForAlert.frame.size.height/3,_viewForAlert.frame.size.width,30)];
        lblAlert.text=_strAlertTitle;
        
        lblAlert.textAlignment=NSTextAlignmentCenter;
        lblAlert.textColor=[UIColor whiteColor];
        lblAlert.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:20];
        //[lblAlert sizeToFit];
        [_viewForAlert addSubview:lblAlert];
        
        _btnCloseAlert=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseAlert.frame=CGRectMake(_viewForAlert.frame.size.width/2-60,CGRectGetMaxY(lblAlert.frame)+50,120,40);
        }
        else
        {
            _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-60,_viewForAlert.frame.size.height/2,120,50)];
        }
        [_btnCloseAlert setTitle:@"Close" forState:UIControlStateNormal];
        [_btnCloseAlert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseAlert.layer.borderWidth=2.0;
        _btnCloseAlert.layer.cornerRadius=4.0;
        _btnCloseAlert.layer.borderColor=[[UIColor orangeColor]CGColor];
        [_btnCloseAlert addTarget:self action:@selector(viewForCloseAlert) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewForAlert addSubview:_btnCloseAlert];
    }];
}

#pragma mark-Close Comment View
-(void)viewForCloseAlert
{
    @try
    {
        for(UIView *subview in [_viewForAlert subviews])
        {
            [subview removeFromSuperview];
        }
        
        
        
        CGRect frameForViewAlert=_viewForAlert.frame;
        
        frameForViewAlert=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForAlert.frame=frameForViewAlert;
           
        } completion:^(BOOL finished){
            [_viewForAlert removeFromSuperview];
           
            
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

@end
