//
//  UpdatePDetails.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 24/10/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "HeaderAndConstants.h"
#import "AppDelegate.h"
#import "WebConnection1.h"

@interface UpdatePDetails : UIViewController<WebRequestResult1>
{
    WebConnection1 *connection;
    NSMutableDictionary *dict;
    DBManager *objDBManager;
    CGRect vscreenSize;
    CGFloat vscreenHeight,vscreenWidth;
    UIToolbar *toolBarsDOB;
    NSString *strDOB;
    NSDate   *dteDOB;
    UIBarButtonItem *doneButton;
    UIToolbar *tbDOB;
    NSArray *arrtbDOB;
}

#pragma mark: UIView properties declarations here
@property (nonatomic)IBOutlet UIView *viewForAlert;

@property (nonatomic)IBOutlet NSString *strAlertTitle;

#pragma mark: UIButton Properties declarations here
@property(nonatomic)IBOutlet UIButton *btnBack,*btnBackPress,*btnSubmit,*btnCloseAlert;

#pragma mark: UIImageView Properties declarations here
@property (nonatomic) IBOutlet UIImageView *imgTrangleBG,*imgLogo;

#pragma mark: UITextField Properties declarations here
@property (nonatomic)IBOutlet UITextField *txtEmail,*txtFName,*txtLName,*txtMob,*txtPass,*txtDob;

#pragma mark:- Picker view declarations here
@property (nonatomic)UIDatePicker *dteDOB1;

#pragma mark: Bool properties declarations here
@property(nonatomic)BOOL isKeyboardOnScreen;

@property (nonatomic) NSMutableArray *arrLoginInfo,*arrDBData;
@end
