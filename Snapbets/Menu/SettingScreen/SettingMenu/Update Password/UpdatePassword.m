//
//  UpdatePassword.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 24/10/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "UpdatePassword.h"

@interface UpdatePassword ()

@end

@implementation UpdatePassword

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadInitialUI];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}
#pragma mark: Load InitialUI
-(void)loadInitialUI
{
    UIImageView *ImgBG=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,screenWidth,screenHeight)];
    ImgBG.image=[UIImage imageNamed:@"img_notificationBG.png"];
    ImgBG.alpha=0.8;
    [self.view addSubview:ImgBG];
    
    UIView *viewHeader=[[UIView alloc]initWithFrame:CGRectMake(0,0,screenWidth,45)];
    viewHeader.backgroundColor=[UIColor colorWithRed:240/255.0 green:137/255.0 blue:0/255.0 alpha:1];
    [self.view addSubview:viewHeader];
    
    _btnBack=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBack.frame = CGRectMake(5,8,25,25);
    
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"iconBackArrow.png"] forState:UIControlStateNormal];
    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    _btnBackPress=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBackPress=[[UIButton alloc]initWithFrame:CGRectMake(0,0,55,30)];
    [_btnBackPress addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnBack];
    [self.view addSubview:_btnBackPress];
    
    UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,5,screenWidth,30)];
    lblTitle.text=@"Update Password";
    lblTitle.textColor=[UIColor whiteColor];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.font=[UIFont fontWithName:@"Avenir Light" size:25];
    [self.view addSubview:lblTitle];
    
    UIView *viewForOldPass;
    UIView *viewForNewPass;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        _imgTrangleBG=[[UIImageView alloc]initWithFrame:CGRectMake(100,60,screenWidth-200,220)];
        viewForOldPass=[[UIView alloc]initWithFrame:CGRectMake(120,100,screenWidth-240,40)];
        viewForNewPass=[[UIView alloc]initWithFrame:CGRectMake(120,CGRectGetMaxY(viewForOldPass.frame)+20,screenWidth-240,40)];
        _btnUpdate=[[UIButton alloc]initWithFrame:CGRectMake(120,CGRectGetMaxY(viewForNewPass.frame)+20,screenWidth-240,40)];
    }
    else
    {
        _imgTrangleBG=[[UIImageView alloc]initWithFrame:CGRectMake(10,60,screenWidth-20,220)];
        viewForOldPass=[[UIView alloc]initWithFrame:CGRectMake(20,90,screenWidth-40,40)];
        viewForNewPass=[[UIView alloc]initWithFrame:CGRectMake(20,CGRectGetMaxY(viewForOldPass.frame)+20,screenWidth-40,40)];
        _btnUpdate=[[UIButton alloc]initWithFrame:CGRectMake(20,CGRectGetMaxY(viewForNewPass.frame)+20,screenWidth-40,40)];
    }
    
    _imgTrangleBG.image=[UIImage imageNamed:@"img_Trangle"];
    [self.view addSubview:_imgTrangleBG];
    
    [viewForOldPass setBackgroundColor:[UIColor whiteColor]];
    [viewForNewPass setBackgroundColor:[UIColor whiteColor]];
    
    viewForOldPass.layer.cornerRadius=4.0;
    viewForNewPass.layer.cornerRadius=4.0;
    
    [self.view addSubview:viewForOldPass];
    [self.view addSubview:viewForNewPass];
    
    _txtOldPass=[[UITextField alloc]initWithFrame:CGRectMake(10,5,viewForOldPass.frame.size.width-10,30)];
    _txtConfPass=[[UITextField alloc]initWithFrame:CGRectMake(10,5,viewForNewPass.frame.size.width-10,30)];
    [viewForOldPass addSubview:_txtOldPass];
    [viewForNewPass addSubview:_txtConfPass];
    
    [self getTextFieldWithProperties:_txtOldPass placeholder:@"Old Password" forLogin:YES];
    [self getTextFieldWithProperties:_txtConfPass placeholder:@"New PAssword" forLogin:YES];
    
    [_btnUpdate addTarget:self action:@selector(btnUpdatePressed:) forControlEvents:UIControlEventTouchUpInside];
    [_btnUpdate setTitle:@"Update" forState:UIControlStateNormal];
    [_btnUpdate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _btnUpdate.titleLabel.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
    _btnUpdate.backgroundColor=[UIColor blackColor];
    _btnUpdate.layer.cornerRadius=4.0;
    [self.view addSubview:_btnUpdate];
    
   
    UIImageView *iconSignInFrwd=[[UIImageView alloc]initWithFrame:CGRectMake(_btnUpdate.frame.size.width-30,10,20,20)];
    iconSignInFrwd.image=[UIImage imageNamed:@"icon_forward"];
    [_btnUpdate addSubview:iconSignInFrwd];
}


#pragma mark: Set Proerties for TextFields
-(UITextField*)getTextFieldWithProperties:(UITextField*)textField placeholder:(NSString*)placeholder forLogin:(BOOL)forLogin
{
    //------------- Add properties to textfield ------------- //
    //  textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.placeholder =placeholder;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.keyboardType=UIKeyboardAppearanceDefault;
    
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.keyboardAppearance=UIKeyboardAppearanceLight;
    [textField addTarget:self action:@selector(resignTxt:) forControlEvents:UIControlEventEditingDidEndOnExit];
    _txtOldPass.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Old Password" attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    _txtConfPass.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"New Password" attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    _txtOldPass.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
    _txtConfPass.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
    
    _txtOldPass.textAlignment=NSTextAlignmentLeft;
    _txtConfPass.textAlignment=NSTextAlignmentLeft;
    
    _txtOldPass.textColor=[UIColor blackColor];
    _txtConfPass.textColor=[UIColor blackColor];
    
    //----------- Validate return keys -------------------- //
    if (forLogin && textField==_txtConfPass)
    {        _txtConfPass.returnKeyType=UIReturnKeyDone;      }
    
    else if (forLogin && textField ==_txtOldPass)
    {        _txtOldPass.returnKeyType=UIReturnKeyNext;    }
    
    //----------- Validate secure key entry for password field ----//
    
    if (textField==_txtOldPass||textField==_txtConfPass)
    {        textField.secureTextEntry=YES;    }
    
    //---------- Validate keyboard type for email address --------//
    
    
    return textField;//------- Return fully updated textfield
}

#pragma mark- TextField DelegateMethods
-(void)resignTxt:(UITextField*)txt
{
    //----------- Validate textfield return action ------ //
    if (txt==_txtOldPass)
    {   [_txtConfPass becomeFirstResponder];
    }
    else if(txt==_txtConfPass)
    {
        
    }
    else
    {   [txt resignFirstResponder];    }
    //------------------------------------------------ //
}

#pragma mark: TextField Validation Here
-(BOOL)isValidTextfield
{
    NSString *strMsg;
    if(_txtOldPass.text!=nil && _txtOldPass.text.length>0)
    {
        if (_txtConfPass.text!=nil && _txtConfPass.text.length>0)
        {
            return YES;
        }
        else
        {
            strMsg=NSLocalizedString(@"Please New Password.",nil);
        }
    }
    else
    {
        strMsg=NSLocalizedString(@"Enter Old Password.",nil);
    }
    
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Warning"  message:strMsg  preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    return NO;
}

#pragma mark: UIButton Pressed delegate declarations here
-(IBAction)btnBackPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)btnUpdatePressed:(id)sender
{
    if ([self isValidTextfield]) {
        [self updatePasswordServerCall];
    }
    
}

#pragma mark: User SignIn Server Call
-(void)updatePasswordServerCall
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
          [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *urlString=base_URL;
        NSLog(@"%@",[theAppDelegate.arrUserLoginData objectAtIndex:5]);
        
        NSLog(@"%@",[theAppDelegate.arrUserLoginData objectAtIndex:6]);
        
        NSString *strURL=[NSString stringWithFormat:@"wsprofileupdatepassword.php?uid=%@&password=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],_txtConfPass.text];
        urlString=[urlString stringByAppendingString:strURL];
        NSURL *urlSignIn=[NSURL URLWithString:urlString];
        request=[NSMutableURLRequest requestWithURL:[urlSignIn standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"The Internet connection appears to be offline" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    request = nil;
}

#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    [theAppDelegate stopSpinner];
    [self.view setUserInteractionEnabled:YES];
    // [theAppDelegate stopSpinner];
    _txtOldPass.text=@"";
    _txtConfPass.text=@"";
    
    dict =[[NSMutableDictionary alloc]initWithDictionary:response];
    NSString *strMsg=[dict valueForKey:@"message"];
    NSLog(@"%@",strMsg);
    if ([strMsg isEqualToString:@"sus"])
    {
        _strAlertTitle=@"Password Updated successfully";
        [self viewForAlertPopUp];
    }
    else
    {
        _strAlertTitle=@"Failed to update password";
        [self viewForAlertPopUp];
    }
}


#pragma mark: view For Alert
-(void)viewForAlertPopUp
{
    [_viewForAlert removeFromSuperview];
    _viewForAlert =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForAlert.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForAlert.tag=20;
    [self.view addSubview:_viewForAlert];
    
    CGRect frameForViewForHelp=_viewForAlert.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForAlert.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForAlert.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UILabel *lblAlert=[[UILabel alloc]initWithFrame:CGRectMake(0,_viewForAlert.frame.size.height/3,_viewForAlert.frame.size.width,30)];
        lblAlert.text=_strAlertTitle;
        lblAlert.textAlignment=NSTextAlignmentCenter;
        lblAlert.textColor=[UIColor whiteColor];
        lblAlert.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:20];
        //[lblAlert sizeToFit];
        [_viewForAlert addSubview:lblAlert];
        
        _btnCloseAlert=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseAlert.frame=CGRectMake(_viewForAlert.frame.size.width/2-60,CGRectGetMaxY(lblAlert.frame)+50,120,40);
        }
        else
        {
            _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-60,_viewForAlert.frame.size.height/2,120,50)];
        }
        [_btnCloseAlert setTitle:@"Close" forState:UIControlStateNormal];
        [_btnCloseAlert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseAlert.layer.borderWidth=2.0;
        _btnCloseAlert.layer.cornerRadius=4.0;
        _btnCloseAlert.layer.borderColor=[[UIColor orangeColor]CGColor];
        [_btnCloseAlert addTarget:self action:@selector(viewForCloseAlert) forControlEvents:UIControlEventTouchUpInside];
        [_viewForAlert addSubview:_btnCloseAlert];
    }];
}

#pragma mark-Close Comment View
-(void)viewForCloseAlert
{
    @try
    {
        for(UIView *subview in [_viewForAlert subviews])
        {
            [subview removeFromSuperview];
        }
        
        
        
        CGRect frameForViewAlert=_viewForAlert.frame;
        
        frameForViewAlert=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForAlert.frame=frameForViewAlert;
            
        } completion:^(BOOL finished){
            [_viewForAlert removeFromSuperview];
            
            
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

@end
