//
//  UpdatePassword.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 24/10/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderAndConstants.h"
#import "DBManager.h"
#import "AppDelegate.h"
#import "WebConnection1.h"

@interface UpdatePassword : UIViewController<WebRequestResult1>
{
    DBManager *objDBManager;
    WebConnection1 *connection;
    NSMutableDictionary *dict;
    CGRect vscreenSize;
    CGFloat vscreenHeight,vscreenWidth;
    
}

@property (nonatomic)IBOutlet UIView *viewForAlert;

#pragma mark: UIButton Properties declarations here
@property(nonatomic)IBOutlet UIButton *btnBack,*btnBackPress,*btnUpdate,*btnCloseAlert;

#pragma mark: UITextField Properties declarations here
@property (nonatomic)IBOutlet UITextField *txtOldPass,*txtConfPass;

#pragma mark: UIImageView Properties declarations here
@property (nonatomic)IBOutlet UIImageView *imgTrangleBG;

@property(nonatomic)NSString *strAlertTitle;
@end
