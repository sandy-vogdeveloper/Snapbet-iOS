//
//  BlockedUserListScreen.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 12/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "BlockedUserListScreen.h"

@interface BlockedUserListScreen ()

@end

@implementation BlockedUserListScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    _arrBlockedUsers=[[NSMutableArray alloc]init];
    _arrUnBlockedUsers=[[NSMutableArray alloc]init];
    
    [self loadInitialUI];
    [ self getBlockedAndUnblockedUserAPI];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark: Load InitialUI
-(void)loadInitialUI
{
    _btnBack=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBack.frame = CGRectMake(5,8,25,25);
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"iconBackArrow.png"] forState:UIControlStateNormal];
    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    _btnBackPress=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBackPress=[[UIButton alloc]initWithFrame:CGRectMake(0,0,55,30)];
    [_btnBackPress addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnBack];
    [self.view addSubview:_btnBackPress];

   
    _tblBlockUsers=[[UITableView alloc]initWithFrame:CGRectMake(0,50,screenWidth,screenHeight-50)];
    _tblBlockUsers.delegate=self;
    _tblBlockUsers.dataSource=self;
    _tblBlockUsers.tableFooterView=[[UIView alloc]init];
    _tblBlockUsers.backgroundColor=[UIColor clearColor];
    [self.view addSubview:_tblBlockUsers];
}

#pragma mark: UIButton pressed delegate declaratinos here
-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnBlockPressed:(id)sender
{
    _isBlockUser=YES;
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    NSLog(@"%ld",(long)row);
    _strReciverID=[[_arrUnBlockedUsers valueForKey:@"uid"]objectAtIndex:row];
    [self userBlkUnBlkAPI];
}

-(IBAction)btnUnBlockPressed:(id)sender
{
    _isBlockUser=NO;
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    NSLog(@"%ld",(long)row);
        _strReciverID=[[_arrBlockedUsers valueForKey:@"uid"]objectAtIndex:row];
    [self userBlkUnBlkAPI];
}


#pragma mark: view For Alert
-(void)viewForAlertUI
{
    [_viewForAlert removeFromSuperview];
    _viewForAlert =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForAlert.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForAlert.tag=20;
    [self.view addSubview:_viewForAlert];
    
    CGRect frameForViewForHelp=_viewForAlert.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForAlert.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForAlert.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForAlert addSubview:img];
        UILabel *lblAlert=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForAlert.frame.size.width,30)];
        lblAlert.text=_strAlertTitle;//_strVoteAlertTitle
        lblAlert.textAlignment=NSTextAlignmentCenter;
        lblAlert.textColor=[UIColor whiteColor];
        lblAlert.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForAlert addSubview:lblAlert];
        
        _btnCloseAlert=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseAlert.frame=CGRectMake(_viewForAlert.frame.size.width/2-60,CGRectGetMaxY(lblAlert.frame)+50,120,40);
        }
        else
        {
            _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,CGRectGetMaxY(lblAlert.frame)+50,100,40)];
        }
        [_btnCloseAlert setTitle:@"Close" forState:UIControlStateNormal];
        [_btnCloseAlert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseAlert.layer.borderWidth=2.0;
        _btnCloseAlert.layer.cornerRadius=4.0;
        _btnCloseAlert.layer.borderColor=[[UIColor orangeColor]CGColor];
        //[_btnVoteSeeMore addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        [_btnCloseAlert addTarget:self action:@selector(viewForCloseAlert) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewForAlert addSubview:_btnCloseAlert];
    }];
}

#pragma mark-Close Comment View
-(void)viewForCloseAlert
{
    @try
    {
        for(UIView *subview in [_viewForAlert subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewAlert=_viewForAlert.frame;
        
        
        frameForViewAlert=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForAlert.frame=frameForViewAlert;
            
        } completion:^(BOOL finished){
            [_viewForAlert removeFromSuperview];
            
       }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}


/*------    TableView delegate and datasource declaration here   -------*/

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
#pragma mark- TableView DataSource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return [_arrBlockedUsers count];
    }
    else
    {
        return [_arrUnBlockedUsers count];
    }
    return 0;
}

//--------------------- cellforrow At Index TableView -----------------------//

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@""];
    if (cell == nil)
    {
        //  cell.transform= CGAffineTransformMakeRotation(M_PI/2);
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    UILabel *lblCellTitle=[[UILabel alloc]init];
    UILabel *lblSubTitle=[[UILabel alloc]init];
   
        lblCellTitle.frame=CGRectMake(5,5,screenWidth-10,30);
        lblSubTitle.frame=CGRectMake(5,CGRectGetMaxY(lblCellTitle.frame)-10,screenWidth-10,30);
   
    if (indexPath.section==0) {
        _btnUnblock=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-80,10,70,30)];
    }
    else if (indexPath.section==1)
    {
        _btnBlock=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-80,10,70,30)];
    }
    
    if (indexPath.section==0)
    {
        lblCellTitle.text=[[_arrBlockedUsers valueForKey:@"name"]objectAtIndex:indexPath.row];
       // lblSubTitle.text=@"therealfrank";
        [_btnUnblock setTitle:@"Unblock" forState:UIControlStateNormal];
    }
    else
    {
        lblCellTitle.text=[[_arrUnBlockedUsers valueForKey:@"name"]objectAtIndex:indexPath.row];
      //  lblSubTitle.text=@"therealfrank";
        [_btnBlock setTitle:@"Block" forState:UIControlStateNormal];
    }
    lblCellTitle.textColor=[UIColor blackColor];
    lblSubTitle.textColor=[UIColor blackColor];
    
    lblCellTitle.font=[UIFont fontWithName:@"AvenirNext-Regular" size:14];
    lblSubTitle.font=[UIFont fontWithName:@"AvenirNext-Regular" size:12];
    
    [_btnBlock setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnUnblock setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
   
    _btnBlock.titleLabel.font=[UIFont fontWithName:@"AvenirNext-Regular" size:14];
    _btnUnblock.titleLabel.font=[UIFont fontWithName:@"AvenirNext-Regular" size:14];
   
    _btnBlock.layer.cornerRadius=5.0;
    _btnUnblock.layer.cornerRadius=5.0;
    
    _btnBlock.backgroundColor=[UIColor orangeColor];
    _btnUnblock.backgroundColor=[UIColor orangeColor];
    
    [_btnBlock addTarget:self action:@selector(btnBlockPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_btnUnblock addTarget:self action:@selector(btnUnBlockPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.contentView addSubview:lblCellTitle];
    [cell.contentView addSubview:lblSubTitle];
   
    if (indexPath.section==0) {
        [cell.contentView addSubview:_btnUnblock];
        _btnUnblock.tag=indexPath.row;
    }
    else
    {
        [cell.contentView addSubview:_btnBlock];
        _btnBlock.tag=indexPath.row;
    }
    
     cell.backgroundColor=[[UIColor whiteColor]colorWithAlphaComponent:.6];
    UIView *viewForCellSept=[[UIView alloc]initWithFrame:CGRectMake(0,49,screenWidth,1)];
    viewForCellSept.backgroundColor=[UIColor darkGrayColor];
    [cell.contentView addSubview:viewForCellSept];
    
    return cell;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    _lblTags=[[UILabel alloc] init];
    _lblTags.font=[UIFont fontWithName:@"AvenirNext-Regular" size:16];
    NSString *strSelectCat;   
    
    if (section==0) {
        strSelectCat=@"  Blocked Users";
        _lblTags.text=strSelectCat;
    }
    else
    {
        strSelectCat=@"  Unblocked Users";
        _lblTags.text=strSelectCat;
    }

    _lblTags.backgroundColor=[UIColor clearColor];
    CGSize sizeSelectCat=[strSelectCat sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-Regular" size:16]}];
    [_lblTags setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/100*20,25,sizeSelectCat.width, 30)];
    [_lblTags sizeToFit];
    return _lblTags;
}
#pragma Mark-delegate methods
//--------------------- Tableview didselected index ---------------------//
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

#pragma mark: Get BlockedUnblocked User API
-(void)getBlockedAndUnblockedUserAPI
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        _arrBlockedUsers=[[NSMutableArray alloc]init];
        _arrUnBlockedUsers=[[NSMutableArray alloc]init];
        _isBListAPI=YES;
        _isBlockedUnB=NO;
        NSString *urlString;
        NSString *strBaseURL=base_URL;
        urlString=[NSString stringWithFormat:@"wsfriendlist_1.php?uid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0]];
        
        strBaseURL=[strBaseURL stringByAppendingString:urlString];
        NSURL *urlAllTicket=[NSURL URLWithString:strBaseURL];
        request=[NSMutableURLRequest requestWithURL:[urlAllTicket standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
        request = nil;
    }
    else
    {
        
    }
}

#pragma mark: SrverAPI to Block and Unblock user
-(void)userBlkUnBlkAPI
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *urlString;
        _isBListAPI=NO;
        _isBlockedUnB=YES;
        NSString *strBaseURL=base_URL;
        if (_isBlockUser) {
           
            urlString =[NSString stringWithFormat:@"wsblockunblock_1.php?senderid=%@&receiverid=%@&status=B",[theAppDelegate.arrUserLoginData objectAtIndex:0],_strReciverID];
        }
        else
        {
            urlString=[NSString stringWithFormat:@"wsblockunblock_1.php?senderid=%@&receiverid=%@&status=A",[theAppDelegate.arrUserLoginData objectAtIndex:0],_strReciverID];
        }
        
        strBaseURL=[strBaseURL stringByAppendingString:urlString];
        NSURL *url=[NSURL URLWithString:strBaseURL];
        request=[NSMutableURLRequest requestWithURL:[url standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
        request = nil;
    }
    else
    {
        
    }
    
}

#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    theAppDelegate.stopSpinner;
    [self.view setUserInteractionEnabled:YES];
    dict=[[NSMutableDictionary alloc]init];
    dict =[[NSMutableDictionary alloc]initWithDictionary:response];
    NSString *strMessage=[dict valueForKey:@"message"];
    if (_isBListAPI) {
        if ([strMessage isEqualToString:@"sus"]) {
            
            NSMutableArray *arrInfo=[[NSMutableArray alloc]init];
            NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
            arrTemp=[dict valueForKey:@"info"];
            
            if ([arrTemp isEqual:(id)[NSNull null]]) {
                
            }
            else
            {
                arrInfo=arrTemp;
                for (int i=0;i<[arrInfo count];i++)
                {
                    NSString *strStatus=[[arrInfo valueForKey:@"status"]objectAtIndex:i];
                    
                    if ([strStatus isEqualToString:@"B"])
                    {
                        [_arrBlockedUsers addObject:[arrInfo objectAtIndex:i]];
                    }
                    else
                    {
                        [_arrUnBlockedUsers addObject:[arrInfo objectAtIndex:i]];
                    }                    
                }
                [_tblBlockUsers reloadData];
            }
            
        }
        else
        {
            
        }
    }
    else if (_isBlockedUnB)
    {
        if ([strMessage isEqualToString:@"sus"])
        {
            if (_isBlockUser) {
                _strAlertTitle=@"User Blocked Successfully";
                [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:0.5];
                
                [self performSelector:@selector(getBlockedAndUnblockedUserAPI) withObject:nil afterDelay:1];
            }
            else
            {
                _strAlertTitle=@"User Unblocked Successfully";
                [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:0.5];
                [self performSelector:@selector(getBlockedAndUnblockedUserAPI) withObject:nil afterDelay:1];
            }
        }
        else
        {
            
        }
    }
}
@end
