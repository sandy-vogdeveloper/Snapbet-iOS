//
//  BlockedUserListScreen.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 12/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderAndConstants.h"
#import "AppDelegate.h"
#import "WebConnection1.h"

@interface BlockedUserListScreen : UIViewController<UITableViewDelegate,UITableViewDataSource,WebRequestResult1>
{
    WebConnection1 *connection;
    NSMutableDictionary *dict;
    CGRect vscreenSize;
    CGFloat vscreenHeight,vscreenWidth;
    
}

#pragma mark: UIView Properties declarations here
@property (nonatomic) UIView *viewForAlert;

#pragma mark:UIButton Properties declarations here
@property (nonatomic)IBOutlet UIButton *btnBack,*btnBackPress,*btnBlock,*btnUnblock,*btnCloseAlert;

#pragma mark: UITableView Properties declarations here
@property(nonatomic)IBOutlet UITableView *tblBlockUsers;

#pragma mark: UILabel Properties declarations here
@property (nonatomic)IBOutlet UILabel *lblTags;

#pragma mark: Bool Properties declarations here
@property (nonatomic)BOOL isBListAPI,isBlockedUnB,isBlockUser;

#pragma mark: NSString Properties declarations here
@property (nonatomic)NSString *strReciverID,*strAlertTitle;

#pragma mark: NSMutableArray Properties declarations here
@property (nonatomic)NSMutableArray *arrBUser,*arrUnBUser,*arrBlockedUsers,*arrUnBlockedUsers;
@end
