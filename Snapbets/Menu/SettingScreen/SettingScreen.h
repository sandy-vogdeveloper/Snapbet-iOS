//
//  SettingScreen.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 08/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderAndConstants.h"
#import "BlockedUserListScreen.h"
#import "UpdateProfileScreen.h"
#import "APPChildViewController.h"
#import "WebConnection1.h"
#import "DBManager.h"
#import "ViewController.h"
#import "FrndRequestScreen/FrndRequestScreen.h"
#import <MessageUI/MessageUI.h>
@interface SettingScreen : UIViewController<UITableViewDelegate,UITableViewDataSource,UIPageViewControllerDataSource,WebRequestResult1,MFMailComposeViewControllerDelegate>
{
    CGRect vscreenSize;
    CGFloat vscreenHeight,vscreenWidth;
    NSInteger curntIndexPge;
    WebConnection1 *connection;
    NSMutableDictionary *dict;
    DBManager *objDBManager;
    NSUInteger pageViewIndex,slectedShareIndex;
}

#pragma mark:UIButton Properties declarations here
@property (nonatomic)IBOutlet UIButton *btnBack,*btnBackPress,*btnBlockUsers,*btnCloseTandC,*btnClose,*btnNext,*btnLogOut,*btnClosePandP;

#pragma mark: UILabel Properties declarations here
@property (nonatomic)IBOutlet UILabel *lblTags;

#pragma mark: NSMutableArray properties declarations here
@property (nonatomic)NSMutableArray *arrSectionList;

#pragma mark: UITableView properties declarations here
@property(nonatomic)IBOutlet UITableView *tblSetting;

#pragma mark: UISwitch properties declarations here
@property (nonatomic)IBOutlet UISwitch *switchAutoAccNFriend,*switchAllFbFrnds,*switchSilenceNoti,*switchProfileToPrivate;

#pragma mark: UIView properties declarations here
@property(nonatomic)IBOutlet UIView *viewForTandC,*viewForSocialLinking,*viewForTitle,*viewForPandP;

#pragma mark- UIPageViewController Properties
@property (strong, nonatomic) UIPageViewController *pageController;

#pragma mark: UIView Properties declaratiosn here
@property (nonatomic)IBOutlet UIView *viewForTutorial;

#pragma mark: NSMutable Array Properties declarations here
@property (nonatomic) NSMutableArray *arrDefaultSetting;

#pragma mark: NSString Properties declarations here
@property (nonatomic) NSString *strYesNo,*strSelectedSetting,*strAutofbfriend,*strFBfriends,*strNotification,*strPrivacy,*strSocialURL;

#pragma mark: Bool Properties declarations here
@property(nonatomic) BOOL isLogOut,isSettingUpdate,isDSetting,isAFrndDisable,isSOn,isSOn1,isSOn2,isSOn3;
@end
