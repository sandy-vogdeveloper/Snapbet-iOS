//
//  NotificationScreen.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 08/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "NotificationScreen.h"

@interface NotificationScreen ()

@end

@implementation NotificationScreen

- (void)viewDidLoad
{
    [super viewDidLoad];
    _arrNotificationData=[[NSMutableArray alloc]init];
    objDBManager=[DBManager getSharedInstance];
     [self loadInitialUI];
    [self getDBData];
   
    // Do any additional setup after loading the view from its nib.
}

-(void)getDBData
{
    NSArray *arrDBData=[objDBManager selectTableDataWithQuery:@"select * from tblNotifications"];
    if ([arrDBData count])
    {
        _arrNotificationData=[arrDBData mutableCopy];
        NSLog(@"%@",_arrNotificationData);
        [_tblNotifications reloadData];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}


-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark: Load Initial UI
-(void)loadInitialUI
{
    _btnBack=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBack.frame = CGRectMake(5,8,25,25);
    
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"iconBackArrow.png"] forState:UIControlStateNormal];
    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    /*----------- Button Pressed Event Fired and goto SidebarViewController--------*/
    _btnBackPress=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBackPress.frame = CGRectMake(0,0,105,55);
    [_btnBackPress addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnBack];
    [self.view addSubview:_btnBackPress];
    
    _tblNotifications=[[UITableView alloc]initWithFrame:CGRectMake(0,50,screenWidth,screenHeight-50)];
    _tblNotifications.delegate=self;
    _tblNotifications.dataSource=self;
    _tblNotifications.backgroundColor=[UIColor clearColor];
    _tblNotifications.tableFooterView=[[UIView alloc]init];
    [self.view addSubview:_tblNotifications];
}

#pragma mark: UIButton Pressed delegate declarations here
-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*--------------     Bottom TableView delegate and datasource declaration here     -----------*/
#pragma mark- TableView DataSource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_arrNotificationData count];
}

//--------------------- cellforrow At Index TableView -----------------------//

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@""];
    if (cell == nil)
    {
        //  cell.transform= CGAffineTransformMakeRotation(M_PI/2);
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    UILabel *lblTitle1=[[UILabel alloc]initWithFrame:CGRectMake(10,10,cell.frame.size.width,0)];
    lblTitle1.text=[[_arrNotificationData objectAtIndex:indexPath.row]objectAtIndex:6];
    lblTitle1.textColor=[UIColor whiteColor];
    lblTitle1.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
    [lblTitle1 setNumberOfLines:0];
    [lblTitle1 sizeToFit];
    [cell.contentView addSubview:lblTitle1];
    
    UILabel *lblTitle2=[[UILabel alloc]initWithFrame:CGRectMake(10,CGRectGetMaxY(lblTitle1.frame)+5,cell.frame.size.width,30)];
    NSString *strSubTitle=[[_arrNotificationData objectAtIndex:indexPath.row]objectAtIndex:3];
    if ([strSubTitle isEqualToString:@"snap"])
    {
        strSubTitle=@"Snapbets";
    }
    lblTitle2.text=[strSubTitle capitalizedString];
    lblTitle2.textColor=[UIColor whiteColor];
    lblTitle2.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:13];
    //[cell.contentView addSubview:lblTitle2];
    
    tableView.separatorColor =UIColor.clearColor;
    UIView *viewSept=[[UIView alloc]initWithFrame:CGRectMake(0,69,screenWidth,1)];
    viewSept.backgroundColor=[UIColor whiteColor];
    [cell.contentView addSubview:viewSept];
    
    cell.backgroundColor= [[UIColor colorWithRed:80/255.0 green:54/255.0 blue:21/255.0 alpha:1]colorWithAlphaComponent:.6];
    return cell;
}

#pragma Mark-delegate methods
//--------------------- Tableview didselected index ---------------------//
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strAlert=[[_arrNotificationData objectAtIndex:indexPath.row]objectAtIndex:3];
    if ([strAlert isEqualToString:@"sanp"]||[strAlert isEqualToString:@"snap"]||[strAlert isEqualToString:@"invite"]|| [strAlert isEqualToString:@"vote"] || [strAlert isEqualToString:@"join_snap"])
    {
        SpecifiBetPage *specificBet=[[SpecifiBetPage alloc]initWithNibName:@"SpecifiBetPage" bundle:nil];
        specificBet.strThemeName=[[_arrNotificationData objectAtIndex:indexPath.row]objectAtIndex:2];
        specificBet.selectedBetID1=[[_arrNotificationData objectAtIndex:indexPath.row]objectAtIndex:1];
        specificBet.strBetImg=[[_arrNotificationData objectAtIndex:indexPath.row]objectAtIndex:5];
        specificBet.strSelectBUID=[[_arrNotificationData objectAtIndex:indexPath.row]objectAtIndex:0];
        [self.navigationController pushViewController:specificBet animated:YES];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

@end
