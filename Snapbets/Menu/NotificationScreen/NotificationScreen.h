//
//  NotificationScreen.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 08/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderAndConstants.h"
#import "DBManager.h"
#import "SpecifiBetPage.h"

@interface NotificationScreen : UIViewController<UITableViewDataSource,UITableViewDelegate>

{
    DBManager *objDBManager;
}
#pragma mark:UIButton Properties declarations here
@property (nonatomic)IBOutlet UIButton *btnBack,*btnBackPress;

@property (nonatomic)IBOutlet UITableView *tblNotifications;

@property(nonatomic)NSMutableArray *arrNotificationData;
@end
