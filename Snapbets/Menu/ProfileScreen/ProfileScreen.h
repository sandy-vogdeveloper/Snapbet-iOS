//
//  ProfileScreen.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 08/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderAndConstants.h"
#import "AsyncImageView.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface ProfileScreen : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    CGRect vscreenSize;
    CGFloat vscreenHeight,vscreenWidth;
}

@property (nonatomic) AVPlayerViewController *playerViewController;
#pragma mark: UIView properties declarations here
@property (nonatomic)IBOutlet UIView *viewForAddFrnd,*viewForVideoScreen,*viewForVideoArea;

#pragma mark: NSString Properties declarations here
@property (nonatomic)NSString *strFriendTitle;

#pragma mark: UIButton Properties declarations here
@property (nonatomic)IBOutlet UIButton *btnBack,*btnBackPress,*btnCloseFrndPopUp,*btnAddProfile,*btnVote,*btnPlay1,*btnPlay2,*btnPlay3,*btnCloseVideoScreen;

#pragma mark:UIImageView Properties declarations here
@property(nonatomic)IBOutlet UIImageView *imgProfileDetailsBG,*imgBetsBG,*imgUserProf,*imgProfBlue;
@property (nonatomic) IBOutlet AsyncImageView *asyProfileImg,*asyProfBlur,*asySelectedBG,*asyImgTumbnil;

#pragma mark: UITableView Properties declarations here
@property (nonatomic)IBOutlet UITableView *tblProfileDetails;

#pragma mark: NSMutableArray properties declarations here
@property(nonatomic)NSMutableArray *arrTblImges,*arrTblTitles,*arrSectionList,*arrProfileDetails,*arrActivebet,*arrBethistory,*arrResponse,*arrDataForTableDisplay;

#pragma mark: UILabel Properties declarations here
@property (nonatomic)IBOutlet UILabel *lblTags,*lblWins,*lblCreates,*lblResponse,*lblVotes,*lblUserName;

#pragma mark: NSString Properties declarations here
@property(nonatomic)NSString *strUserID,*strVideoName,*strBetImage,*strVideotitle;

#pragma mark: Bool properties declarations here
@property(nonatomic)BOOL isMenuCollapsed,isProfileAPI,isAddFrndAPI,isActiveBet,isResponses,isBitHistory;

@end
