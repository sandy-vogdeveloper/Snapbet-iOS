//
//  ProfileScreen.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 08/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "ProfileScreen.h"
#import "WebConnection1.h"

@interface ProfileScreen ()<WebRequestResult1>
{
    WebConnection1 *connection;
    NSMutableDictionary *dict;
}
@end

@implementation ProfileScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    _isActiveBet=NO;
    _isResponses=NO;
    _isBitHistory=NO;
    
    [self profileDetailsServerAPI];
    _arrProfileDetails=[[NSMutableArray alloc]init];
    [self loadInitialUI];
    
    _arrTblImges=[[NSMutableArray alloc]initWithObjects:@"icon_ActiveBts",@"icon_ProfResponse",@"icon_ProfBetHist",nil];
    _arrTblTitles=[[NSMutableArray alloc]initWithObjects:@"Active Bets",@"Responses",@"Bet History",nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark: Load InitialUI
-(void)loadInitialUI
{
    _asyProfBlur=[[AsyncImageView alloc]initWithFrame:CGRectMake(0,0,screenWidth,_imgProfBlue.frame.size.height)];
    _asyProfBlur.backgroundColor = [UIColor clearColor];
    [_asyProfBlur setContentMode:UIViewContentModeScaleAspectFill];
    _asyProfBlur.layer.masksToBounds=YES;
   // _asyProfBlur.layer.cornerRadius=(_asyProfBlur.frame.size.height/2);
    _asyProfBlur.alpha=0.7;
    [self.view addSubview:_asyProfBlur];
    
    /*-----------  Button For Menu Selection for SidebarViewController-----------*/
    _btnBack=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBack.frame = CGRectMake(5,8,25,25);
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"icon_BackMove"] forState:UIControlStateNormal];
    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    /*----------- Button Pressed Event Fired and goto SidebarViewController--------*/
    _btnBackPress=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBackPress.frame = CGRectMake(0,0,105,55);
    [_btnBackPress addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnBack];
    [self.view addSubview:_btnBackPress];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        _tblProfileDetails=[[UITableView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+250,screenWidth,screenHeight/2-30)];
    }
    else
    {
        if (screenWidth<325)
        {
            _tblProfileDetails=[[UITableView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+50,screenWidth,screenHeight/2-30)];
        }
        else
        {
            _tblProfileDetails=[[UITableView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_imgProfileDetailsBG.frame)+80,screenWidth,screenHeight/2-30)];
        }
    }
    [_btnAddProfile addTarget:self action:@selector(btnAddFriendPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    _tblProfileDetails.delegate=self;
    _tblProfileDetails.dataSource=self;
    _tblProfileDetails.tableFooterView=[[UIView alloc]init];
    _tblProfileDetails.backgroundColor=[UIColor clearColor];
    [self.view addSubview:_tblProfileDetails];
    if (theAppDelegate.isProfFromMenu)
    {       _btnAddProfile.hidden=YES;  }
    else{   _btnAddProfile.hidden=NO;   }
}

-(void)viewForUpdateData
{
    _lblWins.text=[_arrProfileDetails valueForKey:@"win"];
    _lblCreates.text=[NSString stringWithFormat:@"%@",[_arrProfileDetails valueForKey:@"created"]];
    _lblResponse.text=[_arrProfileDetails valueForKey:@"response"];
   _lblUserName.text=[_arrProfileDetails valueForKey:@"username"];
    NSString *strImg=[_arrProfileDetails valueForKey:@"profileimage"];
    
    CGFloat xPosition=_imgUserProf.frame.origin.x;
    CGFloat yPosition=_imgUserProf.frame.origin.y;
    
    if ([strImg containsString:@"https://graph.facebook.com/"])
    {
        NSLog(@"Found!!");
        _asyProfileImg=[[AsyncImageView alloc]initWithFrame:CGRectMake(xPosition,yPosition,_imgUserProf.frame.size.width,_imgUserProf.frame.size.height)];
        _asyProfileImg.backgroundColor = [UIColor clearColor];
         [_asyProfileImg loadImageFromURL:[NSURL URLWithString:strImg]];
        _asyProfileImg.contentMode=UIViewContentModeScaleAspectFit;
        _asyProfileImg.layer.masksToBounds=YES;
        _asyProfileImg.layer.cornerRadius =_asyProfileImg.frame.size.width / 2;
        _asyProfileImg.clipsToBounds = YES;
        [self.view bringSubviewToFront:_asyProfileImg];
        [self.view addSubview:_asyProfileImg];
        
        [_asyProfBlur loadImageFromURL:[NSURL URLWithString:strImg]];
        
    }
    else if ([strImg containsString:@"."])
    {
        NSString *strBase=ImgBase_URL;
        strBase=[strBase stringByAppendingString:strImg];
        
        _asyProfileImg=[[AsyncImageView alloc]initWithFrame:CGRectMake(xPosition,yPosition,_imgUserProf.frame.size.width,_imgUserProf.frame.size.height)];
        
        _asyProfileImg.backgroundColor = [UIColor clearColor];
        [_asyProfileImg setContentMode:UIViewContentModeScaleAspectFill];
        [_asyProfileImg loadImageFromURL:[NSURL URLWithString:strBase]];
        _asyProfileImg.layer.cornerRadius=_asyProfileImg.frame.size.height/2;
        [_asyProfileImg setClipsToBounds:YES];
        _asyProfileImg.layer.masksToBounds = YES;
        
        [self.view bringSubviewToFront:_asyProfileImg];
        [self.view addSubview:_asyProfileImg];
        
        [_asyProfBlur loadImageFromURL:[NSURL URLWithString:strBase]];
    }
    else
    {
            [self.view bringSubviewToFront:_imgUserProf];
        _imgUserProf.image=[UIImage imageNamed:@"icon_prof"];
        _imgProfBlue.image=[UIImage imageNamed:@"icon_prof"];
        _imgProfBlue.alpha=0.5;
    }
}
-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnAddFriendPressed:(id)sender
{
    [self addFriendServerAPI];
}
-(IBAction)btnVotePressed:(id)sender
{
    
}

-(IBAction)btnPlay1Pressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    NSInteger row=btn.tag;
    NSLog(@"%ld",row);
    if (_isActiveBet)
    {
        row=row-1;
        _strBetImage=[[_arrActivebet valueForKey:@"image"]objectAtIndex:row];
        _strVideoName=[[_arrActivebet valueForKey:@"video"]objectAtIndex:row];
        _strVideotitle=[[_arrActivebet valueForKey:@"name"]objectAtIndex:row];
        
        [self viewForVideoScreenPopUpUI];
    }
    else if (_isResponses)
    {
        row=row-2;
        _strBetImage=[[_arrResponse valueForKey:@"image"]objectAtIndex:row];
        _strVideoName=[[_arrResponse valueForKey:@"video"]objectAtIndex:row];
        _strVideotitle=[[_arrResponse valueForKey:@"name"]objectAtIndex:row];
        [self viewForVideoScreenPopUpUI];
    }
    else if (_isBitHistory)
    {
        row=row-3;
        _strBetImage=[[_arrBethistory valueForKey:@"image"]objectAtIndex:row];
        _strVideoName=[[_arrBethistory valueForKey:@"video"]objectAtIndex:row];
        _strVideotitle=[[_arrBethistory valueForKey:@"name"]objectAtIndex:row];
        [self viewForVideoScreenPopUpUI];
    }
}

#pragma mark-Close Comment View
-(void)closeViewButonPressed
{
    @try
    {
        for(UIView *subview in [_viewForAddFrnd subviews])
        {
            [subview removeFromSuperview];
        }
      
        CGRect frameForViewForAddFrnd=_viewForAddFrnd.frame;
      
        
        frameForViewForAddFrnd=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForAddFrnd.frame=frameForViewForAddFrnd;
         
        } completion:^(BOOL finished){
            [_viewForAddFrnd removeFromSuperview];
             //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark-Close Comment View
-(void)ViewForCloseVideoUI
{
    @try
    {
        for(UIView *subview in [_viewForVideoScreen subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForAddFrnd=_viewForVideoScreen.frame;
        
        
        frameForViewForAddFrnd=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            [_playerViewController.player pause];
            [_playerViewController.view removeFromSuperview];
            _viewForVideoScreen.frame=frameForViewForAddFrnd;
            
        } completion:^(BOOL finished){
            [_viewForVideoScreen removeFromSuperview];
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark: Viewfor Buy single Theme Package
-(void)viewForAddFriendUI
{
    [_viewForAddFrnd removeFromSuperview];
    _viewForAddFrnd =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForAddFrnd.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForAddFrnd.tag=20;
    [self.view addSubview:_viewForAddFrnd];
    CGRect frameForViewForHelp=_viewForAddFrnd.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForAddFrnd.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForAddFrnd.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UILabel *lblText=[[UILabel alloc]initWithFrame:CGRectMake(0,screenHeight/3,_viewForAddFrnd.frame.size.width,40)];
        lblText.text=_strFriendTitle;
        lblText.textAlignment=NSTextAlignmentCenter;
        lblText.textColor=[UIColor whiteColor];
        lblText.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:18];
        [_viewForAddFrnd addSubview:lblText];
        
         _btnCloseFrndPopUp=[[UIButton alloc]initWithFrame:CGRectMake(_viewForAddFrnd.frame.size.width/2-70,CGRectGetMaxY(lblText.frame)+50,140,40)];
        
        [_btnCloseFrndPopUp setTitle:@"Close" forState:UIControlStateNormal];
        
         [_btnCloseFrndPopUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
          _btnCloseFrndPopUp.layer.borderColor=[[UIColor orangeColor]CGColor];
        _btnCloseFrndPopUp.layer.borderWidth=2.0;
          _btnCloseFrndPopUp.layer.cornerRadius=5.0;
        
        [_btnCloseFrndPopUp addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
            [_viewForAddFrnd addSubview:_btnCloseFrndPopUp];
    }];
}

#pragma mark: Viewfor Play Video popUP UI
-(void)viewForVideoScreenPopUpUI
{
    [_viewForVideoScreen removeFromSuperview];
    _viewForVideoScreen =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForVideoScreen.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForVideoScreen.tag=20;
    [self.view addSubview:_viewForVideoScreen];
    CGRect frameForViewForHelp=_viewForVideoScreen.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForVideoScreen.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForVideoScreen.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        _viewForVideoArea=[[UIView alloc]init];
        UILabel *lbltitle=[[UILabel alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            lbltitle.frame=CGRectMake(0,[UIScreen mainScreen].bounds.size.height/100*20,_viewForVideoScreen.frame.size.width,30);
            _viewForVideoArea.frame=CGRectMake(60,[UIScreen mainScreen].bounds.size.height/100*25,_viewForVideoScreen.frame.size.width-120,[UIScreen mainScreen].bounds.size.height/100*50);
        }
        else
        {
            lbltitle.frame=CGRectMake(0,[UIScreen mainScreen].bounds.size.height/100*20,_viewForVideoScreen.frame.size.width,30);
            _viewForVideoArea.frame=CGRectMake(10,[UIScreen mainScreen].bounds.size.height/100*25,_viewForVideoScreen.frame.size.width-20,[UIScreen mainScreen].bounds.size.height/100*50);
        }
        
        lbltitle.text=_strVideotitle;
        lbltitle.textAlignment=NSTextAlignmentCenter;
        lbltitle.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:18];
        lbltitle.textColor=[UIColor whiteColor];
        [_viewForVideoScreen addSubview:lbltitle];
        
        _viewForVideoArea.backgroundColor=[UIColor colorWithRed:38/255.0 green:38/255.0 blue:38/255.0 alpha:1];
        _viewForVideoArea.layer.borderWidth=2.0;
        _viewForVideoArea.layer.borderColor=[[UIColor whiteColor]CGColor];
        [_viewForVideoScreen addSubview:_viewForVideoArea];
        _asySelectedBG=[[AsyncImageView alloc]initWithFrame:CGRectMake(0,0,_viewForVideoArea.frame.size.width,_viewForVideoArea.frame.size.height)];
        _asySelectedBG.backgroundColor = [UIColor clearColor];
        NSString *imgBaseURL=ImgBase_URL;
       
        imgBaseURL=[imgBaseURL stringByAppendingString:_strBetImage];
        
        NSString *webStr = [NSString stringWithFormat:@"%@",imgBaseURL];
        NSURL *imageUrl = [[NSURL alloc] initWithString:[webStr stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
        [_asySelectedBG setContentMode:UIViewContentModeScaleAspectFill];
        [_asySelectedBG setClipsToBounds:YES];
        [_asySelectedBG loadImageFromURL:imageUrl];
        [_asySelectedBG sizeToFit];
        [_viewForVideoArea addSubview:_asySelectedBG];
        
        if([_strVideoName isEqualToString:@"No"]||[_strVideoName isEqualToString:@"(null)"])
        {
            
        }
        else
        {
            UIButton *btnPlay=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoArea.frame.size.width/2-25,_viewForVideoArea.frame.size.height/2-22,50,50)];
            [btnPlay setBackgroundImage:[UIImage imageNamed:@"img_AppIcon"] forState:UIControlStateNormal];
            [btnPlay addTarget:self action:@selector(playVideo) forControlEvents:UIControlEventTouchUpInside];
            [_viewForVideoArea addSubview:btnPlay];
        }
        
        _btnCloseVideoScreen=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoScreen.frame.size.width-40,20,30,30)];
        [_btnCloseVideoScreen setBackgroundImage:[UIImage imageNamed:@"icon_Cross"] forState:UIControlStateNormal];
        [_btnCloseVideoScreen addTarget:self action:@selector(ViewForCloseVideoUI) forControlEvents:UIControlEventTouchUpInside];
        [_viewForVideoScreen addSubview:_btnCloseVideoScreen];
    }];
}

#pragma mark: Play selected challage video
-(void)playVideo
{
    [_playerViewController.view removeFromSuperview];
    NSString *strVBase=vBase_URL;
    NSString *strVideoName=_strVideoName;
    strVBase=[strVBase stringByAppendingString:strVideoName];
    
    NSURL *vURL=[NSURL URLWithString:strVBase];
    _playerViewController = [[AVPlayerViewController alloc] init];
    _playerViewController.player = [AVPlayer playerWithURL:vURL];
    _playerViewController.view.frame =CGRectMake(0,0,_viewForVideoArea.frame.size.width,_viewForVideoArea.frame.size.height);
    //[_playerViewController setVideoGravity:AVLayerVideoGravityResizeAspect];
     [_playerViewController.player play];
    _playerViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:_playerViewController animated:YES completion:nil];    
   
    //[_viewForVideoArea addSubview:_playerViewController.view];
}

/*--------------     Bottom TableView delegate and datasource declaration here     -----------*/
#pragma mark- TableView DataSource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_arrTblTitles count];
}

//--------------------- cellforrow At Index TableView -----------------------//

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@""];
    if (cell == nil)
    {
        //  cell.transform= CGAffineTransformMakeRotation(M_PI/2);
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    
    tableView.separatorColor =UIColor.clearColor;
    cell.backgroundColor= [UIColor clearColor];
    [self.tblProfileDetails setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;

    NSString *strTitleString=[_arrTblTitles objectAtIndex:indexPath.row];
    UIView *viewForCell=[[UIView alloc]init];
    [cell.contentView addSubview:viewForCell];
    
    UIImageView *imgCell=[[UIImageView alloc]initWithFrame:CGRectMake(5,5,30,30)];
    imgCell.image=[UIImage imageNamed:[_arrTblImges objectAtIndex:indexPath.row]];
    [viewForCell addSubview:imgCell];
    
    UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imgCell.frame)+30,5,100,30)];
    
    lblTitle.textColor=[UIColor blackColor];
    lblTitle.font=[UIFont fontWithName:@"Avenir-Light" size:16];
    [viewForCell addSubview:lblTitle];
    
    if ([strTitleString isEqualToString:@"Active Bets"]||[strTitleString isEqualToString:@"Responses"]||[strTitleString isEqualToString:@"Bet History"])
    {
        viewForCell.frame=CGRectMake(0,0,screenWidth,40);
        viewForCell.backgroundColor=[[UIColor whiteColor]colorWithAlphaComponent:.8];
        UIImageView *imgForward=[[UIImageView alloc]initWithFrame:CGRectMake(viewForCell.frame.size.width-30,10,20,20)];
        //imgForward.image=[UIImage imageNamed:@"icon_BlckFrwd"];
        [viewForCell addSubview:imgForward];
        lblTitle.text=[NSString stringWithFormat:@"%@",[_arrTblTitles objectAtIndex:indexPath.row]];
    }
    else
    {
        UIView *viewFirstBG=[[UIView alloc]init];
        UIView *viewForCell1=[[UIView alloc]init];
        UIImageView *imgTemp=[[UIImageView alloc]init];
        UILabel *lblVideoName=[[UILabel alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            viewForCell.frame=CGRectMake(-2,0,screenWidth+4,150);
            viewFirstBG.frame=CGRectMake(0,0,screenWidth/3,150);
            viewForCell1.frame=CGRectMake(CGRectGetMaxX(viewFirstBG.frame),0,screenWidth-100,150);
            imgTemp.frame=CGRectMake(0,viewForCell1.frame.size.height-35,screenWidth-viewFirstBG.frame.size.width*1.5,35);
            _btnVote=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imgTemp.frame),viewForCell1.frame.size.height-35,150,35)];
            lblVideoName.frame=CGRectMake(0,viewFirstBG.frame.size.height-35,viewFirstBG.frame.size.width,35);
        }
        else
        {
            viewForCell.frame=CGRectMake(-2,0,screenWidth+4,100);
            viewFirstBG.frame=CGRectMake(0,0,screenWidth/3,100);
            viewForCell1.frame=CGRectMake(CGRectGetMaxX(viewFirstBG.frame),0,screenWidth-100,100);
            imgTemp.frame=CGRectMake(0,viewForCell1.frame.size.height-30,viewForCell1.frame.size.width/2+23,30);
            _btnVote=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imgTemp.frame),viewForCell1.frame.size.height-30,100,30)];
            lblVideoName.frame=CGRectMake(0,viewFirstBG.frame.size.height-25,viewFirstBG.frame.size.width,25);
        }
        
        viewForCell.backgroundColor=[UIColor colorWithRed:243.0/255.0 green:144.0/255.0 blue:0/255.0 alpha:1];
        viewForCell.layer.borderWidth=1.0;
        viewForCell.layer.borderColor=[[UIColor whiteColor]CGColor];
    
        
        viewFirstBG.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.7];
        viewFirstBG.layer.borderWidth=1.0;
        viewFirstBG.layer.borderColor=[[UIColor whiteColor]CGColor];
        
        [viewForCell addSubview:viewFirstBG];
        
        NSString *strBURL=ImgBase_URL;
        
          strBURL=[strBURL stringByAppendingString:[[_arrDataForTableDisplay objectAtIndex:indexPath.row]valueForKey:@"image"]];
        
        NSString *webStr1 = [NSString stringWithFormat:@"%@",strBURL];
        NSURL *imageUrl1 = [[NSURL alloc] initWithString:[webStr1 stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
        
        _asyImgTumbnil=[[AsyncImageView alloc]init];
        _asyImgTumbnil.frame=CGRectMake(0,0,viewFirstBG.frame.size.width,viewFirstBG.frame.size.height);
        [_asyImgTumbnil loadImageFromURL:imageUrl1];
        [_asyImgTumbnil setContentMode:UIViewContentModeScaleAspectFill];
        [_asyImgTumbnil sizeToFit];
        _asyImgTumbnil.clipsToBounds = YES;
        [viewFirstBG bringSubviewToFront:_asyImgTumbnil];
        [viewFirstBG addSubview:_asyImgTumbnil];
        
        _btnPlay1=[[UIButton alloc]initWithFrame:CGRectMake(viewFirstBG.frame.size.width/2-25,viewFirstBG.frame.size.height/2-35,50,50)];
        [_btnPlay1 setBackgroundImage:[UIImage imageNamed:@"img_AppIcon"] forState:UIControlStateNormal];
        [_btnPlay1 addTarget:self action:@selector(btnPlay1Pressed:) forControlEvents:UIControlEventTouchUpInside];
        _btnPlay1.tag=indexPath.row;
        [viewFirstBG addSubview:_btnPlay1];
        
        NSLog(@"%ld",(long)indexPath.row);
    
        NSString *strVideoBy=[[_arrDataForTableDisplay objectAtIndex:indexPath.row]valueForKey:@"name"];
        
        lblVideoName.text=strVideoBy;
        lblVideoName.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:14];
        lblVideoName.textColor=[UIColor whiteColor];
        lblVideoName.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7];
        lblVideoName.textAlignment=NSTextAlignmentCenter;
        [viewFirstBG addSubview:lblVideoName];
        
        viewForCell1.backgroundColor=[UIColor colorWithRed:243.0/255.0 green:144.0/255.0 blue:0/255.0 alpha:1];
        viewForCell1.layer.borderWidth=1.0;
        viewForCell1.layer.borderColor=[[UIColor whiteColor]CGColor];
        [viewForCell addSubview:viewForCell1];
        
        UILabel *lblDisChallenge=[[UILabel alloc]initWithFrame:CGRectMake(5,5,viewForCell1.frame.size.width,0)];
        
        strVideoBy=[strVideoBy stringByAppendingString:@" Snow posted a response"];
        lblDisChallenge.text=strVideoBy;
        lblDisChallenge.textColor=[UIColor blackColor];
        lblDisChallenge.font=[UIFont fontWithName:@"Avenir-Medium" size:15];
        [lblDisChallenge setNumberOfLines:0];
        [lblDisChallenge sizeToFit];
        [viewForCell1 addSubview:lblDisChallenge];
        
        imgTemp.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7];
        
        UILabel *lblVotes=[[UILabel alloc]initWithFrame:CGRectMake(0,0,imgTemp.frame.size.width,30)];
        NSString *strVotes=@"Votes ";
        @try {
            strVotes=[strVotes stringByAppendingString:[[_arrDataForTableDisplay objectAtIndex:indexPath.row]valueForKey:@"votes"]];
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        lblVotes.text=strVotes;
        lblVotes.textColor=[UIColor whiteColor];
        lblVotes.textAlignment=NSTextAlignmentCenter;
        lblVotes.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
        [imgTemp addSubview:lblVotes];
        [viewForCell1 addSubview:imgTemp];
                
        [_btnVote setTitle:@"Vote" forState:UIControlStateNormal];
        [_btnVote setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnVote.titleLabel.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
        _btnVote.backgroundColor=[[UIColor colorWithRed:166/255.0 green:87/255.0 blue:19/255.0 alpha:1]colorWithAlphaComponent:0.7];
        [_btnVote addTarget:self action:@selector(btnVotePressed:) forControlEvents:UIControlEventTouchUpInside];
        [viewForCell1 addSubview:_btnVote];

    }
    
    if (indexPath.row==0)
    {
        UIImageView *imgBdge=[[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(lblTitle.frame)+30,5,45,30)];
        imgBdge.backgroundColor=[UIColor orangeColor];
        imgBdge.layer.cornerRadius=10.0;
        [viewForCell addSubview:imgBdge];
       
        UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(0,0,45,30)];
        NSInteger count=[_arrActivebet count];
        lbl.text=[NSString stringWithFormat:@"%ld",(long)count];
      
        lbl.textColor=[UIColor whiteColor];
        lbl.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
        lbl.textAlignment=NSTextAlignmentCenter;
        [imgBdge addSubview:lbl];
    }
    return cell;
}

#pragma Mark-delegate methods
//--------------------- Tableview didselected index ---------------------//
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strTitleString=[_arrTblTitles objectAtIndex:indexPath.row];
    
    if ([strTitleString isEqualToString:@"Active Bets"]) {
        
        if (_isMenuCollapsed)
        {
            _isMenuCollapsed=NO;
            _arrTblImges=[[NSMutableArray alloc]initWithObjects:@"icon_ActiveBts",@"icon_ProfResponse",@"icon_ProfBetHist",nil];
            _arrTblTitles=[[NSMutableArray alloc]initWithObjects:@"Active Bets",@"Responses",@"Bet History",nil];
            [_tblProfileDetails reloadData];
        }
        else
        {
            _isMenuCollapsed=YES;
            _isActiveBet=YES;
            _isBitHistory=NO;
            _isResponses=NO;
            
            _arrTblImges=[[NSMutableArray alloc]init];
            _arrTblTitles=[[NSMutableArray alloc]init];
            _arrDataForTableDisplay=[[NSMutableArray alloc]init];
            
            [_arrTblImges addObject:@"icon_ActiveBts"];
            [_arrTblTitles addObject:@"Active Bets"];
            
            for (int i=0;i<[_arrActivebet count];i++)
            {
                [_arrTblImges addObject:@""];
            }
            
            for (int j=0;j<[_arrActivebet count];j++)
            {
                [_arrTblTitles addObject:@""];
            }
            
            [_arrTblImges addObject:@"icon_ProfResponse"];
            [_arrTblImges addObject:@"icon_ProfBetHist"];
            
            [_arrTblTitles addObject:@"Responses"];
            [_arrTblTitles addObject:@"Bet History"];
            
            [_arrDataForTableDisplay addObject:@""];
           
            for (int i=0;i<[_arrActivebet count];i++)
            {
                NSMutableArray *arr=[_arrActivebet objectAtIndex:i];
                [_arrDataForTableDisplay addObject:arr];
            }
            [_arrDataForTableDisplay addObject:@""];
             [_arrDataForTableDisplay addObject:@""];
            [_tblProfileDetails reloadData];
        }
    }
    else if ([strTitleString isEqualToString:@"Responses"])
    {
        if (_isMenuCollapsed)
        {
            _isMenuCollapsed=NO;
            _arrTblImges=[[NSMutableArray alloc]initWithObjects:@"icon_ActiveBts",@"icon_ProfResponse",@"icon_ProfBetHist",nil];
            _arrTblTitles=[[NSMutableArray alloc]initWithObjects:@"Active Bets",@"Responses",@"Bet History",nil];
            [_tblProfileDetails reloadData];
        }
        else
        {
            _isMenuCollapsed=YES;
            _isResponses=YES;
            _isBitHistory=NO;
            _isActiveBet=NO;
            
            _arrTblImges=[[NSMutableArray alloc]init];
            _arrTblTitles=[[NSMutableArray alloc]init];
            _arrDataForTableDisplay=[[NSMutableArray alloc]init];
            
            [_arrTblImges addObject:@"icon_ActiveBts"];
            [_arrTblImges addObject:@"icon_ProfResponse"];
            
            [_arrTblTitles addObject:@"Active Bets"];
            [_arrTblTitles addObject:@"Responses"];
            
            for (int i=0;i<[_arrResponse count];i++)
            {
                [_arrTblImges addObject:@""];
            }
            
            for (int j=0;j<[_arrResponse count];j++)
            {
                [_arrTblTitles addObject:@""];
            }
            
            [_arrTblImges addObject:@"icon_ProfBetHist"];
            [_arrTblTitles addObject:@"Bet History"];
            
            [_arrDataForTableDisplay addObject:@""];
            [_arrDataForTableDisplay addObject:@""];
            for (int i=0;i<[_arrResponse count];i++)
            {
                NSMutableArray *arr=[_arrResponse objectAtIndex:i];
                [_arrDataForTableDisplay addObject:arr];
            }
            [_arrDataForTableDisplay addObject:@""];
            [_tblProfileDetails reloadData];
            
        }
    }
    else if ([strTitleString isEqualToString:@"Bet History"])
    {
        if (_isMenuCollapsed)
        {
            _isMenuCollapsed=NO;
            _arrTblImges=[[NSMutableArray alloc]initWithObjects:@"icon_ActiveBts",@"icon_ProfResponse",@"icon_ProfBetHist",nil];
            _arrTblTitles=[[NSMutableArray alloc]initWithObjects:@"Active Bets",@"Responses",@"Bet History",nil];
            [_tblProfileDetails reloadData];
        }
        else
        {
            _isMenuCollapsed=YES;
            _isBitHistory=YES;
            _isActiveBet=NO;
            _isResponses=NO;
            
            _arrTblImges=[[NSMutableArray alloc]init];
            _arrTblTitles=[[NSMutableArray alloc]init];
            _arrDataForTableDisplay=[[NSMutableArray alloc]init];
            
            [_arrTblImges addObject:@"icon_ActiveBts"];
            [_arrTblImges addObject:@"icon_ProfResponse"];
            [_arrTblImges addObject:@"icon_ProfBetHist"];
            
            [_arrTblTitles addObject:@"Active Bets"];
            [_arrTblTitles addObject:@"Responses"];
            [_arrTblTitles addObject:@"Bet History"];
            
            for (int i=0;i<[_arrBethistory count];i++)
            {
                [_arrTblImges addObject:@""];
            }
            
            for (int j=0;j<[_arrBethistory count];j++)
            {
                [_arrTblTitles addObject:@""];
            }
            [_arrDataForTableDisplay addObject:@""];
            [_arrDataForTableDisplay addObject:@""];
            [_arrDataForTableDisplay addObject:@""];
            for (int i=0;i<[_arrBethistory count];i++)
            {
                NSMutableArray *arr=[_arrBethistory objectAtIndex:i];
                [_arrDataForTableDisplay addObject:arr];
            }

            [_tblProfileDetails reloadData];
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strTitleString=[_arrTblTitles objectAtIndex:indexPath.row];

    if (_isMenuCollapsed)
    {
        if ([strTitleString isEqualToString:@"Active Bets"])
        {
            return 40;
        }
        else if ([strTitleString isEqualToString:@"Responses"])
        {
            return 40;
        }
        else if ([strTitleString isEqualToString:@"Bet History"])
        {
            return 40;
        }
        else
        {
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                return 150;
            }
            else
            {
                return 100;
            }
        }
    }
    else
    {
        return 50;
    }
    
}

#pragma mark: Add Friend API 
-(void)addFriendServerAPI
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isAddFrndAPI=YES;
        _isProfileAPI=NO;
        
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        
        NSString *strBaseURL=base_URL;
        NSString *urlString;
            urlString=[NSString stringWithFormat:@"wsaddfriend.php?senderid=%@&receiverid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],[_arrProfileDetails valueForKey:@"id"]];
        
        strBaseURL=[strBaseURL stringByAppendingString:urlString];
        NSURL *urlProfile=[NSURL URLWithString:strBaseURL];
        request=[NSMutableURLRequest requestWithURL:[urlProfile standardizedURL]];
    }
    if(!connection)
        connection= [[WebConnection1 alloc] init];
    connection.delegate = self;
    [connection makeConnection:request];
    request = nil;

}
#pragma mark: User SignIn Server Call
-(void)profileDetailsServerAPI
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isAddFrndAPI=NO;
        _isProfileAPI=YES;
         [theAppDelegate showSpinnerInView:self.view];
         [self.view setUserInteractionEnabled:NO];
    
         NSString *strBaseURL=base_URL;
        NSString *urlString;
        if (theAppDelegate.isProfFromMenu)
        {
            urlString=[NSString stringWithFormat:@"wsviewprofile.php?id=%@",[theAppDelegate.arrUserLoginData  objectAtIndex:0]];
        }
        else
        {
            urlString=[NSString stringWithFormat:@"wsviewprofile.php?id=%@",_strUserID];
        }
         strBaseURL=[strBaseURL stringByAppendingString:urlString];
         NSURL *urlProfile=[NSURL URLWithString:strBaseURL];
         request=[NSMutableURLRequest requestWithURL:[urlProfile standardizedURL]];
     }
    if(!connection)
        connection= [[WebConnection1 alloc] init];
    connection.delegate = self;
    [connection makeConnection:request];
    request = nil;
}

#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    @try
    {
        [self.view setUserInteractionEnabled:YES];
        [theAppDelegate stopSpinner];
        dict =[[NSMutableDictionary alloc]initWithDictionary:response];
        NSString *strMsg=[dict valueForKey:@"message"];
        if ([strMsg isEqualToString:@"sus"])
        {
            if (_isProfileAPI)
            {
                _arrBethistory=[[NSMutableArray alloc]init];
                _arrProfileDetails=[[NSMutableArray alloc]init];
                _arrBethistory=[[NSMutableArray alloc]init];
                _arrActivebet=[[NSMutableArray alloc]init];
                
                _arrProfileDetails=[dict valueForKey:@"info"];
                _arrActivebet=[dict valueForKey:@"activebet"];
                _arrBethistory=[dict valueForKey:@"bethistory"];
                _arrResponse=[dict valueForKey:@"responses"];
        
                [self viewForUpdateData];
                [_tblProfileDetails reloadData];
            }
            else if (_isAddFrndAPI)
            {
                _strFriendTitle=[_arrProfileDetails valueForKey:@"username"];
                NSArray *arr=[_strFriendTitle componentsSeparatedByString:@" "];
                _strFriendTitle=[arr objectAtIndex:0];
                _strFriendTitle=[_strFriendTitle stringByAppendingString:@" has been added as a friend!"];
                [self performSelector:@selector(viewForAddFriendUI) withObject:nil afterDelay:0.2];
            }
        }
        else
        {
            if(_isAddFrndAPI)
            {
                _strFriendTitle=[_arrProfileDetails valueForKey:@"username"];
                NSArray *arr=[_strFriendTitle componentsSeparatedByString:@" "];
                _strFriendTitle=[arr objectAtIndex:0];
            
                _strFriendTitle=[_strFriendTitle stringByAppendingString:@" allready added as a friend!"];
                [self performSelector:@selector(viewForAddFriendUI) withObject:nil afterDelay:0.2];
            }
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}
@end
