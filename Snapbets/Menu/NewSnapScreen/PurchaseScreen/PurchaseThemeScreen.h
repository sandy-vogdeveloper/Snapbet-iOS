//
//  PurchaseThemeScreen.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 13/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderAndConstants.h"
#import "NewSnapScreen.h"
#import "WebConnection1.h"
#import "SnapbetsThemeScreen.h"
#import "AsyncImageView.h"
#import "SnapbetsThemeScreen.h"

@interface PurchaseThemeScreen : UIViewController<UITableViewDataSource,UITableViewDelegate,WebRequestResult1,AVPlayerViewControllerDelegate>
{
    CGRect vscreenSize;
    CGFloat vscreenHeight,vscreenWidth;
    WebConnection1 *connection;
    NSMutableDictionary *dict;
    NSInteger selectedVideoIndex;
}

@property (nonatomic)AsyncImageView *asySelectedBG;
@property (nonatomic) AVPlayerViewController *playerViewController;
@property (nonatomic, strong) UIView *activityBackgroundView,*viewForLoadingVideos,*viewForLoader,*viewForVideoScreen,*viewForVideoArea;

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

#pragma mark: UIButton Properties declarations here
@property (nonatomic)IBOutlet UIButton *btnBack,*btnBackPress,*btnBuySnap;

#pragma mark: UITableView properties declarations here
@property (nonatomic)IBOutlet UITableView *tblPurchase;

@property   (nonatomic)NSMutableArray *arrPurchaseList;

@property (nonatomic)NSString *strVideoName,*strUplodedImg;

@property (nonatomic)BOOL isGetList,isUploadImg;

@property (nonatomic)UILabel *lblHedgTitle;
@end
