//
//  PurchaseThemeScreen.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 13/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "PurchaseThemeScreen.h"

@interface PurchaseThemeScreen ()
@property (nonatomic)NewSnapScreen *ns;
@end

@implementation PurchaseThemeScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    _isGetList=NO;
    _isUploadImg=NO;
    [self loadInitialUI];
    [self loadPurchaseList];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)prefersStatusBarHidden
{
    return true;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}
#pragma mark: LoadInitialUI
-(void)loadInitialUI
{
    _btnBack=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnBack.frame = CGRectMake(5,8,25,25);
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"icon_BackMove"] forState:UIControlStateNormal];
    
    _btnBackPress=[[UIButton alloc]initWithFrame:CGRectMake(0,0,55,30)];
    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_btnBackPress addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnBack];
    [self.view addSubview:_btnBackPress];
    
    _tblPurchase=[[UITableView alloc]initWithFrame:CGRectMake(0,35,screenWidth,screenHeight-35)];
    _tblPurchase.delegate=self;
    _tblPurchase.dataSource=self;
    _tblPurchase.tableFooterView=[[UIView alloc]init];
    _tblPurchase.backgroundColor=[UIColor clearColor];
    [self.view addSubview:_tblPurchase];
}

#pragma mark:UIButton Pressed delegate properties declaration here
-(IBAction)btnBackPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)btnSelectPressed:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSInteger row = button.tag;
    
    if (theAppDelegate.isConnected)
    {
        _ns=[[NewSnapScreen alloc]initWithNibName:@"NewSnapScreen" bundle:nil];
        theAppDelegate.strSelectedThmeID=[[_arrPurchaseList valueForKey:@"id"]objectAtIndex:row];
        theAppDelegate.strVideoName=[[_arrPurchaseList valueForKey:@"videourl"]objectAtIndex:row];
        theAppDelegate.strVTitle=[[_arrPurchaseList valueForKey:@"v_name"]objectAtIndex:row];
        theAppDelegate.strVDescription=[[_arrPurchaseList valueForKey:@"v_details"]objectAtIndex:row];
        _strVideoName=[[_arrPurchaseList valueForKey:@"videourl"]objectAtIndex:row];
        theAppDelegate.strUplodedImg=[[_arrPurchaseList valueForKey:@"thumbnail"]objectAtIndex:row];
        [self dismissViewControllerAnimated:YES completion:nil];
        //[self generateImage];
    }
    else
    {
        theAppDelegate.alert.message=@"Check Connection";
        [theAppDelegate.alert show];
    }
}

#pragma mark: UIButton BuySnap Pressed
-(IBAction)btnBuySnapPressed:(id)sender
{
    SnapbetsThemeScreen *buy=[[SnapbetsThemeScreen alloc]initWithNibName:@"SnapbetsThemeScreen" bundle:nil];
    [self.navigationController pushViewController:buy animated:YES];
}
#pragma mark: getnerate Thumbnil from video
-(void)generateImage
{
    NSString *strBase=vBase_URL;
    strBase=[strBase stringByAppendingString:_strVideoName];
    //strBase = [strBase stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    strBase=[strBase stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *url=[NSURL URLWithString:strBase];
    AVURLAsset *asset = [AVURLAsset assetWithURL:url];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generate.appliesPreferredTrackTransform=TRUE;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 120);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    NSLog(@"err==%@, imageRef==%@", err, imgRef);
    NSData *data = UIImageJPEGRepresentation([[UIImage alloc] initWithCGImage:imgRef],1);
    if(imgRef==NULL)
    {
        [theAppDelegate stopSpinner];
        [self.view setUserInteractionEnabled:YES];
    
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:@"Failed to upload Thumbil." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        UIImage *image=[[UIImage alloc]initWithData:data];
        [self serviceCallForUploadThumbnail:[self getCompressedImage:image]];
    }
}
-(NSData*)getCompressedImage:(UIImage*)img
{
    //>213832
    //------------  Image Compression --------- //
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    //int maxFileSize = 250*1024;
    int maxFileSize = 640*480;
    NSData  *imageData = UIImageJPEGRepresentation(img, compression);
    NSLog(@"Initial Size of Image==>%li",(long)imageData.length);
    
    while ([imageData length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        imageData = UIImageJPEGRepresentation(img, compression);
    }
    
    NSLog(@"Compressed Size of Image==>%li",(long)imageData.length);
    //------------  Image Compression Algorithm--------- //
    
    return imageData;
}
#pragma mark: UploadFirstImage
-(void)serviceCallForUploadThumbnail:(NSData*)imgData
{
    NSString *strBase=base_URL;
    NSString *url=[NSString stringWithFormat:@"uploadimage_ios.php"];
    strBase=[strBase stringByAppendingString:url];
    
    NSLog(@"Image upload URL :%@",url);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strBase]];
    
    NSData *imageData = imgData;    
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"unique-consistent-string";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"imageCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"Some Caption"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    if(imageData)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=imagename.png\r\n",@"userfile"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    // [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
   
    if (request!=nil)
    {
        _isGetList=NO;
        _isUploadImg=YES;
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
   
        if(!connection)
        connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    //synchronous filling of data from HTTP POST response
  /*  NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    //convert data into string
    NSString   *responseString = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];
    NSLog(@"Response String %@",responseString);
    
    NSError *error1;
    NSMutableDictionary *mainDict=[NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error1];
    NSArray *arr=[[NSArray alloc]init];
    arr=[mainDict valueForKey:@"info"];
    responseString=[arr description];
    
    if (responseString.length>0)
    {
        [theAppDelegate stopSpinner];
        [self.view setUserInteractionEnabled:YES];
        NSString *strBURL=ImgBase_URL;
        strBURL=[strBURL stringByAppendingString:responseString];
        //_ns.strUplodedImg=responseString;
        theAppDelegate.strUplodedImg=responseString;
        [self dismissViewControllerAnimated:YES completion:nil];
        
        //[self.navigationController pushViewController:_ns animated:YES];
    }
    else
    {
        [self hideLoadingIndicators];
        //_imgProfile.image=[UIImage imageNamed:@""];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:@"Failed to upload Thumbil." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        //[[[UIAlertView alloc]initWithTitle:@"Failed" message:@"Failed to upload picture to server." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    [self.view setUserInteractionEnabled:YES];
   */
}

/*------    TableView delegate and datasource declaration here   ------*/
#pragma mark: UITableView Delegate and Datasource properties declarations here
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_arrPurchaseList count];
}

//--------------------- cellforrow At Index TableView -----------------------//

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@""];
    if (cell == nil)
    {
        //  cell.transform= CGAffineTransformMakeRotation(M_PI/2);
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    tableView.separatorColor =UIColor.clearColor;
    cell.backgroundColor= [UIColor clearColor];
    [self.tblPurchase setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    UIView *viewForCell=[[UIView alloc]init];
    UIView *viewTemp=[[UIView alloc]initWithFrame:CGRectMake(-2,0,130,98)];
    
    UIView *viewSept1=[[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(viewTemp.frame),0,2,viewForCell.frame.size.height)];
    UILabel *lblBlank=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(viewSept1.frame),viewForCell.frame.size.height-30,120,30)];
    
    UIButton *btnSelect=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(lblBlank.frame),viewForCell.frame.size.height-30,80,30)];
    
    UIView *viewCellSept=[[UIView alloc]initWithFrame:CGRectMake(0,98,screenWidth,2)];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        viewForCell.frame=CGRectMake(0,0,screenWidth,149);
        viewTemp.frame=CGRectMake(-2,0,200,148);
        viewSept1.frame=CGRectMake(CGRectGetMaxX(viewTemp.frame),0,2,viewForCell.frame.size.height);
        lblBlank.frame=CGRectMake(CGRectGetMaxX(viewSept1.frame),viewForCell.frame.size.height-30,screenWidth-viewTemp.frame.size.width*2,30);
        btnSelect.frame=CGRectMake(CGRectGetMaxX(lblBlank.frame),viewForCell.frame.size.height-30,viewTemp.frame.size.width,30);
        viewCellSept.frame=CGRectMake(0,148,screenWidth,2);
    }
    else
    {
        viewForCell.frame=CGRectMake(0,0,screenWidth,99);
        viewTemp.frame=CGRectMake(-2,0,130,98);
        viewSept1.frame=CGRectMake(CGRectGetMaxX(viewTemp.frame),0,2,viewForCell.frame.size.height);
        if (screenWidth<325)
        {
            lblBlank.frame=CGRectMake(CGRectGetMaxX(viewSept1.frame),viewForCell.frame.size.height-30,120,30);
            btnSelect.frame=CGRectMake(CGRectGetMaxX(lblBlank.frame),viewForCell.frame.size.height-30,80,30);
        }
        else
        {
            lblBlank.frame=CGRectMake(CGRectGetMaxX(viewSept1.frame),viewForCell.frame.size.height-30,(screenWidth-viewTemp.frame.size.width)-80,30);
            btnSelect.frame=CGRectMake(viewForCell.frame.size.width-80,viewForCell.frame.size.height-30,80,30);
        }
        
        viewCellSept.frame=CGRectMake(0,98,screenWidth,2);
        
    }
    
    viewForCell.backgroundColor=[UIColor colorWithRed:243/255.0 green:144/255.0 blue:0/255.0 alpha:1];
    [cell.contentView addSubview:viewForCell];
    
     viewTemp.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.7];
    [viewForCell addSubview:viewTemp];
    
      viewSept1.backgroundColor=[UIColor whiteColor];
    [viewForCell addSubview:viewSept1];
    
    UIButton *btnPlay=[[UIButton alloc]initWithFrame:CGRectMake(viewTemp.frame.size.width/2-20,viewTemp.frame.size.height/2-30,40,40)];
    [btnPlay setBackgroundImage:[UIImage imageNamed:@"img_AppIcon"] forState:UIControlStateNormal];
     btnPlay.tag=indexPath.row;
    [btnPlay addTarget:self action:@selector(btnPlayVideoPressed:) forControlEvents:UIControlEventTouchUpInside];
    [viewTemp addSubview:btnPlay];
    
    UILabel *lblName=[[UILabel alloc]initWithFrame:CGRectMake(0,viewTemp.frame.size.height-25,viewTemp.frame.size.width,25)];
    lblName.text=[[_arrPurchaseList valueForKey:@"v_name"]objectAtIndex:indexPath.row];
    lblName.textColor=[UIColor whiteColor];
    lblName.textAlignment=NSTextAlignmentCenter;
    lblName.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.6];
    lblName.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
    [viewTemp addSubview:lblName];
    
    UILabel *lblChallngrsDetails=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(viewTemp.frame)+5,10,viewForCell.frame.size.width-130,0)];
    lblChallngrsDetails.text=[[_arrPurchaseList valueForKey:@"v_details"]objectAtIndex:indexPath.row];
    lblChallngrsDetails.textColor=[UIColor blackColor];
    lblChallngrsDetails.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
    [lblChallngrsDetails setNumberOfLines:3];
    [lblChallngrsDetails sizeToFit];
    [viewForCell addSubview:lblChallngrsDetails];
    
    lblBlank.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.6];
    [viewForCell addSubview:lblBlank];
    
     [btnSelect setTitle:@"Select" forState:UIControlStateNormal];
    btnSelect.titleLabel.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
    [btnSelect setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSelect.backgroundColor=[[UIColor colorWithRed:166/255.0 green:87/255.0 blue:19/255.0 alpha:1]colorWithAlphaComponent:0.6];
    btnSelect.tag=indexPath.row;
    [btnSelect addTarget:self action:@selector(btnSelectPressed:) forControlEvents:UIControlEventTouchUpInside];
    [viewForCell addSubview:btnSelect];
    viewCellSept.backgroundColor=[UIColor whiteColor];
    [viewForCell addSubview:viewCellSept];
    return cell;
}

#pragma Mark-delegate methods
//--------------------- Tableview didselected index ---------------------//
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        return 150;
    }
    else
    {
        
        return 100;
    }
}

#pragma mark: Upload new snap to server
-(void)loadPurchaseList
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isUploadImg=NO;
        _isGetList=YES;
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *urlString=base_URL;
        NSString *strURL=[NSString stringWithFormat:@"wsthemepurchaselist.php?uid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0]];
        urlString=[urlString stringByAppendingString:strURL];
        NSURL *urlSignIn=[NSURL URLWithString:urlString];
        request=[NSMutableURLRequest requestWithURL:[urlSignIn standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        
    }
    request = nil;
}
#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    [theAppDelegate stopSpinner];
    [self.view setUserInteractionEnabled:YES];
    // [theAppDelegate stopSpinner];
    
    dict =[[NSMutableDictionary alloc]initWithDictionary:response];
    NSString *strMsg=[dict valueForKey:@"message"];
    NSLog(@"%@",strMsg);
    if ([strMsg isEqualToString:@"sus"])
    {
        if (_isGetList)
        {
            _arrPurchaseList=[[NSMutableArray alloc]init];
            _arrPurchaseList=[dict valueForKey:@"info"];
            [_tblPurchase reloadData];
        }
        else if (_isUploadImg)
        {
            NSString *strImg=[dict valueForKey:@"info"];
            NSLog(@"%@",strImg);
            theAppDelegate.strUplodedImg=strImg;
            [self dismissViewControllerAnimated:YES completion:nil];
            //[self.navigationController pushViewController:_ns animated:YES];
        }
    }
    else if ([strMsg isEqualToString:@"not found"])
    {
        if(_isGetList)
        {
            [self viewForNoPurchase];
        }
    }
}

#pragma mark: Viewfor Loading alert
-(void)ViewForDataLoadingAlert
{
    [_viewForLoader removeFromSuperview];
    _viewForLoader=[[UIView alloc]initWithFrame:CGRectMake(screenWidth/2-100,screenHeight/3-40,200,150)];
    [self.view setUserInteractionEnabled:NO];
    _viewForLoader.backgroundColor=[UIColor whiteColor];
    _viewForLoader.layer.borderColor=[[UIColor blackColor]CGColor];
    _viewForLoader.layer.borderWidth=0.5;
    _viewForLoader.layer.cornerRadius=5.0;
    UILabel *lblLoad=[[UILabel alloc]initWithFrame:CGRectMake(0,5,_viewForLoader.frame.size.width,30)  ];
    lblLoad.text=@"Wait";
    lblLoad.textAlignment=NSTextAlignmentCenter;
    lblLoad.textColor=[UIColor blackColor];
    lblLoad.font=[UIFont boldSystemFontOfSize:14];
    [_viewForLoader addSubview:lblLoad];
    UILabel *lblTit=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblLoad.frame),_viewForLoader.frame.size.width,30)];
    lblTit.text=@"Processing";
    lblTit.textColor=[UIColor blackColor];
    lblTit.font=[UIFont boldSystemFontOfSize:16];
    lblTit.textAlignment=NSTextAlignmentCenter;
    [_viewForLoader addSubview:lblTit];
    
    static const CGFloat activityIndicatorSize = 40.f;
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:   UIActivityIndicatorViewStyleWhiteLarge];
    _activityIndicator.alpha = 0.f;
    _activityIndicator.hidesWhenStopped = YES;
    [_activityIndicator setFrame:CGRectMake(_viewForLoader.frame.size.width/2-20,_viewForLoader.frame.size.height-60,activityIndicatorSize,activityIndicatorSize)];
    [self.view addSubview:_viewForLoader];

    [_tblPurchase bringSubviewToFront:self.view];
    [self showLoadingIndicators];
}

#pragma mark: Show Loading Indicator
- (void)showLoadingIndicators
{
    [_viewForLoader addSubview:_activityIndicator];
    [_activityIndicator startAnimating];
    _activityIndicator.color=[UIColor blackColor];
    //_activityIndicator.backgroundColor=[UIColor blackColor];
    [UIView animateWithDuration:0.2f animations:^{
        _activityBackgroundView.alpha = 1.f;
        _activityIndicator.alpha = 1.f;
    }];
}

#pragma mark: Hide LoadingIndicator

- (void)hideLoadingIndicators
{
    [UIView animateWithDuration:0.2f delay:0.0 options:0 animations:^{
        self.activityBackgroundView.alpha = 0.0f;
        self.activityIndicator.alpha = 0.f;
    } completion:^(BOOL finished) {
        [self.activityBackgroundView removeFromSuperview];
        [self.activityIndicator removeFromSuperview];
    }];
}

#pragma mark: ViewForNoPurchase
-(void)viewForNoPurchase
{
    UIImageView *imgNPurchase=[[UIImageView alloc]initWithFrame:CGRectMake(0,50,screenWidth,screenHeight-40)];
    imgNPurchase.image=[UIImage imageNamed:@"img_NoPurchase"];
    [self.view addSubview:imgNPurchase];
    [self.view bringSubviewToFront:imgNPurchase];
    [_tblPurchase removeFromSuperview];
    UILabel *lblTit=[[UILabel alloc]initWithFrame:CGRectMake(50,80,screenWidth-100,40)];
    lblTit.text=@"No Purchases";
    lblTit.textColor=[UIColor blackColor];
    lblTit.font=[UIFont fontWithName:@"Avenir-Medium" size:35];
    lblTit.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:lblTit];
    
    UILabel *lblTit1=[[UILabel alloc]initWithFrame:CGRectMake(50,CGRectGetMaxY(lblTit.frame),screenWidth-100,40)];
    lblTit1.text=@"Available";
    lblTit1.textColor=[UIColor blackColor];
    lblTit1.font=[UIFont fontWithName:@"Avenir-Medium" size:35];
    lblTit1.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:lblTit1];    
    
    [_btnBuySnap removeFromSuperview];
    _btnBuySnap=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth/2-40,screenHeight/2+20,100,40)];
    [_btnBuySnap setTitle:@"Buy Snapbet" forState:UIControlStateNormal];
    [_btnBuySnap setBackgroundImage:[UIImage imageNamed:@"img_BuySnap"] forState:UIControlStateNormal];
    _btnBuySnap.titleLabel.font=[UIFont fontWithName:@"Avenir-Medium" size:16];
    [_btnBuySnap setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnBuySnap addTarget:self action:@selector(btnBuySnapPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnBuySnap];
}

-(IBAction)btnPlayVideoPressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    selectedVideoIndex=btn.tag;
    NSLog(@"%ld",(long)selectedVideoIndex);
    NSString *strVName=[[_arrPurchaseList valueForKey:@"videourl"]objectAtIndex:selectedVideoIndex];
    if ([strVName containsString:@".mp4"])
    {
        [self playVideo];
    }
}

#pragma mark: Play selected challage video
-(void)playVideo
{
    [_playerViewController.view removeFromSuperview];
    NSString *strVBase=vBase_URL;
    NSString *strVideoName=[[_arrPurchaseList valueForKey:@"videourl"]objectAtIndex:selectedVideoIndex];
    strVBase=[strVBase stringByAppendingString:strVideoName];
    strVBase=[strVBase stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *vURL=[NSURL URLWithString:strVBase];
    _playerViewController = [[AVPlayerViewController alloc] init];
    _playerViewController.player = [AVPlayer playerWithURL:vURL];
    _playerViewController.view.frame =CGRectMake(0,0,_viewForVideoArea.frame.size.width,_viewForVideoArea.frame.size.height);
    //[_playerViewController setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_playerViewController.player play];
    
    _playerViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:_playerViewController animated:YES completion:nil];
    //[_viewForVideoArea addSubview:_playerViewController.view];
    
}

@end
