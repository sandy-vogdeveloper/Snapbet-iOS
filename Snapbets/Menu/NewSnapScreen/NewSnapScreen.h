//
//  NewSnapScreen.h
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 08/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PurchaseThemeScreen.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "WebConnection1.h"
#import "AsyncImageView.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MainDashboardScreen.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ICGVideoTrimmerView.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import "PECropViewController.h"
#import "HeaderAndConstants.h"


@interface NewSnapScreen : UIViewController<UIPopoverControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,WebRequestResult1,UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate,ICGVideoTrimmerDelegate,PECropViewControllerDelegate,UIGestureRecognizerDelegate>
{
    UIImage *croppedIImage;
    UIToolbar *toolBarsDOB;
    NSString *strDOB;
    NSDate   *dteDOB;
    UIBarButtonItem *doneButton;
    UIToolbar *tbExpiryDte;
    NSArray *arrtbDOB;
    //UIPopoverController *popOverForDatePicker;
    CGRect vscreenSize;
    CGFloat yOffset;
    CGFloat vscreenHeight,vscreenWidth;
    WebConnection1 *connection;
    NSMutableDictionary *dict;
    NSURL *urlVideoFileName;
    NSInteger selectedlanid;
    NSTimeInterval videoTimeDuration;
    UIView *vSept3;
}


@property (nonatomic) IBOutlet ICGVideoTrimmerView *trimmerView;

@property (nonatomic) AVPlayerViewController *playerViewController;

#pragma UIImageView properties declarations here
@property (nonatomic)IBOutlet UIImageView *imgHeader,*imgSubHeader,*imgLogo;
@property (nonatomic) UIImage *image;
@property (nonatomic)AsyncImageView *asyImgHBG,*asySelectedBG;

#pragma mark: UIButton Properties declarations here
@property (nonatomic)IBOutlet UIButton *btnLoadVideo,*btnLoadPurchase,*btnAddFriend,*btnCalender,*btnUploadBet,*btnBack,*btnBackPress,*btnClosePicker,*btnClose,*btnCloseView,*btnCloseVideoScreen,*btnIconPlay,*btnCloseAlert,*btnCloseAddFrnd,*btnOriginal,*btnTrim,*btnSubmit,*btnCloseVideoTrim,*btnCancel,*btnCancel1,*btnSend1;

#pragma mark: UILabel Properties declarations here
@property (nonatomic)IBOutlet UILabel *lblBetName,*lblBetDescription,*lblExpDate,*lblPrivate,*lblPublic,*lblBetExp,*lblAddFrnd,*lblPlaceholder,*lblTime,*lblTotalTime;

@property (nonatomic)IBOutlet UITableView *tblVideoQuality,*tblExpiry,*tblAddFrnd;
@property(nonatomic)IBOutlet UISwitch *switchBet;
@property (nonatomic)IBOutlet UITextField *txtBetName;
@property (nonatomic)IBOutlet UITextView *txtDesc;

#pragma mark:- Picker view declarations here
@property (nonatomic)UIDatePicker *dteExpiry;

@property (nonatomic)IBOutlet UIView *viewForUpload,*viewForLoadingVideos,*viewForVideoQuality,*viewForVideExpiry,*viewForLoader,*viewForVideoScreen,*viewForVideoArea,*viewForAlert,*viewForAlertVideo,*viewForAddFrnd,*viewForVideoTrim,*viewForTrimAlertUI;

@property (nonatomic, strong) UIView *activityBackgroundView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@property (nonatomic)NSMutableArray *arrMyVideoData;

@property (nonatomic, strong) NSURL *videoURL;
@property (nonatomic, strong) NSMutableArray *videoURLArray;
@property (nonatomic, strong) NSMutableArray *assetItems,*arrSelectedFBFrnd,*arrSelectedFrndsUIDs;
@property (nonatomic, strong) NSMutableDictionary *dic;

#pragma mark- UIPickerView Properties
@property (strong, nonatomic) IBOutlet UIPickerView *picker;

#pragma mark: NSString Properties declarations here
@property (nonatomic) NSString *strVideoName,*strSelectedThmeID,*strSwitchStatus,*strGallerySelectedVideo,*strBetExpiryDate,*strUplodedImg,*strAlertTitle,*strVTitle,*strVDescription;
@property (nonatomic) NSString *strPVideoURL,*strPVideoName,*pickerValue,*strVoteAlertTitle,*tempOrignalVideoPath;

#pragma mark: UICollectionView Properties declarations here
@property (nonatomic)IBOutlet UICollectionView *collMyVideos;

#pragma mark: NSMutable Array Properties declarations here
@property(nonatomic)NSMutableArray *arrSeconds,*arrMinute,*arrHour,*arrFBFriendList;

#pragma mark:Bool Properties declaratiosn here
@property (nonatomic)BOOL isGUploadVideo,isNSnapUpload,isSnapSuccess,isUploadAPI,isFrndListAPI,isFListExpanded,isFromAllList;

#pragma mark- Connetion Objects
@property(nonatomic) NSMutableData *webdata;
@property (nonatomic)NSURLConnection *connUploadVideo;
@property (nonatomic)NSMutableDictionary *dictVideo;
@end
