//
//  NewSnapScreen.m
//  Snapbets
//
//  Created by Bhimashankar Vibhute on 08/09/16.
//  Copyright © 2016 Syneotek Software Solution. All rights reserved.
//

#import "NewSnapScreen.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>


@interface NewSnapScreen ()
{
    NSString *purchaseDate;
    NSMutableArray *arrPurchaseDates,*arrExpiryDates;
    NSURL *selectedVdoURL,*strGOrgVPath;
    NSTimeInterval durationOfVideo;
    NSMutableArray *arrSelectedCells,*arrSelectedUIDs;
}
@property (strong, nonatomic) NSString *originalVideoPath;

@property (strong, nonatomic) NSString *tmpVideoPath;
@property (nonatomic) CGFloat startTime;
@property (nonatomic) CGFloat stopTime;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *myActivityIndicator;


//ICG Video Data
@property (assign, nonatomic) BOOL isPlaying;
@property (strong, nonatomic) AVPlayer *player;
@property (strong, nonatomic) AVPlayerItem *playerItem;
@property (strong, nonatomic) AVPlayerLayer *playerLayer;
@property (strong, nonatomic) NSTimer *playbackTimeCheckerTimer;
@property (assign, nonatomic) CGFloat videoPlaybackPosition;


@property (weak, nonatomic) IBOutlet UIButton *trimButton;
@property (weak, nonatomic) IBOutlet UIView *videoPlayer;
@property (weak, nonatomic) IBOutlet UIView *videoLayer;
@property (strong, nonatomic) AVAssetExportSession *exportSession;
@property (strong, nonatomic) AVAsset *asset;

@property (strong, nonatomic) NSString *tempVideoPath;



@property (assign, nonatomic) BOOL restartOnPlay;
@end

@implementation NewSnapScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isFromAllList=NO;
    _isUploadAPI=NO;
    _isFrndListAPI=NO;
    _isFListExpanded=NO;
    [self getDataFromDB];
    
    _arrMyVideoData=[[NSMutableArray alloc]init];
    _arrMyVideoData=[[NSMutableArray alloc]init];
    _videoURLArray=[[NSMutableArray alloc]init];
    _assetItems=[[NSMutableArray alloc]init];
    _strAlertTitle=[[NSString alloc]init];
    _arrFBFriendList=[[NSMutableArray alloc]init];
    _arrSelectedFBFrnd=[[NSMutableArray alloc]init];
    _arrSelectedFrndsUIDs=[[NSMutableArray alloc]init];
    arrSelectedCells=[[NSMutableArray alloc]init];
    arrSelectedUIDs=[[NSMutableArray alloc]init];
    
    yOffset=0.0;
    _isSnapSuccess=NO;
    [_dic removeAllObjects];
    
    _arrSeconds=[[NSMutableArray alloc]init];
    _arrMinute=[[NSMutableArray alloc]init];
    _arrHour=[[NSMutableArray alloc]init];
    
    _imgHeader=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,screenWidth,[UIScreen mainScreen].bounds.size.height/100*20)];
    _imgHeader.image=[UIImage imageNamed:@"icon_BG"];
    [self.view addSubview:_imgHeader];
    
    [_asyImgHBG removeFromSuperview];
    _asyImgHBG=[[AsyncImageView alloc]initWithFrame:CGRectMake(screenWidth/2-50,CGRectGetMaxY(_imgHeader.frame)-50,100,100)];
    _asyImgHBG.backgroundColor=[UIColor clearColor];
    [_asyImgHBG setContentMode:UIViewContentModeScaleAspectFill];
    [_asyImgHBG sizeToFit];
    _asyImgHBG.clipsToBounds = YES;
    [self.view addSubview:_asyImgHBG];
    
    if(screenWidth<325)
    {
        _imgSubHeader=[[UIImageView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_imgHeader.frame),screenWidth,screenHeight/2.8)];
    }
    else
    {
     _imgSubHeader=[[UIImageView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_imgHeader.frame),screenWidth,screenHeight/3.2)];
    }
    
    _imgSubHeader.backgroundColor=[UIColor blackColor];
    [self.view addSubview:_imgSubHeader];
    
    _imgLogo=[[UIImageView alloc]initWithFrame:CGRectMake(screenWidth/2-50,CGRectGetMaxY(_imgHeader.frame)-50,100,100)];
    _imgLogo.image=[UIImage imageNamed:@"icon_Share"];
    [self.view addSubview:_imgLogo];
    
    _btnIconPlay=[[UIButton alloc]initWithFrame:CGRectMake(0,0,_imgLogo.frame.size.width,_imgLogo.frame.size.height)];
    //[_btnIconPlay setBackgroundImage:[UIImage imageNamed:@"img_AppIcon"] forState:UIControlStateNormal];
    [_btnIconPlay addTarget:self action:@selector(viewForVideoScreenPopUpUI) forControlEvents:UIControlEventTouchUpInside];
    [_btnIconPlay setUserInteractionEnabled:NO];
    [_imgLogo addSubview:_btnIconPlay];
    
    _btnBack=[[UIButton alloc]init];
    _btnBack.frame = CGRectMake(5,8,25,25);
   
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"iconBackArrow"] forState:UIControlStateNormal];
    [_btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnBack];
    
    _btnBackPress=[[UIButton alloc]initWithFrame:CGRectMake(0,0,55,30)];
    [_btnBackPress addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnBackPress];
    
    _btnLoadVideo=[[UIButton alloc]init];
    _btnLoadPurchase=[[UIButton alloc]init];
    _txtBetName=[[UITextField alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_btnLoadVideo.frame)+5,screenWidth,30)];
    
    UIView *viewSept=[[UIView alloc]init];
    _txtDesc=[[UITextView alloc]init];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        _btnLoadVideo.frame=CGRectMake(60,CGRectGetMaxY(_imgLogo.frame)+20,screenWidth/2-120,35);
        _btnLoadPurchase.frame=CGRectMake(screenWidth/2+60,CGRectGetMaxY(_imgLogo.frame)+20,screenWidth/2-120,35);
        _txtBetName.frame=CGRectMake(0,CGRectGetMaxY(_btnLoadVideo.frame)+20,screenWidth,30);
        viewSept.frame=CGRectMake(0,CGRectGetMaxY(_txtBetName.frame)+5,screenWidth,1);
        _txtDesc.frame=CGRectMake(25,CGRectGetMaxY(viewSept.frame)+10,screenWidth,30);
    }
    else
    {
        _btnLoadVideo.frame=CGRectMake(25,CGRectGetMaxY(_imgLogo.frame)+20,screenWidth/2-50,40);
        _btnLoadPurchase.frame=CGRectMake(screenWidth/2+25,CGRectGetMaxY(_imgLogo.frame)+20,screenWidth/2-50,40);
        _txtBetName.frame=CGRectMake(0,CGRectGetMaxY(_btnLoadVideo.frame)+5,screenWidth,30);
        viewSept.frame=CGRectMake(0,CGRectGetMaxY(_txtBetName.frame)+5,screenWidth,1);
        _txtDesc.frame=CGRectMake(5,CGRectGetMaxY(_txtBetName.frame)+10,screenWidth-10,50);
    }
    
    [_btnLoadVideo setTitle:@"Load Snapbet" forState:UIControlStateNormal];
    [_btnLoadVideo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _btnLoadVideo.layer.borderWidth=1.0;
    _btnLoadVideo.layer.borderColor=[[UIColor orangeColor]CGColor];
    _btnLoadVideo.layer.cornerRadius=5.0;
    [_btnLoadVideo addTarget:self action:@selector(btnLoadVideoPressed:) forControlEvents:UIControlEventTouchUpInside];
    _btnLoadVideo.titleLabel.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
    [self.view addSubview:_btnLoadVideo];
    
    [_btnLoadPurchase setTitle:@"Load Purchase" forState:UIControlStateNormal];
    [_btnLoadPurchase setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _btnLoadPurchase.layer.borderWidth=1.0;
    _btnLoadPurchase.layer.borderColor=[[UIColor orangeColor]CGColor];
    _btnLoadPurchase.titleLabel.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
    _btnLoadPurchase.layer.cornerRadius=5.0;
    [_btnLoadPurchase addTarget:self action:@selector(btnLoadPurchaseScreenPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnLoadPurchase];
    // Do any additional setup after loading the view from its nib.
    
    [self getTextFieldWithProperties:_txtBetName placeholder:@"Bet Name" forLogin:YES];
    _txtBetName.textAlignment=NSTextAlignmentCenter;
    _lblBetName.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:20];
    [self.view addSubview:_txtBetName];
    
    viewSept.backgroundColor=[UIColor grayColor];
    [self.view addSubview:viewSept];
    _txtDesc.backgroundColor=[UIColor clearColor];
    _txtDesc.textColor=[UIColor whiteColor];
    _txtDesc.delegate=self;
    _txtDesc.textAlignment=NSTextAlignmentLeft;
    _txtDesc.returnKeyType=UIReturnKeyDone;
    
    _txtDesc.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:15.0];
    [self.view addSubview:_txtDesc];
    
    _lblPlaceholder=[[UILabel alloc]initWithFrame:CGRectMake(0,0,_txtDesc.frame.size.width,30)];
     _lblPlaceholder.text=@"Bet description goes here, use up to 90 characters";
    _lblPlaceholder.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:13.0];
    _lblPlaceholder.textAlignment=NSTextAlignmentLeft;
    _lblPlaceholder.textColor=[UIColor grayColor];
    
     [_txtDesc addSubview:_lblPlaceholder];
    _switchBet.transform=CGAffineTransformMakeScale(0.75,0.75);
   
    _lblPublic=[[UILabel alloc]initWithFrame:CGRectMake(screenWidth/2-80,CGRectGetMaxY(_imgSubHeader.frame)+30,100,30)];
    
    [_switchBet removeFromSuperview];
    _switchBet=[[UISwitch alloc]initWithFrame:CGRectMake(screenWidth/2-10,CGRectGetMaxY(_imgSubHeader.frame)+30,50,25)];
     [_switchBet setOn:NO animated:YES];
    _strSwitchStatus=@"public";
    _switchBet.onTintColor=[UIColor orangeColor];
    _switchBet.transform=CGAffineTransformMakeScale(0.75,0.75);
    [self.view addSubview:_switchBet];
    
      //[_switchBet setOn:YES animated:YES];
    [_switchBet addTarget:self action:@selector(switchChanged:)forControlEvents:UIControlEventValueChanged];
        
    
     _lblPrivate=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_switchBet.frame)+20,CGRectGetMaxY(_imgSubHeader.frame)+30,80,30)];
    
    UIView *vSept=[[UIView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_lblPrivate.frame)+10,screenWidth,1)];
    vSept.backgroundColor=[UIColor orangeColor];
    _lblBetExp=[[UILabel alloc]initWithFrame:CGRectMake(40,CGRectGetMaxY(vSept.frame)+3,150,30)];
    
    _btnCalender=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-80,CGRectGetMaxY(vSept.frame)+4,80,28)];
    
    _lblExpDate=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_switchBet.frame),CGRectGetMaxY(vSept.frame)+3,70,30)];

    UIView *vSept2=[[UIView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_lblBetExp.frame)+3,screenWidth,1)];
    vSept2.backgroundColor=[UIColor orangeColor];
    _lblAddFrnd=[[UILabel alloc]initWithFrame:CGRectMake(40,CGRectGetMaxY(vSept2.frame)+5,150,30)];
    
    _btnAddFriend=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-50,CGRectGetMaxY(vSept2.frame)+4,30,30)];
    vSept3=[[UIView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_lblAddFrnd.frame)+3,screenWidth,1)];
    vSept3.backgroundColor=[UIColor orangeColor];
    _lblBetExp.textColor=[UIColor blackColor];
    _lblPrivate.textColor=[UIColor blackColor];
    _lblPublic.textColor=[UIColor blackColor];
    _lblBetExp.textColor=[UIColor blackColor];
    _lblExpDate.textColor=[UIColor blackColor];
    _lblAddFrnd.textColor=[UIColor blackColor];
    
    NSDate *currentDate = [NSDate date];
    NSDate *sevenDays = [currentDate dateByAddingTimeInterval:7*24*60*60];
    NSLog(@"7 days ago: %@", sevenDays);
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    _strBetExpiryDate = [dateformate stringFromDate:sevenDays]; // CONVERT date to string
    NSLog(@"date :%@",_strBetExpiryDate);
    NSLog(@"1 Week");
    
    _lblPrivate.text=@"Private";
    _lblPublic.text=@"Public";
    _lblBetExp.text=@"Bet Expiry";
    _lblExpDate.text=@"1 Week";
    _lblAddFrnd.text=@"Add Friends";
    [_btnCalender setTitle:@"1 Week" forState:UIControlStateNormal];
    
    _lblPrivate.font=[UIFont fontWithName:@"Avenir Medium" size:18.0];
    _lblPublic.font=[UIFont fontWithName:@"Avenir Medium" size:18.0];
    _lblBetExp.font=[UIFont fontWithName:@"Avenir Medium" size:18.0];
    _lblExpDate.font=[UIFont fontWithName:@"Avenir Medium" size:14];
    _lblAddFrnd.font=[UIFont fontWithName:@"Avenir Medium" size:18.0];
    _btnCalender.titleLabel.font=[UIFont fontWithName:@"Avenir Medium" size:14];
    [_btnCalender setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //[_btnCalender setBackgroundImage:[UIImage imageNamed:@"icon_Calender'"] forState:UIControlStateNormal];
    [_btnAddFriend setBackgroundImage:[UIImage imageNamed:@"icon_Snap"] forState:UIControlStateNormal];
     [_btnCalender addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [_btnAddFriend addTarget:self action:@selector(btnAddFriendPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_switchBet addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_lblPublic];
    [self.view addSubview:_lblPrivate];
    [self.view addSubview:vSept];
    [self.view addSubview:_lblBetExp];
    [self.view addSubview:_btnCalender];
    //[self.view addSubview:_lblExpDate];
    [self.view addSubview:vSept2];
    [self.view addSubview:_lblAddFrnd];
    [self.view addSubview:_btnAddFriend];
    [self.view addSubview:vSept3];
    
    _btnUploadBet=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-130,screenHeight-45,120,40)];
    //[_btnUploadBet setBackgroundColor:[UIColor orangeColor]];
    [_btnUploadBet setBackgroundImage:[UIImage imageNamed:@"img_BuySnap"] forState:UIControlStateNormal];
    [_btnUploadBet setTitle:@"Upload Bet" forState:UIControlStateNormal];
    [_btnUploadBet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _btnUploadBet.titleLabel.font=[UIFont fontWithName:@"Avenir Medium" size:16];
    _btnUploadBet.layer.cornerRadius=3.0;
    [self.view addSubview:_btnUploadBet];
    
    [_btnUploadBet addTarget:self action:@selector(btnUploadBetPressed:) forControlEvents:UIControlEventTouchUpInside];
                                   //242 137 9]
    /*
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"/{friend-list-id}"
                                  parameters:
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        // Handle the result
    }];*/
    
    [self GetAllFriendList];
    [self fetchUserInfo];
}

-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id,name,link,first_name, last_name, picture.type(large), email, birthday , bio ,location , friends ,hometown"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
        {
            if (!error)
             {
                 NSLog(@"result : %@",result);                 
                 NSArray *arrFrnds=[result valueForKey:@"friends"];
                 NSArray *arrData=[arrFrnds valueForKey:@"data"];
                 NSLog(@"%@",arrData);
                 if([arrData count])
                 {
                     for (int i=0;i<[arrData count];i++)
                     {
                        // [_arrFBFriendList addObject:[arrData objectAtIndex:i]];
                     }
                 }
             }
         }];        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}

-(void)viewWillAppear:(BOOL)animated
{
    
    if (theAppDelegate.strVideoName.length>0|| theAppDelegate.strVDescription.length>0) {
        
        _strVideoName=theAppDelegate.strVideoName;
        _strSelectedThmeID=theAppDelegate.strSelectedThmeID;
        _strUplodedImg=theAppDelegate.strUplodedImg;
        _strVTitle=theAppDelegate.strVTitle;
        _strVDescription=theAppDelegate.strVDescription;
        theAppDelegate.strVDescription=@"";
        _imgLogo.image=[UIImage imageNamed:@""];
        NSString *strBURL=ImgBase_URL;
        strBURL=[strBURL stringByAppendingString:_strUplodedImg];
        [_asyImgHBG loadImageFromURL:[NSURL URLWithString:strBURL]];
        _asyImgHBG.layer.cornerRadius=4.0;
        [self.view bringSubviewToFront:_asyImgHBG];
        [_asyImgHBG bringSubviewToFront:_btnIconPlay];
        
        _txtBetName.text=_strVTitle;
        _lblPlaceholder.text=@"";
        _txtDesc.text=_strVDescription;
        theAppDelegate.strVideoName=[[NSString alloc]init];
        theAppDelegate.strSelectedThmeID=[[NSString alloc]init];
        theAppDelegate.strUplodedImg=[[NSString alloc]init];
        
        if ([_strVideoName containsString:@".mp4"]) {
            [self performSelector:@selector(loadPlayVideoButton) withObject:nil afterDelay:1];
        }
        else
        {
            UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewForVideoScreenPopUpUI)];
            [self.asyImgHBG addGestureRecognizer:gestureRecognizer];
            self.asyImgHBG.userInteractionEnabled = YES;                        
        }
        
    }
    self.tempVideoPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"tmpMov.mov"];
    [self.navigationController setNavigationBarHidden:YES];
}


- (IBAction)switchChanged:(id)sender {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    if ([_switchBet isOn])
    {
        _strSwitchStatus=[[NSString alloc]init];
        _strSwitchStatus=@"private";
        NSLog(@"On");
    }
    else
    {
        _strSwitchStatus=[[NSString alloc]init];
        _strSwitchStatus=@"public";
        
        NSLog(@"Off");
    }
}

#pragma mark: Set Proerties for TextFields
-(UITextField*)getTextFieldWithProperties:(UITextField*)textField placeholder:(NSString*)placeholder forLogin:(BOOL)forLogin
{
        //------------- Add properties to textfield ------------- //
        //  textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.placeholder =placeholder;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.keyboardType=UIKeyboardAppearanceDefault;
        
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.keyboardAppearance=UIKeyboardAppearanceLight;
        [textField addTarget:self action:@selector(resignTxt:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
        _txtBetName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Bet Name" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        _txtBetName.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:25];
    
        _txtBetName.textAlignment=NSTextAlignmentCenter;
    
        _txtBetName.textColor=[UIColor whiteColor];
        _txtBetName.delegate=self;
    
    if (forLogin && textField==_txtBetName)
    {        _txtBetName.returnKeyType=UIReturnKeyDone;      }
    return textField;
}

-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(_txtBetName.text.length >= 12 && range.length == 0)
    {
        return NO;
    }
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return YES;
    }
    if (_txtDesc.text.length>=90 && range.length==0) {
        return NO;
    }
    return YES;
}
    

#pragma mark- TextField DelegateMethods
-(void)resignTxt:(UITextField*)txt
{
    //----------- Validate textfield return action ------ //
    if (txt==_txtBetName)
    {
        [txt resignFirstResponder];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.lblPlaceholder.hidden = YES;
}

- (void)textViewDidChange:(UITextView *)txtView
{
    self.lblPlaceholder.hidden = ([txtView.text length] > 0);
}

- (void)textViewDidEndEditing:(UITextView *)txtView
{
    self.lblPlaceholder.hidden = ([txtView.text length] > 0);
}

-(IBAction)btnLoadPurchaseScreenPressed:(id)sender
{
    if (_strVideoName.length>0 || _strUplodedImg.length>0)
    {
       // _strAlertTitle=@"This video has been already uploaded";
        [self viewForAlertPopUp];
            }
    else
    {
        PurchaseThemeScreen *purch=[[PurchaseThemeScreen alloc]initWithNibName:@"PurchaseThemeScreen" bundle:nil];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:purch];
        [self presentViewController:nav animated:YES completion:nil];
        
       // [self presentViewController:purch animated:YES completion:nil];
        
        //[self.navigationController pushViewController:purch animated:YES];
    }
}

-(IBAction)btnUploadBetPressed:(id)sender
{
    if (_txtBetName.text.length>0 && _txtDesc.text.length>0) {
        if(_strUplodedImg.length==0)
        {
            _strUplodedImg=@"No.png";
        }
        
        if (_strVideoName.length==0) {
            _strVideoName=@"";
        }
        [self uploadNewSnap];
    }
    else
    {
        if (_txtBetName.text.length==0)
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please enter Bet Name" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else if (_txtDesc.text.length==0)
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@" use upto 90 characters" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else if (_strBetExpiryDate.length==0)
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please select Bet expiry period" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];

        }
//        else if ([_lblExpDate.text isEqualToString:@"00/00/0000"])
//        {
//            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please select Expiry date" preferredStyle:UIAlertControllerStyleAlert];
//            
//            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//            [alertController addAction:ok];
//            
//            [self presentViewController:alertController animated:YES completion:nil];
//
//        }
    }
}

-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)buttonAction:(UIButton *)sender
{
    [self viewForVideoExpiryDate];
   /* _btnClosePicker=[[UIButton alloc] initWithFrame:CGRectMake(0,screenHeight-230,screenWidth,30)];
    [_btnClosePicker setTitle:@"Done" forState:UIControlStateNormal];
    [_btnClosePicker setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
     [_btnClosePicker.titleLabel setTextAlignment:NSTextAlignmentLeft];
    [_btnClosePicker addTarget:self action:@selector(btnClosePickerPressed:) forControlEvents:UIControlEventTouchUpInside];
    _btnClosePicker.backgroundColor=[UIColor lightGrayColor];
    [self.view addSubview:_btnClosePicker];
    [tbExpiryDte setBarStyle:UIBarStyleBlackOpaque];
    arrtbDOB= [NSArray arrayWithObjects:
               doneButton, nil];
    
    _dteExpiry= [[UIDatePicker alloc] init];
    _dteExpiry.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _dteExpiry.datePickerMode = UIDatePickerModeDate;
        
    [_dteExpiry addTarget:self action:@selector(dueDateChanged:) forControlEvents:UIControlEventValueChanged];
      //  CGSize pickerSize = [_dteExpiry sizeThatFits:CGSizeZero];
    _dteExpiry.frame = CGRectMake(0.0, screenHeight-200,screenWidth,200);
    _dteExpiry.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_dteExpiry];*/
    }
    
-(void) dueDateChanged:(UIDatePicker *)sender
{
     NSDateFormatter* dateFormatter =[[NSDateFormatter alloc] init];
     [dateFormatter setDateStyle:NSDateFormatterLongStyle];
     [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
     NSDate *date = _dteExpiry.date;
     NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
     [DateFormatter setDateFormat:@"yyyy-MM-dd"];
     NSString *dateString = [DateFormatter stringFromDate:date];
     self.lblExpDate.text = dateString;
     NSLog(@"Picked the date %@", [dateFormatter stringFromDate:[sender date]]);
    }

-(IBAction)btnLoadVideoPressed:(id)sender
{
    if (_strVideoName.length>0||_strUplodedImg.length>0)
    {
        //_strAlertTitle=@"This video has been already uploaded";
        [self viewForAlertPopUp];
    }
    else
    {
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            // Cancel button tappped do nothing.
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take Photo / Video" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            // take photo button tapped.
            [self takePhoto];
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose Photo / Video" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            // choose photo button tapped.
            [self ChoosePhotoFromExitsing];
            
        }]];
        
        [self presentViewController:actionSheet animated:YES completion:nil];
    }
}

#pragma mark: Choose From Gallery
-(void)ChoosePhotoFromExitsing
{
    UIImagePickerController *imageController= [[UIImagePickerController alloc]init];    
    imageController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imageController.allowsEditing = YES;
    imageController.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    imageController.delegate = self;
    
    //[self presentViewController:imageController animated:YES completion:nil];
    [self performSelector:@selector(showGallery:) withObject:imageController afterDelay:1];
}

-(void)showGallery:(id)imageController
{
    [self presentViewController:imageController animated:YES completion:nil];
}
#pragma mark- TakePhotoFromCamera
-(void)takePhoto
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imageController = [[UIImagePickerController alloc] init];
        imageController.sourceType = UIImagePickerControllerSourceTypeCamera;
        // imageController.allowsEditing = NO;
        imageController.mediaTypes  = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
        imageController.delegate = self;
        imageController.allowsEditing = NO;
        imageController.sourceType = UIImagePickerControllerSourceTypeCamera;
        
       // imageController.modalPresentationStyle= UIModalPresentationFullScreen;
       //imageController.mediaTypes = [NSArray arrayWithObjects:(NSString*)kUTTypeImage, (NSString*)kUTTypeMovie, nil];
        if([arrPurchaseDates count])
        {
            NSDate *currDate = [NSDate date];
            NSDateFormatter *tempDateFormatter = [[NSDateFormatter alloc]init];
            [tempDateFormatter setDateFormat:@"dd-MMM-yy"];
            NSString *strCDate= [tempDateFormatter stringFromDate:currDate];            
            
            NSString *strExp=[arrExpiryDates objectAtIndex:0];
            if ([strExp isEqualToString:@"No Expiry"]) {
                theAppDelegate.strTrimingTime=@"90";
                theAppDelegate.GlobDurOfOrignalVideo=90;
                NSLog(@"Duration Of Video--------------------------------%f",theAppDelegate.GlobDurOfOrignalVideo);
                //imageController.videoMaximumDuration=90.0f;
            }
            else if ([strCDate isEqualToString:[arrExpiryDates objectAtIndex:0]])
            {
                theAppDelegate.strTrimingTime=@"15";
                theAppDelegate.GlobDurOfOrignalVideo=15;
                NSLog(@"Duration Of Video--------------------------------%f",theAppDelegate.GlobDurOfOrignalVideo);
                //imageController.videoMaximumDuration=15.0f;
            }
            else
            {
                theAppDelegate.strTrimingTime=@"90";
                theAppDelegate.GlobDurOfOrignalVideo=90;
                NSLog(@"Duration Of Video--------------------------------%f",theAppDelegate.GlobDurOfOrignalVideo);
            //imageController.videoMaximumDuration=90.0f;
            
            }
        }
        else
        {
            theAppDelegate.strTrimingTime=@"15";
            theAppDelegate.GlobDurOfOrignalVideo=15;
            NSLog(@"Duration Of Video--------------------------------%f",theAppDelegate.GlobDurOfOrignalVideo);
            //imageController.videoMaximumDuration=15.0f;
            
        }
        [self presentViewController:imageController animated:YES completion:nil];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Camera is not available" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}
/*
#pragma mark: UIAlertView delegate declarations here
    - (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
        
        //Checks For Approval
        if (buttonIndex == 0)
        {
            //[self viewForVideoUploadQuality];
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                UIImagePickerController *imageController = [[UIImagePickerController alloc] init];
                imageController.sourceType = UIImagePickerControllerSourceTypeCamera;
               // imageController.allowsEditing = NO;
                imageController.mediaTypes  = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
                imageController.delegate = self;
                imageController.allowsEditing = YES;
                imageController.videoMaximumDuration=15.0f;
                [self presentViewController:imageController animated:YES completion:nil];
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Camera is not available" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        else
        {
            UIImagePickerController *imageController= [[UIImagePickerController alloc]init];
            
            imageController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            imageController.allowsEditing = YES;
            imageController.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            imageController.delegate = self;
            //[self presentViewController:imageController animated:YES completion:nil];
            [self performSelector:@selector(showGallery:) withObject:imageController afterDelay:1];
            
            //  [self viewForVideoUpload];
            // [self viewForSpinner];
            //[self buildAssetsLibrary];
        }
        
    }
*/
-(IBAction)btnClosePickerPressed:(id)sender
{
    [_btnClosePicker removeFromSuperview];
    [_dteExpiry removeFromSuperview];
}

#pragma mark: Button addFriend pressed
-(IBAction)btnAddFriendPressed:(id)sender
{
    /*[_viewForAddFrnd removeFromSuperview];
    _viewForAddFrnd =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForAddFrnd.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForAddFrnd.tag=20;
    [self.view addSubview:_viewForAddFrnd];
    CGRect frameForViewForHelp=_viewForAddFrnd.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForAddFrnd.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForAddFrnd.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        _btnCloseAddFrnd=[[UIButton alloc]initWithFrame:CGRectMake(_viewForAddFrnd.frame.size.width-40,10,30,30)];
        [_btnCloseAddFrnd setBackgroundImage:[UIImage imageNamed:@"icon_Cross"] forState:UIControlStateNormal];
        [_btnCloseAddFrnd addTarget:self action:@selector(closeAddFrndView) forControlEvents:UIControlEventTouchUpInside];
        [_viewForAddFrnd addSubview:_btnCloseAddFrnd];
        
        _tblAddFrnd=[[UITableView alloc]initWithFrame:CGRectMake(0,50,screenWidth,screenHeight-100)];
        _tblAddFrnd.delegate=self;
        _tblAddFrnd.dataSource=self;
        _tblAddFrnd.backgroundColor=[UIColor clearColor];
        _tblAddFrnd.tableFooterView=[[UIView alloc]init];
        [_viewForAddFrnd addSubview:_tblAddFrnd];
    }];*/
    
    if(_isFListExpanded)
    {
        _isFListExpanded=NO;
        [_tblAddFrnd removeFromSuperview];
        [_btnAddFriend setBackgroundImage:[UIImage imageNamed:@"icon_Snap"] forState:UIControlStateNormal];
        
    }
    else
    {
        [_btnAddFriend setBackgroundImage:[UIImage imageNamed:@"icon_minus1"] forState:UIControlStateNormal];
        
        _isFListExpanded=YES;
        [_tblAddFrnd removeFromSuperview];
        _tblAddFrnd=[[UITableView alloc]init];
        if(screenWidth<325)
        {
            _tblAddFrnd.frame=CGRectMake(0,vSept3.frame.origin.y,screenWidth,50);
        }
        else
        {
            _tblAddFrnd.frame=CGRectMake(0,vSept3.frame.origin.y,screenWidth,120);
        }
        _tblAddFrnd.delegate=self;
        _tblAddFrnd.dataSource=self;
        _tblAddFrnd.backgroundColor=[UIColor clearColor];
        _tblAddFrnd.tableFooterView=[[UIView alloc]init];
        [self.view addSubview:_tblAddFrnd];
    }
}

-(void)closeAddFrndView
{
    for(UIView *subview in [_viewForAddFrnd subviews])
    {
        [subview removeFromSuperview];
        [self.btnCloseAddFrnd removeFromSuperview];
        
    }
    
    CGRect frameForViewForHelp=_viewForAddFrnd.frame;
    frameForViewForHelp=CGRectMake(0, screenHeight/2,screenWidth,0);
    
    [UIView animateWithDuration:0.5f animations:^{
        _viewForAddFrnd.frame=frameForViewForHelp;
    } completion:^(BOOL finished)
     {
         [_viewForAddFrnd removeFromSuperview];
        
     }];
}

#pragma mark: view For Video Expiry
-(void)viewForVideoExpiryDate
{
    [_viewForVideExpiry removeFromSuperview];
    _viewForVideExpiry =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForVideExpiry.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.9];
    _viewForVideExpiry.tag=20;
    //_viewForVideExpiry.layer.borderWidth=1.0;
    //_viewForVideExpiry.layer.borderColor=[[UIColor whiteColor]CGColor];
    
    [self.view addSubview:_viewForVideExpiry];
    CGRect frameForViewForExpiry=_viewForVideExpiry.frame;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        frameForViewForExpiry=CGRectMake(0,screenHeight/4.5,screenWidth,screenHeight/2.2);
    }
    else
    {
        frameForViewForExpiry=CGRectMake(0,screenHeight/4.5,screenWidth,screenHeight/2.2);
    }
    
    [UIView animateWithDuration:0.5f animations:^{
        _viewForVideExpiry.frame=frameForViewForExpiry;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForVideExpiry.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,10,_viewForVideExpiry.frame.size.width,30)];
        lblTitle.text=@"Set Bet Expiry";
        lblTitle.textColor=[UIColor whiteColor];
        lblTitle.font=[UIFont boldSystemFontOfSize:30];
        lblTitle.textAlignment=NSTextAlignmentCenter;
        [_viewForVideExpiry addSubview:lblTitle];
        
        UIView *viewSelectedBorder=[[UIView alloc]initWithFrame:CGRectMake(_viewForVideExpiry.frame.size.width/2-80,_viewForVideExpiry.frame.size.height/2-27,160,35)];
        viewSelectedBorder.backgroundColor=[UIColor clearColor];
        viewSelectedBorder.layer.borderColor=[UIColor orangeColor].CGColor;
        viewSelectedBorder.layer.borderWidth=2;
        viewSelectedBorder.layer.cornerRadius=3;
        [_viewForVideExpiry addSubview:viewSelectedBorder];
        [self showPicker];
        
        _btnCloseView=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideExpiry.frame.size.width/2-60,_viewForVideExpiry.frame.size.height-50,120,40)];
        [_btnCloseView setTitle:@"Okay" forState:UIControlStateNormal];
        [_btnCloseView setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseView.titleLabel.font=[UIFont fontWithName:@"Avenir Medium" size:20];
        [_btnCloseView setBackgroundImage:[UIImage imageNamed:@"img_BuySnap"] forState:UIControlStateNormal];
        [_btnCloseView addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
        [_viewForVideExpiry addSubview:_btnCloseView];
        
//            _tblExpiry=[[UITableView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblTitle.frame),_viewForVideExpiry.frame.size.width,_viewForVideExpiry.frame.size.height-30)];
//            _tblExpiry.delegate=self;
//            _tblExpiry.dataSource=self;
//            _tblExpiry.backgroundColor=[UIColor clearColor];
//            _tblExpiry.tableFooterView=[[UIView alloc]init];
//            _tblExpiry.scrollEnabled=NO;
//            [_viewForVideExpiry addSubview:_tblExpiry];
        
    }];
    [self disableSubViews:_viewForVideExpiry];
}

-(void)showPicker
{
    NSMutableArray *array=[[NSMutableArray alloc]init];
    
    UIImageView *shadowView=[[UIImageView alloc]initWithFrame:CGRectMake(10,0,screenWidth-20,10)];
    shadowView.image=[UIImage imageNamed:@"Bottom-shadow.png"];
    
    
    _picker= [[UIPickerView alloc] initWithFrame:CGRectMake(0,20, _viewForVideExpiry.frame.size.width,_viewForVideExpiry.frame.size.height-60)];
    _picker.delegate=self;
    _picker.dataSource=self;
    _picker.tag=1;
    _picker.showsSelectionIndicator = YES;
    _picker.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    _picker.hidden = NO;
    _picker.tintColor=[UIColor whiteColor];
    
    [array removeAllObjects];
    [array addObject:@"1 Week"];
    [array addObject:@"2 Weeks"];
    [array addObject:@"1 Month"];
    
    [_viewForVideExpiry addSubview:_picker];
    [_viewForVideExpiry addSubview:shadowView];
    
}

#pragma mark- PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView: (UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 3;
}

- (NSString *)pickerView:(UIPickerView *)pickerView  titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *strTitle;
    if (row==0) {
        
        return strTitle=@"1 Week";
    }
    else if (row==1)
    {
        return strTitle=@"2 Weeks";
    }
    else if (row==2)
    {
        return strTitle=@"1 Month";
    }
    return 0;
}

#pragma mark PickerView Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    _pickerValue=nil;
    NSDate *currentDate = [NSDate date];
    
    if (row==0)
    {
        NSDate *sevenDays = [currentDate dateByAddingTimeInterval:7*24*60*60];
        NSLog(@"7 days ago: %@", sevenDays);
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
        _strBetExpiryDate = [dateformate stringFromDate:sevenDays]; // CONVERT date to string
        NSLog(@"date :%@",_strBetExpiryDate);
        
        NSLog(@"1 Week");
        _lblExpDate.text=@"1 Week";
        [_btnCalender setTitle:@"1 Week" forState:UIControlStateNormal];
        _pickerValue=@"1 Week";
    }
    else if (row==1)
    {
        NSDate *sevenDays = [currentDate dateByAddingTimeInterval:14*24*60*60];
        NSLog(@"7 days ago: %@", sevenDays);
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
        _strBetExpiryDate = [dateformate stringFromDate:sevenDays]; // CONVERT date to string
        NSLog(@"date :%@",_strBetExpiryDate);
        _lblExpDate.text=@"2 Weeks";
        [_btnCalender setTitle:@"2 Weeks" forState:UIControlStateNormal];
        NSLog(@"2 Weeks");
        _pickerValue=@"2 Weeks";
    }
    else if (row==2)
    {
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        [dateComponents setMonth:1];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:currentDate options:0];
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
        _strBetExpiryDate = [dateformate stringFromDate:newDate]; // CONVERT date to string
        NSLog(@"date :%@",_strBetExpiryDate);
        _lblExpDate.text=@"1 Month";
        [_btnCalender setTitle:@"1 Month" forState:UIControlStateNormal];
        _pickerValue=@"1 Month";
    }
    
    selectedlanid=row+1;
    // selectedlanid=row;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 44)];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    label.textAlignment = NSTextAlignmentCenter;
    if (row == 0) {
        label.text =@"1 Week";
    }
    else if (row == 1) {
        label.text =@"2 Weeks";
    }
    else if (row == 2) {
        label.text =@"1 Month";
    }
    
    return label;
}

#pragma mark- TableView DataSource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==_tblAddFrnd)
    {
        return  [_arrFBFriendList count];
    }
    else
    {
        return 3;
    }
    return 0;
}

//--------------------- cellforrow At Index TableView -----------------------//

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@""];
    if (cell == nil)
    {
        //  cell.transform= CGAffineTransformMakeRotation(M_PI/2);
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    
    tableView.separatorColor =UIColor.clearColor;
    cell.backgroundColor= [UIColor clearColor];
   
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    UILabel *lblTitle=[[UILabel alloc]init];
   
    
    if (tableView==_tblVideoQuality)
    {
        UIButton *btn=[[UIButton alloc]init];
         [self.tblVideoQuality setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        //--------UIButton btn10Credits ---------//
        btn =[[UIButton alloc] initWithFrame:CGRectMake(_tblVideoQuality.frame.size.width/2-40,10,10,10)];
        btn.backgroundColor = [UIColor whiteColor];
        btn.layer.cornerRadius =btn.frame.size.width/2;
        btn.layer.borderWidth =0.8;
        btn.layer.borderColor =[UIColor grayColor].CGColor;
        btn.tag = indexPath.row+1;
        //[btn addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchDown];
        [cell.contentView addSubview:btn];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            lblTitle.frame=CGRectMake(_tblVideoQuality.frame.size.width/2-20,0,_tblVideoQuality.frame.size.width/2,30);
        }
        else
        {
            lblTitle.frame=CGRectMake(_tblVideoQuality.frame.size.width/2-20,0,_tblVideoQuality.frame.size.width/2,30);
        }
        if (indexPath.row==0)
        {
            lblTitle.text=@"Low";
        }
        else if (indexPath.row==1)
        {
            lblTitle.text=@"Medium";
        }
        else if (indexPath.row==2)
        {
            lblTitle.text=@"High";
        }
        lblTitle.textColor=[UIColor whiteColor];
        lblTitle.textAlignment=NSTextAlignmentLeft;
        lblTitle.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:lblTitle];
    }
    else if (tableView==_tblExpiry)
    {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            lblTitle.frame=CGRectMake(_tblExpiry.frame.size.width/2-20,0,_tblExpiry.frame.size.width/2,30);
        }
        else
        {
            lblTitle.frame=CGRectMake(_tblExpiry.frame.size.width/2-20,0,_tblExpiry.frame.size.width/2,30);
        }
        if (indexPath.row==0)
        {
            lblTitle.text=@"1 Week";
        }
        else if (indexPath.row==1)
        {
            lblTitle.text=@"2 Weeks";
        }
        else if (indexPath.row==2)
        {
            lblTitle.text=@"1 Month";
        }
        lblTitle.textColor=[UIColor whiteColor];
        lblTitle.textAlignment=NSTextAlignmentLeft;
        lblTitle.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:lblTitle];
    }
    else if (tableView==_tblAddFrnd)
    {
        [[UITableViewCell appearance] setTintColor:[UIColor orangeColor]];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        UILabel *lblCellTitle=[[UILabel alloc]initWithFrame:CGRectMake(5,5,screenWidth-10,30)];
        
            lblCellTitle.text=[[_arrFBFriendList valueForKey:@"name"]objectAtIndex:indexPath.row];
        lblCellTitle.textColor=[UIColor blackColor];
        lblCellTitle.font=[UIFont fontWithName:@"AvenirNext-Regular" size:16];
        [cell.contentView addSubview:lblCellTitle];
        
        UIView *viewsept=[[UIView alloc]initWithFrame:CGRectMake(0,39,screenWidth,1)];
        viewsept.backgroundColor=[[UIColor grayColor]colorWithAlphaComponent:0.6];
        [cell.contentView addSubview:viewsept];
        
             UIButton *btnAdd;
             if (cell.accessoryView == nil)
             {
                    btnAdd=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-35,7.5,25,25)];
                    
                    NSString *indexPat=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
                    if ([arrSelectedCells containsObject:indexPat])
                    {
                        [btnAdd setBackgroundImage:[UIImage imageNamed:@"icon_minus1"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [btnAdd setBackgroundImage:[UIImage imageNamed:@"icon_Snap"] forState:UIControlStateNormal];
                    }
                    
                    [btnAdd addTarget:self action:@selector(btnAddPressed:) forControlEvents:UIControlEventTouchUpInside];
                    btnAdd.tag=indexPath.row;
                    cell.accessoryView=btnAdd;
                }
            }
    return cell;
}

#pragma Mark-delegate methods
//--------------------- Tableview didselected index ---------------------//
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self closeViewButonPressed];
    if (tableView==_tblVideoQuality)
    {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
     
            UIImagePickerController *imageController=[[UIImagePickerController alloc] init];
            imageController.sourceType = UIImagePickerControllerSourceTypeCamera;
            if (indexPath.row==0)
            {
                [imageController setVideoQuality:UIImagePickerControllerQualityTypeLow];
            }
            else if (indexPath.row==1)
            {
                [imageController setVideoQuality:UIImagePickerControllerQualityTypeMedium];
            }
            else if (indexPath.row==2)
            {
                [imageController setVideoQuality:UIImagePickerControllerQualityTypeHigh];
            }
            imageController.allowsEditing = NO;
            imageController.mediaTypes  = [UIImagePickerController availableMediaTypesForSourceType:   UIImagePickerControllerSourceTypeCamera];
            imageController.delegate = self;
            [self presentViewController:imageController animated:YES completion:nil];
        }
        else
        {
         UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Camera is not available" preferredStyle:UIAlertControllerStyleAlert];
     
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
    else if (tableView==_tblExpiry)
    {
         NSDate *currentDate = [NSDate date];
        
        if (indexPath.row==0)
        {
            NSDate *sevenDays = [currentDate dateByAddingTimeInterval:7*24*60*60];
            NSLog(@"7 days ago: %@", sevenDays);
            NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
            [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
            _strBetExpiryDate = [dateformate stringFromDate:sevenDays]; // CONVERT date to string
            NSLog(@"date :%@",_strBetExpiryDate);
            
            NSLog(@"1 Week");
            _lblExpDate.text=@"1 Week";
            [_btnCalender setTitle:@"1 Week" forState:UIControlStateNormal];
            [_btnCalender setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
        else if (indexPath.row==1)
        {
            NSDate *sevenDays = [currentDate dateByAddingTimeInterval:14*24*60*60];
            NSLog(@"7 days ago: %@", sevenDays);
            NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
            [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
            _strBetExpiryDate = [dateformate stringFromDate:sevenDays]; // CONVERT date to string
            NSLog(@"date :%@",_strBetExpiryDate);
            _lblExpDate.text=@"2 Weeks";
            [_btnCalender setTitle:@"2 Weeks" forState:UIControlStateNormal];
            NSLog(@"2 Weeks");
        }
        else if (indexPath.row==2)
        {
            NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
            [dateComponents setMonth:1];
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:currentDate options:0];
            NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
            [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
            _strBetExpiryDate = [dateformate stringFromDate:newDate]; // CONVERT date to string
            NSLog(@"date :%@",_strBetExpiryDate);
            _lblExpDate.text=@"1 Month";
            [_btnCalender setTitle:@"1 Month" forState:UIControlStateNormal];
            NSLog(@"1 Month");
        }
    }
    else
    {
        UITableViewCell *selectedCell=[tableView cellForRowAtIndexPath:indexPath];
        NSString *strFB=[[_arrFBFriendList valueForKey:@"name"]objectAtIndex:indexPath.row];
        NSString *strSledUID=[[_arrFBFriendList valueForKey:@"uid"]objectAtIndex:indexPath.row];
        if (selectedCell.accessoryType == UITableViewCellAccessoryCheckmark)
        {
            selectedCell.accessoryType = UITableViewCellAccessoryNone;
            [_arrSelectedFBFrnd removeObject:strFB];
            [_arrSelectedFrndsUIDs removeObject:strSledUID];
        }
        else
        {
            selectedCell.accessoryType = UITableViewCellAccessoryCheckmark;
            [_arrSelectedFBFrnd addObject:strFB];
            [_arrSelectedFrndsUIDs addObject:strSledUID];
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        return 40;
    }
    else
    {
        if (tableView==_tblAddFrnd) {
            return 40;
        }
        return 30;
    }
}

-(IBAction)btnAddPressed:(UIButton *)sender
{
    NSString *indexPat=[NSString stringWithFormat:@"%ld",(long)sender.tag];
    
    NSString *uid=[[_arrFBFriendList valueForKey:@"uid"]objectAtIndex:sender.tag];
    
    if ([arrSelectedCells containsObject:indexPat] && [_arrSelectedFrndsUIDs containsObject:uid])
    {
        [arrSelectedCells removeObject:indexPat];
        [_arrSelectedFrndsUIDs removeObject:uid];
    }
    else
    {
        [arrSelectedCells addObject:indexPat];
        [_arrSelectedFrndsUIDs addObject:uid];
    }
    
    [_tblAddFrnd reloadData];
}
#pragma mark-Close Comment View
-(void)closeViewButonPressed
{
    @try
    {
        [self enableSubViews];
        for(UIView *subview in [_viewForVideoQuality subviews])
        {
            [subview removeFromSuperview];
        }
        for(UIView *subview in [_viewForVideExpiry subviews])
        {
            [subview removeFromSuperview];
        }
        
        for(UIView *subview in [_viewForVideoScreen subviews])
        {
            [subview removeFromSuperview];
        }
        
        for(UIView *subview in [_viewForVideoTrim subviews])
        {
            [subview removeFromSuperview];
        }
        
        CGRect frameForViewForHelp=_viewForVideoQuality.frame;
        CGRect frameForViewForVExpiry=_viewForVideExpiry.frame;
        CGRect frameForViewForVideo=_viewForVideoScreen.frame;
        CGRect frameForViewForVideoTrim=_viewForVideoTrim.frame;
        
        frameForViewForHelp=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        frameForViewForVideo=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        frameForViewForVideoTrim=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            frameForViewForVExpiry=CGRectMake(150, screenHeight/2,screenWidth-300,0);
        }
        else
        {
            frameForViewForVExpiry=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        }
        [UIView animateWithDuration:0.5f animations:^{
            _viewForVideoQuality.frame=frameForViewForHelp;
            _viewForVideExpiry.frame=frameForViewForVExpiry;
            _viewForVideoScreen.frame=frameForViewForVideo;
            _viewForVideoTrim.frame=frameForViewForVideoTrim;
            
        } completion:^(BOOL finished){
            [_viewForVideoQuality removeFromSuperview];
            [_viewForVideExpiry removeFromSuperview];
            [_btnCloseView removeFromSuperview];
            [_viewForVideoScreen removeFromSuperview];
            [_viewForVideoTrim removeFromSuperview];
                  //   [self enableSubViews];
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}



#pragma mark: Disable SubView
-(void)disableSubViews:(UIView *)view
{
    for(UIView *subview in [self.view subviews])
    {
        if ((subview.tag==view.tag&&[subview isMemberOfClass:[UIView class]]))
        {
            
        }
        else
        {
            subview.userInteractionEnabled=NO;
            //subview.alpha=.5;
        }
    }
}

#pragma mark: Enable Subview
-(void)enableSubViews
{
    for(UIView *subview in [self.view subviews])
    {
        subview.userInteractionEnabled=YES;
        // subview.alpha=1;
    }
}
#pragma mark:ViewForVideo selection
-(void)viewForVideoUpload
{
    [_viewForUpload removeFromSuperview];
    [_btnClose removeFromSuperview];
    _viewForUpload =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    
    _viewForUpload.backgroundColor=[UIColor blackColor];
    //_viewForUpload.alpha=1;
    _viewForUpload.layer.cornerRadius=5.f;
    _viewForUpload.layer.borderColor=[[UIColor whiteColor]CGColor];
    _viewForUpload.layer.borderWidth=2.0f;
    _viewForUpload.tag=20;
    [self.view addSubview:_viewForUpload];
    CGRect frameForViewForHelp=_viewForUpload.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForUpload.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        
        vscreenSize=_viewForUpload.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        _btnClose=[[UIButton alloc]initWithFrame:CGRectMake(_viewForUpload.frame.size.width-40,10,30,30)];
        [_btnClose setBackgroundImage:[UIImage imageNamed:@"img_Close1"] forState:UIControlStateNormal];
        [_btnClose addTarget:self action:@selector(closeUploadView:) forControlEvents:UIControlEventTouchUpInside];
        [_viewForUpload addSubview:_btnClose];
        [_collMyVideos removeFromSuperview];
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
        layout.minimumInteritemSpacing =4;
        layout.minimumLineSpacing =[UIScreen mainScreen].bounds.size.height/100;
        _collMyVideos=[[UICollectionView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_btnClose.frame)+10,_viewForUpload.frame.size.width,_viewForUpload.frame.size.height-80) collectionViewLayout:layout];
        [_collMyVideos setDataSource:self];
        [_collMyVideos setDelegate:self];
        [_collMyVideos setShowsVerticalScrollIndicator:NO];
        [_collMyVideos registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [_collMyVideos setBackgroundColor:[UIColor clearColor]];
        [_viewForUpload addSubview:_collMyVideos];
    }];
    //  [self disableSubViews:viewForUpload];
    
}

#pragma mark: Viewfor Play Video popUP UI
-(void)viewForVideoScreenPopUpUI
{
    [_viewForVideoScreen removeFromSuperview];
    _viewForVideoScreen =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForVideoScreen.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForVideoScreen.tag=20;
    [self.view addSubview:_viewForVideoScreen];
    CGRect frameForViewForHelp=_viewForVideoScreen.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForVideoScreen.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForVideoScreen.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        _viewForVideoArea=[[UIView alloc]init];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _viewForVideoArea.frame=CGRectMake(60,[UIScreen mainScreen].bounds.size.height/100*25,_viewForVideoScreen.frame.size.width-120,[UIScreen mainScreen].bounds.size.height/100*50);
        }
        else
        {
            _viewForVideoArea.frame=CGRectMake(10,[UIScreen mainScreen].bounds.size.height/100*25,_viewForVideoScreen.frame.size.width-20,[UIScreen mainScreen].bounds.size.height/100*50);
        }
        _viewForVideoArea.backgroundColor=[UIColor colorWithRed:38/255.0 green:38/255.0 blue:38/255.0 alpha:1];
        _viewForVideoArea.layer.borderWidth=2.0;
        _viewForVideoArea.layer.borderColor=[[UIColor whiteColor]CGColor];
        [_viewForVideoScreen addSubview:_viewForVideoArea];
        
        _asySelectedBG=[[AsyncImageView alloc]initWithFrame:CGRectMake(0,0,_viewForVideoArea.frame.size.width,_viewForVideoArea.frame.size.height)];
        _asySelectedBG.backgroundColor = [UIColor clearColor];
        NSString *imgBaseURL=ImgBase_URL;
        imgBaseURL=[imgBaseURL stringByAppendingString:_strUplodedImg];
        
        NSString *webStr = [NSString stringWithFormat:@"%@",imgBaseURL];
        NSURL *imageUrl = [[NSURL alloc] initWithString:[webStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
        
        [_asySelectedBG setContentMode:UIViewContentModeScaleAspectFill];
        [_asySelectedBG sizeToFit];
        _asySelectedBG.clipsToBounds = YES;
        [_asySelectedBG loadImageFromURL:imageUrl];
        [_viewForVideoArea addSubview:_asySelectedBG];
        
        if ([_strVideoName isEqualToString:@""]) {
            
        }
        else
        {
            UIButton *btnPlay=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoArea.frame.size.width/2-25,_viewForVideoArea.frame.size.height/2-22,50,44)];
            [btnPlay setBackgroundImage:[UIImage imageNamed:@"img_AppIcon"] forState:UIControlStateNormal];
            [btnPlay addTarget:self action:@selector(playVideo) forControlEvents:UIControlEventTouchUpInside];
            [_viewForVideoArea addSubview:btnPlay];
        }
            UIButton *btnPrviCont=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoScreen.frame.size.width/2-50,CGRectGetMaxY(_viewForVideoArea.frame)+25,30,20)];
            [btnPrviCont setBackgroundImage:[UIImage imageNamed:@"icon_Privious"] forState:UIControlStateNormal];
        // [_viewForVideoScreen addSubview:btnPrviCont];
        
        UIButton *btnPlyPause=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoScreen.frame.size.width/2-12,CGRectGetMaxY(_viewForVideoArea.frame)+25,24,20)];
        [btnPlyPause setBackgroundImage:[UIImage imageNamed:@"icon_Pause"] forState:UIControlStateNormal];
        ///[_viewForVideoScreen addSubview:btnPlyPause];
        
        UIButton *btnNxtCont=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(btnPlyPause.frame)+8,CGRectGetMaxY(_viewForVideoArea.frame)+25,30,20)];
        [btnNxtCont setBackgroundImage:[UIImage imageNamed:@"icon_Next"] forState:UIControlStateNormal];
        //[_viewForVideoScreen addSubview:btnNxtCont];
        
        _btnCloseVideoScreen=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoScreen.frame.size.width-40,20,30,30)];
        [_btnCloseVideoScreen setBackgroundImage:[UIImage imageNamed:@"icon_Cross"] forState:UIControlStateNormal];
        [_btnCloseVideoScreen addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
        [_viewForVideoScreen addSubview:_btnCloseVideoScreen];
    }];
}

#pragma mark: Play selected challage video
-(void)playVideo
{
    [_playerViewController.view removeFromSuperview];
    NSString *strVBase=vBase_URL;
    NSString *strVideoName=_strVideoName;
    strVBase=[strVBase stringByAppendingString:strVideoName];
    
    NSURL *vURL=[NSURL URLWithString:strVBase];
    _playerViewController = [[AVPlayerViewController alloc] init];
    _playerViewController.player = [AVPlayer playerWithURL:vURL];
    _playerViewController.view.frame =CGRectMake(0,0,_viewForVideoArea.frame.size.width,_viewForVideoArea.frame.size.height);
    //[_playerViewController setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_playerViewController.player play];
    _playerViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:_playerViewController animated:YES completion:nil];
    //[_viewForVideoArea addSubview:_playerViewController.view];
}
#pragma mark: ViewFor Spinner declaration
-(void)viewForSpinner
{
    [_viewForLoadingVideos removeFromSuperview];
    _viewForLoadingVideos=[[UIView alloc]initWithFrame:CGRectMake(screenWidth/2-100,screenHeight/3-40,200,150)];
    [_viewForUpload addSubview:_viewForLoadingVideos];
    [self.view setUserInteractionEnabled:NO];
    _viewForLoadingVideos.backgroundColor=[UIColor whiteColor];
    _viewForLoadingVideos.layer.cornerRadius=5.0;
    UILabel *lblLoad=[[UILabel alloc]initWithFrame:CGRectMake(0,5,_viewForLoadingVideos.frame.size.width,30)];
    lblLoad.text=@"Wait";
    lblLoad.textAlignment=NSTextAlignmentCenter;
    lblLoad.textColor=[UIColor blackColor];
    lblLoad.font=[UIFont boldSystemFontOfSize:14];
    [_viewForLoadingVideos addSubview:lblLoad];
    UILabel *lblTit=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblLoad.frame),_viewForLoadingVideos.frame.size.width,30)];
    lblTit.text=@"Loading local videos";
    lblTit.textColor=[UIColor blackColor];
    lblTit.font=[UIFont boldSystemFontOfSize:16];
    lblTit.textAlignment=NSTextAlignmentCenter;
    [_viewForLoadingVideos addSubview:lblTit];
    
    static const CGFloat activityIndicatorSize = 40.f;
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:   UIActivityIndicatorViewStyleWhiteLarge];
    _activityIndicator.alpha = 0.f;
    _activityIndicator.hidesWhenStopped = YES;
    [_activityIndicator setFrame:CGRectMake(_viewForLoadingVideos.frame.size.width/2-20,_viewForLoadingVideos.frame.size.height-60,activityIndicatorSize,activityIndicatorSize)];
    [self showLoadingIndicators];
}

- (void)showLoadingIndicators
{
    [_viewForLoader addSubview:_activityIndicator];
    [_activityIndicator startAnimating];
    _activityIndicator.color=[UIColor blackColor];
    //_activityIndicator.backgroundColor=[UIColor blackColor];
    [UIView animateWithDuration:0.2f animations:^{
        _activityBackgroundView.alpha = 1.f;
        _activityIndicator.alpha = 1.f;
    }];
}

- (void)hideLoadingIndicators
{
    [UIView animateWithDuration:0.2f delay:0.0 options:0 animations:^{
        self.activityBackgroundView.alpha = 0.0f;
        self.activityIndicator.alpha = 0.f;
    } completion:^(BOOL finished) {
        [_btnUploadBet setUserInteractionEnabled:YES];
        [self.activityBackgroundView removeFromSuperview];
        [self.activityIndicator removeFromSuperview];
    }];
}

#pragma mark-Close Comment View
-(void)closeUploadView:(id)sender
{
    @try
    {
        // [self enableSubViews];
        
        //        _btnUpload=[[UIButton alloc]initWithFrame:CGRectMake(screenWidth-70,screenHeight-80,50,50)];
        //        [_btnUpload setBackgroundImage:[UIImage imageNamed:@"img_FedUpload"] forState:UIControlStateNormal];
        //        [_btnUpload addTarget:self action:@selector(showVideoList:) forControlEvents:UIControlEventTouchUpInside];
        //        _btnUpload.layer.cornerRadius=3.0;
        //        [self.view addSubview:_btnUpload];
        [self viewDidLoad];
        [_btnClose removeFromSuperview];
        for(UIView *subview in [_viewForUpload subviews])
        {
            [subview removeFromSuperview];
            [self.btnClose removeFromSuperview];
        }
        CGRect frameForViewForHelp=_viewForUpload.frame;
        
        frameForViewForHelp=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForUpload.frame=frameForViewForHelp;
        } completion:^(BOOL finished){
            [_viewForUpload removeFromSuperview];
            [_collMyVideos removeFromSuperview];
            //   [self enableSubViews];
        }];
    } @catch (NSException *exception)
    {
        
    } @finally    {
        
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *strCapturedVideo=info[UIImagePickerControllerMediaURL];
    NSLog(@"%@",strCapturedVideo);
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    //check the media type string so we can determine if its a video
    if ([mediaType isEqualToString:@"public.movie"])
    {
        NSLog(@"got a movie");
        
        selectedVdoURL =  [info objectForKey:UIImagePickerControllerMediaURL];
        strGOrgVPath =  [info objectForKey:UIImagePickerControllerMediaURL];
        
        NSURL *videoURL=[info objectForKey:@"UIImagePickerControllerMediaURL"];
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
        self.asset = [AVAsset assetWithURL:videoURL];
        NSTimeInterval durationInSeconds = 0.0;
        videoTimeDuration=0.0;
        
        
        if (asset)
        {
            durationInSeconds = CMTimeGetSeconds(asset.duration);
            durationOfVideo = CMTimeGetSeconds(asset.duration);
            theAppDelegate.durationOfOrignalVideo = CMTimeGetSeconds(asset.duration);
            
            NSLog(@"Duration Of Video--------------------------------%f",durationInSeconds);
            
            [picker dismissViewControllerAnimated:YES completion:NULL];
            [self performSelector:@selector(viewForVideoTrimming) withObject:nil afterDelay:0.5];
        }
    }
    else
    {
        //[theAppDelegate showSpinnerInView:self.view];
        
        [picker dismissViewControllerAnimated:YES completion:^{
           
            // -------------- UIImageView for Profile ----------//
            
            _image=[[UIImage alloc]init];
            //_image = [info valueForKey:UIImagePickerControllerOriginalImage];
            _image = [self scaleAndRotateImage:[info valueForKey:UIImagePickerControllerOriginalImage]];
            [self openEditor:nil];
            
            //_imgProfile.image =_image;
           /* NSData *dataImage = [[NSData alloc] init];
            dataImage = UIImagePNGRepresentation(_image);
            _strUplodedImg=[dataImage base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            
            [self serviceCallForUploadPhoto1:[self getCompressedImage:_image]];*/
        
            //NSData *imageData=UIImageJPEGRepresentation(image, 0.1);
        }];
    }
}

- (UIImage *)scaleAndRotateImage:(UIImage *) image {
    int kMaxResolution = 1200;
    
    CGImageRef imgRef = image.CGImage;
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1)
        {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();    
    return imageCopy;
}

#pragma mark-Upload Video To Server
-(void)loadVideoToServer
{
    if (theAppDelegate.isConnected)
    {
        NSData *data = [NSData dataWithContentsOfURL:selectedVdoURL];
        if(data==nil)
        {
            _strVideoName=[[NSString alloc]init];
        }
        else
        {
            NSString *urlString = @"http://54.67.95.152/snapbets/aws/uploadvideofile.php?";
            NSString *filename = @"aa";
            NSMutableURLRequest *request=[[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:urlString]];
            [request setHTTPMethod:@"POST"];
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            NSMutableData *postbody = [NSMutableData data];
            [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=    \"userfile\"; filename=\"%@\"\r\n", filename]   dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:[NSData dataWithData:data]];
            [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [request setHTTPBody:postbody];
        
            [self ViewForDataLoadingAlert];
        
            _collMyVideos=[[NSURLConnection alloc]initWithRequest:request delegate:self];
        }
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Please check internet connection" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

-(NSData*)getCompressedImage:(UIImage*)img
{
    //>213832
    //------------  Image Compression --------- //
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    //int maxFileSize = 250*1024;
    int maxFileSize = 640*480;
    NSData  *imageData = UIImageJPEGRepresentation(img, compression);
    NSLog(@"Initial Size of Image==>%li",(long)imageData.length);
    
    while ([imageData length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        imageData = UIImageJPEGRepresentation(img, compression);
    }
    
    NSLog(@"Compressed Size of Image==>%li",(long)imageData.length);
    //------------  Image Compression Algorithm--------- //
    
    return imageData;
}

#pragma mark-View For Video Trimming
-(void)viewForVideoTrimming
{
     [_viewForVideoTrim removeFromSuperview];
    _viewForVideoTrim =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForVideoTrim.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForVideoTrim.tag=20;
    [self.view addSubview:_viewForVideoTrim];
    
    CGRect frameForViewForHelp=_viewForVideoTrim.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForVideoTrim.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForAlert.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        _btnCloseVideoTrim=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoTrim.frame.size.width-40,10,30,30)];
        [_btnCloseVideoTrim setBackgroundImage:[UIImage imageNamed:@"icon_Cross"] forState:UIControlStateNormal];
        [_btnCloseVideoTrim addTarget:self action:@selector(closeViewButonPressed) forControlEvents:UIControlEventTouchUpInside];
        _btnCloseVideoTrim.layer.borderColor=[[UIColor orangeColor]CGColor];
        [_viewForVideoTrim addSubview:_btnCloseVideoTrim];
    
        NSString *tempDir = NSTemporaryDirectory();
        self.tmpVideoPath = [tempDir stringByAppendingPathComponent:@"tmpMov.mov"];
        self.tempOrignalVideoPath = [tempDir stringByAppendingPathComponent:@"tmpMov.mov"];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            
            [_trimmerView removeFromSuperview];
            _trimmerView=[[ICGVideoTrimmerView alloc]initWithFrame:CGRectMake(0,200, self.view.frame.size.width, 100) asset:self.asset];
        }
        else
        {
            [_trimmerView removeFromSuperview];
            _trimmerView=[[ICGVideoTrimmerView alloc]initWithFrame:CGRectMake(0,100, self.view.frame.size.width, 80) asset:self.asset];
        }
        
        [_trimmerView removeFromSuperview];
        _trimmerView=[[ICGVideoTrimmerView alloc]initWithFrame:CGRectMake(0,100, self.view.frame.size.width, 80) asset:self.asset];
        [self.trimmerView setThemeColor:[UIColor lightGrayColor]];
        [self.trimmerView setAsset:self.asset];
        [self.trimmerView setShowsRulerView:YES];
        [self.trimmerView setRulerLabelInterval:5];
       
        [self.trimmerView setTrackerColor:[UIColor cyanColor]];
        [self.trimmerView setDelegate:self];
        [self.trimmerView resetSubviews];
        [self.trimmerView hideTracker:true];
        [self.trimButton setHidden:NO];
        [_viewForVideoTrim addSubview:self.trimmerView];
        
        
        _lblTime=[[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.trimmerView.frame)+15, _viewForVideoTrim.frame.size.width, 30)];
        _lblTime.textColor=[UIColor whiteColor];
        _lblTime.textAlignment=NSTextAlignmentCenter;
        _lblTime.text = [NSString stringWithFormat:@"0 secs - %.0f secs", durationOfVideo];
        _lblTime.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForVideoTrim addSubview:_lblTime];
        
        
        _lblTotalTime=[[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_lblTime.frame)+15, _viewForVideoTrim.frame.size.width, 30)];
        _lblTotalTime.textColor=[UIColor whiteColor];
        _lblTotalTime.text=[NSString stringWithFormat:@"Total Time : %.0f secs",durationOfVideo];
        _lblTotalTime.textAlignment=NSTextAlignmentCenter;
        _lblTotalTime.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForVideoTrim addSubview:_lblTotalTime];
        
        _btnOriginal=[[UIButton alloc]initWithFrame:CGRectMake(30,CGRectGetMaxY(_lblTotalTime.frame)+30,_viewForVideoTrim.frame.size.width/2-40,50)];
        //_btnTrim=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoTrim.frame.size.width/2+15,CGRectGetMaxY(_lblTotalTime.frame)+30,_viewForVideoTrim.frame.size.width/2-30,50)];
        _btnTrim=[[UIButton alloc]initWithFrame:CGRectMake(30,CGRectGetMaxY(_lblTotalTime.frame)+30,_viewForVideoTrim.frame.size.width/2-40,50)];
        
        [_btnTrim setTitle:@"Trim" forState:UIControlStateNormal];
        
        [_btnTrim setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        _btnTrim.layer.borderWidth=2.0;
        _btnTrim.layer.borderColor=[[UIColor orangeColor]CGColor];
        
        _btnOriginal.backgroundColor=[UIColor orangeColor];
        
        _btnOriginal.layer.cornerRadius=5.0;
        _btnTrim.layer.cornerRadius=5.0;
        
        [_btnOriginal addTarget:self action:@selector(btnOriginalPressed) forControlEvents:UIControlEventTouchUpInside];
        
        [_btnTrim addTarget:self action:@selector(trimVideo:) forControlEvents:UIControlEventTouchUpInside];
        //[_viewForVideoTrim addSubview:_btnOriginal];
        [_viewForVideoTrim addSubview:_btnTrim];
        
        UILabel *lblBuy=[[UILabel alloc]initWithFrame:CGRectMake(0,10,_btnOriginal.frame.size.width,30)];
        lblBuy.text=@"Original";
        lblBuy.textAlignment=NSTextAlignmentCenter;
        lblBuy.textColor=[UIColor whiteColor];
        lblBuy.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_btnOriginal addSubview:lblBuy];
        
        
        _btnSubmit=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnSubmit.frame=CGRectMake(_viewForVideoTrim.frame.size.width/2+15,CGRectGetMaxY(_lblTotalTime.frame)+30,_viewForVideoTrim.frame.size.width/2-30,50);
        }
        else
        {
            _btnSubmit=[[UIButton alloc]initWithFrame:CGRectMake(_viewForVideoTrim.frame.size.width/2+15,CGRectGetMaxY(_lblTotalTime.frame)+30,_viewForVideoTrim.frame.size.width/2-30,50)];
        }
        [_btnSubmit setTitle:@"Submit" forState:UIControlStateNormal];
        [_btnSubmit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnSubmit.layer.cornerRadius=4.0;
        NSLog(@"Duration Of Video--------------------------------%f",durationOfVideo);
        NSLog(@"Duration Of Video--------------------------------%f",theAppDelegate.GlobDurOfOrignalVideo);
        
        if ( durationOfVideo < theAppDelegate.GlobDurOfOrignalVideo || theAppDelegate.GlobDurOfOrignalVideo == durationOfVideo )
        {
            
        }
        else
        {
            _btnSubmit.alpha=0.7;
            _btnSubmit.userInteractionEnabled=NO;
        }
       
        _btnSubmit.backgroundColor=[UIColor orangeColor];
        [_btnSubmit addTarget:self action:@selector(btnSubmitPressed) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewForVideoTrim addSubview:_btnSubmit];
        
        UILabel *lblInfo=[[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_btnSubmit.frame)+15, _viewForVideoTrim.frame.size.width, 30)];
        lblInfo.textColor=[UIColor whiteColor];
        lblInfo.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        lblInfo.textAlignment=NSTextAlignmentCenter;
        if ([arrPurchaseDates count])
        {
            lblInfo.text=[NSString stringWithFormat:@"Select video 90 seconds or less"];
        }
        else
        {
            lblInfo.text=[NSString stringWithFormat:@"Select video 15 seconds or less"];
        }
        [_viewForVideoTrim addSubview:lblInfo];
        
    }];
}

-(void)btnSubmitPressed
{
    @try
    {
        
        
        NSDate *today=[NSDate date];
        NSDateFormatter *format=[[NSDateFormatter alloc]init];
        [format setDateFormat:@"dd-MMM-yy"];
        
       // [self enableSubViews];
       // for(UIView *subview in [_viewForVideoTrim subviews])
       // {
            //[subview removeFromSuperview];
       // }
        CGRect frameForViewForHelp=_viewForVideoTrim.frame;
       // frameForViewForHelp=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        [UIView animateWithDuration:0.5f animations:^{
           // _viewForVideoTrim.frame=frameForViewForHelp;
            
        } completion:^(BOOL finished)
        {
            //[_viewForVideoTrim removeFromSuperview];
            
            AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:selectedVdoURL options:nil];
            durationOfVideo = CMTimeGetSeconds(asset.duration);
            NSLog(@"Duration Of Video--------------------------------%f",durationOfVideo);
            if ((durationOfVideo > 16 || durationOfVideo == 16) && [arrPurchaseDates count] == 0)
            {
                _isSnapSuccess=NO;
                _strAlertTitle=@"Video should be 15 seconds or less";
                [self performSelector:@selector(viewForTrimingVideoAlertUI) withObject:nil afterDelay:0.5];
            }
            else if ([arrPurchaseDates count] && (durationOfVideo > 91 || durationOfVideo == 91))
            {
                _isSnapSuccess=NO;
                _strAlertTitle=@"Video should be 90 seconds or less";
                [self performSelector:@selector(viewForTrimingVideoAlertUI) withObject:nil afterDelay:0.5];
            }
            else if([arrPurchaseDates count])
            {
                if ([arrExpiryDates count] == 1)
                {
                    NSString *strExpireDate = [arrExpiryDates objectAtIndex:0];
                    NSDate *expireDate=[format dateFromString:strExpireDate];
                    NSComparisonResult result1=[today compare:expireDate];
                    if (result1 == NSOrderedDescending)
                    {
                        _isSnapSuccess=NO;
                        _strAlertTitle=@"Video should be 15 seconds or less";
                        [self performSelector:@selector(viewForTrimingVideoAlertUI) withObject:nil afterDelay:0.5];
                    }
                    else
                    {
                        [_viewForVideoTrim removeFromSuperview];
                        [self loadVideoToServer];
                    }
                }
                else
                {
                    NSString *strExpireDate1 = [arrExpiryDates objectAtIndex:0];
                    NSString *strExpireDate2 = [arrExpiryDates objectAtIndex:0];
            
                    NSDate *expireDate1=[format dateFromString:strExpireDate1];
                    NSDate *expireDate2=[format dateFromString:strExpireDate2];
            
                    NSComparisonResult result1=[expireDate1 compare:expireDate2];
                    if (result1 == NSOrderedSame)
                    {
                        NSComparisonResult result1=[today compare:expireDate1];
                        if (result1 == NSOrderedDescending)
                        {
                            _isSnapSuccess=NO;
                            _strAlertTitle=@"Video should be 15 seconds or less";
                            [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:0.5];
                        }
                        else
                        {
                            [_viewForVideoTrim removeFromSuperview];
                            [self loadVideoToServer];
                        }
                    }
                    else if (result1 == NSOrderedAscending)
                    {
                        NSComparisonResult result1=[today compare:expireDate2];
                        if (result1 == NSOrderedDescending)
                        {
                            _isSnapSuccess=NO;
                            _strAlertTitle=@"Video should be 15 seconds or less";
                            [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:0.5];
                        }
                        else
                        {
                            [_viewForVideoTrim removeFromSuperview];
                            [self loadVideoToServer];
                        }
                    }
                    else
                    {
                        NSComparisonResult result1=[today compare:expireDate1];
                        if (result1 == NSOrderedDescending)
                        {
                            _isSnapSuccess=NO;
                            _strAlertTitle=@"Video should be 15 seconds or less";
                            [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:0.5];
                        }
                        else
                        {
                            [_viewForVideoTrim removeFromSuperview];
                            [self loadVideoToServer];
                        }
                    }
                }
            }
            else
            {
                [_viewForVideoTrim removeFromSuperview];
                [self loadVideoToServer];
            }
            
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

-(void)btnOriginalPressed
{
    selectedVdoURL = strGOrgVPath;
    [self playMovie:selectedVdoURL];
}

-(void)btnTrimPressed
{
    //[self delee];
    
//    NSURL *videoFileUrl = [NSURL fileURLWithPath:self.originalVideoPath];
    
    AVAsset *anAsset = [[AVURLAsset alloc] initWithURL:selectedVdoURL options:nil];
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:anAsset];
    if ([compatiblePresets containsObject:AVAssetExportPresetMediumQuality]) {
        
        self.exportSession = [[AVAssetExportSession alloc]
                              initWithAsset:anAsset presetName:AVAssetExportPresetPassthrough];
        // Implementation continues.
        
        NSURL *furl = [NSURL fileURLWithPath:self.tempOrignalVideoPath];
        
        self.exportSession.outputURL = furl;
        self.exportSession.outputFileType = AVFileTypeQuickTimeMovie;
        
        CMTime start = CMTimeMakeWithSeconds(self.startTime, anAsset.duration.timescale);
        CMTime duration = CMTimeMakeWithSeconds(self.stopTime-self.startTime, anAsset.duration.timescale);
        CMTimeRange range = CMTimeRangeMake(start, duration);
        self.exportSession.timeRange = range;
        
        [self.exportSession exportAsynchronouslyWithCompletionHandler:^{
            
            switch ([self.exportSession status]) {
                case AVAssetExportSessionStatusFailed:
                    selectedVdoURL=[NSURL fileURLWithPath:self.tempOrignalVideoPath];
                    NSLog(@"Export failed: %@", [[self.exportSession error] localizedDescription]);
                    break;
                case AVAssetExportSessionStatusCancelled:
                    selectedVdoURL=[NSURL fileURLWithPath:self.tempOrignalVideoPath];
                    NSLog(@"Export canceled");
                    break;
                default:
                    NSLog(@"NONE");
                    dispatch_async(dispatch_get_main_queue(), ^
                    {
                        selectedVdoURL=[NSURL fileURLWithPath:self.tmpVideoPath];
                        [self playMovie:selectedVdoURL];
                    });
                    
                    break;
            }
        }];
    }
}

-(void)playMovie: (NSURL *) url
{
//    NSURL *url = [NSURL fileURLWithPath:path];
    MPMoviePlayerViewController *theMovie = [[MPMoviePlayerViewController alloc] initWithContentURL:url];
    [self presentMoviePlayerViewControllerAnimated:theMovie];
    theMovie.moviePlayer.movieSourceType = MPMovieSourceTypeFile;
    [theMovie.moviePlayer play];
}


#pragma mark - ICGVideoTrimmerDelegate

- (void)trimmerView:(ICGVideoTrimmerView *)trimmerView didChangeLeftPosition:(CGFloat)startTime rightPosition:(CGFloat)endTime
{
    _restartOnPlay = YES;
    [self.player pause];
    self.isPlaying = NO;
    
    [self.trimmerView hideTracker:true];
    
    if (startTime != self.startTime) {
        //then it moved the left position, we should rearrange the bar
        [self seekVideoToPos:startTime];
    }
    else{ // right has changed
        [self seekVideoToPos:endTime];
    }
    self.startTime = startTime;
    self.stopTime = endTime;
    
    _lblTime.text = [NSString stringWithFormat:@"%.0f secs - %.0f secs", startTime, endTime];
    _lblTotalTime.text=[NSString stringWithFormat:@"Total Time : %.0f secs",endTime - startTime];
    durationOfVideo=endTime-startTime;
    _btnTrim.backgroundColor=[UIColor orangeColor];
}

- (void)seekVideoToPos:(CGFloat)pos
{
    self.videoPlaybackPosition = pos;
    CMTime time = CMTimeMakeWithSeconds(self.videoPlaybackPosition, self.player.currentTime.timescale);
    //NSLog(@"seekVideoToPos time:%.2f", CMTimeGetSeconds(time));
    [self.player seekToTime:time toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
}
#pragma mark - Actions

- (void)deleteTempFile
{
    NSURL *url = [NSURL fileURLWithPath:self.tempVideoPath];
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL exist = [fm fileExistsAtPath:url.path];
    NSError *err;
    if (exist) {
        [fm removeItemAtURL:url error:&err];
        NSLog(@"file deleted");
        if (err) {
            NSLog(@"file remove error, %@", err.localizedDescription );
        }
    } else {
        NSLog(@"no file by that name");
    }
}


- (IBAction)trimVideo:(id)sender
{
    [self deleteTempFile];
    
    _btnSubmit.alpha=1.0;
    _btnSubmit.userInteractionEnabled=YES;
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:self.asset];
    if ([compatiblePresets containsObject:AVAssetExportPresetMediumQuality]) {
        
        self.exportSession = [[AVAssetExportSession alloc]
                              initWithAsset:self.asset presetName:AVAssetExportPresetPassthrough];
        // Implementation continues.
        
        NSURL *furl = [NSURL fileURLWithPath:self.tempVideoPath];
        
        self.exportSession.outputURL = furl;
        self.exportSession.outputFileType = AVFileTypeQuickTimeMovie;
        
        CMTime start = CMTimeMakeWithSeconds(self.startTime, self.asset.duration.timescale);
        CMTime duration = CMTimeMakeWithSeconds(self.stopTime - self.startTime, self.asset.duration.timescale);
        CMTimeRange range = CMTimeRangeMake(start, duration);
        self.exportSession.timeRange = range;
        
        [self.exportSession exportAsynchronouslyWithCompletionHandler:^{
            
            switch ([self.exportSession status]) {
                case AVAssetExportSessionStatusFailed:
                    
                    NSLog(@"Export failed: %@", [[self.exportSession error] localizedDescription]);
                    break;
                case AVAssetExportSessionStatusCancelled:
                    
                    NSLog(@"Export canceled");
                    break;
                default:
                    NSLog(@"NONE");
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        selectedVdoURL=[NSURL fileURLWithPath:self.tempVideoPath];
                        [self playMovie:selectedVdoURL];
                    });
                    
                    break;
            }
        }];
        
    }
}

#pragma mark: UploadFirstImage
-(void)serviceCallForUploadPhoto1:(NSData*)imgData
{
    if ([theAppDelegate isConnected])
    {
        NSString *strBase=base_URL;
        NSString *url=[NSString stringWithFormat:@"uploadimage_ios.php"];
        strBase=[strBase stringByAppendingString:url];
        
        NSLog(@"Image upload URL :%@",url);
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strBase]];
        
        NSData *imageData = imgData;
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"unique-consistent-string";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"imageCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"Some Caption"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        if(imageData)
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=imagename.png\r\n",@"userfile"] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[NSData dataWithData:imageData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        // [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        NSHTTPURLResponse* response =[[NSHTTPURLResponse alloc] init];
        NSError* error = [[NSError alloc] init];
        
        //synchronous filling of data from HTTP POST response
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        //convert data into string
        NSString   *responseString = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];
        
        NSLog(@"Response String %@",responseString);
        
        NSError *error1;
        NSMutableDictionary *mainDict=[NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error1];
        
        NSString *strMSG=[mainDict valueForKey:@"message"];
        NSArray *arr=[[NSArray alloc]init];
        arr=[mainDict valueForKey:@"info"];
        responseString=[arr description];
        
        if ([strMSG isEqualToString:@"sus"])
        {
            _strVideoName=@"";
            _strUplodedImg=responseString;
            _strSelectedThmeID=@"0";
            _imgLogo.image=[UIImage imageNamed:@""];
            NSString *strBURL=ImgBase_URL;
            strBURL=[strBURL stringByAppendingString:responseString];
            [self.view bringSubviewToFront:_asyImgHBG];
            [_asyImgHBG loadImageFromURL:[NSURL URLWithString:strBURL]];
            _asyImgHBG.layer.cornerRadius=6.0;
            [self performSelector:@selector(loadPlayVideoButton) withObject:nil afterDelay:4];
        }
        else
        {
            //_imgProfile.image=[UIImage imageNamed:@""];
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:@"Failed to upload picture to server." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
            
            //[[[UIAlertView alloc]initWithTitle:@"Failed" message:@"Failed to upload picture to server." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
    }
    else
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"No Internet connection"
                                              message:@""
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"ok", @"Cancel action")
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"Cancel action");
                                       }];
        
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    [theAppDelegate stopSpinner];
    [self.view setUserInteractionEnabled:YES];
}

#pragma mark: UploadFirstImage
-(void)serviceCallForUploadThumbnail:(NSData*)imgData
{
        NSString *strBase=base_URL;
        NSString *url=[NSString stringWithFormat:@"uploadimage_ios.php"];
        strBase=[strBase stringByAppendingString:url];
        
        NSLog(@"Image upload URL :%@",url);
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strBase]];
        
        NSData *imageData = imgData;        
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"unique-consistent-string";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"imageCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"Some Caption"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        if(imageData)
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=imagename.png\r\n",@"userfile"] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[NSData dataWithData:imageData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        // [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        NSHTTPURLResponse* response =[[NSHTTPURLResponse alloc] init];
        NSError* error = [[NSError alloc] init];
        
        //synchronous filling of data from HTTP POST response
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        //convert data into string
        NSString   *responseString = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];
        NSLog(@"Response String %@",responseString);
        
        NSError *error1;
        NSMutableDictionary *mainDict=[NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error1];
        NSArray *arr=[[NSArray alloc]init];
        arr=[mainDict valueForKey:@"info"];
        responseString=[arr description];
        
        if (responseString.length>0)
        {
            _imgLogo.image=[UIImage imageNamed:@""];
            NSString *strBURL=ImgBase_URL;
            strBURL=[strBURL stringByAppendingString:responseString];
            
            [_asyImgHBG loadImageFromURL:[NSURL URLWithString:strBURL]];
            _asyImgHBG.layer.cornerRadius=4.0;
            [self.view bringSubviewToFront:_asyImgHBG];
            _strUplodedImg=responseString;
        
        }
        else
        {
            //_imgProfile.image=[UIImage imageNamed:@""];
            _strVideoName=@"";
            _strAlertTitle=@"Failed to create Thumbil.";
            [self viewForAlertUI];
}
    [self.view setUserInteractionEnabled:YES];
}


#pragma mark: Viewfor Loading alert
-(void)ViewForDataLoadingAlert
{
    [_viewForLoader removeFromSuperview];
    _viewForLoader=[[UIView alloc]initWithFrame:CGRectMake(screenWidth/2-100,screenHeight/3-40,200,150)];
    [self.view setUserInteractionEnabled:NO];
    _viewForLoader.backgroundColor=[UIColor whiteColor];
    _viewForLoader.layer.cornerRadius=5.0;
    UILabel *lblLoad=[[UILabel alloc]initWithFrame:CGRectMake(0,5,_viewForLoader.frame.size.width,30)  ];
    lblLoad.text=@"Wait...";
    lblLoad.textAlignment=NSTextAlignmentCenter;
    lblLoad.textColor=[UIColor blackColor];
    lblLoad.font=[UIFont boldSystemFontOfSize:14];
    [_viewForLoader addSubview:lblLoad];
    UILabel *lblTit=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblLoad.frame),_viewForLoader.frame.size.width,30)];
    lblTit.text=@"Video Uploading";
    lblTit.textColor=[UIColor blackColor];
    lblTit.font=[UIFont boldSystemFontOfSize:14];
    lblTit.textAlignment=NSTextAlignmentCenter;
    [_viewForLoader addSubview:lblTit];
    
    static const CGFloat activityIndicatorSize = 40.f;
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:   UIActivityIndicatorViewStyleWhiteLarge];
    _activityIndicator.alpha = 0.f;
    _activityIndicator.hidesWhenStopped = YES;
    [_activityIndicator setFrame:CGRectMake(_viewForLoader.frame.size.width/2-20,_viewForLoader.frame.size.height-60,activityIndicatorSize,activityIndicatorSize)];
    [self.view addSubview:_viewForLoader];
    [self showLoadingIndicators];
}

#pragma mark: view For Alert
-(void)viewForAlertUI
{
    [_viewForAlert removeFromSuperview];
    _viewForAlert =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForAlert.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForAlert.tag=20;
    [self.view addSubview:_viewForAlert];
    
    CGRect frameForViewForHelp=_viewForAlert.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForAlert.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForAlert.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForAlert addSubview:img];
        UILabel *lblAlert=[[UILabel alloc]init];
        if(_isSnapSuccess)
        {
            UILabel *lblCong=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(img.frame)+15,_viewForAlert.frame.size.width,30)];
            lblCong.text=@"Congratulations!";
             lblCong.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:22];
            lblCong.textAlignment=NSTextAlignmentCenter;
            lblCong.textColor=[UIColor whiteColor];
            [_viewForAlert addSubview:lblCong];
            lblAlert.frame=CGRectMake(0,CGRectGetMaxY(lblCong.frame)+20,_viewForAlert.frame.size.width,30);            
        }
        else
        {
            lblAlert.frame=CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForAlert.frame.size.width,30);
        }
        lblAlert.text=_strAlertTitle;//_strVoteAlertTitle
        lblAlert.textAlignment=NSTextAlignmentCenter;
        lblAlert.textColor=[UIColor whiteColor];
        lblAlert.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForAlert addSubview:lblAlert];
        
        _btnCloseAlert=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseAlert.frame=CGRectMake(_viewForAlert.frame.size.width/2-60,CGRectGetMaxY(lblAlert.frame)+50,120,40);
        }
        else
        {
            _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForAlert.frame.size.width/2-50,CGRectGetMaxY(lblAlert.frame)+50,100,40)];
        }
        [_btnCloseAlert setTitle:@"Close" forState:UIControlStateNormal];
        [_btnCloseAlert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseAlert.layer.borderWidth=2.0;
        _btnCloseAlert.layer.cornerRadius=4.0;
        _btnCloseAlert.layer.borderColor=[[UIColor orangeColor]CGColor];
        //[_btnVoteSeeMore addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        [_btnCloseAlert addTarget:self action:@selector(viewForCloseAlert) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewForAlert addSubview:_btnCloseAlert];
    }];
}

#pragma mark: view For Alert
-(void)viewForTrimingVideoAlertUI
{
    [_viewForTrimAlertUI removeFromSuperview];
    _viewForTrimAlertUI =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForTrimAlertUI.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForTrimAlertUI.tag=20;
    [self.view addSubview:_viewForTrimAlertUI];
    
    CGRect frameForViewForHelp=_viewForTrimAlertUI.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForTrimAlertUI.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForTrimAlertUI.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(_viewForTrimAlertUI.frame.size.width/2-50,([UIScreen mainScreen].bounds.size.height/100*25),100,100)];
        img.image=[UIImage imageNamed:@"icon_Share"];
        [_viewForTrimAlertUI addSubview:img];
        UILabel *lblAlert=[[UILabel alloc]init];
        lblAlert.frame=CGRectMake(0,CGRectGetMaxY(img.frame)+20,_viewForTrimAlertUI.frame.size.width,30);
        lblAlert.text=_strAlertTitle;//_strVoteAlertTitle
        lblAlert.textAlignment=NSTextAlignmentCenter;
        lblAlert.textColor=[UIColor whiteColor];
        lblAlert.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:16];
        [_viewForTrimAlertUI addSubview:lblAlert];
        
        _btnCloseAlert=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseAlert.frame=CGRectMake(_viewForTrimAlertUI.frame.size.width/2-60,CGRectGetMaxY(lblAlert.frame)+50,120,40);
        }
        else
        {
            _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForTrimAlertUI.frame.size.width/2-50,CGRectGetMaxY(lblAlert.frame)+50,100,40)];
        }
        [_btnCloseAlert setTitle:@"Close" forState:UIControlStateNormal];
        [_btnCloseAlert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseAlert.layer.borderWidth=2.0;
        _btnCloseAlert.layer.cornerRadius=4.0;
        _btnCloseAlert.layer.borderColor=[[UIColor orangeColor]CGColor];
        //[_btnVoteSeeMore addTarget:self action:@selector(btnFBSharePressed:) forControlEvents:UIControlEventTouchUpInside];
        [_btnCloseAlert addTarget:self action:@selector(viewForCloseTrimAlert) forControlEvents:UIControlEventTouchUpInside];
        [_viewForTrimAlertUI addSubview:_btnCloseAlert];
    }];
}

#pragma mark: view For Alert
-(void)viewForAlertPopUp
{
    [_viewForAlertVideo removeFromSuperview];
    _viewForAlertVideo =[[UIView alloc]initWithFrame:CGRectMake(30, screenHeight/2,screenWidth-50,0)];
    _viewForAlertVideo.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:.8];
    _viewForAlertVideo.tag=20;
    [self.view addSubview:_viewForAlertVideo];
    
    CGRect frameForViewForHelp=_viewForAlertVideo.frame;
    
    frameForViewForHelp=CGRectMake(0,0,screenWidth,screenHeight);
    [UIView animateWithDuration:0.5f animations:^{
        _viewForAlertVideo.frame=frameForViewForHelp;
    } completion:^(BOOL finished) {
        vscreenSize=_viewForAlertVideo.frame;
        vscreenHeight=vscreenSize.size.height;
        vscreenWidth=vscreenSize.size.width;
        
        UIImageView *imgBG=[[UIImageView alloc]initWithFrame:CGRectMake(-80,50,_viewForAlertVideo.frame.size.width+160,_viewForAlertVideo.frame.size.height-100)];
        imgBG.image=[UIImage imageNamed:@"img_AppIcon.png"];
        imgBG.alpha=0.1;
        [_viewForAlertVideo addSubview:imgBG];
        
        UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,[UIScreen mainScreen].bounds.size.height/100*30,_viewForAlertVideo.frame.size.width,30)];
        lblTitle.text=@"A media file has already been uploaded";
        lblTitle.textColor=[UIColor whiteColor];
        lblTitle.textAlignment=NSTextAlignmentCenter;
        if(screenWidth<325)
        {
            lblTitle.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:18];
        }
        else
        {
            lblTitle.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:20];
        }
        
        [_viewForAlertVideo addSubview:lblTitle];
        
        UILabel *lblAlert=[[UILabel alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(lblTitle.frame)+40,_viewForAlertVideo.frame.size.width,30)];
        lblAlert.text=_strAlertTitle;
        
        lblAlert.textAlignment=NSTextAlignmentCenter;
        lblAlert.textColor=[UIColor whiteColor];
        lblAlert.font=[UIFont fontWithName:@"AvenirNext-DemiBold" size:18];
        [lblAlert sizeToFit];
        [_viewForAlertVideo addSubview:lblAlert];
        
        _btnCloseAlert=[[UIButton alloc]init];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _btnCloseAlert.frame=CGRectMake(_viewForAlertVideo.frame.size.width/2-60,CGRectGetMaxY(lblAlert.frame)+50,120,40);
        }
        else
        {
            _btnCloseAlert=[[UIButton alloc]initWithFrame:CGRectMake(_viewForAlertVideo.frame.size.width/2-60,_viewForAlertVideo.frame.size.height/2,120,50)];
        }
        [_btnCloseAlert setTitle:@"Close" forState:UIControlStateNormal];
        [_btnCloseAlert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnCloseAlert.layer.borderWidth=2.0;
        _btnCloseAlert.layer.cornerRadius=4.0;
        _btnCloseAlert.layer.borderColor=[[UIColor orangeColor]CGColor];
         [_btnCloseAlert addTarget:self action:@selector(viewForCloseAlert) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewForAlertVideo addSubview:_btnCloseAlert];
    }];
}

#pragma mark-Close Comment View
-(void)viewForCloseTrimAlert
{
    @try
    {
        for(UIView *subview in [_viewForTrimAlertUI subviews])
        {
            [subview removeFromSuperview];
        }
        
        
        CGRect frameForViewAlert=_viewForTrimAlertUI.frame;
        
        frameForViewAlert=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForTrimAlertUI.frame=frameForViewAlert;
            
        } completion:^(BOOL finished){
            [_viewForTrimAlertUI removeFromSuperview];
            
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark-Close Comment View
-(void)viewForCloseAlert
{
    @try
    {
        for(UIView *subview in [_viewForAlert subviews])
        {
            [subview removeFromSuperview];
        }
        
        for(UIView *subview in [_viewForAlertVideo subviews])
        {
            [subview removeFromSuperview];
        }

        CGRect frameForViewAlert=_viewForAlert.frame;
        CGRect frameForViewVAlert=_viewForAlertVideo.frame;
        
        frameForViewAlert=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        frameForViewVAlert=CGRectMake(30, screenHeight/2,screenWidth-60,0);
        
        [UIView animateWithDuration:0.5f animations:^{
            
            _viewForAlert.frame=frameForViewAlert;
            _viewForAlertVideo.frame=frameForViewVAlert;
            
            if([_strAlertTitle isEqualToString:@"Snapbet uploaded successfully"])
            {
                MainDashboardScreen *mainDash=[[MainDashboardScreen alloc]initWithNibName:@"MainDashboardScreen" bundle:nil];
                [self.navigationController pushViewController:mainDash animated:YES];
            }
        } completion:^(BOOL finished){
            [_viewForAlert removeFromSuperview];
            [_viewForAlertVideo removeFromSuperview];
            
        }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark:- Collection view delegate, datasource method declarations here...!!
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_videoURLArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UILabel *lbl in cell.subviews)
    {
        if ([lbl isKindOfClass:[UILabel class]])
        {
            [lbl removeFromSuperview];
        }
    }
    
    for (UIImageView *img in cell.contentView.subviews) {
        if ([img isKindOfClass:[UIImageView class]]) {
            [img removeFromSuperview];
        }
    }
    UIImageView *imgPoster=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,cell.frame.size.width,cell.frame.size.height)];
    //imgPoster.contentMode=UIViewContentModeScaleAspectFill;
    
    NSData *imgData=[_arrMyVideoData objectAtIndex:indexPath.row];
    imgPoster.image=[UIImage imageWithData:imgData];
    // [imgPoster setClipsToBounds:YES];
    NSLog(@"%ld",(long)indexPath.row);
    imgPoster.layer.borderWidth=0.5;
    imgPoster.layer.borderColor=[[UIColor whiteColor]CGColor];
    [cell.contentView addSubview:imgPoster];
    
    
    NSString *strSecond=[_arrSeconds objectAtIndex:indexPath.row];
    NSString *strMinute=[_arrMinute objectAtIndex:indexPath.row];
    NSString *strHour=[_arrHour objectAtIndex:indexPath.row];
    
    if ([strSecond isEqualToString:@"0"]) {
        strSecond=@"00";
    }
    if ([strMinute isEqualToString:@"0"]) {
        strMinute=@"00:";
        strMinute=[strMinute stringByAppendingString:strSecond];
    }
    else if (strMinute.length>0&&[strHour isEqualToString:@"0"])
    {
        strMinute=[strMinute stringByAppendingString:@":"];
        strMinute=[strMinute stringByAppendingString:strSecond];
    }
    
    if ([strHour isEqualToString:@"0"]) {
        strHour=@"";
    }
    
    if (strHour.length>0) {
        strHour=[strHour stringByAppendingString:@":"];
        
        strHour=[strHour stringByAppendingString:strMinute];
        strHour=[strHour stringByAppendingString:strSecond];
    }
    UILabel *lblTime=[[UILabel alloc]initWithFrame:CGRectMake(imgPoster.frame.size.width-50,imgPoster.frame.size.height-30,40,30)];
    if (strHour.length>0) {
        lblTime.text=strHour;
    }
    else
    {
        lblTime.text=strMinute;
    }
    lblTime.textColor=[UIColor whiteColor];
    lblTime.font=[UIFont systemFontOfSize:14];
    [imgPoster addSubview:lblTime];
    
    //  cell.layer.borderColor=[[UIColor whiteColor]CGColor];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    _strGallerySelectedVideo=[[_videoURLArray valueForKey:@"VideoUrl"] objectAtIndex:indexPath.row];
    [self performSelector:@selector(closeUploadView:) withObject:nil afterDelay:0];
    [self uploadGalleryVideo];
}

#pragma mark: Cell Sizes Declarations here
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (screenWidth<325)
    {
        return CGSizeMake(100,80);
        
    }
    else
    {
        return CGSizeMake(100,100);
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5,[UIScreen mainScreen].bounds.size.width/100/20,0,3); // top, left, bottom,
}

//-----  cell line spacing declarations here -----//
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if (screenWidth<325)
    {
        return 5.0;
    }
    else
    {
        return 5.0;
    }
    return 0;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y < yOffset)
    {
        // scrolls down.
        yOffset = scrollView.contentOffset.y;
    }
    else
    {
        // scrolls up.
        yOffset = scrollView.contentOffset.y;
        // Your Action goes here...
    }
}

#pragma mark: Upload Video to Server Gallery pickuped
-(void)uploadGalleryVideo
{
    if (theAppDelegate.isConnected)
    {
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSURL *url =[NSURL URLWithString:_strGallerySelectedVideo];
        NSData *data = [NSData dataWithContentsOfURL:url];
        NSString *urlString = @"http://54.67.95.152/snapbets/aws/uploadvideofile.php?";
        NSString *filename = @"aa";
        NSMutableURLRequest *request=[[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        NSMutableData *postbody = [NSMutableData data];
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@.mov\"\r\n", filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[NSData dataWithData:data]];
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:postbody];
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        NSString *str = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        NSLog(@"Data ---- %@", str);
        NSError *error;
        NSMutableDictionary *dict1=[NSJSONSerialization
                                    JSONObjectWithData:returnData
                                    options:kNilOptions
                                    error:&error];
        if ([[dict1 valueForKey:@"message"] isEqualToString:@"sus"]) {
            _strVideoName=[dict1 valueForKey:@"info"];
            [theAppDelegate stopSpinner];
            [self.view setUserInteractionEnabled:YES];
            NSLog(@"%@",_strVideoName);
            [_btnIconPlay setUserInteractionEnabled:YES];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Congratulations" message:@"Video uploaded successfully" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
        }
        else
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Failed to upload video" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Failed to upload video" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}
#pragma mark: Video Uploading to SErver API

#pragma mark: Upload new snap to server
-(void)uploadNewSnap
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isUploadAPI=YES;
        _isFrndListAPI=NO;
     
        //NSString *strBetName=[_txtBetName.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSString *strBetName=_txtBetName.text;
        NSString *strBetDesc=_txtDesc.text;
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *urlString=base_URL;
        
        NSMutableArray *arrUserID=[[NSMutableArray alloc]init];
        
        for (int i=0;i<[_arrSelectedFrndsUIDs count];i++) {
            
            NSString *strIDs=[_arrSelectedFrndsUIDs objectAtIndex:i];
            [arrUserID addObject:strIDs];
        }
        NSString *strUIDList = [arrUserID componentsJoinedByString:@","];
        NSLog(@"%@",strUIDList);
        
        NSString *strURL=[NSString stringWithFormat:@"wsaddsnap.php?uid=%@&tid=%@&name=%@&p_status=%@&details=%@&video=%@&expiry=%@&image=%@&private_user=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0],_strSelectedThmeID,strBetName,_strSwitchStatus,strBetDesc,_strVideoName,_strBetExpiryDate,_strUplodedImg,strUIDList];
        urlString=[urlString stringByAppendingString:strURL];
        urlString=[urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *urlSignIn=[NSURL URLWithString:urlString];
        request=[NSMutableURLRequest requestWithURL:[urlSignIn standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        
    }
    request = nil;

}

#pragma mark: Upload new snap to server
-(void)GetAllFriendList
{
    NSMutableURLRequest *request;
    if (theAppDelegate.isConnected)
    {
        _isFrndListAPI=YES;
        _isUploadAPI=NO;
        [theAppDelegate showSpinnerInView:self.view];
        [self.view setUserInteractionEnabled:NO];
        NSString *strBURL=base_URL;
        
        NSString *urlString =[NSString stringWithFormat:@"wsfriendlist.php?uid=%@",[theAppDelegate.arrUserLoginData objectAtIndex:0]];

        strBURL=[strBURL stringByAppendingString:urlString];
        strBURL=[strBURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *urlSignIn=[NSURL URLWithString:strBURL];
        request=[NSMutableURLRequest requestWithURL:[urlSignIn standardizedURL]];
        
        if(!connection)
            connection= [[WebConnection1 alloc] init];
        connection.delegate = self;
        [connection makeConnection:request];
    }
    else
    {
        
    }
    request = nil;
}

#pragma mark: Web Response From JSON
-(void) webResponse:(NSMutableDictionary*)response
{
    [theAppDelegate stopSpinner];
    [self.view setUserInteractionEnabled:YES];
     
    dict =[[NSMutableDictionary alloc]initWithDictionary:response];
    NSString *strMsg=[dict valueForKey:@"message"];
    
   if ([strMsg isEqualToString:@"sus"])
    {
        if (_isUploadAPI)
        {
            _isSnapSuccess=YES;
            _strAlertTitle=@"Snapbet uploaded successfully";
            [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:0.5];
         
            _txtBetName.text=@"";
            _txtDesc.text=@"";
            //_lblExpDate.text=@"0000-00-00";
            _strVideoName=[[NSString alloc]init];
            _strSelectedThmeID=[NSString alloc];
            _strBetExpiryDate=[[NSString alloc]init];
        }
        else if (_isFrndListAPI)
        {
            NSMutableArray *arrData=[dict valueForKey:@"info"];
            
            if([arrData isKindOfClass:[NSNull class]])
            {

            }
            else
            {
                for (int i=0;i<[arrData count];i++) {
                    
                    NSArray *arrInof=[arrData objectAtIndex:i];
                    [_arrFBFriendList addObject:arrInof];
                }
            }
        }
    }
    else
    {
        if (_isUploadAPI)
        {
            _isSnapSuccess=NO;
            _strAlertTitle=@"Snapbet uploaded failed";
            [self performSelector:@selector(viewForAlertUI) withObject:nil afterDelay:0.5];
        
            _txtBetName.text=@"";
            _txtDesc.text=@"";
            //_lblExpDate.text=@"0000-00-00";
            _strVideoName=[[NSString alloc]init];
            _strSelectedThmeID=[NSString alloc];
            _strBetExpiryDate=[[NSString alloc]init];
        }
    }
}

#pragma mark: getnerate Thumbnil from video
-(void)generateImage
{
    NSString *strBase=vBase_URL;
    strBase=[strBase stringByAppendingString:_strVideoName];
    
    NSURL *partOneUrl = [NSURL URLWithString:strBase];
    AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL:partOneUrl options:nil];
    AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
    generate1.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 2);
    CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
    if(oneRef==NULL)
    {
        UIImage *one = [[UIImage alloc]init];
        one = [UIImage imageNamed:@"icon_ShareBG"];
        [self serviceCallForUploadThumbnail:[self getCompressedImage:one]];
    }
    else
    {
        
        UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
        [self serviceCallForUploadThumbnail:[self getCompressedImage:one]];
    }
    
    /*
    NSString *strBase=vBase_URL;
    strBase=[strBase stringByAppendingString:_strVideoName];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL URLWithString:strBase] options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generate.appliesPreferredTrackTransform=TRUE;
    CGSize maxSize = CGSizeMake(128, 128);
    generate.maximumSize = maxSize;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 250);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
     
    NSLog(@"err==%@, imageRef==%@", err, imgRef);
    NSData *data = UIImageJPEGRepresentation([[UIImage alloc] initWithCGImage:imgRef],1);
    if(imgRef==NULL)
    {
        
    }
    else
    {
        UIImage *image=[[UIImage alloc]initWithData:data];
        [self serviceCallForUploadThumbnail:[self getCompressedImage:image]];
    }*/
}

#pragma mark- Connection  Methods
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _webdata =[[NSMutableData alloc] init];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_webdata appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    @try
    {
        NSError *error;
        [_viewForLoader removeFromSuperview];
        [self.view setUserInteractionEnabled:YES];
        [self hideLoadingIndicators];
        _dictVideo=[NSJSONSerialization JSONObjectWithData:_webdata options:NSJSONReadingMutableContainers error:&error];
        NSString *strMSG=[_dictVideo valueForKey:@"message"];
        if([strMSG isEqualToString:@"sus"])
        {
            _strSelectedThmeID=@"0";
            _strVideoName=[_dictVideo valueForKey:@"info"];
            [_btnIconPlay setUserInteractionEnabled:YES];
            [self generateImage];
            [self performSelector:@selector(loadPlayVideoButton) withObject:nil afterDelay:4];
        }
        else
        {
            _strVideoName=@"";
        }
    }
    
    @catch (NSException *exception) {
        
    } @finally {
        
    }
}

-(void)loadPlayVideoButton
{
    [_btnIconPlay removeFromSuperview];
    _btnIconPlay=[[UIButton alloc]initWithFrame:CGRectMake(0,0,_imgLogo.frame.size.width,_imgLogo.frame.size.height)];
        //[_btnIconPlay setBackgroundImage:[UIImage imageNamed:@"img_AppIcon"] forState:UIControlStateNormal];
        [_btnIconPlay addTarget:self action:@selector(viewForVideoScreenPopUpUI) forControlEvents:UIControlEventTouchUpInside];
        [_asyImgHBG addSubview:_btnIconPlay];
        
        UIButton *btnPlay=[[UIButton alloc]initWithFrame:CGRectMake(_btnIconPlay.frame.size.width/2-15,_btnIconPlay.frame.size.height/2-15,30,30)];
        [btnPlay setBackgroundImage:[UIImage imageNamed:@"img_AppIcon"] forState:UIControlStateNormal];
        [btnPlay addTarget:self action:@selector(viewForVideoScreenPopUpUI) forControlEvents:UIControlEventTouchUpInside];
        [_asyImgHBG addSubview:btnPlay];
}

#pragma mark-Get Data From Database
-(void)getDataFromDB
{
    NSString *selectQuery;
    DBManager *objDBManager=[DBManager getSharedInstance];
    NSMutableArray *arrData=[[NSMutableArray alloc]init];
    arrPurchaseDates=[[NSMutableArray alloc]init];
    arrExpiryDates=[[NSMutableArray alloc]init];
    
    selectQuery=[NSString stringWithFormat:@"select * from tblInApp where UID='%@'",[theAppDelegate.arrUserLoginData objectAtIndex:0]];
    
    NSArray *arrTemp=[objDBManager selectTableDataWithQuery:selectQuery];
    NSLog(@"%@",arrTemp);
    
    if ([arrTemp count])
    {
        for (int i=0; i<[arrTemp count]; i++)
        {
            arrData=[arrTemp objectAtIndex:i];
            NSString *strPurchaseDate=[arrData objectAtIndex:3];
            NSString *strExpiryDate=[arrData objectAtIndex:4];
            
            [arrPurchaseDates addObject:strPurchaseDate];
            [arrExpiryDates addObject:strExpiryDate];
        }
    }
}

- (IBAction)openEditor:(id)sender
{
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image =_image;
    UIImage *image = _image;
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    CGFloat length = MIN(width, height);
    controller.imageCropRect = CGRectMake((width - length) / 2,
                                          (height - length) / 2,
                                          length,
                                          length);
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    
    [self presentViewController:navigationController animated:YES completion:NULL];
}

#pragma mark - PECropViewControllerDelegate methods

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    croppedIImage=croppedImage;
    [controller dismissViewControllerAnimated:YES completion:NULL];
    [theAppDelegate showSpinnerInView:self.view];
    [self performSelector:@selector(UploadImage) withObject:nil afterDelay:0.1];
    
    //NSData *imageData=UIImageJPEGRepresentation(croppedImage, 0.1);
    
}

-(void)UploadImage
{
    //_imgProfile.image =_image;
    
    NSData *dataImage = [[NSData alloc] init];
    dataImage = UIImagePNGRepresentation(_image);
    _strUplodedImg=[dataImage base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    [self serviceCallForUploadPhoto1:[self getCompressedImage:croppedIImage]];
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

@end
